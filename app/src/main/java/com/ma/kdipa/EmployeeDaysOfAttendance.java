package com.ma.kdipa;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fivehundredpx.android.blur.BlurringView;
import com.ma.kdipa.JavaResources.AnimationChartColor;
import com.ma.kdipa.JavaResources.AnimationProgressBar;
import com.ma.kdipa.JavaResources.AnimationValueChart;
import com.ma.kdipa.JavaResources.AttendanceEntriesPerYear;
import com.ma.kdipa.JavaResources.AttendanceEntry;
import com.ma.kdipa.JavaResources.AttendancePerEmployee;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.CustomSolidArc;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterYearsFilter;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Utilitaire.emphasizeContent;

public class EmployeeDaysOfAttendance extends EmployeeNavigationDrawer {


    private static final int DURATION = 1000;
    private static final String TAG = "EmployeeDaysOfAttendanc";
    private static Handler handler;
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    DialogBoxLoadingClass loadingDialogBox;
    CustomSolidArc chart;
    TextView chartValueTV;
    TextView attendancesTV;
    TextView vacationsTV;
    TextView absencesTV;
    TextView descriptionTV;
    float ratio = 0f;//ratio must be per 1000
    ProgressBar attendancesPB;
    ProgressBar vacationsPB;
    ProgressBar absencesPB;
    Integer selectedYear;
    Selection selection;
    BlurringView blurringView;
    ImageView snapShotIV;
    ImageView darkIV;
    ImageView closeIV;
    TextView yearTV;
    TextView yearsEntryTV;
    RecyclerView yearsRV;

    TextView employeeTV; //will be replaced with spinner in manager screen
    AttendancePerEmployee attendancePerEmployee;
    RecyclerViewAdapterYearsFilter yearsFilterAdapter;
    Spinner employeeSpinner;
    private RecyclerView recyclerView;

    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.e_attendance,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);


        globalFindViewById();

        try {
            if (isManager) {
                processEmployeeSpinner();
            } else {
                loadData();
            }
        } catch (Exception e) {
            Toast.makeText(EmployeeDaysOfAttendance.this, e.toString(), Toast.LENGTH_SHORT).show();
        }

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 1) {
                    loadingDialogBox.dismiss();
                    findViewById(R.id.container).setVisibility(View.VISIBLE);
                    try {
                        initialization();
                    } catch (Exception e) {
                        Toast.makeText(EmployeeDaysOfAttendance.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (msg.arg1 == -1) {//empty
                    findViewById(R.id.container).setVisibility(View.GONE);
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeDaysOfAttendance.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.no_data_available))
                            .setBody(getString(R.string.no_data_available_so_far))
                            .setPositive("Ok")
                            .create().show();
                } else {
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeDaysOfAttendance.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeDaysOfAttendance.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void processEmployeeSpinner() {
        employeeSpinner = findViewById(R.id.employeeSpinner);
        employeeSpinner.setVisibility(View.VISIBLE);
        employeeTV.setVisibility(View.INVISIBLE);
        EmployeesList employeesList = EmployeesList.getInstance();
        if (employeesList == null) {
            EmployeesList.newInstance(this, InternalStorage.readFullInternalStorage(this, EMPLOYEES_LIST));
            employeesList = EmployeesList.getInstance();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.drop_down_list_element,
                locale.equals("ar") ? employeesList.getEmployeesArNamesList() : employeesList.getEmployeesNamesList());        //set the spinners adapter to the previously created one.
        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private AttendanceEntriesPerYear getSingleRowYear(Integer year) {
        for (int i = 0; i < attendancePerEmployee.getAttendanceEntriesPerYears().size(); i++) {
            if (attendancePerEmployee.getAttendanceEntriesPerYears().get(i).getYear().equals(year)) {
                return attendancePerEmployee.getAttendanceEntriesPerYears().get(i);
            }
        }
        return null;
    }

    private void fade(int duration, float from, float to) {
        AlphaAnimation anim = new AlphaAnimation(from, to); //0 means invisible
        anim.setDuration(duration);
        anim.setRepeatCount(0);
//                    anim.setRepeatMode(Animation.REVERSE);
        blurringView.startAnimation(anim);
        yearsRV.startAnimation(anim);
        closeIV.startAnimation(anim);
        yearsEntryTV.startAnimation(anim);
        darkIV.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {

                if (from == 0.0f && to == 1.0f) {//if we are handling a fadeIn
//                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

                    snapShotIV.setVisibility(View.VISIBLE);
                    darkIV.setVisibility(View.VISIBLE);
                    blurringView.setVisibility(View.VISIBLE);
                    yearsRV.setVisibility(View.VISIBLE);
                    closeIV.setVisibility(View.VISIBLE);
                    yearsEntryTV.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                if (from == 1.0f && to == 0.0f) {//if we are handling a fadeOut
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//hide status bar because it causes problemwhen screen shoting
                    snapShotIV.setVisibility(View.INVISIBLE);
                    darkIV.setVisibility(View.INVISIBLE);
                    blurringView.setVisibility(View.INVISIBLE);
                    yearsRV.setVisibility(View.INVISIBLE);
                    closeIV.setVisibility(View.INVISIBLE);
                    yearsEntryTV.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void initialization() {
        selection = Selection.ATTENDANCE;
        int lastItemPosition = attendancePerEmployee.getAttendanceEntriesPerYears().size() - 1;


        Integer maxYear = (attendancePerEmployee.getAttendanceEntriesPerYears().get(attendancePerEmployee.getAttendanceEntriesPerYears().size() - 1).getYear());
        selectedYear = maxYear;

        yearTV.setText(String.valueOf(maxYear));

        filterGenerateScreen(getSingleRowYear(maxYear).getAttendanceEntry());
        processYearsRV();


        yearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                recyclerViewAdapterYearsFilter.selectLastElement();
                ConstraintLayout constraintLayout = findViewById(R.id.container);
                snapShotIV.setImageBitmap(Utilitaire.getBitmapFromView((View) constraintLayout));

                blurringView.setBlurredView(snapShotIV);
                blurringView.invalidate();
                fade(300, 0.0f, 1.0f);//fadeIn
            }
        });
        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int position = yearsFilterAdapter.getSelectedItemPosition();
                if (position != -1) {
                    selectedYear = (attendancePerEmployee.getAttendanceEntriesPerYears().get(position).getYear());
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            filterGenerateScreen(getSingleRowYear(selectedYear).getAttendanceEntry());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 300);
                yearTV.setText(String.valueOf(selectedYear));
                fade(300, 1.0f, 0.0f);//fadeOut
            }
        });


        employeeTV.setText(attendancePerEmployee.getName());

        switch (selection) {
            case VACATIONS: {
                updateChart(attendancePerEmployee.getAttendanceEntriesPerYears().get(lastItemPosition).getAttendanceEntry().getVacationsRatio() / 10.0f);
                break;
            }
            case ABSENCES: {
                updateChart(attendancePerEmployee.getAttendanceEntriesPerYears().get(lastItemPosition).getAttendanceEntry().getAbsencesRatio() / 10.0f);

                break;
            }
            case ATTENDANCE: {
                updateChart(attendancePerEmployee.getAttendanceEntriesPerYears().get(lastItemPosition).getAttendanceEntry().getAttendanceRatio() / 10.0f);

                break;
            }

            default:
                throw new IllegalStateException("Unexpected value: " + selection);
        }


    }

    private void updateChart(float percent) {
        chartValueTV.setText(percent + "%");
        chart.setPiePercent(percent);
    }

    private void filterGenerateScreen(AttendanceEntry attendanceEntry) {

        AnimationChartColor anim;

        String firstInDescription;
        String secondInDescription;

        switch (selection) {
            case VACATIONS: {
                anim = new AnimationChartColor(
                        chart,
                        chart.getPieColor(),
                        Color.parseColor("#E1A868")
                );
                ratio = attendanceEntry.getVacationsRatio() / 10.0f;
                firstInDescription = String.valueOf(attendanceEntry.getVacations());
                secondInDescription = getString(R.string.days_of_vacation);

                break;
            }
            case ABSENCES: {
                anim = new AnimationChartColor(
                        chart,
                        chart.getPieColor(),
                        Color.parseColor("#E1A868")
                );
                ratio = attendanceEntry.getAbsencesRatio() / 10.0f;
                firstInDescription = String.valueOf(attendanceEntry.getAbsences());
                secondInDescription = getString(R.string.days_of_absence);

                break;
            }
            case ATTENDANCE: {
                anim = new AnimationChartColor(
                        chart,
                        chart.getPieColor(),
                        Color.parseColor("#E1A868")

                );
                ratio =attendanceEntry.getAttendanceRatio() / 10.0f;
                firstInDescription = String.valueOf(attendanceEntry.getAttendance());
                secondInDescription = getString(R.string.days_of_attendance2);

                break;
            }

            default:
                throw new IllegalStateException("Unexpected value: " + selection);
        }
        anim.setDuration(DURATION);
        AnimationSet as = new AnimationSet(true);
        as.addAnimation(anim);


        AnimationValueChart animationValueChart = new AnimationValueChart(chart, chart.getPiePercent(), ratio);
        animationValueChart.setDuration(DURATION);
        as.addAnimation(animationValueChart);

        chart.startAnimation(as);


        Utilitaire.fade(500, 1.0f, 0.0f, chartValueTV, attendancesTV, vacationsTV, absencesTV, descriptionTV);
        //wait the end of the latter fade out then fade in
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                chartValueTV.setText(String.format("%s%%", String.valueOf((int) ratio)));
                attendancesTV.setText(String.valueOf(attendanceEntry.getAttendance()));
                vacationsTV.setText(String.valueOf(attendanceEntry.getVacations()));
                absencesTV.setText(String.valueOf(attendanceEntry.getAbsences()));
                updateDescription(firstInDescription, secondInDescription, String.valueOf(attendanceEntry.getTotalDays()));
                Utilitaire.fade(500, 0.0f, 1.0f, chartValueTV, attendancesTV, vacationsTV, absencesTV, descriptionTV);
            }
        }, 500);


        AnimationProgressBar anim1 = new AnimationProgressBar(
                attendancesPB,
                attendancesPB.getProgress(),
                attendanceEntry.getAttendanceRatio()
        );
        anim1.setDuration(DURATION);
        attendancesPB.startAnimation(anim1);


        AnimationProgressBar anim3 = new AnimationProgressBar(
                vacationsPB,
                vacationsPB.getProgress(),
                attendanceEntry.getVacationsRatio()
        );
        anim3.setDuration(DURATION);
        vacationsPB.startAnimation(anim3);

        AnimationProgressBar anim4 = new AnimationProgressBar(
                absencesPB,
                absencesPB.getProgress(),
                attendanceEntry.getAbsencesRatio()
        );
        anim4.setDuration(DURATION);
        absencesPB.startAnimation(anim4);

    }

    private void updateDescription(String s1, String s_bis, String s2) {
        String s = s1 + " " + s_bis + ", OUT OF " + s2 + " DAYS";
        if (getResources().getConfiguration().locale.getLanguage().equals("ar")) {
            s = s1 + " " + s_bis + "، من أصل " + s2 + " يوماً";
        }
        Typeface face = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        descriptionTV.setText(emphasizeContent(s, face, s1, s2));

    }

    private void globalFindViewById() {
        descriptionTV = findViewById(R.id.description);

        recyclerView = findViewById(R.id.vacations_rv);
        employeeTV = findViewById(R.id.employeeTV);

        chartValueTV = findViewById(R.id.chartValue);
        chart = findViewById(R.id.chart);

        attendancesTV = findViewById(R.id.attendanceTV);
        vacationsTV = findViewById(R.id.vacationsTV);
        absencesTV = findViewById(R.id.absencesTV);

        attendancesPB = findViewById(R.id.AttendancePB);
        vacationsPB = findViewById(R.id.vacationsPB);
        absencesPB = findViewById(R.id.absencesPB);


        yearsRV = findViewById(R.id.yearsRV);
        blurringView = findViewById(R.id.blurringView);
        snapShotIV = findViewById(R.id.snapshot);
        darkIV = findViewById(R.id.darkIV);
        yearTV = findViewById(R.id.year);
        closeIV = findViewById(R.id.closeIV);
        yearsEntryTV = findViewById(R.id.yearsEntryTV);

        //get the spinner from the xml.
//        dropdown1 = findViewById(R.id.drop_down_list1);

    }

//    public void closeButtonClick(View view) {
//        Integer selectedYear = (attendancePerEmployee.getAttendanceEntriesPerYears().get(yearsFilterAdapter.getSelectedItemPosition()).getYear());
//        yearTV.setText(String.valueOf(selectedYear));
//        fade(300, 1.0f, 0.0f);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                filterGenerateScreen(getSingleRowYear(selectedYear).getAttendanceEntry());
//
//            }
//        }, 300);
//
//    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)) {
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread = new LoadingThread();
            loadingThread.start();
        } else {
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }
    }

    private void processYearsRV() {
        ArrayList<Integer> yearsList = new ArrayList<>();
        for (int i = 0; i < attendancePerEmployee.getAttendanceEntriesPerYears().size(); i++) {
            yearsList.add(attendancePerEmployee.getAttendanceEntriesPerYears().get(i).getYear());
        }
        yearsFilterAdapter = new RecyclerViewAdapterYearsFilter(yearsList);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(RecyclerView.VERTICAL);
        yearsRV.setLayoutManager(linearLayoutManager2);
        yearsRV.setAdapter(yearsFilterAdapter);
    }

    public void updateChart(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.vacationsCL: {
                selection = Selection.VACATIONS;
                break;
            }
            case R.id.absencesCL: {
                selection = Selection.ABSENCES;
                break;
            }
            case R.id.attendanceCL: {
                selection = Selection.ATTENDANCE;
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }

        filterGenerateScreen(getSingleRowYear(selectedYear).getAttendanceEntry());


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//            }
//        },DURATION);
    }

    enum Selection {
        ATTENDANCE,
        VACATIONS,
        ABSENCES

    }

    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }


            attendancePerEmployee = new AttendancePerEmployee();
            String resulat = "";
            BackgroundWorker backgroundWorker = new BackgroundWorker(EmployeeDaysOfAttendance.this);

            try {
                ArrayList<AttendanceEntriesPerYear> attendanceEntriesPerYears = new ArrayList<>();

                if (isManager) {
                    resulat = backgroundWorker.execute("managerGetDaysOfAttendance", email, password, EmployeesList.getInstance().getEmployeesList().get(employeeSpinner.getSelectedItemPosition()).getId()).get();
                } else {
                    resulat = backgroundWorker.execute("getDaysOfAttendance", email, password).get();
                }
                JSONArray jsonArray = new JSONArray(resulat);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    attendanceEntriesPerYears.
                            add(new AttendanceEntriesPerYear(
                                    jsonObject.getInt("year"),
                                    new AttendanceEntry(
                                            jsonObject.getInt("attendance"),
                                            jsonObject.getInt("vacations"),
                                            jsonObject.getInt("absences")
//                                            jsonObject.getInt("totalDays")
                                    )
                            ));
                }


                attendancePerEmployee = new AttendancePerEmployee(
                        name,
                        name,
                        attendanceEntriesPerYears
                );
                if (attendanceEntriesPerYears.size() == 0) {
                    Message msg = Message.obtain();
                    msg.arg1 = -1;
                    handler.sendMessage(msg);
                } else {
                    Message msg = Message.obtain();
                    msg.arg1 = 1;
                    handler.sendMessage(msg);
                }

//                TextView textView=null;
//                textView.setText("Trigger exception now !");
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
                Message msg = Message.obtain();
                msg.arg1 = 0;
                handler.sendMessage(msg);
            }
        }
    }

}


