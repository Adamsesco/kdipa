package com.ma.kdipa.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountsManagerResponse {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("version")
    @Expose
    public Integer version;
    @SerializedName("createdBy")
    @Expose
    public Integer createdBy;
    @SerializedName("createdOn")
    @Expose
    public Long createdOn;
    @SerializedName("modifiedBy")
    @Expose
    public Integer modifiedBy;
    @SerializedName("modifiedOn")
    @Expose
    public Long modifiedOn;
    @SerializedName("account")
    @Expose
    public String account;
    @SerializedName("firstName")
    @Expose
    public String firstName;
    @SerializedName("lastName")
    @Expose
    public String lastName;
    @SerializedName("failedLoginAttempts")
    @Expose
    public Object failedLoginAttempts;
    @SerializedName("passwordExpiryDate")
    @Expose
    public Object passwordExpiryDate;
    @SerializedName("changePassOnLogin")
    @Expose
    public Boolean changePassOnLogin;
    @SerializedName("phone")
    @Expose
    public Object phone;
    @SerializedName("active")
    @Expose
    public Boolean active;
    @SerializedName("moci")
    @Expose
    public Boolean moci;
    @SerializedName("finance")
    @Expose
    public Boolean finance;
    @SerializedName("accountManager")
    @Expose
    public Boolean accountManager;
    @SerializedName("lastModifiedOss")
    @Expose
    public String lastModifiedOss;
    @SerializedName("userGroupOss")
    @Expose
    public String userGroupOss;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("new")
    @Expose
    public Boolean _new;
}
