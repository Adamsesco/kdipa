package com.ma.kdipa.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCompaniesResponse {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("version")
    @Expose
    public Integer version;
    @SerializedName("userId")
    @Expose
    public Integer userId;
    @SerializedName("companyId")
    @Expose
    public Integer companyId;
    @SerializedName("companyName")
    @Expose
    public String companyName;
    @SerializedName("assignToUser")
    @Expose
    public Object assignToUser;

    public GetCompaniesResponse() {
    }

    public GetCompaniesResponse(Integer id, Integer version, Integer userId, Integer companyId, String companyName, Object assignToUser) {
        this.id = id;
        this.version = version;
        this.userId = userId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.assignToUser = assignToUser;
    }
}
