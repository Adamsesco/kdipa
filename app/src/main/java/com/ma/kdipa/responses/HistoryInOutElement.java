package com.ma.kdipa.responses;

public  class HistoryInOutElement {

    public String ActualIn;
    public String ActualOut;

    public HistoryInOutElement(String actualIn, String actualOut) {
        ActualIn = actualIn;
        ActualOut = actualOut;
    }

    public HistoryInOutElement() {
    }
}
