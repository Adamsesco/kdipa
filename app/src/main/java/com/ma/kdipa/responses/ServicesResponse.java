package com.ma.kdipa.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServicesResponse {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("version")
    @Expose
    public Integer version;
    @SerializedName("createdBy")
    @Expose
    public Long createdBy;
    @SerializedName("createdOn")
    @Expose
    public Long createdOn;
    @SerializedName("modifiedBy")
    @Expose
    public Long modifiedBy;
    @SerializedName("modifiedOn")
    @Expose
    public Long modifiedOn;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("startHour")
    @Expose
    public Integer startHour;
    @SerializedName("endHour")
    @Expose
    public Integer endHour;
    @SerializedName("durationInHours")
    @Expose
    public Integer durationInHours;
    @SerializedName("workDayMonday")
    @Expose
    public Boolean workDayMonday;
    @SerializedName("workDayTuesday")
    @Expose
    public Boolean workDayTuesday;
    @SerializedName("workDayWednesday")
    @Expose
    public Boolean workDayWednesday;
    @SerializedName("workDayThursday")
    @Expose
    public Boolean workDayThursday;
    @SerializedName("workDayFriday")
    @Expose
    public Boolean workDayFriday;
    @SerializedName("workDaySaturday")
    @Expose
    public Boolean workDaySaturday;
    @SerializedName("workDaySunday")
    @Expose
    public Boolean workDaySunday;
    @SerializedName("new")
    @Expose
    public Boolean _new;
}
