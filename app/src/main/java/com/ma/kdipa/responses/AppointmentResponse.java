package com.ma.kdipa.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentResponse {

    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("count")
    @Expose
    public Object count;
    @SerializedName("total")
    @Expose
    public Object total;
    public class Result {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("version")
        @Expose
        public Integer version;
        @SerializedName("bookingServiceId")
        @Expose
        public Integer bookingServiceId;
        @SerializedName("service")
        @Expose
        public String service;
        @SerializedName("accountManagerId")
        @Expose
        public Integer accountManagerId;
        @SerializedName("accManager")
        @Expose
        public String accManager;
        @SerializedName("meetingDate")
        @Expose
        public Long meetingDate;
        @SerializedName("meetingTime")
        @Expose
        public String meetingTime;
        @SerializedName("details")
        @Expose
        public String details;
        @SerializedName("meetingTimeFormated")
        @Expose
        public String meetingTimeFormated;

    }
}
