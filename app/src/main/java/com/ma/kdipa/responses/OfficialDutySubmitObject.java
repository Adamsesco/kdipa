package com.ma.kdipa.responses;

public class OfficialDutySubmitObject {

        private String email;
        private String password;
        private String dutyType;
        private String fromDate;
        private String toDate;
        private String attendanceExemption;
        private String employees;
        private String delegatedEmployees;

        public OfficialDutySubmitObject() {
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDutyType() {
            return dutyType;
        }

        public void setDutyType(String dutyType) {
            this.dutyType = dutyType;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getAttendanceExemption() {
            return attendanceExemption;
        }

        public void setAttendanceExemption(String attendanceExemption) {
            this.attendanceExemption = attendanceExemption;
        }

        public String getEmployees() {
            return employees;
        }

        public void setEmployees(String employees) {
            this.employees = employees;
        }

        public String getDelegatedEmployees() {
            return delegatedEmployees;
        }

        public void setDelegatedEmployees(String delegatedEmployees) {
            this.delegatedEmployees = delegatedEmployees;
        }

        @Override
        public String toString() {
            return "OfficialDutySubmitObject{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", dutyType='" + dutyType + '\'' +
                    ", fromDate='" + fromDate + '\'' +
                    ", toDate='" + toDate + '\'' +
                    ", attendanceExemption='" + attendanceExemption + '\'' +
                    ", employees='" + employees + '\'' +
                    ", delegatedEmployees='" + delegatedEmployees + '\'' +
                    '}';
        }
    }
