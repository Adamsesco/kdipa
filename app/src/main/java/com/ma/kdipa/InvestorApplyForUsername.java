package com.ma.kdipa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.ma.kdipa.JavaResources.BackgroundWorkerInvestor;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.Utilitaire;

import java.util.concurrent.ExecutionException;

public class InvestorApplyForUsername  extends AppCompatActivity{

    TextView submit;
    ProgressDialog progressDialog;

    Handler handler;
    DialogBoxLoadingClass loadingDialogBox;



    String firstName;
    String lastName;
    String email="";
    String company="";
    String phone="";

    EditText firstNameET;
    EditText lastNameET;
    EditText emailET;
    EditText companyET;
    EditText phoneET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i_apply_username);



        findViewsById();

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==201||msg.arg1==200){
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorApplyForUsername.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityCommonLogin();
                            })
                            .create().show();
                }else if(msg.arg1==500||msg.arg1==400||msg.arg1==404||msg.arg1==401){
                    progressDialog.dismiss();
                    submit.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorApplyForUsername.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass dialogBoxCustomClass=new DialogBoxCustomClass(InvestorApplyForUsername.this, getString(R.string.error)+" "+msg.arg1,getString(R.string.an_error_occured), "OK");
//                    dialogBoxCustomClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    dialogBoxCustomClass.show();
                }
            }
        };

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void findViewsById() {
        submit=findViewById(R.id.submit);

        firstNameET=findViewById(R.id.user_firstname_edit_text);
        lastNameET=findViewById(R.id.user_lastname_edit_text);
        emailET=findViewById(R.id.user_email_edit_text);
        companyET=findViewById(R.id.user_company_edit_text);
        phoneET=findViewById(R.id.user_phone_number_edit_text);
    }
    private void retrieveParams() {
        firstName=firstNameET.getText().toString();
        lastName=lastNameET.getText().toString();
        email=emailET.getText().toString();
        company=companyET.getText().toString();
        phone=phoneET.getText().toString();
    }

    private void startActivityCommonLogin() {
        Intent intent = new Intent(getApplicationContext(), CommonLogin.class);
        Pair<View, String> p1 = Pair.create((View)findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent,options.toBundle());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onTestButtonClick(View view) {
        Toast.makeText(this, "clicking was done successfully !", Toast.LENGTH_SHORT).show();
    }




    public void submitClick(View view) {
        if (Utilitaire.isNetworkAvailable(this)){
            submit.setEnabled(false);
            progressDialog= new ProgressDialog(this, R.style.customDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);



            ApplyThread applyThread=new ApplyThread();
            applyThread.start();
        }else{
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }
    }

    private class ApplyThread extends Thread{
        ApplyThread() {
        }
        @Override
        public void run() {

                BackgroundWorkerInvestor backgroundWorkerInvestor= new BackgroundWorkerInvestor(InvestorApplyForUsername.this);
                try {
                    retrieveParams();
                    String[] fullResponse= backgroundWorkerInvestor.execute(
                            "applyForUsername",
                            email,
                            firstName,
                            lastName,
                            company,
                            phone
                    ).get();
                    switch (fullResponse[0]) {
                        case "400": {
                            Message msg = Message.obtain();
                            msg.arg1 = 400;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "500": {
                            Message msg = Message.obtain();
                            msg.arg1 = 500;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "401": {
                            Message msg=Message.obtain();
                            msg.arg1=401;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "404": {
                            Message msg = Message.obtain();
                            msg.arg1 = 404;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "200": {
                            Message msg = Message.obtain();
                            msg.arg1 = 200;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "201": {
                            Message msg = Message.obtain();
                            msg.arg1 = 201;
                            handler.sendMessage(msg);
                            break;
                        }
                        default:{
                            Message msg = Message.obtain();
                            msg.arg1 = 0;
                            handler.sendMessage(msg);
                            break;
                        }
                    }
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                    Message msg=Message.obtain();
                    msg.arg1=404;
                    handler.sendMessage(msg);
                }
        }
    }


}
