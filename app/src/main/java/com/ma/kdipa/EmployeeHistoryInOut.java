package com.ma.kdipa;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterHistoryInOut;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterNews;
import com.ma.kdipa.JavaResources.SingleRowNews;
import com.ma.kdipa.requests.HistoryInOutRequest;
import com.ma.kdipa.requests.UserCredentials;
import com.ma.kdipa.responses.HistoryInOutElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeHistoryInOut extends EmployeeNavigationDrawer {


    private static final String TAG = "EmployeeHistoryInOut";

    protected DrawerLayout mDrawer;
    RecyclerView rv;
    DialogBoxLoadingClass loadingDialogBox;
    private ArrayList<SingleRowNews> list;
    private ArrayList<String> fullNamesList;
    private TextView employeeTV;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView;
        contentView = inflater.inflate(
                R.layout.e_history_in_out,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        rv = findViewById(R.id.rv);

        ImageView v = mDrawer.findViewById(R.id.historyInOut_iv);
        v.setImageResource(R.drawable.history_en);
        v.setTag(R.drawable.history_en);
        mDrawer.findViewById(R.id.historyInOut).setBackgroundColor(Color.parseColor("#F2F2F2"));

        loadData();


    }

    void loadData() {
        showLoading();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
        String today = simpleDateFormat.format(Calendar.getInstance().getTime());


        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
        String onMonthEarlier = simpleDateFormat.format(calendar.getTime());

        getAPIManager().getHistoryInOut(new HistoryInOutRequest(email, password,onMonthEarlier , today)).enqueue(new Callback<List<HistoryInOutElement>>() {
            @Override
            public void onResponse(Call<List<HistoryInOutElement>> call, Response<List<HistoryInOutElement>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    if(response.body()==null) return;
                    List<HistoryInOutElement> list = new ArrayList<>();
//                    if (Constants.DEMO)
//                        list.add(new HistoryInOutElement("2022-01-26T07:45:00.000Z", "2022-01-25T06:45:00.000Z"));
                    list.addAll(response.body());

                    RecyclerViewAdapterHistoryInOut recyclerViewAdapter = new RecyclerViewAdapterHistoryInOut(list);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EmployeeHistoryInOut.this);
                    linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                    rv.setLayoutManager(linearLayoutManager);
                    rv.setAdapter(recyclerViewAdapter);

                } else {

                }

            }

            @Override
            public void onFailure(Call<List<HistoryInOutElement>> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                hideLoading();


            }
        });

    }

    private void setData() {
        RecyclerView recyclerView = findViewById(R.id.rv);
        RecyclerViewAdapterNews recyclerViewAdapter;


        recyclerViewAdapter = new RecyclerViewAdapterNews(list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

}
