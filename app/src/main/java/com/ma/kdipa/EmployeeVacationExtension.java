package com.ma.kdipa;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DELEGATED_LIST;
import static com.ma.kdipa.JavaResources.Constants.VACATION_AR_TYPES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_PREFERENCES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_TYPES;
import static com.ma.kdipa.JavaResources.Constants.YEAR_MONTH_DAY;
import static com.ma.kdipa.JavaResources.Utilitaire.isInteger;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DelegatedList;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.MySpinnerAdapter;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.ShotVacationExtension;
import com.ma.kdipa.JavaResources.ShotVacationReturnExtension;
import com.ma.kdipa.JavaResources.Spinner;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.VacationCustomSpinnerAdapter;
import com.ma.kdipa.JavaResources.VacationsEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class EmployeeVacationExtension extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {


    private static final String PASSWORD = "PASSWORD";
    private static final String EMAIL = "EMAIL";
    private static final String NAME = "NAME";
    private static final String USER_INFO = "USER_INFO";
    private static final String LOCALE = "LOCALE";
    private static final String TAG = "EmployeeVacationReturn";
    public ArrayList<VacationsEntry> vacationsEntriesSorted;
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    SubmitObject submitObject = new SubmitObject();
    ArrayList<String> vacationIDSList;
    Spinner historySpinner;
    TextView startFromTV;
    TextView toTV;
    TextView extendToTV;
    TextView submitTV;
    ProgressDialog progressDialog;
    Handler handler;
    String startFromTxt = "";
    String toTxt = "";
    String extendToTxt = "";
    String typeTxt = "";
    DateType dateType;
    ShotVacationExtension shotVacationExtension;
    Spinner delegatedEmployeeSpinner;

//    private void loadOldData() {
//        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
//
//        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
//            email = sharedPreferences.getString(EMAIL, "");
//            password = sharedPreferences.getString(PASSWORD, "");
//            name = sharedPreferences.getString(NAME, "");
//        }
//
//        String[] txt= InternalStorage.readFullInternalStorage(this, VACATION_PREFERENCES).split("\n");
//        Log.d(TAG, "loadData: txt: "+ Arrays.toString(txt));
//        txt=(txt.length<=1)?new String[]{"",""}:txt;
//        vacationIDSList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_IDS).split("\n")));
//
//        ArrayList<String> vacationTypeList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_TYPES).split("\n")));
//        ArrayList<String> vacationArList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_AR_TYPES).split("\n")));
//
//
//        shotVacationExtension =new ShotVacationExtension(
//                this,
//                vacationTypeList,
//                vacationArList,
//                txt[0],//jsonObject.getString("departureDate"),
//                txt[1],//jsonObject.getString("returnDate")
//                ""
//        );
//        startFromTxt=txt[0];
//        toTxt=txt[1];
//        extendToTxt="";
//
//
//
//        startFromTV.setText(shotVacationExtension.getstartDate());
//        toTV.setText(shotVacationExtension.gettoDate());
//        extendToTV.setText(shotVacationExtension.getExtendedToDate());
//
//        typeTxt=shotVacationExtension.getTypesList().get(0);
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(this,
//                locale.equals("ar")?shotVacationExtension.getTypesArList():shotVacationExtension.getTypesList()
//        );
//        //set the spinners adapter to the previously created one.
//        typesSpinner.setAdapter(adapter);
//        typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                typeTxt=shotVacationExtension.getTypesList().get(i);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });
//
//
//
//
//
//
//
//    }
    DialogBoxLoadingClass loadingDialogBox;
    ShotVacationReturnExtension ShotVacationReturnExtension;
    Handler getHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.e_vacation_extension,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);
        mDrawer.findViewById(R.id.vacation_extension).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);


        globalFindViewById();
        setDefaultValues();

        loadData();


        processTheCalendar();

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 200) {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    InternalStorage.write(EmployeeVacationExtension.this,
                            VACATION_PREFERENCES,
                            startFromTxt + "\n" + extendToTxt,
                            false, false
                    );
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationExtension.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityEmployeeDashboard();
                            })
                            .create().show();

                } else {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationExtension.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationExtension.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };
        getHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 200) {
                    loadingDialogBox.dismiss();
                    setData();
                } else {
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationExtension.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationReturn.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }

            }
        };

    }

    private void setData() {
        if (ShotVacationReturnExtension.getVacationsEntries() == null || ShotVacationReturnExtension.getVacationsEntries().size() == 0)
            return;

        //set the spinners adapter to the previously created one

        VacationCustomSpinnerAdapter customAdapter = new VacationCustomSpinnerAdapter(this, ShotVacationReturnExtension.getVacationsEntries());
        historySpinner.setAdapter(customAdapter);
        Log.d(TAG, "setData: sdsdfsfsfzefze: called");

        historySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                submitObject.setVacationId(vacationsEntriesSorted.get(i).getVacationId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }

    private void startActivityEmployeeDashboard() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }

    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);

    }

    private void globalFindViewById() {
        historySpinner = findViewById(R.id.vacations);

        startFromTV = findViewById(R.id.startFrom);
        toTV = findViewById(R.id.startTo1);
        extendToTV = findViewById(R.id.extendTo);


        submitTV = findViewById(R.id.submit);


    }

    public void showCalendarForDepartureDate(View view) {
        dateType = DateType.START_DATE;
        showCalendar();
    }

    public void showCalendarForReturnDate(View view) {
        dateType = DateType.TO_DATE;
        showCalendar();
    }

    public void showCalendarForDepartureDate2(View view) {
        dateType = DateType.EXTENDED_TO_DATE;
        showCalendar();
    }

    public void delete1Click(View view) {
        startFromTV.setText("");
        startFromTxt = "";
    }

    public void delete2Click(View view) {
        toTV.setText("");
        toTxt = "";
    }

    public void delete3Click(View view) {
        extendToTV.setText("");
        extendToTxt = "";
    }

    public void showCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(-constraintLayout.getWidth(), 0, 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.INVISIBLE || constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                constraintLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void hideCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, -constraintLayout.getWidth(), 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.VISIBLE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                constraintLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void submitClick(View view) {

        if (validate()) {
            submitObject.setEmail(email);
            submitObject.setPassword(password);
//            submitObject.setVacationType(vacationIDSList.get(typesSpinner.getSelectedItemPosition()));
            submitObject.setOldFromDate(startFromTxt);
            submitObject.setOldToDate(toTxt);
            submitObject.setNewToDate(extendToTxt);
            submitObject.setAdvancedPayment("true");
            submitObject.setDelegatedEmployee(delegatedEmployeeSpinner.getSelectedItem().toString());

            submitTV.setEnabled(false);

            progressDialog = new ProgressDialog(this, R.style.customDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.loading));


            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            SubmitThread submitThread = new SubmitThread();
            submitThread.start();
        }
    }

    boolean validate() {
        @SuppressLint("SimpleDateFormat") String currentDate = new SimpleDateFormat(YEAR_MONTH_DAY).format(Calendar.getInstance().getTime());
//        if (historySpinner.getSelectedItemPosition() == -1) {
//            Toast.makeText(this, R.string.vacation_type_required, Toast.LENGTH_SHORT).show();
//            return false;
//        }
        if (delegatedEmployeeSpinner.getSelectedItemPosition() == -1) {
            Toast.makeText(this, R.string.delegated_employee_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (startFromTxt.equals("")) {
            Toast.makeText(this, R.string.empty_startFromTxt, Toast.LENGTH_SHORT).show();
            return false;
        } else if (toTxt.equals("")) {
            Toast.makeText(this, R.string.empty_toTxt, Toast.LENGTH_SHORT).show();
            return false;
        } else if (extendToTxt.equals("")) {
            Toast.makeText(this, R.string.empty_extendToTxt, Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Utilitaire.isSecondDateHigher(startFromTxt, toTxt, YEAR_MONTH_DAY, false)) {
            Toast.makeText(this, R.string.from_higher_than_to, Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Utilitaire.isSecondDateHigher(startFromTxt, extendToTxt, YEAR_MONTH_DAY, false)) {
            Toast.makeText(this, R.string.fromHigherThatExtendedTo, Toast.LENGTH_SHORT).show();
            return false;
        } else if (delegatedEmployeeSpinner.getSelectedItem() == null) {
            Toast.makeText(this, R.string.delegated_employee_empty, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void processTheCalendar() {
        RobotoCalendarView robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }

    @Override
    public void onDayClick(Date date) {
        String locale = getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat formatForDisplay = new SimpleDateFormat(
                locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String displayed = formatForDisplay.format(date);
        displayed = (locale.equals("ar")) ? Utilitaire.replaceArabicNumbers(displayed) : displayed;

        SimpleDateFormat formatForOutput = new SimpleDateFormat(Constants.YEAR_MONTH_DAY, new Locale("en"));
        hideCalendar();
        switch (dateType) {
            case START_DATE:
                startFromTV.setText(displayed);
                startFromTxt = formatForOutput.format(date);
                break;
            case TO_DATE:
                toTV.setText(displayed);
                toTxt = formatForOutput.format(date);
                break;
            case EXTENDED_TO_DATE:
                extendToTV.setText(displayed);
                extendToTxt = formatForOutput.format(date);
                break;
        }
    }

    @Override
    public void onDayLongClick(Date date) {

    }

    @Override
    public void onRightButtonClick() {
    }

    @Override
    public void onLeftButtonClick() {
    }

    @Override
    public void onBackPressed() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        if (constraintLayout.getVisibility() == View.VISIBLE) {
            hideCalendar();
            Log.d(TAG, "backClick: backClick hide calendar");
        } else {
            super.onBackPressed();
            Log.d(TAG, "backClick: backClick super");
        }
    }

    private void submit() {
        try {
            Log.d(TAG, "submit: " + submitObject.toString());
            int positionInList=historySpinner.getSelectedItemPosition()==-1?0:historySpinner.getSelectedItemPosition();
            String responseCode = new BackgroundWorker(this).execute(
                    "postVacationExtension",
                    submitObject.getEmail(),
                    submitObject.getPassword(),
                    vacationsEntriesSorted.get(positionInList).getVacationId(),
                    submitObject.getOldFromDate(),
                    submitObject.getOldToDate(),
                    submitObject.getNewToDate(),
                    submitObject.getAdvancedPayment(),
                    submitObject.getDelegatedEmployee()
            ).get();
            Message msg = Message.obtain();
            if (isInteger(responseCode)) {
                msg.arg1 = Integer.parseInt(responseCode);
            } else {
                msg.arg1 = 7;
            }
            handler.sendMessage(msg);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            handler.sendMessage(msg);
        }

    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)) {
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread = new LoadingThread();
            loadingThread.start();
        } else {

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }
        DelegatedList delegatedList = DelegatedList.getInstance();

        DelegatedList.newInstance(this, InternalStorage.readFullInternalStorage(this, DELEGATED_LIST));
        delegatedList = DelegatedList.getInstance();

        MySpinnerAdapter adapter2 = new MySpinnerAdapter(this,
                locale.equals("ar") ? delegatedList.getEmployeesArNamesList() : delegatedList.getEmployeesNamesList()
        );
        //set the spinners adapter to the previously created one.
        delegatedEmployeeSpinner = findViewById(R.id.delegatedEmployeeSpinner);
        delegatedEmployeeSpinner.setAdapter(adapter2);
        delegatedEmployeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    enum DateType {
        START_DATE,
        TO_DATE,
        EXTENDED_TO_DATE
    }

    class SubmitObject {
        private String email;
        private String password;
        private String vacationId;
        private String oldFromDate;
        private String oldToDate;
        private String newToDate;
        private String advancedPayment;
        private String delegatedEmployee;


        public SubmitObject() {
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getVacationId() {
            return vacationId;
        }

        public void setVacationId(String vacationId) {
            this.vacationId = vacationId;
        }

        public String getOldFromDate() {
            return oldFromDate;
        }

        public void setOldFromDate(String oldFromDate) {
            this.oldFromDate = oldFromDate;
        }

        public String getOldToDate() {
            return oldToDate;
        }

        public void setOldToDate(String oldToDate) {
            this.oldToDate = oldToDate;
        }

        public String getNewToDate() {
            return newToDate;
        }

        public void setNewToDate(String newToDate) {
            this.newToDate = newToDate;
        }

        public String getAdvancedPayment() {
            return advancedPayment;
        }

        public void setAdvancedPayment(String advancedPayment) {
            this.advancedPayment = advancedPayment;
        }

        public String getDelegatedEmployee() {
            return delegatedEmployee;
        }

        public void setDelegatedEmployee(String delegatedEmployee) {
            this.delegatedEmployee = delegatedEmployee;
        }

        @Override
        public String toString() {
            return "SubmitObject{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", vacationType='" + vacationId + '\'' +
                    ", oldFromDate='" + oldFromDate + '\'' +
                    ", oldToDate='" + oldToDate + '\'' +
                    ", newToDate='" + newToDate + '\'' +
                    ", advancedPayment='" + advancedPayment + '\'' +
                    ", delegatedEmployee='" + delegatedEmployee + '\'' +
                    '}';
        }


    }

    private class SubmitThread extends Thread {

        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }
    }

    private class LoadingThread extends Thread {


        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                submitObject.setEmail(email);
                password = sharedPreferences.getString(PASSWORD, "");
                submitObject.setPassword(password);
                name = sharedPreferences.getString(NAME, "");
            }

            ArrayList<String> vacationTypeList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_TYPES).split("\n")));
            ArrayList<String> vacationArList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_AR_TYPES).split("\n")));

            String[] txt = InternalStorage.readFullInternalStorage(EmployeeVacationExtension.this, VACATION_PREFERENCES).split("\n");
            txt = (txt.length <= 1) ? new String[]{"", ""} : txt;

/////////////////////////
            String resulat = "";
            ArrayList<VacationsEntry> vacationsEntries = new ArrayList<>();

            BackgroundWorker backgroundWorker = new BackgroundWorker(EmployeeVacationExtension.this);

            try {
                if (isManager) {
                    resulat = backgroundWorker.execute("managerHistoryVacation", email, password, managerId).get();
                } else {
                    resulat = backgroundWorker.execute("historyVacation", email, password).get();
                }

                JSONObject jsonObject = new JSONObject(resulat);
                if (!(jsonObject.get("year") instanceof JSONObject)) {
                    Message msg = Message.obtain();
                    msg.arg1 = -1;
                    getHandler.sendMessage(msg);
                    return;
                }
                JSONObject yearsObject = jsonObject.getJSONObject("year");
                Iterator<String> iter = yearsObject.keys();
                while (iter.hasNext()) {
                    String year = iter.next();
                    Log.d(TAG, "setData: here: " + year);
                    JSONArray vacationEntriesArray = yearsObject.getJSONArray(year);
                    for (int j = 0; j < vacationEntriesArray.length(); j++) {

                        JSONObject vacationEntry = vacationEntriesArray.getJSONObject(j);
                        Log.d(TAG, "loadData: vacationEntry: " + vacationEntry.toString());
                        String dateFrom = vacationEntry.getString("DateFrom");
                        String dateTo = vacationEntry.getString("DateTo");
                        String period = vacationEntry.getString("Period");
                        String typeAr = vacationEntry.getString("Name-Ar");
                        String typeEn = vacationEntry.getString("Name-En");
                        String VacationId = vacationEntry.getString("VacationId");
                        Log.d(TAG, "loadData: dateFrom: " + dateFrom);
                        vacationsEntries.add(
                                new VacationsEntry(
                                        String.valueOf(j + 1),
                                        typeEn,
                                        typeAr,
                                        dateFrom,
                                        dateTo,
                                        period,
                                        VacationId,
                                        EmployeeVacationExtension.this
                                )
                        );
                    }

                }
                vacationsEntriesSorted = new ArrayList<>();
                int l = vacationsEntries.size();
                int max = l - 1;
                for (int i = 0; i < l; i++) {
                    vacationsEntriesSorted.add(vacationsEntries.get(max - i));
                    vacationsEntriesSorted.get(i).setNumber(String.valueOf(max - i + 1));
                }
                ShotVacationReturnExtension = new ShotVacationReturnExtension(
                        vacationTypeList,
                        vacationArList,
                        vacationsEntriesSorted,
                        txt[1],//jsonObject.getString("returnDate")
                        locale
                );

                Message msg = Message.obtain();
                msg.arg1 = 200;
                getHandler.sendMessage(msg);

            } catch (JSONException | InterruptedException | ExecutionException e) {
                Message msg = Message.obtain();
                msg.arg1 = 0;
                getHandler.sendMessage(msg);
                e.printStackTrace();
            }


        }
    }


}

