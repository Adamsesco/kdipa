package com.ma.kdipa;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.JavaResources.Utilitaire;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.Locale;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.LOCALE;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.USE_TOUCH_ID;

public class EmployeeSettings extends EmployeeNavigationDrawer {

    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name="";
    String email="";
    String password="";
    String locale="";
    String useTouchId="";

    EditText nameET;
    EditText lstNameET;
    EditText emailET;
    EditText passwordET;



    private static final String TAG = "EmployeeSettings";


    SwitchButton switcher;
    private Spinner dropdown;
    protected DrawerLayout mDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;


        Locale current = getResources().getConfiguration().locale;

        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(LOCALE)) {
            locale = sharedPreferences.getString(LOCALE, "");

        } else{
            locale="en";
        }

        contentView = inflater.inflate(R.layout.common_settings, null, false);

        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        switcher=findViewById(R.id.touch_id);
        dropdown=findViewById(R.id.drop_down_language);

        switcher.setVisibility(View.GONE);
        (findViewById(R.id.view_separator)).setVisibility(View.GONE);
        (findViewById(R.id.use_touch_id_entry)).setVisibility(View.GONE);

        if (sharedPreferences.contains(USE_TOUCH_ID)) {
            useTouchId = sharedPreferences.getString(USE_TOUCH_ID, "true");
            switcher.setChecked(!useTouchId.equals("false"));
        }else {
            useTouchId="true";
            switcher.setChecked(true);
        }

        ImageView v =mDrawer.findViewById(R.id.settings_iv);
        v.setImageResource(R.drawable.settings_en);
        v.setTag(R.drawable.settings_en);
        mDrawer.findViewById(R.id.settings).setBackgroundColor(Color.parseColor("#F2F2F2"));



        ArrayList<String> languagesList=new ArrayList<>();
        languagesList.add("English");
        languagesList.add("Arabic");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.drop_down_list_element, languagesList);
        //set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dropdown.setSelection(getResources().getConfiguration().locale.getLanguage().equals("ar") ?1:0);

        nameET=findViewById(R.id.name);
        lstNameET=findViewById(R.id.surname);
        emailET=findViewById(R.id.email);
        passwordET=findViewById(R.id.password);


        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
        }
        if (name.contains(" ")) {
            nameET.setText(name.split(" ")[0]);
            try {
                lstNameET.setText(name.split(" ")[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            nameET.setText(name);
            lstNameET.setText("");
        }
        emailET.setText(email);
        passwordET.setText(password);

    }


    public void saveChangesClick(View view) {
        locale= (dropdown.getSelectedItemPosition()==0?
                "en":
                "ar"
        );
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = new Locale(locale);
//        res.updateConfiguration(conf, dm);


        // Refresh the activity
        sharedPreferences
                .edit()
                .putString(LOCALE, locale)
                .putString(USE_TOUCH_ID, switcher.isChecked()?"true":"false")
                .apply();

        finish();
        startActivity(getIntent());
    }
}
