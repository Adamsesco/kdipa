package com.ma.kdipa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.ma.kdipa.JavaResources.BackgroundWorkerInvestor;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.CustomBottomSheetView;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.MySpinnerAdapter;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.Spinner;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.responses.AccountsManagerResponse;
import com.ma.kdipa.responses.ServicesResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ma.kdipa.JavaResources.Constants.ALREADY_BOOKED_AT_THAT_TIME;
import static com.ma.kdipa.JavaResources.Constants.BOOKING_ERROR;
import static com.ma.kdipa.JavaResources.Constants.TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class InvestorBooking extends InvestorNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {
    private static final int TO_COMMON_LOGIN = 1897;//arbitrary number but essentially unique
    private static final String TAG = "InvestorBooking";
    private static String prev = "0";
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    String token = "";
    String[] fullResponse;
    String chosenDate = "";
    String chosenTime = "";
    TextView submit;
    ProgressDialog progressDialog;
    Handler handler;
    DialogBoxLoadingClass loadingDialogBox;
    ArrayList<String> phantoms = new ArrayList<>();
    float gap;
    float gapOptions;
    CoordinatorLayout coordinatorLayout;
    CoordinatorLayout coordinatorLayoutOptions;
    CustomBottomSheetView customView;
    CustomBottomSheetView customViewOptions;
    BottomSheetBehavior bottomSheetBehavior;
    BottomSheetBehavior bottomSheetBehaviorOptions;
    private RobotoCalendarView robotoCalendarView;


    private Spinner servicesSpinner;
    private MySpinnerAdapter servicesMySpinnerAdapter;
    private List<String> dataSetServices;
    private List<ServicesResponse> bodyServices;


    private Spinner accountManagerSpinner;
    private MySpinnerAdapter accountManagerMySpinnerAdapter;
    private List<String> dataSetAccountsManager;
    private List<AccountsManagerResponse> bodyAccountsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.i_booking,
                null, false);


        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(TOKEN)) {
            token = sharedPreferences.getString(TOKEN, "");
        }
        processTheBottomSheet();
        processTheBottomSheetOfOptions();


        findViewById(R.id.submitDetailsTV).setOnClickListener(view -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });
        findViewById(R.id.addDetailsTV).setOnClickListener(view -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

            findViewById(R.id.submitOptionsTV).setOnClickListener(view -> {
            bottomSheetBehaviorOptions.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });
        findViewById(R.id.optionsTV).setOnClickListener(view -> {
            bottomSheetBehaviorOptions.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        ImageView v = mDrawer.findViewById(R.id.booking_iv);
        submit = findViewById(R.id.submit);
        v.setImageResource(R.drawable.booking_en);
        v.setTag(R.drawable.booking_en);
        mDrawer.findViewById(R.id.booking).setBackgroundColor(Color.parseColor("#F2F2F2"));

        robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);


        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                progressDialog.dismiss();
                submit.setEnabled(true);
                if (msg.arg1 == 200 || msg.arg1 == 201) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.successfully_booked))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityInvestorDashboard();
                            })
                            .create().show();
                } else if (msg.arg1 == 500 || msg.arg1 == 400 || msg.arg1 == 404 || msg.arg1 == 0) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass dialogBoxCustomClass=new DialogBoxCustomClass(InvestorBooking.this, getString(R.string.error)+" "+msg.arg1,getString(R.string.an_error_occured), "OK");
//                    dialogBoxCustomClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    dialogBoxCustomClass.show();
                } else if (msg.arg1 == TO_COMMON_LOGIN) {
                    lunchActivity(CommonLogin.class);
                } else if (msg.arg1 == ALREADY_BOOKED_AT_THAT_TIME) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.already_booked_at_that_time))
                            .setPositive("ok")
                            .create().show();
                } else if (msg.arg1 == BOOKING_ERROR) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                }
            }
        };

    }
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }else  if (bottomSheetBehaviorOptions.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehaviorOptions.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else {
            super.onBackPressed();
        }
    }

    private void startActivityInvestorDashboard() {
        Intent intent = new Intent(getApplicationContext(), InvestorDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(InvestorBooking.this, p1);
        startActivity(intent, options.toBundle());
    }

    private void setUnavailableSlots(int... ints) {
        for (int anInt : ints) {
            Log.d(TAG, "textView" + (anInt));
            setAsPhantom((TextView) mDrawer.findViewWithTag("textView" + (anInt)));
            phantoms.add("textView" + (anInt));
        }

    }

    private void resetSlots() {
        for (int i = 1; i <= 9; i++) {
            Log.d(TAG, "textView" + i);
            setClickable((TextView) mDrawer.findViewWithTag("textView" + i));
            phantoms.clear();
        }

    }

    private void setAsPhantom(TextView bouton) {
        int pl = bouton.getPaddingLeft();
        int pt = bouton.getPaddingTop();
        int pr = bouton.getPaddingRight();
        int pb = bouton.getPaddingBottom();
        bouton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_phantom_bg));
        bouton.setTextColor(ContextCompat.getColor(this, R.color.phantom_text));
        bouton.setPadding(pl, pt, pr, pb);
    }

    private void setClickable(TextView bouton) {
        int pl = bouton.getPaddingLeft();
        int pt = bouton.getPaddingTop();
        int pr = bouton.getPaddingRight();
        int pb = bouton.getPaddingBottom();
        bouton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_disabled_bg_2));
        bouton.setTextColor(ContextCompat.getColor(this, R.color.clickale_text));
        bouton.setPadding(pl, pt, pr, pb);
    }

    private void setClicked(TextView bouton) {
        int pl = bouton.getPaddingLeft();
        int pt = bouton.getPaddingTop();
        int pr = bouton.getPaddingRight();
        int pb = bouton.getPaddingBottom();
        bouton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_bg));
        bouton.setTextColor(ContextCompat.getColor(this, R.color.white));
        bouton.setPadding(pl, pt, pr, pb);
    }

    @Override
    public void onDayClick(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
        chosenDate = simpleDateFormat.format(date);
    }

    @Override
    public void onDayLongClick(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
        chosenDate = simpleDateFormat.format(date);
    }

    @Override
    public void onRightButtonClick() {

        //resetSlots();
        final int min = 1;
        final int max = 9;
        /*
        setUnavailableSlots(
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min
        );*/
    }

    @Override
    public void onLeftButtonClick() {
//        Toast.makeText(this, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
        //resetSlots();
        final int min = 1;
        final int max = 9;
        /*
        setUnavailableSlots(
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min,
                new Random().nextInt((max - min) + 1) + min
        );*/
    }

    public void clickOn(View view) {

        TextView tv = mDrawer.findViewById(R.id.textView1);
        boolean isPhantom = false;
        for (int i = 0; i < phantoms.size(); i++) {
            if (view.getTag().equals(phantoms.get(i))) isPhantom = true;
        }
        if (!isPhantom) {
            if (!prev.equals("0")) setClickable(mDrawer.findViewWithTag(prev));
            prev = (String) view.getTag();
            setClicked((TextView) view);
            chosenTime = ((TextView) view).getText().toString().replace(" ", "_");
//            Toast.makeText(this, "is not phantom", Toast.LENGTH_SHORT).show();
        }


    }

    public void confirmAppointmentClick(View view) {
        if (Utilitaire.isNetworkAvailable(this)) {
            if (isValid()) {
                submit.setEnabled(false);
                progressDialog = new ProgressDialog(InvestorBooking.this, R.style.customDialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.loading_to_server));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);


                BookingThread bookingThread = new BookingThread();
                bookingThread.start();
            }
        } else {

           new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }

    }

    private boolean isValid() {
        if (chosenTime.equals("") || chosenDate.equals("")) {
            new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.date_time_missing))
                    .setPositive(getString(R.string.retry))
                    .create().show();
            return false;
        }
        if (servicesSpinner.getSelectedItemPosition() == -1) {
            new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.options_service_required))
                    .setPositive(getString(R.string.retry))
                    .create().show();
            return false;
        }
        if (accountManagerSpinner.getSelectedItemPosition() == -1) {
            new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.options_account_manager_required))
                    .setPositive(getString(R.string.retry))
                    .create().show();
            return false;
        }
        return true;
    }

    private void processTheBottomSheet() {
//        findViewById(R.id.layContent).getLayoutParams().height = (int) (Utilitaire.getScreenWidthHeight(this)[1] * 0.19);
        final float peekHeight;


        /**positioning coordinatorLayout**/
        coordinatorLayout = findViewById(R.id.cd);

        customView = findViewById(R.id.custom_view);
        customView.setOnClickListener(view -> {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        bottomSheetBehavior = BottomSheetBehavior.from(customView);

        int screenHeight = Utilitaire.getScreenWidthHeight(this)[1];
//        peekHeight = Utilitaire.getScreenWidthHeight(this)[0] / 7.0f;
        peekHeight = 0;
//        bottomSheetBehavior.setPeekHeight((int) (screenHeight*0.1272));
        bottomSheetBehavior.setPeekHeight((int) (peekHeight));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
            }

            @Override
            public void onSlide(@NonNull View view, float p) {
                gap = (peekHeight * (1 - p));
//                ((CustomBottomSheetView) view).setCorner((int) gap);
            }
        });


    }

    private void processTheBottomSheetOfOptions() {
//        findViewById(R.id.layContent).getLayoutParams().height = (int) (Utilitaire.getScreenWidthHeight(this)[1] * 0.19);
        final float peekHeight;


        /**positioning coordinatorLayout**/
        coordinatorLayoutOptions = findViewById(R.id.cd);

        customViewOptions = findViewById(R.id.optionsCustomView);
        customViewOptions.setOnClickListener(view -> {
            if (bottomSheetBehaviorOptions.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehaviorOptions.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        bottomSheetBehaviorOptions = BottomSheetBehavior.from(customViewOptions);

        int screenHeight = Utilitaire.getScreenWidthHeight(this)[1];
//        peekHeight = Utilitaire.getScreenWidthHeight(this)[0] / 7.0f;
        peekHeight = 0;
//        bottomSheetBehaviorOptions.setPeekHeight((int) (screenHeight*0.1272));
        bottomSheetBehaviorOptions.setPeekHeight((int) (peekHeight));
        bottomSheetBehaviorOptions.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehaviorOptions.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehaviorOptions.setState(bottomSheetBehaviorOptions.STATE_EXPANDED);
//                }
            }

            @Override
            public void onSlide(@NonNull View view, float p) {
                gapOptions = (peekHeight * (1 - p));
//                ((CustomBottomSheetView) view).setCorner((int) gapOptions);
            }
        });

        loadDataServices();


    }

    void loadDataServices() {
        showLoading();
        getAPIManager().getServices(token, Constants.INVESTOR_API_VALUE).enqueue(new Callback<List<ServicesResponse>>() {
            @Override
            public void onResponse(Call<List<ServicesResponse>> call, Response<List<ServicesResponse>> response) {
                hideLoading();
                if (response.body() == null) {
                    return;
                }

                setDataServices(response.body());
            }

            @Override
            public void onFailure(Call<List<ServicesResponse>> call, Throwable t) {
                hideLoading();
                Log.d(TAG, "onResponse: failure: " + t);
                lunchActivity(CommonLogin.class);
                new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.an_error_occured))
                        .setPositive(getString(R.string.cancel))
                        .create().show();
            }
        });

    }

    private void setDataServices(List<ServicesResponse> body) {
        servicesSpinner = findViewById(R.id.servicesSpinner);
        bodyServices = body;
        if (dataSetServices != null)
            dataSetServices.clear();
        else
            dataSetServices = new ArrayList<>();
        for (int i = 0; i < body.size(); i++) {
            dataSetServices.add(body.get(i).name);
        }
        if (servicesMySpinnerAdapter == null) {
            servicesMySpinnerAdapter = new MySpinnerAdapter(this,
                    dataSetServices
            );
        } else {
            servicesMySpinnerAdapter.notifyDataSetChanged();
        }
        //set the spinners adapter to the previously created one.
        servicesSpinner.setAdapter(servicesMySpinnerAdapter);
        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected: adam i:" + i);
                if (i != -1) {
                    loadDataAccountManager(body.get(i).id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void loadDataAccountManager(int id) {
        showLoading();
        getAPIManager().getAccountManager(token, Constants.INVESTOR_API_VALUE, Constants.INVESTOR_BASE_URL + "bookingServiceAccountManager/getAccountManagersByService/" + id).enqueue(new Callback<List<AccountsManagerResponse>>() {
            @Override
            public void onResponse(Call<List<AccountsManagerResponse>> call, Response<List<AccountsManagerResponse>> response) {
                hideLoading();
                if (response.body() == null) {
                    return;
                }
                setDataAccountsManager(response.body());
            }

            @Override
            public void onFailure(Call<List<AccountsManagerResponse>> call, Throwable t) {
                hideLoading();
                Log.d(TAG, "onResponse: failure: " + t);
                lunchActivity(CommonLogin.class);
                new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorBooking.this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.an_error_occured))
                        .setPositive(getString(R.string.cancel))
                        .create().show();
            }
        });

    }

    private void setDataAccountsManager(List<AccountsManagerResponse> body) {
        accountManagerSpinner = findViewById(R.id.accountManagerSpinner);
        if (body == null || body.size() == 0)
            accountManagerSpinner.setVisibility(View.INVISIBLE);
        else
            accountManagerSpinner.setVisibility(View.VISIBLE);
        bodyAccountsManager = body;
        if (dataSetAccountsManager != null)
            dataSetAccountsManager.clear();
        else
            dataSetAccountsManager = new ArrayList<>();
        for (int i = 0; i < body.size(); i++) {
            dataSetAccountsManager.add(body.get(i).name);
        }
        if (accountManagerMySpinnerAdapter == null) {
            accountManagerMySpinnerAdapter = new MySpinnerAdapter(this,
                    dataSetAccountsManager
            );
        } else {
            accountManagerMySpinnerAdapter.notifyDataSetChanged();
        }
        //set the spinners adapter to the previously created one.
        accountManagerSpinner.setAdapter(accountManagerMySpinnerAdapter);
        accountManagerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private class BookingThread extends Thread {
        BookingThread() {
        }

        @Override
        public void run() {
            if (sharedPreferences.contains(TOKEN)) {
                token = sharedPreferences.getString(TOKEN, "");
                BackgroundWorkerInvestor backgroundWorkerInvestor = new BackgroundWorkerInvestor(InvestorBooking.this);
                try {

                    fullResponse = backgroundWorkerInvestor.execute(
                            //endPoint:
                            "bookingForm",
                            //header:
                            token,
                            //request:
                            null,
                            "null",
                            String.valueOf(bodyServices.get(servicesSpinner.getSelectedItemPosition()).id),
                            String.valueOf(bodyAccountsManager.get(accountManagerSpinner.getSelectedItemPosition()).id),
                            chosenDate,
                            chosenTime,
                            ((EditText) findViewById(R.id.contentEt)).getText().toString()
                    ).get();
                    switch (fullResponse[0]) {
                        case "400": {
                            JSONObject jsonObject = new JSONObject(fullResponse[1]);
                            if (jsonObject.has("code")) {
                                String code = jsonObject.getString("code");
                                if (code.equals("Account manager is already booked, choose different date/time!")) {
                                    Message msg = Message.obtain();
                                    msg.arg1 = ALREADY_BOOKED_AT_THAT_TIME;
                                    handler.sendMessage(msg);
                                } else {
                                    fullResponse[1] = code;
                                    Message msg = Message.obtain();
                                    msg.arg1 = BOOKING_ERROR;
                                    handler.sendMessage(msg);
                                }
                            } else {
                                Message msg = Message.obtain();
                                msg.arg1 = 400;
                                handler.sendMessage(msg);
                            }
                            break;
                        }
                        case "500": {
                            Message msg = Message.obtain();
                            msg.arg1 = 500;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "200": {
                            Message msg = Message.obtain();
                            msg.arg1 = 200;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "201": {
                            Message msg = Message.obtain();
                            msg.arg1 = 201;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "401": {
                            Message msg = Message.obtain();
                            msg.arg1 = TO_COMMON_LOGIN;
                            handler.sendMessage(msg);
                            break;
                        }
                        case "404": {
                            Message msg = Message.obtain();
                            msg.arg1 = 404;
                            handler.sendMessage(msg);
                            break;
                        }
                        default: {
                            Message msg = Message.obtain();
                            msg.arg1 = 7;
                            handler.sendMessage(msg);
                            break;
                        }
                    }
                } catch (ExecutionException | InterruptedException | JSONException e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    msg.arg1 = 0;
                    handler.sendMessage(msg);
                }
            } else {
                Message msg = Message.obtain();
                msg.arg1 = TO_COMMON_LOGIN;
                handler.sendMessage(msg);
            }
        }
    }


}
