package com.ma.kdipa;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.ma.kdipa.JavaResources.BackgroundWorkerInvestor;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.Utilitaire;

import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.FORGOT_PASSWORD;
import static com.ma.kdipa.JavaResources.Utilitaire.isValid;

public class CommonForgotPassword  extends AppCompatActivity {
    private static final String TAG = "CommonForgotPassword";
    DialogBoxLoadingClass progressDialog;
    Handler handler;
    DialogBoxLoadingClass loadingDialogBox;




    String email="";



    String employeeEmailDomainName="burgan-systems.com";


    EditText emailET;
    EditText passwordET;
    private TextView submitTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_forgot_pass);

        emailET=findViewById(R.id.useremail_edit_text);
        submitTV =findViewById(R.id.submit);
        passwordET=findViewById(R.id.password_edit_text);
        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==200){
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);

                    new DialogBoxCustom2.DialogBoxCustom2Builder(CommonForgotPassword.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                }else if (msg.arg1==400){
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(CommonForgotPassword.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.incorrect_email))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                }else if(msg.arg1==0){
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(CommonForgotPassword.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                }
            }
        };

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
    private void startActivityInvestorDashboard() {
        Intent intent = new Intent(getApplicationContext(), InvestorDashboard.class);
        Pair<View, String> p1 = Pair.create((View)findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(CommonForgotPassword.this, p1);
        startActivity(intent,options.toBundle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onTestButtonClick(View view) {
        Toast.makeText(this, "clicking was done successfully !", Toast.LENGTH_SHORT).show();
    }

    public void submitClick(View view) {
        email=emailET.getText().toString();
        if (isValid(email)) {
            if (Utilitaire.isNetworkAvailable(this)) {
                submitTV.setEnabled(false);
                progressDialog = new DialogBoxLoadingClass(CommonForgotPassword.this);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);

                ForgotPasswordThread forgotPasswordThread = new ForgotPasswordThread();
                forgotPasswordThread.start();
            } else {
                new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
            }
        }else{
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.wrong_email_address))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }

    }




    private class ForgotPasswordThread extends Thread{
        ForgotPasswordThread() {
        }

        @Override
        public void run() {
            BackgroundWorkerInvestor backgroundWorker=new BackgroundWorkerInvestor(getApplicationContext());
            String[] fullResponse;
            try {
                fullResponse=backgroundWorker.execute(FORGOT_PASSWORD, email ).get();
                if (fullResponse[0].equals("200")){
                    Message msg=Message.obtain();
                    msg.arg1=200;
                    handler.sendMessage(msg);
                }else if (fullResponse[0].equals("400")) {
                    Message msg=Message.obtain();
                    msg.arg1=400;
                    handler.sendMessage(msg);
                }else{
                    Message msg=Message.obtain();
                    msg.arg1=0;
                    handler.sendMessage(msg);
                }
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
            }
        }
    }
}
