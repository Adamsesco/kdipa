package com.ma.kdipa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.FileUtils;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterVacationNew;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.Utilitaire;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class EmployeeSickLeave extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {
    private static final String TAG = "InvestorBooking";
    private static final int PICKFILE_RESULT_CODE = 1;
    private static final int PICK_FROM_GALLERY = 3215;
    private static String prev = "0";

    //    ArrayList<String> attachmentsList;
//    private ArrayList<Image> images = new ArrayList<>();
    protected DrawerLayout mDrawer;
    //    RecyclerView recyclerView;
    ImageView attachmentIV;
    View deleteIV;
    View submitTV;
    RecyclerViewAdapterVacationNew recyclerViewAdapter;
    ArrayList<String> phantoms = new ArrayList<>();
    Uri URI = null;
    String attachmentPath = "";
    String outputDate = "";

//    private ImagePicker getImagePicker() {
////        final boolean returnAfterCapture = ((Switch) findViewById(R.id.ef_switch_return_after_capture)).isChecked();
////        final boolean isSingleMode = ((Switch) findViewById(R.id.ef_switch_single)).isChecked();
////        final boolean useCustomImageLoader = ((Switch) findViewById(R.id.ef_switch_imageloader)).isChecked();
////        final boolean folderMode = ((Switch) findViewById(R.id.ef_switch_folder_mode)).isChecked();
////        final boolean includeVideo = ((Switch) findViewById(R.id.ef_switch_include_video)).isChecked();
////        final boolean onlyVideo = ((Switch) findViewById(R.id.ef_switch_only_video)).isChecked();
////        final boolean isExclude = ((Switch) findViewById(R.id.ef_switch_include_exclude)).isChecked();
//
//        ImagePicker imagePicker = ImagePicker.create(this)
//                .language("en") // Set image picker language
////                .theme(R.style.ImagePickerTheme)
//                .returnMode(ReturnMode.NONE) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
//                .folderMode(false) // set folder mode (false by default)
//                .includeVideo(false) // include video (false by default)
//                .onlyVideo(false) // include video (false by default)
//                .toolbarArrowColor(Color.RED) // set toolbar arrow up color
//                .toolbarFolderTitle("Folder") // folder selection title
//                .toolbarImageTitle("Tap to select") // image selection title
//                .toolbarDoneButtonText("DONE"); // done button text
//
//        ImagePickerComponentHolder.getInstance()
//                .setImageLoader(new DefaultImageLoader());
//
//
//
//        imagePicker.multi(); // multi mode (default mode)
//
////        imagePicker.exclude(images); // don't show anything on this selected images
//        imagePicker.origin(images); // original selected images, used in multi mode
//
//
//
//        return imagePicker.limit(10) // max images can be selected (99 by default)
//                .showCamera(true) // show camera or not (true by default)
//                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
//                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()); // can be full path
//
//
//    }
//    private void printImages(List<Image> images) {
//        if (images == null) return;
//        StringBuilder stringBuffer = new StringBuilder();
//        for (int i = 0, l = images.size(); i < l; i++) {
//            stringBuffer.append(images.get(i).getPath()).append("\n");
//
//
//            Cursor cursor = MediaStore.Images.Thumbnails.queryMiniThumbnails(
//                    getContentResolver(), Uri.fromFile(new File(images.get(i).getPath())),
//                    MediaStore.Images.Thumbnails.MINI_KIND,
//                    null );
//            if( cursor != null && cursor.getCount() > 0 ) {
//                cursor.moveToFirst();//**EDIT**
//                String uri = cursor.getString( cursor.getColumnIndex( MediaStore.Images.Thumbnails.DATA ) );
//            }
//        }
//        ((ImageView) findViewById(R.id.)).setImageURI(Uri.fromFile(new File(images.get(0).getPath())));

    //        ((TextView)findViewById(R.id.file)).setText(stringBuffer.toString());
//        textView.setOnClickListener(v -> ImageViewerActivity.start(MainActivity.this, images));
//    }
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
    private RobotoCalendarView robotoCalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.e_sick_leave, null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

//        ImageView v =mDrawer.findViewById(R.id.booking_iv);
//        v.setImageResource(R.drawable.booking_en);
//        v.setTag(R.drawable.booking_en);

        robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);


        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());

//        attachmentsList =new ArrayList<>();

        int[] dimensions = Utilitaire.getScreenWidthHeight(this);


//        recyclerViewAdapter = new RecyclerViewAdapterVacationNew(attachmentsList,dimensions[0], dimensions[1] );

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
//        recyclerView = findViewById(R.id.attachmentsRV);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setAdapter(recyclerViewAdapter);
//        recyclerView.getRecycledViewPool().setMaxRecycledViews(0,0);


//        findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EmployeeEmergencyLeave.this.captureImage();
//            }
//        });

        attachmentIV = findViewById(R.id.attachmentIV);
        deleteIV = findViewById(R.id.deleteIV);
        submitTV = findViewById(R.id.submitTV);

        submitTV.setOnClickListener(view -> {
            submit();
        });

    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    private void start() {
//        getImagePicker().start(); // start image picker activity with request code
    }

    @Override
    public void onDayClick(Date date) {
        processDayClick(date);
    }

    private void processDayClick(Date date) {
        String locale = getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(Constants.YEAR_MONTH_DAY2, new Locale("en"));
        outputDate = simpleDateFormat2.format(date);

    }

    @Override
    public void onDayLongClick(Date date) {
//        Toast.makeText(this, "onDayLongClick: " + date, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRightButtonClick() {

    }

    @Override
    public void onLeftButtonClick() {
//        Toast.makeText(this, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @NonNull
    public MultipartBody.Part prepareFilePart(String partName, File file) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("*/*"),
                        file
                );
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public void submit() {
        if (!validate()) {
            return;
        }
        RequestBody emailPart = RequestBody.create(MultipartBody.FORM, email);
        RequestBody passwordPart = RequestBody.create(MultipartBody.FORM, password);
        RequestBody trxDatePart = RequestBody.create(MultipartBody.FORM, outputDate);
        MultipartBody.Part filePart;
        if (attachmentPath.equals(""))
            filePart = null;
        else
            filePart = prepareFilePart("filename", new File(attachmentPath));
        showLoading();
        getAPIManager().postSickLeave(
                emailPart, passwordPart,  trxDatePart, filePart
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeSickLeave.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                lunchActivity(EmployeeDashboard.class);
                            })
                            .create().show();
                } else if (response.code() == 401) {
                    showError();
                } else if (response.code() == 402) {
                    showError();
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideLoading();
            }
        });


//        Intent intentToken= getIntent();
//        Bundle bundleToken = intentToken.getExtras();
//
//        if(bundleToken!=null ) {
//            token = (String) bundleToken.get("TOKEN");
//            seekAndGetUserType();

//        }
    }

    private void showError() {
        new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeSickLeave.this)
                .setMode(DialogBoxCustom2.ERROR)
                .setTitle(getString(R.string.error))
                .setBody(getString(R.string.an_error_occured))
                .setPositive(getString(R.string.cancel))
                .create().show();
    }

    private boolean validate() {
        if (outputDate.equals("")) {
            Toast.makeText(this, getString(R.string.date_missing), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void attachmentClick(View view) {


        openFolder();


    }

    private void openFolder() {
        if (!areAllPermissionsGranted()) return;

        Intent intent = new Intent();
        intent.setType("*/*");
//        intent.setType("file/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
//            images = (ArrayList<Image>) ImagePicker.getImages(data);
//            printImages(images);
//            return;
//        }else
        if (requestCode == PICK_FROM_GALLERY && resultCode == RESULT_OK) {
            URI = data.getData();
            try {
                attachmentPath = FileUtils.getPath(EmployeeSickLeave.this, URI);
                showAttachment();
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Tecket_Attach.setText(URI.getLastPathSegment());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showAttachment() {
        if (attachmentPath.toLowerCase().indexOf(".pdf") > 0) {
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageBitmap(Utilitaire.generateImageFromPdf(this, Uri.fromFile(new File(attachmentPath))));
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else if (
                attachmentPath.toLowerCase().indexOf(".jpg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".jpeg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".png") > 0
        ) {
            String thumbnailPath = Utilitaire.generateThumbnail(this, new File(attachmentPath)).getPath();
            //this line is needed to clear imageuri (if exists aready),
            // the main thing is that this instruction will be usful when user
            // chose image then choose an other image:
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageURI(Uri.fromFile(new File(thumbnailPath)));
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(this, getString(R.string.file_type_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteClick(View view) {
        deleteIV.setVisibility(View.INVISIBLE);
        attachmentIV.setVisibility(View.INVISIBLE);
        attachmentPath = "";
    }


}
