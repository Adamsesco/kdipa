package com.ma.kdipa;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Utilitaire.translateToLeft;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kennyc.bottomsheet.BottomSheetListener;
import com.kennyc.bottomsheet.BottomSheetMenuDialogFragment;
import com.ma.kdipa.FilePicker.Image.ImagePicker;
import com.ma.kdipa.JavaResources.BackgroundWorkerPermission;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.FileUtils;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.suke.widget.SwitchButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class EmployeePermissionNew extends EmployeeNavigationDrawer
        implements RobotoCalendarView.RobotoCalendarListener {

    private static final String TAG = "EmployeePermissionNew";
    private static final int PICK_FROM_GALLERY = 2544;
    private static String prev = "0";
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;
    int width;
    int height;
    Duty duty;
    Reason reason;
    TextView dutyStartTV;
    TextView dutyEndTV;
    TextView duringDuty1TV;
    TextView duringDuty2TV;
    String outputDate = "";
    String selectedTime = "";
    ArrayList<String> timesList;
    String amPmTxt = "";
    String amPmTxt2 = "";
    TextView doneTV;
    ProgressDialog progressDialog;
    Handler handler;
    DialogBoxLoadingClass loadingDialogBox;
    RecyclerView timesRecyclerView;
    Spinner hoursSpinner;
    Spinner minSpinner;
    Spinner amPmSpinner;
    String attachmentPath = "";
    ImageView attachmentIV;
    ArrayList<String> phantoms = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    Uri cameraPhotoURI;
    Uri URI = null;
    SwitchButton isMedicalSwitch;
    private ImageView deleteIV;
    private RobotoCalendarView robotoCalendarView;
    private String mCurrentPhotoCapturePath;

    private static String convertAmPmTo24(String time, String amPmTxt) {
        Log.d(TAG, "convertAmPmTo24() called with: time = [" + time + "], amPmTxt = [" + amPmTxt + "]");
        if (time.equals("")) {
            return "";
        }
        String hour = time.split(":")[0];
        String min = time.split(":")[1];
        if (amPmTxt.equals("AM")) return time;
        if (amPmTxt.equals("PM")) {
            return (Integer.parseInt(hour) + 12) + ":" + min;
        }
        Log.d(TAG, "convertAmPmTo24: time: " + time);
        return time;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        Log.d(TAG, "onCreate: sdfsdf213dsf called");
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView;
        contentView = inflater.inflate(
                R.layout.e_new_permission,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
//        findViewById(R.id.cl).getLayoutParams().height=(int) (height*1.21411);
        findViewsById();
        setDefaultValues();

        reason = Reason.DATE_NOT_SELECTED;
        processTheCalendar();
//        processTheSpinnerButtons();
//        processTheRecyclerView();

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 1) {
                    progressDialog.dismiss();
                    doneTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeePermissionNew.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityEmployeeDashboard();
                            })
                            .create().show();
                } else if (msg.arg1 == 0) {
                    progressDialog.dismiss();
                    doneTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeePermissionNew.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeePermissionNew.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.hidable).getVisibility() == View.VISIBLE) {
            translateToLeft(findViewById(R.id.hidable));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);
        duty = Duty.START;
    }


//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    private void findViewsById() {
        isMedicalSwitch = findViewById(R.id.isMedicalSwitch);

        dutyStartTV = findViewById(R.id.time1);
        dutyEndTV = findViewById(R.id.time2);
        duringDuty1TV = findViewById(R.id.time3);
        duringDuty2TV = findViewById(R.id.time4);

        attachmentIV = findViewById(R.id.attachmentIV);
        deleteIV = findViewById(R.id.deleteIV);


        doneTV = findViewById(R.id.submit);
        hoursSpinner = findViewById(R.id.hoursSpinner);
        minSpinner = findViewById(R.id.minSpinner);
        amPmSpinner = findViewById(R.id.amPmSpinner);


    }

    private void processTheCalendar() {
        robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }

    private void setAsPhantom(TextView tv) {
        int pl = tv.getPaddingLeft();
        int pt = tv.getPaddingTop();
        int pr = tv.getPaddingRight();
        int pb = tv.getPaddingBottom();
        tv.setBackground(ContextCompat.getDrawable(this, R.drawable.button_phantom_bg));
        tv.setTextColor(ContextCompat.getColor(this, R.color.phantom_text));
        tv.setPadding(pl, pt, pr, pb);
    }

    private void startActivityEmployeeDashboard() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onDayClick(Date date) {
        processDayClick(date);
    }

    private void processDayClick(Date date) {
        String locale = getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String dateTxt = simpleDateFormat.format(date);

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(Constants.YEAR_MONTH_DAY, new Locale("en"));
        outputDate = simpleDateFormat2.format(date);

    }

    boolean validate() {
        boolean validate = false;
        if (
                !outputDate.equals("") &&
                        (!dutyEndTV.getText().toString().equals("") || !dutyStartTV.getText().toString().equals("") || (!duringDuty1TV.getText().toString().equals("") && !duringDuty2TV.getText().toString().equals("")))
        ) {
            validate = true;
        } else {
            if (outputDate.equals("")) {
                reason = Reason.DATE_NOT_SELECTED;
            } else if (
                    (
                            !dutyEndTV.getText().toString().equals("")
                                    || !dutyStartTV.getText().toString().equals("")
                                    || (
                                    !duringDuty1TV.getText().toString().equals("") &&
                                            !duringDuty2TV.getText().toString().equals("")
                            )
                    )
            ) {
                reason = Reason.TIME_NOT_SELECTED;
            }
        }
        return validate;
    }

    public void attachmentClick(View view) {
        showFilePickerSheet();
    }

    protected void showFilePickerSheet() {
        assert getFragmentManager() != null;
        new BottomSheetMenuDialogFragment.Builder(this, R.style.bottomSheetCustom)
                .setSheet(R.menu.file_picker_sheet)
                .setTitle(R.string.choose_action)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onSheetShown(@NonNull BottomSheetMenuDialogFragment bottomSheet, @Nullable Object object) {
                    }

                    @Override
                    public void onSheetItemSelected(@NonNull BottomSheetMenuDialogFragment bottomSheet, MenuItem item, @Nullable Object object) {
                        switch (item.getItemId()) {
                            case R.id.gallery:
                                if (!areAllPermissionsGranted()) return;
                                openFolder();
                                break;
                            case R.id.camera:
                                if (!areAllPermissionsGranted()) return;
                                new ImagePicker.Builder(EmployeePermissionNew.this)
                                        .mode(ImagePicker.Mode.CAMERA)
                                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                        .extension(ImagePicker.Extension.PNG)
                                        .scale(600, 600)
                                        .allowMultipleImages(false)
                                        .enableDebuggingMode(true)
                                        .build();
                                break;
                        }
                    }

                    @Override
                    public void onSheetDismissed(@NonNull BottomSheetMenuDialogFragment bottomSheet, @Nullable Object object, int dismissEvent) {

                    }
                }).show(getSupportFragmentManager());

    }

    private void openFolder() {
        if (!areAllPermissionsGranted()) return;

        Intent intent = new Intent();
        intent.setType("*/*");
//        intent.setType("file/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: 65s5dfsd2f called with request code:  "+ requestCode);
        Log.d(TAG, "onActivityResult: (requestCode == 65s5dfsd2f ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK): "+(requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK));

        Log.d(TAG, "onActivityResult: 65s5dfsd2f requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE : "+(requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE ));
        Log.d(TAG, "onActivityResult:65s5dfsd2f  resultCode == RESULT_OK: "+(resultCode == RESULT_OK));
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            Log.d(TAG, "onActivityResult:65s5dfsd2f  called");

            for (String mPath : mPaths) {
                Bitmap bitmap = null;
                try {
                    Uri u = Uri.fromFile(new File(mPath));
//                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), u);
                    attachmentPath = (mPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            showAttachment();
        } else if (requestCode == PICK_FROM_GALLERY && resultCode == RESULT_OK) {
            URI = data.getData();
            try {
                attachmentPath = FileUtils.getPath(EmployeePermissionNew.this, URI);
                showAttachment();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void showAttachment() {
        if (attachmentPath.toLowerCase().indexOf(".pdf") > 0) {
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageBitmap(Utilitaire.generateImageFromPdf(this, Uri.fromFile(new File(attachmentPath))));
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else if (
                attachmentPath.toLowerCase().indexOf(".jpg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".jpeg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".png") > 0
        ) {
            String thumbnailPath = Utilitaire.generateThumbnail(this, new File(attachmentPath)).getPath();
            //this line is needed to clear imageuri (if exists aready),
            // the main thing is that this instruction will be usful when user
            // chose image then choose an other image:
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageURI(Uri.fromFile(new File(thumbnailPath)));
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(this, getString(R.string.file_type_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteClick(View view) {
        deleteIV.setVisibility(View.INVISIBLE);
        attachmentIV.setVisibility(View.INVISIBLE);
        attachmentPath = "";
    }

    private void setGalleryIntent(List<Intent> galleryIntents) {
        final Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        final PackageManager packageManager = getPackageManager();
        //queryIntentActivities() returns a list of all activities that can handle the Intent.
        //whereas resolveActivity() returns the "best" Activity that can handle the Intent
        final List<ResolveInfo> listGalleries = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGalleries) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            galleryIntents.add(intent);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        attachmentPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(attachmentPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onDayLongClick(Date date) {
        processDayClick(date);
    }

    @Override
    public void onRightButtonClick() {
    }

    @Override
    public void onLeftButtonClick() {
    }

    private void feedAmPmOutputAndDutyTVs() {
        if (duty == Duty.DURING_START || duty == Duty.START) {
            amPmTxt = amPmSpinner.getSelectedItem().toString();
        } else if (duty == Duty.DURING_END || duty == Duty.END) {
            amPmTxt2 = amPmSpinner.getSelectedItem().toString();
        }
        if (selectedTime.equals("")) {
            return;
        }
        switch (duty) {
            case START:
                dutyStartTV.setText(selectedTime);
                break;
            case END:
                dutyEndTV.setText(selectedTime);
                break;
            case DURING_START:
                duringDuty1TV.setText(selectedTime);
                break;
            case DURING_END:
                duringDuty2TV.setText(selectedTime);
                break;
        }
    }

    public void submitClick(View view) {
        if (validate()) {
            doneTV.setEnabled(false);
            progressDialog = new ProgressDialog(this, R.style.customDialog);
            progressDialog.setMax(100);
//            loadingDialogBox.setIndeterminate(false);
//            loadingDialogBox.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            SubmitThread submitThread = new SubmitThread();
            submitThread.start();
        } else {
            switch (reason) {
                case DATE_NOT_SELECTED:
                    Toast.makeText(this, R.string.date_missing, Toast.LENGTH_SHORT).show();
                    break;
                case TIME_NOT_SELECTED:
                    Toast.makeText(this, R.string.time_missing, Toast.LENGTH_SHORT).show();
                    break;
                case TIME_2_NOT_SELECTED:
                    Toast.makeText(this, R.string.end_time_missing, Toast.LENGTH_SHORT).show();
                    break;
                case AM_PM_NOT_SELECTED:
                    Toast.makeText(this, R.string.am_pm_missing, Toast.LENGTH_SHORT).show();
                    break;
                case AM_PM_2_NOT_SELECTED:
                    Toast.makeText(this, R.string.am_pm_2_missing, Toast.LENGTH_SHORT).show();
                    break;

            }
        }
    }

    private void submit() {

        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
        }
        BackgroundWorkerPermission backgroundWorkerPermission = new BackgroundWorkerPermission(this, progressDialog);
        try {

//            outputTime1 = convertAmPmTo24(outputTime1, amPmTxt);
//            outputTime2 = convertAmPmTo24(outputTime2, amPmTxt2);
            String outputTime11 = duty == Duty.END ? "" : (duty == Duty.START ? (((TextView) findViewById(R.id.time1)).getText().toString()) : (((TextView) findViewById(R.id.time3)).getText().toString()));
            String outputTime22 = duty == Duty.START ? "" : (duty == Duty.END ? (((TextView) findViewById(R.id.time2)).getText().toString()) : (((TextView) findViewById(R.id.time4)).getText().toString()));
            String result = backgroundWorkerPermission.execute(
                    email,
                    outputDate,
                    duty == Duty.START ? "1" : (duty == Duty.END ? "2" : "3"),
                    outputTime11,
                    outputTime22,
                    attachmentPath,
                    password,
                    isMedicalSwitch.isChecked() ? "true" : "false"

            ).get();

            Log.d(TAG, "submit: result: " + result);
            if (!result.equals("")) {
                Message msg = Message.obtain();
                msg.arg1 = 1;
                handler.sendMessage(msg);
            } else {
                Message msg = Message.obtain();
                msg.arg1 = 0;
                handler.sendMessage(msg);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        /*postHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Message msg=Message.obtain();
                    msg.arg1=1;
                    postHandler.sendMessage(msg);
                }
            }, 500);*/


    }

    public void dutyStartClick(View view) {
        ImageView radio1 = findViewById(R.id.radio1);
        radio1.setImageResource(R.drawable.radio_checked_icon);

        ImageView radio2 = findViewById(R.id.radio2);
        ImageView radio3 = findViewById(R.id.radio3);

        radio2.setImageResource(R.drawable.radio_unchecked_icon);
        radio3.setImageResource(R.drawable.radio_unchecked_icon);

        duty = Duty.START;

        dutyEndTV.setText("");
        duringDuty1TV.setText("");
        duringDuty2TV.setText("");

        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minutes) -> {
            ((TextView) findViewById(R.id.time1)).setText(String.format("%02d:%02d", hourOfDay, minutes));
        }, currentHour, currentMinute, true);
        timePickerDialog.show();

    }


    public void duringDutyClick(View view) {


        ImageView radio3 = findViewById(R.id.radio3);
        radio3.setImageResource(R.drawable.radio_checked_icon);

        ImageView radio2 = findViewById(R.id.radio2);
        ImageView radio1 = findViewById(R.id.radio1);

        radio2.setImageResource(R.drawable.radio_unchecked_icon);
        radio1.setImageResource(R.drawable.radio_unchecked_icon);

        dutyStartTV.setText("");
        dutyEndTV.setText("");


//        feedAmPmOutputAndDutyTVs();
//        feedDutyTVs();

//        showTimes();

    }

    public void dutyEndClick(View view) {
        ImageView radio2 = findViewById(R.id.radio2);
        radio2.setImageResource(R.drawable.radio_checked_icon);


        ImageView radio3 = findViewById(R.id.radio3);
        ImageView radio1 = findViewById(R.id.radio1);

        radio3.setImageResource(R.drawable.radio_unchecked_icon);
        radio1.setImageResource(R.drawable.radio_unchecked_icon);

        duty = Duty.END;


        dutyStartTV.setText("");
        duringDuty1TV.setText("");
        duringDuty2TV.setText("");


        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minutes) -> {
            ((TextView) findViewById(R.id.time2)).setText(String.format("%02d:%02d", hourOfDay, minutes));
        }, currentHour, currentMinute, true);
        timePickerDialog.show();


    }

    public void duringDutyStartClick(View view) {
        duringDutyClick(view);
        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);
        duty = Duty.DURING_START;

        timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minutes) -> {

            ((TextView) findViewById(R.id.time3)).setText(String.format("%02d:%02d", hourOfDay, minutes));
        }, currentHour, currentMinute, true);
        timePickerDialog.show();

    }

    public void duringDutyEndClick(View view) {
        duringDutyClick(view);


        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);
        duty = Duty.DURING_END;

        timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minutes) -> {
            ((TextView) findViewById(R.id.time4)).setText(String.format("%02d:%02d", hourOfDay, minutes));
        }, currentHour, currentMinute, true);
        timePickerDialog.show();

    }


    enum Reason {
        SUCCESS,
        DATE_NOT_SELECTED,
        TIME_NOT_SELECTED,
        TIME_2_NOT_SELECTED,
        AM_PM_NOT_SELECTED,
        AM_PM_2_NOT_SELECTED
    }

    enum Duty {START, END, DURING_START, DURING_END}

    ;

    private class SubmitThread extends Thread {
        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }
    }
}