package com.ma.kdipa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Demo extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);

    }


    public void investorClick(View view) {
        startActivity(new Intent(this, InvestorDashboard.class));
    }

    public void employeeClick(View view) {

        startActivity(new Intent(this, EmployeeDashboard.class));

    }
}
