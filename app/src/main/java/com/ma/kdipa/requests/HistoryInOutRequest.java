package com.ma.kdipa.requests;

public class HistoryInOutRequest {


    public String email;
    public String password;
    public String fromDate;
    public String toDate;


    public HistoryInOutRequest(String email, String password, String fromDate, String toDate) {
        this.email = email;
        this.password = password;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
