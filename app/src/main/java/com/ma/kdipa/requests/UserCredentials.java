package com.ma.kdipa.requests;

public class UserCredentials {

    public String email;
    public String password;

    public UserCredentials(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
