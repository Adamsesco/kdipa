package com.ma.kdipa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.ma.kdipa.Intro.Intro;
import com.ma.kdipa.JavaResources.Constants;

import java.util.List;

import static com.ma.kdipa.JavaResources.Constants.DID_SEE_INTRO;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN_2;
import static com.ma.kdipa.JavaResources.Constants.FIRE_BASE_TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    String email = "";
    private static final String TAG = "SplashScreen";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        scheduleSplashScreen();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pkgAppsList = getPackageManager().queryIntentActivities( mainIntent, 0);
        for (int i = 0; i <pkgAppsList.size(); i++) {
            Log.d(TAG, "onCreate: "+pkgAppsList.get(i).activityInfo.name);
        }
    }

    private void scheduleSplashScreen() {
        int splashScreenDuration = 500;

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (true){//if (!didSeeIntro()) {
                    intent = new Intent(SplashScreen.this, Intro.class);
                    startActivity(intent);
                    return;
                }
                if (alreadyConnectedAsEmployee()) {
                    intent = new Intent(SplashScreen.this, EmployeeDashboard.class);
                } else {
                    intent = new Intent(SplashScreen.this, CommonLogin.class);
                }
                Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
                Pair<View, String> p2 = Pair.create((View) findViewById(R.id.theme_logo), "theme_logo");

                try {
                    // Pass data object in the bundle and populate details activity.
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashScreen.this, p1, p2);
                    startActivity(intent, options.toBundle());
                } catch (Exception e) {
                    e.printStackTrace();
                    startActivity(intent);
                }
            }
        }, splashScreenDuration);
    }

    private boolean didSeeIntro() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(Constants.DID_SEE_INTRO)) {
            return sharedPreferences.getBoolean(DID_SEE_INTRO, false);
        }
        return false;
    }

    private boolean alreadyConnectedAsEmployee() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(FIRE_BASE_TOKEN) && sharedPreferences.contains(EMAIL)) {
            email = sharedPreferences.getString(EMAIL, "");
            String domainName = email.split("@")[1];
            return domainName.equals(EMPLOYEE_DOMAIN) || domainName.equals(EMPLOYEE_DOMAIN_2);
        }
        return false;
    }
}

