package com.ma.kdipa;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DelegatedList;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_SIMPLE_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DELEGATED_LIST;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.VACATION_AR_TYPES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_IDS;
import static com.ma.kdipa.JavaResources.Constants.VACATION_TYPES;
import static com.ma.kdipa.JavaResources.Utilitaire.DateFromString;
import static com.ma.kdipa.JavaResources.Utilitaire.formatDate;
import static com.ma.kdipa.JavaResources.Utilitaire.replaceArabicNumbers;

import io.sentry.Sentry;

public class EmployeeDashboard extends EmployeeNavigationDrawer {


    SharedPreferences sharedPreferences;
    String name="";
    String email="";
    String password="";







    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
//    private View mCardFrontLayout;
//    private View mCardBackLayout;




    TextView employeeTV;
    TextView punchInDateTV;
    TextView punchOutDateTV;
    TextView remainedTV;
    TextView takenTV;
    TextView attendedTV;
    TextView vacationsTV;
    TextView absencesTV;

    private ShotEmployeeDashboard shotEmployeeDashboard;
//    private ArrayList<String> fullNamesList;


    protected DrawerLayout mDrawer;
    private AnimatorSet mSetRightOut_remainedTV;
    private AnimatorSet mSetLeftIn_remainedTV;
    private AnimatorSet mSetRightOut_takenTV;
    private AnimatorSet mSetLeftIn_takenTV;
    private AnimatorSet mSetRightOut_attendedTV;
    private AnimatorSet mSetLeftIn_attendedTV;
    private AnimatorSet mSetRightOut_vacationsTV;
    private AnimatorSet mSetLeftIn_vacationsTV;
    private AnimatorSet mSetRightOut_absencesTV;
    private AnimatorSet mSetLeftIn_absencesTV;

    private static final String TAG = "EmployeeDashboard";

    private static Handler handler;
    DialogBoxLoadingClass loadingDialogBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        //make sure the expiration session logic will work
        getApp().touch();
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);



        View contentView;
        contentView = inflater.inflate(
                R.layout.e_dashboard,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);


        ImageView v =mDrawer.findViewById(R.id.home_iv);
        v.setImageResource(R.drawable.home_en);
        v.setTag(R.drawable.home_en);
        mDrawer.findViewById(R.id.home).setBackgroundColor(Color.parseColor("#F2F2F2"));







        //inflating:
        globalFindViewById();

        if (isManager){
            processEmployeeSpinner();
        }else{
            loadData();
        }
        loadAnimations();
        changeCameraDistance();

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
            if (msg.arg1==1){
                loadingDialogBox.dismiss();
                setData();
            }else if(msg.arg1==0){
                loadingDialogBox.dismiss();
                new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeDashboard.this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.an_error_occured))
                        .setPositive(getString(R.string.cancel))
                        .create().show();
//                DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeDashboard.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                cdd.show();
                }
            }
        };

//        if (shotEmployeeDashboard !=null) {
//            setData();
//        } else{
//            DialogBoxCustomClass cdd=new DialogBoxCustomClass(this,"Please make sure you have connection access then reload", "Reload");
//            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            cdd.show();
//        }

    }
    Spinner employeeSpinner;
    private void processEmployeeSpinner() {
        employeeSpinner=findViewById(R.id.employeeSpinner);
        employeeSpinner.setVisibility(View.VISIBLE);
        employeeTV.setVisibility(View.INVISIBLE);
        EmployeesList employeesList=EmployeesList.getInstance();
        if (employeesList == null) {
            EmployeesList.newInstance(this,InternalStorage.readFullInternalStorage(this,EMPLOYEES_LIST) );
            employeesList=EmployeesList.getInstance();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.drop_down_list_element,
                locale.equals("ar")?employeesList.getEmployeesArNamesList():employeesList.getEmployeesNamesList());        //set the spinners adapter to the previously created one.
        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadData();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }




    private void setData() {
        if (employeeTV==null){
        }

        employeeTV.setText(shotEmployeeDashboard.getFullName());
        punchInDateTV.setText(shotEmployeeDashboard.getPunchInDate());
        punchOutDateTV.setText(shotEmployeeDashboard.getPunchOutDate());

        flipCard(remainedTV, mSetLeftIn_remainedTV,  mSetRightOut_remainedTV, shotEmployeeDashboard.getRemained());
        flipCard(takenTV, mSetLeftIn_takenTV, mSetRightOut_takenTV, shotEmployeeDashboard.getTaken());
        flipCard(attendedTV, mSetLeftIn_attendedTV, mSetRightOut_attendedTV, shotEmployeeDashboard.getAttendedDays());
        flipCard(vacationsTV, mSetLeftIn_vacationsTV, mSetRightOut_vacationsTV, shotEmployeeDashboard.getVacations());
        flipCard(absencesTV, mSetLeftIn_absencesTV, mSetRightOut_absencesTV, shotEmployeeDashboard.getAbsences());
    }

    private void loadData() {

        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);

            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }

    }
    private void globalFindViewById() {

        employeeTV=findViewById(R.id.employeeTV);
        punchInDateTV=findViewById(R.id.date_punch_in_tv);
        punchOutDateTV=findViewById(R.id.date_punch_out_tv);
        remainedTV =findViewById(R.id.remained);
        takenTV =findViewById(R.id.taken);
        attendedTV =findViewById(R.id.attended);
        vacationsTV =findViewById(R.id.vacations);
        absencesTV =findViewById(R.id.absences);
        //get the spinner from the xml.

    }


    public void fingerPrintClick(View view) {
        startActivity(new Intent(this, EmployeeGeolocationPunch.class));
    }

    private void loadAnimations() {
        mSetRightOut_remainedTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn_remainedTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
        mSetRightOut_takenTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn_takenTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);

        mSetRightOut_attendedTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn_attendedTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);

        mSetRightOut_vacationsTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn_vacationsTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);

        mSetRightOut_absencesTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn_absencesTV = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);



    }
    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        remainedTV.setCameraDistance(scale);
        takenTV.setCameraDistance(scale);
        attendedTV.setCameraDistance(scale);
        vacationsTV.setCameraDistance(scale);
        absencesTV.setCameraDistance(scale);
    }
    public void flipCard(TextView view, AnimatorSet in, AnimatorSet out, String text) {

        out.setTarget(view);
        in.setTarget(view);
        out.start();
        out.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }
            @Override
            public void onAnimationEnd(Animator animator) {
                view.setText(text);
                in.start();
            }
            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    public void createNewPermissionClick(View view) {
        lunchActivity(EmployeePermissionNew.class);
    }

    public void absencesClick(View view) {
        lunchActivity(EmployeeAbsences.class);
    }

    public void attendanceClick(View view) {
        lunchActivity(EmployeeDaysOfAttendance.class);
    }

    public void vacationsClick(View view) {
        lunchActivity(EmployeeVacationsHistory.class);

    }

    public void permissionsClick(View view) {
        lunchActivity(EmployeePermission.class);

    }
    private class LoadingThread extends Thread{
        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeDashboard.this);
            String result;
            try {
                if(isManager){
                    result=backgroundWorker.execute("managerDashboard", email, password,EmployeesList.getInstance().getEmployeesList().get(employeeSpinner.getSelectedItemPosition()).getId() ).get();
                }else{
                    result=backgroundWorker.execute("dashboard", email, password ).get();
                }
                JSONObject jsonObject=new JSONObject(result);
                String lastPunchIn=jsonObject.getString("lastPunchIn");
                String lastPunchOut=jsonObject.getString("lastPunchOut");
                String permissionsTaken=jsonObject.getString("permissionsTaken");
                String permissionsRemaining=jsonObject.getString("permissionsRemaining");
                String absences=jsonObject.getString("absences");
                String vacations=jsonObject.getString("vacations");
                String attendedDays=jsonObject.getString("attendedDays");
                shotEmployeeDashboard =new ShotEmployeeDashboard(
                        name,
                        lastPunchIn,
                        lastPunchOut,
                        permissionsRemaining,
                        permissionsTaken,
                        attendedDays,
                        vacations,
                        absences,
                        getResources().getConfiguration().locale.getLanguage()
                );
                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
            }

            try {
                backgroundWorker=new BackgroundWorker(EmployeeDashboard.this);
                result=backgroundWorker.execute("newVacation", email, password ).get();
                JSONObject jsonObject= null;
                DelegatedList.newInstance(EmployeeDashboard.this, result);
                InternalStorage.write(EmployeeDashboard.this, DELEGATED_LIST,result,false,false);
                jsonObject = new JSONObject(result);

                InternalStorage.clearFile(EmployeeDashboard.this, VACATION_TYPES);
                InternalStorage.clearFile(EmployeeDashboard.this, VACATION_IDS);
                JSONArray vacationTypes=jsonObject.getJSONArray("vacationTypes");
                for (int i = 0; i < vacationTypes.length(); i++) {
                    JSONObject vacationType=vacationTypes.getJSONObject(i);
                    InternalStorage.write(EmployeeDashboard.this, VACATION_TYPES, vacationType.getString("VacationName"), true, true);
                    InternalStorage.write(EmployeeDashboard.this, VACATION_IDS, vacationType.getString("VacationID"), true, true);
                }

                InternalStorage.clearFile(EmployeeDashboard.this, VACATION_AR_TYPES);
                JSONArray vacationArTypes=jsonObject.getJSONArray("vacationTypesAr");
                for (int i = 0; i < vacationArTypes.length(); i++) {
                    JSONObject vacationArType=vacationArTypes.getJSONObject(i);
                    InternalStorage.write(EmployeeDashboard.this, VACATION_AR_TYPES, vacationArType.getString("VacationName"), true, true);
                }
            } catch (JSONException | ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

        }


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

}
class ShotEmployeeDashboard {
    private static final String TAG = "ShotEmployeeDashboard";

    private String fullName;

    private String punchInDate;
    private String punchOutDate;

    private String remained;
    private String taken;
    private String attendedDays;
    private String vacations;
    private String absences;

    private String locale;

    ShotEmployeeDashboard(
            String fullName,
            String punchInDate,
            String punchOutDate,
            String remained,
            String taken,
            String attendedDays,
            String vacations,
            String absences,
            String locale
    ){
        this.fullName = fullName;
        this.locale=locale;

//        Date punchInDate
        try{
            DateFromString(DATE_INPUT_FORMAT,punchInDate);
            Log.d(TAG, "ShotEmployeeDashboard: punchInDate: "+punchInDate);

            Date date1=DateFromString(DATE_INPUT_FORMAT,punchInDate);
            if (date1 == null) {
                Log.d(TAG, "ShotEmployeeDashboard: null null");
            }
            this.punchInDate=formatDate(date1,locale);

            DateFromString(DATE_INPUT_FORMAT,punchOutDate);
            Log.d(TAG, "ShotEmployeeDashboard: punchOutDate: "+punchOutDate);

            this.punchOutDate=formatDate(DateFromString(DATE_INPUT_FORMAT,punchOutDate),locale);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "ShotEmployeeDashboard: catch error");
            this.punchInDate="";
            this.punchOutDate="";
        }

        this.remained = remained;
        this.taken = taken;
        this.attendedDays = attendedDays;
        this.vacations = vacations;
        this.absences = absences;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPunchInDate() {
        return punchInDate;
    }

    public String getPunchOutDate() {
        return punchOutDate;
    }

    public String getRemained() {
        return remained;
    }

    public String getTaken() {
        return taken;
    }

    public String getAttendedDays() {
        return attendedDays;
    }

    public String getVacations() {
        return vacations;
    }

    public String getAbsences() {
        return absences;
    }






}
