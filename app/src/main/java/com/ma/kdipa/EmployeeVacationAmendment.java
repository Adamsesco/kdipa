
package com.ma.kdipa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.ma.kdipa.JavaResources.MySpinnerAdapter;
import com.ma.kdipa.JavaResources.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DelegatedList;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.ShotVacationAmendment;
import com.ma.kdipa.JavaResources.Utilitaire;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DELEGATED_LIST;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.VACATION_AR_TYPES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_IDS;
import static com.ma.kdipa.JavaResources.Constants.VACATION_PREFERENCES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_TYPES;
import static com.ma.kdipa.JavaResources.Constants.YEAR_MONTH_DAY;
import static com.ma.kdipa.JavaResources.Utilitaire.isInteger;

public class EmployeeVacationAmendment extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener  {


    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name="";
    String email="";
    String password="";

    SubmitObject submitObject=new SubmitObject();


    Spinner delegatedEmployeeSpinner;


    protected DrawerLayout mDrawer;
    Spinner typesSpinner;

    TextView startFromTV;
    TextView toTV;
    TextView startFrom2TV;
    TextView to2TV;

    TextView submitTV;



    ProgressDialog progressDialog;
    Handler handler;



    String startFromTxt="";
    String toTxt="";
    String startFrom2Txt="";
    String to2Txt ="";

    String typeTxt="";


    DateType dateType;
    private static final String TAG = "E_VacationAmendment";
    ShotVacationAmendment shotVacationAmendment;






    ArrayList<String> vacationIDSList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_vacation_amendment,
                null, false);

        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        mDrawer.findViewById(R.id.vacation_amendment).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);





        globalFindViewById();
        setDefaultValues();

        loadData();



        processTheCalendar();

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==200){
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    InternalStorage.write(EmployeeVacationAmendment.this,
                            VACATION_PREFERENCES,
                            startFrom2Txt+"\n"+to2Txt,
                            false, false
                    );
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationAmendment.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityEmployeeDashboard();
                            })
                            .create().show();

                }else {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationAmendment.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationAmendment.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };
    }
    private void startActivityEmployeeDashboard() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View)findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent,options.toBundle());
    }


    private void loadData() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
        }



        String[] txt= InternalStorage.readFullInternalStorage(this, VACATION_PREFERENCES).split("\n");
        txt=(txt.length<=1)?new String[]{"",""}:txt;

        vacationIDSList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationAmendment.this, VACATION_IDS).split("\n")));
        ArrayList<String> vacationTypeList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationAmendment.this, VACATION_TYPES).split("\n")));
        ArrayList<String> vacationArList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationAmendment.this, VACATION_AR_TYPES).split("\n")));
        shotVacationAmendment =new ShotVacationAmendment(
                this,
                vacationTypeList,
                vacationArList,
                txt[0],//jsonObject.getString("departureDate"),
                txt[1],//jsonObject.getString("returnDate")
                "",
                ""
        );
        startFromTxt=txt[0];
        toTxt=txt[1];
        startFrom2Txt="";
        to2Txt="";


        startFromTV.setText(shotVacationAmendment.getstartDate());
        toTV.setText(shotVacationAmendment.gettoDate());
        startFrom2TV.setText(shotVacationAmendment.getStartDate2());
        to2TV.setText(shotVacationAmendment.getToDate2());

        typeTxt= shotVacationAmendment.getTypesList().get(0);

        MySpinnerAdapter adapter = new MySpinnerAdapter(this,
                locale.equals("ar")?shotVacationAmendment.getTypesArList():shotVacationAmendment.getTypesList()
        );

        typesSpinner.setAdapter(adapter);
        typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeTxt= shotVacationAmendment.getTypesList().get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        DelegatedList delegatedList=DelegatedList.getInstance();

        DelegatedList.newInstance(this,InternalStorage.readFullInternalStorage(this,DELEGATED_LIST) );
        delegatedList= DelegatedList.getInstance();


        MySpinnerAdapter adapter2 = new MySpinnerAdapter(this,
                locale.equals("ar")?delegatedList.getEmployeesArNamesList():delegatedList.getEmployeesNamesList()
        );
        //set the spinners adapter to the previously created one.
        delegatedEmployeeSpinner=findViewById(R.id.delegatedEmployeeSpinner);
        delegatedEmployeeSpinner.setAdapter(adapter2);
        delegatedEmployeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);

    }



    private void globalFindViewById() {
        typesSpinner=findViewById(R.id.vacationType);

        startFromTV=findViewById(R.id.startFrom);
        toTV=findViewById(R.id.startTo);
        startFrom2TV=findViewById(R.id.startFrom2);
        to2TV=findViewById(R.id.startTo2);


        submitTV=findViewById(R.id.submit);



    }
    public void showCalendarForDepartureDate(View view) {
        dateType= DateType.START_DATE_1;
        showCalendar();
    }
    public void showCalendarForReturnDate(View view) {
        dateType= DateType.TO_DATE_1;
        showCalendar();
    }
    public void showCalendarForDepartureDate2(View view) {
        dateType= DateType.START_DATE_2;
        showCalendar();
    }
    public void showCalendarForReturnDate2(View view) {
        dateType= DateType.TO_DATE_2;
        showCalendar();
    }

    public void delete1Click(View view) {
        startFromTV.setText("");
        startFromTxt="";
    }

    public void delete2Click(View view) {
        toTV.setText("");
        toTxt="";
    }

    public void delete3Click(View view) {
        startFrom2TV.setText("");
        startFrom2Txt="";
    }

    public void delete4Click(View view) {
        to2TV.setText("");
        to2Txt="";
    }




    public void showCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation=new TranslateAnimation(-constraintLayout.getWidth(),0,0,0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility()==View.INVISIBLE || constraintLayout.getVisibility()==View.GONE){
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                constraintLayout.setVisibility(View.VISIBLE);

            }
            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public void hideCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation=new TranslateAnimation(0,-constraintLayout.getWidth(),0,0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility()==View.VISIBLE){
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                constraintLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void submitClick(View view) {

        if (validate()){


            submitObject.setEmail(email);
            submitObject.setPassword(password);
            submitObject.setVacationType(vacationIDSList.get(typesSpinner.getSelectedItemPosition()));
            submitObject.setOldFromDate(startFromTxt);
            submitObject.setOldToDate(toTxt);
            submitObject.setNewFromDate(startFrom2Txt);
            submitObject.setNewToDate(to2Txt);
            submitObject.setAdvancedPayment("true");
            if (delegatedEmployeeSpinner.getSelectedItem()!=null){
                submitObject.setDelegatedEmployee(delegatedEmployeeSpinner.getSelectedItem().toString());
            }




            submitTV.setEnabled(false);
            progressDialog= new ProgressDialog(this, R.style.customDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);



            SubmitThread submitThread=new SubmitThread();
            submitThread.start();
        }
    }


    boolean validate(){
        @SuppressLint("SimpleDateFormat") String currentDate=new SimpleDateFormat(YEAR_MONTH_DAY).format(Calendar.getInstance().getTime());
        if (typesSpinner.getSelectedItemPosition() == -1) {
            Toast.makeText(this, R.string.vacation_type_required, Toast.LENGTH_SHORT).show();
            return false;
        }


        if (startFromTxt.equals("")){
            Toast.makeText(this, R.string.empty_startFromTxt, Toast.LENGTH_SHORT).show();
            return false;
        }else if (toTxt.equals("")){
            Toast.makeText(this, R.string.empty_toTxt, Toast.LENGTH_SHORT).show();
            return false;
        }else if (startFrom2Txt.equals("")){
            Toast.makeText(this, R.string.empty_startFrom2Txt, Toast.LENGTH_SHORT).show();
            return false;
        }else if (to2Txt.equals("")){
            Toast.makeText(this, R.string.empty_to2Txt, Toast.LENGTH_SHORT).show();
            return false;
        }else if (typeTxt.equals("")){
            Toast.makeText(this, R.string.empty_type, Toast.LENGTH_SHORT).show();
            return false;
        }else if (!Utilitaire.isSecondDateHigher(startFromTxt,toTxt,YEAR_MONTH_DAY, false)){
            Toast.makeText(this, R.string.from_higher_than_to, Toast.LENGTH_SHORT).show();
            return false;
        }else if (!Utilitaire.isSecondDateHigher(startFrom2Txt,to2Txt,YEAR_MONTH_DAY, false)){
            Toast.makeText(this, R.string.to2Txt_older_than_startFrom2Txt, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (delegatedEmployeeSpinner.getSelectedItemPosition() == -1) {
            Toast.makeText(this, R.string.delegated_employee_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    enum DateType{
        START_DATE_1,
        TO_DATE_1,
        START_DATE_2,
        TO_DATE_2
    }

    private void processTheCalendar() {
        RobotoCalendarView robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }
    @Override
    public void onDayClick(Date date) {
        String locale=getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat formatForDisplay=new SimpleDateFormat(
                locale.equals("ar")?CHOOSE_DATE_AR_DISPLAY:CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String displayed=formatForDisplay.format(date);
        displayed= (locale.equals("ar"))?Utilitaire.replaceArabicNumbers(displayed):displayed;
        SimpleDateFormat formatForOutput=new SimpleDateFormat(Constants.YEAR_MONTH_DAY, new Locale("en"));

        hideCalendar();
        switch (dateType){
            case START_DATE_1:
                startFromTV.setText(displayed);
                startFromTxt=formatForOutput.format(date);
                break;
            case TO_DATE_1:
                toTV.setText(displayed);
                toTxt=formatForOutput.format(date);
                break;
            case START_DATE_2:
                startFrom2TV.setText(displayed);
                startFrom2Txt=formatForOutput.format(date);
                break;
            case TO_DATE_2:
                to2TV.setText(displayed);
                to2Txt=formatForOutput.format(date);
                break;


        }
    }

    @Override
    public void onDayLongClick(Date date) {

    }

    @Override
    public void onRightButtonClick() {
    }
    @Override
    public void onLeftButtonClick() {
    }
    @Override
    public void onBackPressed() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        if (constraintLayout.getVisibility()==View.VISIBLE){
            hideCalendar();
        }else{
            super.onBackPressed();
        }
    }


    private class SubmitThread extends Thread{

        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }
    }

    private void submit() {
        try {
            String responseCode=new BackgroundWorker(this).execute(
                    "postVacationAmendment",
                    submitObject.getEmail(),
                    submitObject.getPassword(),
                    submitObject.getVacationType(),
                    submitObject.getOldFromDate(),
                    submitObject.getOldToDate(),
                    submitObject.getNewFromDate(),
                    submitObject.getNewToDate(),
                    submitObject.getAdvancedPayment(),
                    submitObject.getDelegatedEmployee()
            ).get();
            Message msg=Message.obtain();
            if (isInteger(responseCode)){
                msg.arg1=Integer.parseInt(responseCode);
            }else {
                msg.arg1=7;
            }
            handler.sendMessage(msg);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg=Message.obtain();
            msg.arg1=0;
            handler.sendMessage(msg);
        }

    }

    class SubmitObject {
        private String email;
        private String password;
        private String vacationType;
        private String oldFromDate;
        private String oldToDate;
        private String newFromDate;
        private String newToDate;
        private String advancedPayment;
        private String delegatedEmployee;


        public SubmitObject() {
        }

        @Override
        public String toString() {
            return "SubmitObject{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", vacationType='" + vacationType + '\'' +
                    ", oldFromDate='" + oldFromDate + '\'' +
                    ", oldToDate='" + oldToDate + '\'' +
                    ", newFromDate='" + newFromDate + '\'' +
                    ", newToDate='" + newToDate + '\'' +
                    ", advancedPayment='" + advancedPayment + '\'' +
                    ", delegatedEmployee='" + delegatedEmployee + '\'' +
                    '}';
        }

        public String getNewFromDate() {
            return newFromDate;
        }

        public void setNewFromDate(String newFromDate) {
            this.newFromDate = newFromDate;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getVacationType() {
            return vacationType;
        }

        public void setVacationType(String vacationType) {
            this.vacationType = vacationType;
        }

        public String getOldFromDate() {
            return oldFromDate;
        }

        public void setOldFromDate(String oldFromDate) {
            this.oldFromDate = oldFromDate;
        }

        public String getOldToDate() {
            return oldToDate;
        }

        public void setOldToDate(String oldToDate) {
            this.oldToDate = oldToDate;
        }

        public String getNewToDate() {
            return newToDate;
        }

        public void setNewToDate(String newToDate) {
            this.newToDate = newToDate;
        }

        public String getAdvancedPayment() {
            return advancedPayment;
        }

        public void setAdvancedPayment(String advancedPayment) {
            this.advancedPayment = advancedPayment;
        }

        public String getDelegatedEmployee() {
            return delegatedEmployee;
        }

        public void setDelegatedEmployee(String delegatedEmployee) {
            this.delegatedEmployee = delegatedEmployee;
        }

    }


}
