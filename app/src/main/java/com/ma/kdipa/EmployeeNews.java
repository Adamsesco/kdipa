
package com.ma.kdipa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterNews;
import com.ma.kdipa.JavaResources.SingleRowNews;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class EmployeeNews extends EmployeeNavigationDrawer {


    private static final String TAG = "EmployeePermission";

    protected DrawerLayout mDrawer;

    private ArrayList<SingleRowNews> list;
    private ArrayList<String> fullNamesList;
    private TextView employeeTV;
    private LinearLayoutManager linearLayoutManager;

    private static Handler handler;
    DialogBoxLoadingClass loadingDialogBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_news,
                null, false);        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);


        ImageView v =mDrawer.findViewById(R.id.news_iv);
        v.setImageResource(R.drawable.news_en);
        v.setTag(R.drawable.news_en);
        mDrawer.findViewById(R.id.news).setBackgroundColor(Color.parseColor("#F2F2F2"));

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==1){
                    loadingDialogBox.dismiss();
                    setData();
                }else if(msg.arg1==0){
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeNews.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeNews.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };
        loadData();





    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
    private void setData() {
        RecyclerView recyclerView = findViewById(R.id.newsRV);
        RecyclerViewAdapterNews recyclerViewAdapter;


        recyclerViewAdapter = new RecyclerViewAdapterNews(list);

        linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }
    }
    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            try {
                list=new ArrayList<>();
                String result=new BackgroundWorker(EmployeeNews.this).execute("" +
                                "historyNews",
                        email,
                        password
                ).get();

                JSONArray jsonArray=new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject= jsonArray.getJSONObject(i);
                    list.add(new SingleRowNews(
                            jsonObject.getString("date"),
                            jsonObject.getString("title"),
                            jsonObject.getString("titleAr"),
                            jsonObject.getString("content"),
                            jsonObject.getString("contentAr"),
                            locale
                    ));
                }
                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);

            }

        }
    }
}
