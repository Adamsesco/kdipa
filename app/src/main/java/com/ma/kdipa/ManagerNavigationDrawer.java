package com.ma.kdipa;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.IS_IN_FORGROUND;
import static com.ma.kdipa.JavaResources.Constants.LOCALE;
import static com.ma.kdipa.JavaResources.Constants.NOTIFICATION_INTENT;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.Utils;
import com.ma.kdipa.expire_logique.ABFSApplication;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("Registered")
public class ManagerNavigationDrawer extends AppCompatActivity {
    private static final String TAG = "ENavigationDrawer";
    private static final int DELAY = 300;
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            ((ImageView) findViewById(R.id.notification_header_icon)).setImageResource(R.drawable.notifications_en);
            Toast.makeText(getApplicationContext(), R.string.new_notification, Toast.LENGTH_SHORT).show();
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    };
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    String locale = "";

    /*for drawerLayout*/
    int relevantSide;
    DrawerLayout.DrawerListener drawerListener;
    private DrawerLayout dl;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView nv;

    @Override
    protected void onStart() {
        super.onStart();

        if (getApp().wasInBackground()) {
            Log.d(TAG, "onStart: 313dfg1dfg from background");
            getApp().setInBackGround(false);
            String lastTimeConnected = Utils.getValue(this, Constants.LAST_TIME_CONNECTED, null);
            if (lastTimeConnected == null) return;
            Date date = Utilitaire.stringToDate(lastTimeConnected, Constants.DATE_INPUT_FORMAT);
            Date currentTime = Calendar.getInstance().getTime();
            long diff = currentTime.getTime() - date.getTime();
            Log.d(TAG, "onStart: 313dfg1dfg diff: "+diff/1000);
            if (diff >= Constants.EXPIRY_TIME) {
                logoutClick(null);
            }
        } else {
            Log.d(TAG, "onStart: was in foreground 313dfg1dfg");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(LOCALE)) {
            Log.d(TAG, "onCreate: sharedpreferences contains locale");
            locale = sharedPreferences.getString(LOCALE, "");
        } else {
            locale = "en";
        }
        Log.d(TAG, "onCreate: locale: " + locale);

        relevantSide = (locale.equals("ar") ? Gravity.RIGHT : Gravity.LEFT);

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = new Locale(locale);
        res.updateConfiguration(conf, dm);


        setContentView(
                R.layout.m_navigation_drawer_layout
        );


        //navigation drawer processing
//        dl=(DrawerLayout) LayoutInflater.from(getApplicationContext()).inflate(R.layout.common_login, null, false).findViewById(R.id.activity_main);
        dl = findViewById(R.id.navigation_drawer_view);
        handleCopyright();

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);


        dl.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        drawerListener = new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (findViewById(R.id.menu_content).getVisibility() == View.VISIBLE) {
                    findViewById(R.id.menu_content).setVisibility(View.GONE);
                    findViewById(R.id.request_content).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.menu_content).setVisibility(View.VISIBLE);
                    findViewById(R.id.request_content).setVisibility(View.GONE);
                }
                dl.openDrawer(relevantSide);
                dl.removeDrawerListener(drawerListener);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        };

    }

    private void handleCopyright() {
        @SuppressLint("SimpleDateFormat") String year = Utilitaire.replaceArabicNumbers(new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime()));
        String copyrightTxt = locale.equals("ar") ? "KDIPA © 2016–" + year + " حقوق الملكية" : "Copyright © 2016–" + year + " KDIPA";
        ((TextView) findViewById(R.id.copyright2)).setText(copyrightTxt);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    public void triggerNavDrawer(View view) {
        //hide keyboard:
        if (this.getCurrentFocus() != null && this.getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
        dl.openDrawer(relevantSide);
    }

    public void onTestButtonClick(View view) {
        Toast.makeText(this, "clicking was done successfully !", Toast.LENGTH_SHORT).show();
    }

    public void homeClick(View view) {
        ImageView iv = dl.findViewById(R.id.home_iv);
        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeDashboard.class);
                }
            }, DELAY);
        }
    }

    public void absenceClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeAbsences.class);
            }
        }, DELAY);
    }

    public void sick_leaveClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(ManagerNavigationDrawer.this, EmployeeSickLeave.class);
                intent.putExtra(Constants.IS_SICK_LEAVE_INTENT, true);
                startActivity(intent);
            }
        }, DELAY);
    }

    public void logoutClick(View view) {
//        ImageView iv= dl.findViewById(R.id.logout_iv);

        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void run() {
                ManagerNavigationDrawer.this.getSharedPreferences(USER_INFO, MODE_PRIVATE).edit().clear().commit();


                SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(Constants.LAST_LOGIN_USER_INFO, MODE_PRIVATE);
                sharedPreferences.edit()
                        .putString(EMAIL, email)
                        .putString(PASSWORD, password)
                        .apply();


                lunchActivity(CommonLogin.class);
            }
        }, DELAY);
    }

    public void official_dutyClick(View view) {
        ImageView iv = dl.findViewById(R.id.home_iv);
        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(ManagerOfficialDuty.class);
                }
            }, DELAY);
        }
    }

    public void settingsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeSettings.class);
            }
        }, DELAY);
    }

    public void newRequestClick(View view) {
        dl.addDrawerListener(drawerListener);
        dl.closeDrawer(relevantSide);

    }

    public void backClick(View view) {
        dl.addDrawerListener(drawerListener);
        dl.closeDrawer(relevantSide);
    }

    public void historyTransactionsClick(View view) {
        Toast.makeText(this, "historyTransactionsClick", Toast.LENGTH_SHORT).show();
    }

    public void geolocationPunchClick(View view) {
        ImageView iv = dl.findViewById(R.id.geolocationPunch_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeGeolocationPunch.class);
                }
            }, DELAY);
        }
    }

    public void notificationsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeNotifications.class);
            }
        }, DELAY);
    }

    public void newsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeNews.class);
            }
        }, DELAY);
    }

    public void permissionClick(View view) {
        View v = dl.findViewById(R.id.permission);
        if (v.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeePermission.class);
                }
            }, DELAY);
        }
    }

    public void vacationClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationsHistory.class);
            }
        }, DELAY);
    }

    public void vacation_returnClick(View view) {
        View tv = dl.findViewById(R.id.vacation_return);
        if (tv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeVacationReturn.class);
                }
            }, DELAY);
        }
    }


    public void vacation_extensionClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationExtension.class);
            }
        }, DELAY);

    }

    public void vacation_amendmentClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationAmendment.class);
            }
        }, DELAY);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        moveTaskToBack(true);
        if (dl.isDrawerOpen(relevantSide)) {
            dl.closeDrawer(relevantSide);
        } else {
            lunchActivity(EmployeeDashboard.class);

        }


    }

    void lunchActivity(Class<?> someClass) {
        Intent intent = new Intent(getApplicationContext(), someClass);

        Pair<View, String> p1 = Pair.create(findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create(findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(ManagerNavigationDrawer.this, p1, p3);
        startActivity(intent, options.toBundle());
    }

    public void requestClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeRequestStatus.class);
            }
        }, DELAY);

    }

    public void forRippleClick(View view) {
        //only for ripple purpuses
    }

    @Override
    public void onResume() {
        super.onResume();
        this.registerReceiver(mMessageReceiver, new IntentFilter(NOTIFICATION_INTENT));

        activityStateSharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        activityStateSharedPreferences
                .edit()
                .putBoolean(IS_IN_FORGROUND, true)
                .apply();
    }

    //Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mMessageReceiver);
        activityStateSharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        activityStateSharedPreferences
                .edit()
                .putBoolean(IS_IN_FORGROUND, false)
                .apply();
    }

    public ABFSApplication getApp() {
        return (ABFSApplication) this.getApplication();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        getApp().touch();
        Log.d("TAG", "User interaction to " + this.toString());
    }

}
