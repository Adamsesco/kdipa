
package com.ma.kdipa;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.PagerAdapterDotIndicator;
import com.ma.kdipa.JavaResources.ShotPermissionEmployee;
import com.ma.kdipa.JavaResources.SinglePage;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;

public class EmployeePermission extends EmployeeNavigationDrawer {


    private static final String TAG = "EmployeePermission";


    private PieChart chart;
    protected DrawerLayout mDrawer;
    ViewPager viewPager;

    private ShotPermissionEmployee shotPermissionEmployee;
    private ArrayList<String> fullNamesList;
    private TextView employeeTV;
    private TextView totalIV;
    private TextView takenTV;
    private TextView remainedTV;

    boolean successfullyLoaded=false;

    private static Handler handler;
    DialogBoxLoadingClass loadingDialogBox;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_permissions,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);
        mDrawer.findViewById(R.id.permission).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);

        View v =mDrawer.findViewById(R.id.permission);
        v.setTag("not null");

        globalFindViewById();

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==1){
                    findViewById(R.id.chart1).setVisibility(View.VISIBLE);
                    loadingDialogBox.dismiss();
                    successfullyLoaded=true;
                    chartProcessing();
                    PagerAdapterDotIndicator adapter = new PagerAdapterDotIndicator(shotPermissionEmployee.getPermissionsHistoryList());
                    viewPager.setAdapter(adapter);
                }else if(msg.arg1==0){
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeePermission.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeePermission.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };

        if (isManager){
            processEmployeeSpinner();
        }else{
            loadData();
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
    Spinner employeeSpinner;
    private void processEmployeeSpinner() {
        employeeSpinner=findViewById(R.id.employeeSpinner);
        employeeSpinner.setVisibility(View.VISIBLE);
        employeeTV.setVisibility(View.INVISIBLE);
        EmployeesList employeesList=EmployeesList.getInstance();
        if (employeesList == null) {
            EmployeesList.newInstance(this, InternalStorage.readFullInternalStorage(this,EMPLOYEES_LIST) );
            employeesList=EmployeesList.getInstance();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.drop_down_list_element,
                locale.equals("ar")?employeesList.getEmployeesArNamesList():employeesList.getEmployeesNamesList());        //set the spinners adapter to the previously created one.
        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadData();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeePermission.this);
            String result="";



            shotPermissionEmployee=new ShotPermissionEmployee();//this avoid crash when app can't reach server
            try {
                if(isManager){
                    result=backgroundWorker.execute("managerHistoryPermission",
                            email,
                            password,
                            EmployeesList.getInstance().getEmployeesList().get(employeeSpinner.getSelectedItemPosition()).getId()
                    ).get();
                }else{
                    result = backgroundWorker.execute("historyPermission", email, password).get();
                }

                JSONObject jsonObject=new JSONObject(result);

                String name=jsonObject.getString("name");
                String nameAr=jsonObject.getString("nameAr");
                String permissionsRemained=jsonObject.getString("permissionsRemained");
                String permissionsTaken=jsonObject.getString("permissionsTaken");


                JSONArray permissionsHistoryArray=jsonObject.getJSONArray("permissionsHistory");
                ArrayList<SinglePage> singlePagesList = new ArrayList<>();
                for (int i = 0; i < permissionsHistoryArray.length(); i++) {
                    singlePagesList.add(new SinglePage(
                            ((JSONObject)permissionsHistoryArray.get(i)).getString("TrxDate"),
                            ((JSONObject)permissionsHistoryArray.get(i)).getString("Name-En"),
                            ((JSONObject)permissionsHistoryArray.get(i)).getString("TimeFrom"),
                            ((JSONObject)permissionsHistoryArray.get(i)).getString("TimeTo"),
                            ((JSONObject)permissionsHistoryArray.get(i)).getString("Period"),
                            EmployeePermission.this
                    ));
                }
                shotPermissionEmployee=new ShotPermissionEmployee(
                        name,
                        nameAr,
                        permissionsRemained,
                        permissionsTaken,
                        singlePagesList
                );
                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);
            } catch (JSONException | ExecutionException | InterruptedException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
            }

        }


    }
    private void chartProcessing() {
        chart = findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
//        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

//        chart.setCenterTextTypeface(tfLight);
        chart.setCenterText(generateCenterSpannableText());

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.TRANSPARENT);

        chart.getLegend().setEnabled(false);


        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(255);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(false);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(false);



        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
//        chart.setOnChartValueSelectedListener(this);


        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);



        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
//        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);

        // undo all highlights
        chart.highlightValues(null);
        chart.setHighlightPerTapEnabled(false);




        setData();

        chart.highlightValue(0, 0, false);
        chart.invalidate();
    }



    private void globalFindViewById() {
        //get the spinner from the xml.
        employeeTV = findViewById(R.id.employeeTV);
        totalIV = findViewById(R.id.totalIV);
        viewPager = findViewById(R.id.view_pager);


        takenTV=findViewById(R.id.taken);
        remainedTV=findViewById(R.id.remained);

    }
    private void setData() {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        //permission taken:
        int taken=Integer.parseInt(shotPermissionEmployee.getPermissionsTaken());
        int remained=Integer.parseInt(shotPermissionEmployee.getPermissionsRemained());
        int total=taken+remained;
        //% of taken:
        float f= (float) taken/(float)total;
        entries.add(new PieEntry(f));
        entries.add(new PieEntry(1.0f-f));
        int value= (int) ((1.0f-f)*100);
        ((TextView)findViewById(R.id.chart_main_value)).setText(value+"%");

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setDrawValues(false);

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();

//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.COLORFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);
//
//        colors.add(ColorTemplate.getHoloBlue());
        colors.add(ContextCompat.getColor(EmployeePermission.this, R.color.light));
        colors.add(ContextCompat.getColor(EmployeePermission.this, R.color.dark));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
//        data.setValueTypeface(tfLight);
        chart.setData(data);



        dataSet.setSelectionShift(3f);


        chart.invalidate();


        String locale=getResources().getConfiguration().locale.getLanguage();
        String s=shotPermissionEmployee.getPermissionsTaken();
        takenTV.setText((locale.equals("ar")?s+" اذن مؤخوذ":s+" Permissions taken"));
        s=shotPermissionEmployee.getPermissionsRemained();
        remainedTV.setText((locale.equals("ar")?s+" اذن مُتبقٍّ":s+" Permissions remained"));

        employeeTV.setText(shotPermissionEmployee.getName());
        totalIV.setText(String.valueOf(total));




    }
    private void loadData() {

        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }



    }






    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }


    public void createNewPermissionClick(View view) {
        Intent intent = new Intent(getApplicationContext(), EmployeePermissionNew.class);
        Pair<View, String> p1 = Pair.create((View)findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create((View)findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1,p3);
        startActivity(intent,options.toBundle());
    }

    public void takenButtonClick(View view) {
        if (successfullyLoaded){

            chart.highlightValue(0, 0, false);
            chart.invalidate();

            ((TextView)view).setBackground(ContextCompat.getDrawable(this, R.drawable.button_permission_taken));
            ((TextView) findViewById(R.id.remained)).setBackground(ContextCompat.getDrawable(this,R.drawable.button_permission_gray));
        }
    }

    public void remainedButtonClick(View view) {
        if (successfullyLoaded){

            chart.highlightValue(1, 0, false);
            chart.invalidate();

            ((TextView)findViewById(R.id.taken)).setBackground(ContextCompat.getDrawable(this, R.drawable.button_permission_gray));
            ((TextView) findViewById(R.id.remained)).setBackground(ContextCompat.getDrawable(this,R.drawable.button_permission_remained));
        }

    }
}
