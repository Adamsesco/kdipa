
package com.ma.kdipa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fivehundredpx.android.blur.BlurringView;
import com.ma.kdipa.JavaResources.AbsencesEntriesPerYear;
import com.ma.kdipa.JavaResources.AbsencesEntry;
import com.ma.kdipa.JavaResources.AbsencesPerEmployee;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterAbsences;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterYearsFilter;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Utilitaire.fade;

public class    EmployeeAbsences extends EmployeeNavigationDrawer {


    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name="";
    String email="";
    String password="";


    private static Handler handler;
    DialogBoxLoadingClass loadingDialogBox;


    BlurringView blurringView;
    ImageView snapShotIV;
    ImageView darkIV;
    ImageView closeIV;
    TextView yearTV;
    TextView yearsEntryTV;
    RecyclerView yearsRV;

    AbsencesPerEmployee absencesPerEmployee;


    RecyclerViewAdapterYearsFilter recyclerViewAdapterYearsFilter;


    protected DrawerLayout mDrawer;

//    private ArrayList<String> fullNamesList;
//    private Spinner dropdown1;
    TextView employeeTV; //will be replaced with spinner in manager screen

    private RecyclerView recyclerView;
    private int employeeIndex;
    private int yearIndex;

    private static final String TAG = "EmployeeAbsences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_absences,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);



        globalFindViewById();
        if (isManager){
            processEmployeeSpinner();
        }else{
            loadData();
        }
        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==1){
                    loadingDialogBox.dismiss();
                    setData();
                }else if (msg.arg1==-1){//empty
                    findViewById(R.id.container).setVisibility(View.GONE);
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeAbsences.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.no_data_available))
                            .setBody(getString(R.string.no_data_available_so_far))
                            .setPositive(getString(R.string.to_home_page))
                            .setOnPositive(() -> {
                                        startActivity(new Intent(EmployeeAbsences.this, EmployeeDashboard.class));
                                        finish();
                                    }
                            )
                            .create().show();

                }else {
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeAbsences.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeAbsences.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
    Spinner employeeSpinner;
    private void processEmployeeSpinner() {
        employeeSpinner=findViewById(R.id.employeeSpinner);
        employeeSpinner.setVisibility(View.VISIBLE);
        employeeTV.setVisibility(View.INVISIBLE);
        EmployeesList employeesList=EmployeesList.getInstance();
        if (employeesList == null) {
            EmployeesList.newInstance(this, InternalStorage.readFullInternalStorage(this,EMPLOYEES_LIST) );
            employeesList=EmployeesList.getInstance();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.drop_down_list_element,
                locale.equals("ar")?employeesList.getEmployeesArNamesList():employeesList.getEmployeesNamesList());
        //set the spinners adapter to the previously created one.
        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadData();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            absencesPerEmployee=new AbsencesPerEmployee();
            String resulat="";
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeAbsences.this);

            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            try {
                if(isManager){
                    resulat=backgroundWorker.execute("managerHistoryAbsent", email, password,EmployeesList.getInstance().getEmployeesList().get(employeeSpinner.getSelectedItemPosition()).getId() ).get();
                }else{
                    resulat=backgroundWorker.execute("historyAbsent", email, password ).get();
                }
                JSONObject jsonObject=new JSONObject(resulat);
                String name=jsonObject.getString("name");
                String nameAr=jsonObject.getString("nameAr");

                if (!(jsonObject.get("year") instanceof JSONObject)){
                    Message msg=Message.obtain();
                    msg.arg1=-1;
                    handler.sendMessage(msg);
                    return;
                }
                JSONObject yearsObject=jsonObject.getJSONObject("year");

                ArrayList<AbsencesEntriesPerYear> absencesEntriesPerYearsList =new ArrayList<>();
                for (int i = 0; i < yearsObject.length(); i++) {
                    String year=yearsObject.keys().next();
                    Log.d(TAG, "setData: here: "+year);

                    JSONArray datesArray=yearsObject.getJSONArray(year);
                    ArrayList<AbsencesEntry> singleRowEntriesList=new ArrayList<>();
                    for (int j = 0; j < datesArray.length(); j++) {
                        String date = datesArray.getString(j);
                        singleRowEntriesList.add(
                                new AbsencesEntry(
                                        String.valueOf(j+1),
                                        date,
                                        EmployeeAbsences.this
                                )
                        );
                    }
                    absencesEntriesPerYearsList.add(new AbsencesEntriesPerYear(
                            Integer.parseInt(year),
                            singleRowEntriesList
                    ));
                }
                absencesPerEmployee=new AbsencesPerEmployee(
                        name,
                        nameAr,
                        absencesEntriesPerYearsList
                );
                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);

            } catch (JSONException | InterruptedException | ExecutionException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
            }
        }
    }
    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }
    }
    private void processYearsRV() {
        ArrayList<Integer> yearsList=new ArrayList<>();
        for (int i = 0; i < absencesPerEmployee.getYearsList().size(); i++) {
            yearsList.add(absencesPerEmployee.getYearsList().get(i).getYear());
        }
        recyclerViewAdapterYearsFilter = new RecyclerViewAdapterYearsFilter(yearsList);
        LinearLayoutManager linearLayoutManager2=new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(RecyclerView.VERTICAL);
        yearsRV.setLayoutManager(linearLayoutManager2);
        yearsRV.setAdapter(recyclerViewAdapterYearsFilter);

    }

    private void processAbsencesRV(ArrayList<AbsencesEntry> entriesList) {
        RecyclerViewAdapterAbsences recyclerViewAdapterAbsences = new RecyclerViewAdapterAbsences(entriesList);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapterAbsences);

    }

    private AbsencesEntriesPerYear getSingleRowYear(Integer year) {
        for (int i = 0; i < absencesPerEmployee.getYearsList().size(); i++) {
            if (absencesPerEmployee.getYearsList().get(i).getYear().equals(year)){
                return absencesPerEmployee.getYearsList().get(i);
            }
        }
        return null;
    }




    private void setData()  {


        employeeTV.setText(absencesPerEmployee.getName());

        Integer maxYear=absencesPerEmployee.getYearsList().get(absencesPerEmployee.getYearsList().size()-1).getYear();
        yearTV.setText(String.valueOf(maxYear));
        try{
            processAbsencesRV(getSingleRowYear(maxYear).getEntriesList());
        }catch (Exception e){
            e.printStackTrace();
        }
        processYearsRV();

        //initialize years button chooser, this is delayed because we want to make sure the adapter is initialized


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerViewAdapterYearsFilter.selectLastElement();
            }
        }, 1000);



        yearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstraintLayout constraintLayout=findViewById(R.id.container);
                snapShotIV.setImageBitmap( Utilitaire.getBitmapFromView((View) constraintLayout));

                blurringView.setBlurredView(snapShotIV);
                blurringView.invalidate();
                fade(300, 0.0f, 1.0f,
                        snapShotIV,
                        darkIV,
                        blurringView,
                        yearsRV,
                        closeIV,
                        yearsEntryTV

                );//fadeIn

            }
        });
        blurringView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer selectedYear=absencesPerEmployee.getYearsList().get(recyclerViewAdapterYearsFilter.getSelectedItemPosition()).getYear();
                processAbsencesRV(getSingleRowYear(selectedYear).getEntriesList());
                yearTV.setText(String.valueOf(selectedYear));
                fade(300, 1.0f, 0.0f,
                        snapShotIV,
                        darkIV,
                        blurringView,
                        yearsRV,
                        closeIV,
                        yearsEntryTV

                );//fadeOut
                Log.d(TAG, "onClick: blurringView");
            }
        });

    }



    private void globalFindViewById() {

        recyclerView=findViewById(R.id.absences_rv);
        employeeTV=findViewById(R.id.employeeTV);

        yearsRV=findViewById(R.id.yearsRV);
        blurringView = findViewById(R.id.blurringView);
        snapShotIV = findViewById(R.id.snapshot);
        darkIV = findViewById(R.id.darkIV);
        yearTV=findViewById(R.id.year);
        closeIV=findViewById(R.id.closeIV);
        yearsEntryTV=findViewById(R.id.yearsEntryTV);

        //get the spinner from the xml.
//        dropdown1 = findViewById(R.id.drop_down_list1);

    }

    public void closeButtonClick(View view) {
        Integer selectedYear=absencesPerEmployee.getYearsList().get(recyclerViewAdapterYearsFilter.getSelectedItemPosition()).getYear();
        processAbsencesRV(getSingleRowYear(selectedYear).getEntriesList());
        yearTV.setText(String.valueOf(selectedYear));
        fade(300, 1.0f, 0.0f,
                snapShotIV,
                darkIV,
                blurringView,
                yearsRV,
                closeIV,
                yearsEntryTV

        );//fadeOut

        Log.d(TAG, "closeButtonClick");
    }

}
