package com.ma.kdipa;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.ma.kdipa.JavaResources.APIManager;
import com.ma.kdipa.JavaResources.Constants;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.Utils;
import com.ma.kdipa.expire_logique.ABFSApplication;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.LOCALE;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

import java.util.Calendar;
import java.util.Date;

@SuppressLint("Registered")
public class InvestorNavigationDrawer extends AppCompatActivity  {
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    String locale = "";

    private static final String TAG = "EmployeeNavigationDrawe";

    int relevantSide;


    /*for drawerLayout*/
    private DrawerLayout dl;
    private static int DELAY = 300;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView nv;
    DialogBoxLoadingClass loadingDialogBox;


    public APIManager getAPIManager() {
        return APIManager.retrofitInvestor.create(APIManager.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(LOCALE)) {
            locale = sharedPreferences.getString(LOCALE, "");
        } else {
            locale = "en";
        }
        Utilitaire.refreshLocal(this);
        relevantSide =Gravity.START;


        setContentView(R.layout.i_navigation_drawer_layout);


        //navigation drawer processing
//        dl=(DrawerLayout) LayoutInflater.from(getApplicationContext()).inflate(R.layout.common_login, null, false).findViewById(R.id.activity_main);
        dl = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);

        dl.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        findViewById(R.id.complaintsAndSuggestions).setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://support.kdipa.gov.kw/?lang=en_US"));
            startActivity(browserIntent);
        });

    }

    public void showLoading() {
        loadingDialogBox = new DialogBoxLoadingClass(this);
        loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialogBox.show();
        loadingDialogBox.setCancelable(false);
        loadingDialogBox.setCanceledOnTouchOutside(false);
    }

    public void hideLoading() {
        loadingDialogBox.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    public void triggerNavDrawer(View view) {
        //hide keyboard:
        if (this.getCurrentFocus() != null && this.getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        dl.openDrawer(relevantSide);
    }

    public void homeClick(View view) {
        ImageView iv = dl.findViewById(R.id.home_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(InvestorDashboard.class);
                }
            }, DELAY);
        }
    }

    public void bookingClick(View view) {
        ImageView iv = dl.findViewById(R.id.booking_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(InvestorBooking.class);
                }
            }, DELAY);
        }


    }
    public ABFSApplication getApp() {
        return (ABFSApplication) this.getApplication();
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        getApp().touch();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (getApp().wasInBackground()) {
            getApp().setInBackGround(false);
            String lastTimeConnected = Utils.getValue(this, Constants.LAST_TIME_CONNECTED, null);
            if (lastTimeConnected == null) return;
            Date date = Utilitaire.stringToDate(lastTimeConnected, Constants.DATE_INPUT_FORMAT);
            Date currentTime = Calendar.getInstance().getTime();
            long diff = currentTime.getTime() - date.getTime();
            if (diff >= Constants.EXPIRY_TIME) {
                logoutClick(null);
            }
        }
    }

    public void howClick(View view) {
        ImageView iv = dl.findViewById(R.id.how_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(InvestorHowItWorks.class);
                }
            }, DELAY);
        }

    }

    public void logoutClick(View view) {
        ImageView iv = dl.findViewById(R.id.logout_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sharedPreferences
                            .edit()
                            .remove(Constants.USERNAME)
                            .remove(TOKEN)
                            .commit();

                    SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(Constants.LAST_LOGIN_USER_INFO, MODE_PRIVATE);
                    sharedPreferences.edit()
                            .putString(EMAIL, email)
                            .putString(PASSWORD, password)
                            .apply();
                lunchActivity(CommonLogin.class);
                }
            }, DELAY);
        }

    }

    public void settingsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(InvestorSettings.class);
            }
        }, DELAY);


    }

    void lunchActivity(Class<?> someClass) {
        Intent intent = new Intent(getApplicationContext(), someClass);

        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create((View) findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(InvestorNavigationDrawer.this, p1,p3);
//        startActivity(intent,options.toBundle());
        startActivity(intent);
    }

}
