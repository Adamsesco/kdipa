package com.ma.kdipa;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.PermissionUtilsExternalStorageRead;
import com.ma.kdipa.JavaResources.PermissionUtilsExternalStorageWrite;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterRequestStatus;
import com.ma.kdipa.JavaResources.SingleRowRequestStatus;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class EmployeeRequestStatus extends EmployeeNavigationDrawer {
    private ProgressDialog pDialog;


    protected DrawerLayout mDrawer;
    RecyclerViewAdapterRequestStatus recyclerViewAdapter;



    private LinearLayoutManager linearLayoutManager;
    private ArrayList<SingleRowRequestStatus> list;


    private static Handler handler;
    private static Handler deleteHandler;
    DialogBoxLoadingClass loadingDialogBox;
    private static final String TAG = "EmployeeRequestStatus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_request_status,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        mDrawer.findViewById(R.id.request).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);

//        ImageView v =mDrawer.findViewById(R.id.logout_iv);
//        v.setImageResource(R.drawable.home_en);
//        v.setTag(R.drawable.logout);

        if (ContextCompat.checkSelfPermission(EmployeeRequestStatus.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtilsExternalStorageWrite.requestPermission(EmployeeRequestStatus.this, 2,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
        }

        if (ContextCompat.checkSelfPermission(EmployeeRequestStatus.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtilsExternalStorageRead.requestPermission(EmployeeRequestStatus.this, 3,
                    Manifest.permission.READ_EXTERNAL_STORAGE, false);
        }

        loadData();

        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==1){
                    loadingDialogBox.dismiss();
                    setData();
                }else if(msg.arg1==0){
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeRequestStatus.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeRequestStatus.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };
        deleteHandler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
            if (msg.arg1!=-1){
                loadingDialogBox.dismiss();
                new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeRequestStatus.this)
                        .setMode(DialogBoxCustom2.SUCCESS)
                        .setTitle(getString(R.string.success))
                        .setBody(getString(R.string.Successfully_canceled))
                        .setPositive(getString(R.string.continuee))
                        .setOnPositive(() -> {
                        })
                        .create().show();
//                Toast.makeText(EmployeeRequestStatus.this, getString(R.string.Successfully_canceled), Toast.LENGTH_SHORT).show();
                //recyclerViewAdapter.removeAt(msg.arg1);
            }else {
                loadingDialogBox.dismiss();
                new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeRequestStatus.this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.an_error_occured))
                        .setPositive(getString(R.string.cancel))
                        .create().show();
//                DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeRequestStatus.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                cdd.show();
            }
            }
        };
    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);

            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }
        
    }

    private class LoadingThread extends Thread{
        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeRequestStatus.this);
            String result;

            list=new ArrayList<>();
            try {
                result=backgroundWorker.execute("newVacationCancel", email, password ).get();
//                result=getString(R.string.request_json);
                JSONObject jsonObject=new JSONObject(result);
                JSONObject yearsObject=jsonObject.getJSONObject("year");
                Iterator<String> iter = yearsObject.keys();
                while (iter.hasNext()) {
                    String year=iter.next();
                    JSONArray dataArray=yearsObject.getJSONArray(year);
                    for (int j = 0; j < dataArray.length(); j++) {
                        JSONObject data = dataArray.getJSONObject(j);
                        list.add(new SingleRowRequestStatus(
                                EmployeeRequestStatus.this.getResources().getConfiguration().locale.toString(),
                                data.getString("DateFrom"),
                                data.getString("VacationId"),
                                data.getString("Name-En"),
                                data.getString("Name-Ar"),
                                ""
                        ));
                    }
                }

                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
            }
        }
    }
    private class DeletingThread extends Thread{
        int position;
        DeletingThread(int position) {
            this.position=position;
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeRequestStatus.this);
            String result;
            try {
                SimpleDateFormat formatForOutput=new SimpleDateFormat(Constants.YEAR_MONTH_DAY, new Locale("en"));

                result=backgroundWorker.execute("postVacationCancel", email, password, list.get(position).getId(),formatForOutput.format(Calendar.getInstance().getTime()) ).get();
//                result=getString(R.string.request_json);
                JSONObject jsonObject=new JSONObject(result);
                if (jsonObject.has("Result") && jsonObject.getString("Result").equals("true")){
                    Message msg=Message.obtain();
                    msg.arg1=position;
                    deleteHandler.sendMessage(msg);
                }else{
                    Message msg=Message.obtain();
                    msg.arg1=-1;
                    deleteHandler.sendMessage(msg);
                }
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
                Message msg=Message.obtain();
                msg.arg1=-1;
                deleteHandler.sendMessage(msg);
            }
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
        deleteHandler.removeCallbacksAndMessages(null);
    }

    private void setData() {
        RecyclerView recyclerView = findViewById(R.id.requestStatusRV);
        recyclerViewAdapter = new RecyclerViewAdapterRequestStatus(list, this);
        linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);


        recyclerViewAdapter.setOnClickListener(position -> {
            if (Utilitaire.isNetworkAvailable(this)){
                DeletingThread deletingThread=new DeletingThread(position);
                deletingThread.start();
            }else{
                new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
            }
        });
    }

}





