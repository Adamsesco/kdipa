package com.ma.kdipa;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.FilePicker.Image.ImagePicker;
import com.ma.kdipa.JavaResources.ActionBottomDialogFragment;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.BackgroundWorkerEmergencyLeave;
import com.ma.kdipa.JavaResources.BackgroundWorkerSickLeave;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DelegatedList;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.FileUtils;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.MySpinnerAdapter;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.ShotNewVacation;
import com.ma.kdipa.JavaResources.Spinner;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.suke.widget.SwitchButton;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DELEGATED_LIST;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.VACATION_AR_TYPES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_IDS;
import static com.ma.kdipa.JavaResources.Constants.VACATION_PREFERENCES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_TYPES;
import static com.ma.kdipa.JavaResources.Constants.YEAR_MONTH_DAY_with_dash;
import static com.ma.kdipa.JavaResources.Utilitaire.fade;
import static com.ma.kdipa.JavaResources.Utilitaire.isInteger;

public class EmployeeVacationNew extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {


    private static final int PICKFILE_RESULT_CODE = 1;
    private static final String TAG = "EmployeeVacationReturn";
    protected DrawerLayout mDrawer;
    ArrayList<String> vacationIDSList;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    Uri cameraPhotoURI;
    String attachmentPath = "";
    ImageView attachmentIV;
    TextView attachmentNameTV;
    Spinner typesSpinner;
    Spinner delegatedEmployeeSpinner;

    TextView departureDateTV;
    TextView returnDateTV;

    TextView submitTV;

    ProgressDialog progressDialog;
    Handler handler;


    String departureDateTxt = "";
    String returnDateTxt = "";

    String typeTxt = "";
    String delegatedEmployeeTxt = "";


    DateType dateType;
    ShotNewVacation shotNewVacation;
    SubmitObject submitObject = new SubmitObject();
    private ImageView deleteIV;
    private boolean isEmergency = false;
    private boolean isSick = false;
    private SwitchButton switcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.e_new_vacation,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);


        globalFindViewById();
        setDefaultValues();

        loadData();


        processTheCalendar();

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 200) {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    InternalStorage.write(EmployeeVacationNew.this,
                            VACATION_PREFERENCES,
                            departureDateTxt + "\n" + returnDateTxt,
                            false, false
                    );
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationNew.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.new_vacation_success))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityEmployeeDashboard();
                            })
                            .create().show();
                } else {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationNew.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationNew.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void startActivityEmployeeDashboard() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }

    private void loadData() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
        }


        vacationIDSList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationNew.this, VACATION_IDS).split("\n")));
        ArrayList<String> vacationTypeList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationNew.this, VACATION_TYPES).split("\n")));
        ArrayList<String> vacationArList = new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationNew.this, VACATION_AR_TYPES).split("\n")));

        DelegatedList delegatedList = DelegatedList.getInstance();
        DelegatedList.newInstance(this, InternalStorage.readFullInternalStorage(this, DELEGATED_LIST));
        delegatedList = DelegatedList.getInstance();

        String[] txt = InternalStorage.readFullInternalStorage(this, VACATION_PREFERENCES).split("\n");
        txt = (txt.length <= 1) ? new String[]{"", ""} : txt;
        Log.d(TAG, "loadData: txt: " + Arrays.toString(txt));
        shotNewVacation = new ShotNewVacation(
                this,
                vacationTypeList,
                vacationArList,
                txt[0],//jsonObject.getString("departureDate"),
                txt[1],//jsonObject.getString("returnDate"),
                delegatedList.getEmployeesNamesList(),
                delegatedList.getEmployeesArNamesList()
        );
        departureDateTxt = txt[0];
        returnDateTxt = txt[1];


        departureDateTV.setText(shotNewVacation.getDepartureDate());
        returnDateTV.setText(shotNewVacation.getReturnDate());
        typeTxt = shotNewVacation.getTypesList().get(0);

        if (shotNewVacation.getDelegatedEmployeesList().size() != 0) {
            delegatedEmployeeTxt = shotNewVacation.getDelegatedEmployeesList().get(0);
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(this,
                locale.equals("ar") ? shotNewVacation.getTypesArList() : shotNewVacation.getTypesList()
        );
        //set the spinners adapter to the previously created one.
        typesSpinner.setAdapter(adapter);
        typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeTxt = shotNewVacation.getTypesList().get(i);
                typeTxt = shotNewVacation.getTypesList().get(i);
                Log.d(TAG, "onItemSelected: typesSpinner.getSelectedItemPosition(): " + typesSpinner.getSelectedItemPosition());
                if (typeTxt.equals("طارئه") || typeTxt.equals("مــرضـــيــة")) {
                    if (typeTxt.equals("طارئه")) {
                        isEmergency = true;
                    }
                    if (typeTxt.equals(Constants.SICK)) {
                        isSick = true;
                    }
                    fade(200, 0.0f, 1.0f,
                            findViewById(R.id.attachementTV),
                            findViewById(R.id.iconAttachment)
                    );
                    fade(200, 1.0f, 0.0f,
                            findViewById(R.id.in_icon),
                            findViewById(R.id.returnDateEntry),
                            findViewById(R.id.returnDateView),
                            findViewById(R.id.calendar_icon2),
                            findViewById(R.id.returnDateTV),
                            findViewById(R.id.delete_icon2)
                    );
                } else {
                    isSick = false;
                    isEmergency = false;
                    fade(200, 1.0f, 0.0f,
                            findViewById(R.id.attachementTV),
                            findViewById(R.id.iconAttachment)
                    );
                    fade(200, 0.0f, 1.0f,
                            findViewById(R.id.in_icon),
                            findViewById(R.id.returnDateEntry),
                            findViewById(R.id.returnDateView),
                            findViewById(R.id.calendar_icon2),
                            findViewById(R.id.returnDateTV),
                            findViewById(R.id.delete_icon2)
                    );
                    deleteIV.setVisibility(View.INVISIBLE);
                    attachmentIV.setVisibility(View.INVISIBLE);
                    attachmentNameTV.setVisibility(View.INVISIBLE);
                    attachmentPath = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if(getIntent().getBooleanExtra(Constants.IS_SICK_LEAVE_INTENT, false)){
            //+1 is because of custom spinner
            typesSpinner.setSelection(shotNewVacation.getTypesArList().indexOf(Constants.SICK) +1 );
        }
        MySpinnerAdapter adapter2 = new MySpinnerAdapter(this,
                locale.equals("ar") ? shotNewVacation.getDelegatedEmployeesArList() : shotNewVacation.getDelegatedEmployeesList()
        );

        //set the spinners adapter to the previously created one.
        delegatedEmployeeSpinner.setAdapter(adapter2);
        delegatedEmployeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                delegatedEmployeeTxt = shotNewVacation.getDelegatedEmployeesList().get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }

    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);

    }

    private void globalFindViewById() {
        switcher = findViewById(R.id.advancedPayment);

        typesSpinner = findViewById(R.id.vacationType);
        delegatedEmployeeSpinner = findViewById(R.id.delegatedEmployeeSpinner);

        departureDateTV = findViewById(R.id.departureDateTV);
        returnDateTV = findViewById(R.id.returnDateTV);


        submitTV = findViewById(R.id.submit);

        attachmentIV = findViewById(R.id.attachmentIV);
        attachmentNameTV = findViewById(R.id.attachmentNameTV);
        deleteIV = findViewById(R.id.deleteIV);


    }

    public void deleteClick(View view) {
        deleteIV.setVisibility(View.INVISIBLE);
        attachmentIV.setVisibility(View.INVISIBLE);
        attachmentPath = "";
    }

    public void showCalendarForReturnDate(View view) {
        dateType = DateType.RETURN_DATE;
        showCalendar();
    }

    public void showCalendarForDepartureDate(View view) {
        dateType = DateType.DEPARTURE_DATE;
        showCalendar();
    }

    public void deleteDepartureDate(View view) {
        departureDateTV.setText("");
        departureDateTxt = "";
    }

    public void deleteReturnDate(View view) {
        returnDateTV.setText("");
        returnDateTxt = "";
    }

    public void showCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(-constraintLayout.getWidth(), 0, 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.INVISIBLE || constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                constraintLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void hideCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, -constraintLayout.getWidth(), 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.VISIBLE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                constraintLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void submitClick(View view) {
        if (validate()) {
            submitTV.setEnabled(false);
            progressDialog = new ProgressDialog(this, R.style.customDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);


            SubmitThread submitThread = new SubmitThread();
            submitThread.start();
        }
    }

    boolean validate() {
        if (typesSpinner.getSelectedItemPosition() == -1) {
            Toast.makeText(this, R.string.vacation_type_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (departureDateTxt.equals("")) {
            Toast.makeText(this, R.string.empty_startFromTxt, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (returnDateTxt.equals("") && !isEmergency && !isSick) {
            Toast.makeText(this, R.string.empty_toTxt, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(isSick && attachmentPath.equals("")){
            Toast.makeText(this, R.string.attachment_mandatory, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Utilitaire.isSecondDateHigher(departureDateTxt, returnDateTxt, YEAR_MONTH_DAY_with_dash, false) && !isEmergency && !isSick) {
            Toast.makeText(this, R.string.current_higher_that_from, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void processTheCalendar() {
        RobotoCalendarView robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }

    @Override
    public void onDayClick(Date date) {
        String locale = getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat formatForDisplay = new SimpleDateFormat(
                locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String displayed = formatForDisplay.format(date);
        displayed = (locale.equals("ar")) ? Utilitaire.replaceArabicNumbers(displayed) : displayed;

        SimpleDateFormat formatForOutput = new SimpleDateFormat(YEAR_MONTH_DAY_with_dash, new Locale("en"));
        hideCalendar();
        switch (dateType) {
            case DEPARTURE_DATE:
                departureDateTV.setText(displayed);
                departureDateTxt = formatForOutput.format(date);
                break;
            case RETURN_DATE:
                returnDateTV.setText(displayed);
                returnDateTxt = formatForOutput.format(date);
                break;
        }
    }

    @Override
    public void onDayLongClick(Date date) {
    }

    @Override
    public void onRightButtonClick() {
    }

    @Override
    public void onLeftButtonClick() {
    }

    @Override
    public void onBackPressed() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        if (constraintLayout.getVisibility() == View.VISIBLE) {
            hideCalendar();
            Log.d(TAG, "backClick: backClick hide calendar");
        } else {
            super.onBackPressed();
            Log.d(TAG, "backClick: backClick super");
        }
    }

    private void submit() {
        if (isEmergency) {
            emergencySubmit();
        } else if (isSick) {
            sickSubmit();
        } else {
            normalSubmit();
        }
    }

    private void emergencySubmit() {
        BackgroundWorkerEmergencyLeave backgroundWorkerEmergencyLeave = new BackgroundWorkerEmergencyLeave(EmployeeVacationNew.this);
        try {
            String responseCode = backgroundWorkerEmergencyLeave.execute(
                    email,
                    departureDateTxt,
                    attachmentPath,
                    password
            ).get();
            Message msg = Message.obtain();
            if (isInteger(responseCode)) {
                msg.arg1 = Integer.parseInt(responseCode);
            } else {
                msg.arg1 = 7;
            }
            handler.sendMessage(msg);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            handler.sendMessage(msg);
        }
    }
    private void sickSubmit() {
        BackgroundWorkerSickLeave backgroundWorkerSickLeave = new BackgroundWorkerSickLeave(EmployeeVacationNew.this);
        try {
            String responseCode = backgroundWorkerSickLeave.execute(
                    email,
                    departureDateTxt,
                    attachmentPath,
                    password
            ).get();
            Message msg = Message.obtain();
            if (isInteger(responseCode)) {
                msg.arg1 = Integer.parseInt(responseCode);
            } else {
                msg.arg1 = 7;
            }
            handler.sendMessage(msg);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            handler.sendMessage(msg);
        }
    }


    private void normalSubmit() {
        try {
            submitObject.setEmail(email);
            submitObject.setPassword(password);
            submitObject.setFromDate(departureDateTxt);
            submitObject.setToDate(returnDateTxt);
            submitObject.setVacationType(vacationIDSList.get(typesSpinner.getSelectedItemPosition()));

            submitObject.setAdvancedPayment(switcher.isChecked() ? "1" : "0");
            submitObject.setDelegatedEmployee(delegatedEmployeeTxt);


            Log.d(TAG, "submit: " + submitObject.toString());


            String responseCode = new BackgroundWorker(this).execute(
                    "postNewVacation",
                    submitObject.getEmail(),
                    submitObject.getPassword(),
                    submitObject.getVacationType(),
                    submitObject.getFromDate(),
                    submitObject.getToDate(),
                    submitObject.getAdvancedPayment(),
                    submitObject.getDelegatedEmployee()
            ).get();
            Message msg = Message.obtain();
            if (isInteger(responseCode)) {
                msg.arg1 = Integer.parseInt(responseCode);
            } else {
                msg.arg1 = 7;
            }
            handler.sendMessage(msg);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            handler.sendMessage(msg);
        }
    }

    public void attachmentClick(View view) {
        openFolder();
    }
    private void openFolder() {
        if (!areAllPermissionsGranted()) return;

        Intent intent = new Intent();
        intent.setType("*/*");
//        intent.setType("file/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    }


    Uri URI = null;
    private static final int PICK_FROM_GALLERY = 3215;

    private void setGalleryIntent(List<Intent> galleryIntents) {
        final Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listGalleries = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGalleries) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            galleryIntents.add(intent);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        attachmentPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE) {
            String path = "";
            Uri uri = null;

            if (resultCode == RESULT_OK) {
                if (data == null) {
                    showAttachment();
                    return;
                } else {
                    uri = data.getData();
                }
                //try to get the filePath as if it is in external non removable storage
                try {

                    path = FileUtils.getPath(EmployeeVacationNew.this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                File f = null;
                if (path != null) {
                    f = new File(path);
                    if (f.exists()) {
                        if (f.isFile()) {
                            //good, we want it to be a file
                            showAttachment();
                        }
                    } else { //file path doesn't exist
                        //this case is processed only when the file is in sd card
                        String radicalRemovableStoragePath = Utilitaire.getExternalStoragePath(this, true);
                        path = radicalRemovableStoragePath + "/" + DocumentsContract.getDocumentId(uri).split(":")[1];
                        f = new File(path);
                        if (f.exists()) {
                            if (f.isFile()) {
                                //good, we want it to be a file
                                showAttachment();
                            }
                        } else {
                            Toast.makeText(this, R.string.cannot_be_loaded, Toast.LENGTH_SHORT).show();
                        }
                    }
                    attachmentPath = path;
                }
            }

        } else if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            for (String mPath : mPaths) {
                Bitmap bitmap = null;
                try {
                    Uri u = Uri.fromFile(new File(mPath));
//                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), u);
                    attachmentPath = (mPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            showAttachment();
        }else if (requestCode == PICK_FROM_GALLERY && resultCode == RESULT_OK) {
            URI = data.getData();
            try {
                attachmentPath = FileUtils.getPath(EmployeeVacationNew.this, URI);
                showAttachment();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void showAttachment() {
        if (attachmentPath.toLowerCase().indexOf(".pdf") > 0) {
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageBitmap(Utilitaire.generateImageFromPdf(this, Uri.fromFile(new File(attachmentPath))));
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else if (
                attachmentPath.toLowerCase().indexOf(".jpg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".jpeg") > 0 ||
                        attachmentPath.toLowerCase().indexOf(".png") > 0
        ) {
            String thumbnailPath = Utilitaire.generateThumbnail(this, new File(attachmentPath)).getPath();
            //this line is needed to clear imageuri (if exists aready),
            // the main thing is that this instruction will be usful when user
            // chose image then choose an other image:
            attachmentIV.setImageResource(R.drawable.demo);
            attachmentIV.setImageURI(Uri.fromFile(new File(thumbnailPath)));
            Log.d(TAG, "showAttachment: thumbnailPath: " + thumbnailPath);
            deleteIV.setVisibility(View.VISIBLE);
            attachmentIV.setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG, "showAttachment: attachmentPath: " + attachmentPath);
            Toast.makeText(this, getString(R.string.file_type_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    public void showBottomSheet() {
        ActionBottomDialogFragment addPhotoBottomDialogFragment =
                ActionBottomDialogFragment.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                ActionBottomDialogFragment.TAG);
    }


    enum DateType {
        DEPARTURE_DATE,
        RETURN_DATE
    }


//    private void printImage(Image image) {
//        if (image == null) return;
//        StringBuilder stringBuffer = new StringBuilder();
//        stringBuffer.append(image.getPath()).append("\n");
//
//
//        Cursor cursor = MediaStore.Images.Thumbnails.queryMiniThumbnails(
//                getContentResolver(), Uri.fromFile(new File(image.getPath())),
//                MediaStore.Images.Thumbnails.MINI_KIND,
//                null );
//        if( cursor != null && cursor.getCount() > 0 ) {
//            cursor.moveToFirst();//**EDIT**
//            String uri = cursor.getString( cursor.getColumnIndex( MediaStore.Images.Thumbnails.DATA ) );
//        }
////        ((ImageView) findViewById(R.id.imageContainer)).setImageURI(Uri.fromFile(new File(image.getPath())));
//
//        attachmentPath=image.getPath();
//        File file=new File(attachmentPath);
//        Log.d(TAG, "printImage: file length: "+file.length());
//        showAttachment();
//
////        ((TextView)findViewById(R.id.file)).setText(stringBuffer.toString());
////        textView.setOnClickListener(v -> ImageViewerActivity.start(MainActivity.this, images));
//    }

    private class SubmitThread extends Thread {
        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }
    }

    class SubmitObject {
        private String email;
        private String password;
        private String vacationType;
        private String fromDate;
        private String toDate;
        private String advancedPayment;
        private String delegatedEmployee;

        public SubmitObject() {
        }

        public String getVacationType() {
            return vacationType;
        }

        public void setVacationType(String vacationType) {
            this.vacationType = vacationType;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getAdvancedPayment() {
            return advancedPayment;
        }

        public void setAdvancedPayment(String advancedPayment) {
            this.advancedPayment = advancedPayment;
        }

        public String getDelegatedEmployee() {
            return delegatedEmployee;
        }

        public void setDelegatedEmployee(String delegatedEmployee) {
            this.delegatedEmployee = delegatedEmployee;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "SubmitObject{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", vacationType='" + vacationType + '\'' +
                    ", fromDate='" + fromDate + '\'' +
                    ", toDate='" + toDate + '\'' +
                    ", advancedPayment='" + advancedPayment + '\'' +
                    ", delegatedEmployee='" + delegatedEmployee + '\'' +
                    '}';
        }
    }

}
