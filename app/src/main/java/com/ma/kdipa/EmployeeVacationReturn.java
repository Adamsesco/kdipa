
package com.ma.kdipa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.VacationCustomSpinnerAdapter;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.ShotVacationReturnExtension;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.VacationsEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.VACATION_AR_TYPES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_PREFERENCES;
import static com.ma.kdipa.JavaResources.Constants.VACATION_TYPES;
import static com.ma.kdipa.JavaResources.Utilitaire.isInteger;


public class EmployeeVacationReturn extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {


    SharedPreferences sharedPreferences;
    String name="";
    String email="";
    String password="";

    SubmitObject submitObject=new SubmitObject();

    DialogBoxLoadingClass loadingDialogBox;



    protected DrawerLayout mDrawer;
    Spinner historySpinner;

    TextView returnDateTV;

    TextView submitTV;


    String toDateTxt="";
    String returnDateTxt="";

    ProgressDialog progressDialog;
    Handler postHandler;
    Handler getHandler;



    DateType dateType;
    private static final String TAG = "EmployeeVacationReturn";
    ShotVacationReturnExtension shotVacationReturn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_vacation_return,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        mDrawer.findViewById(R.id.vacation_return).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);



        globalFindViewById();
        setDefaultValues();
        loadData();
        processTheCalendar();
        postHandler =new Handler(){
            @SuppressLint("HandlerLeak")
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==200){
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationReturn.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(getString(R.string.submitted_successfully))
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> {
                                startActivityEmployeeDashboard();
                            })
                            .create().show();
//                    Toast.makeText(EmployeeVacationReturn.this, R.string.submitted_successfully, Toast.LENGTH_SHORT).show();

                }else {
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationReturn.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationReturn.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };
        getHandler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==200){
                    loadingDialogBox.dismiss();
                    setData();
                }else {
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationReturn.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(EmployeeVacationReturn.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }

            }
        };


    }

    private void setData() {


        //set the spinners adapter to the previously created one

        Log.d(TAG, "setData: shotVacationReturn.getVacationsEntries(): "+shotVacationReturn.getVacationsEntries());
        VacationCustomSpinnerAdapter customAdapter=new VacationCustomSpinnerAdapter(this,shotVacationReturn.getVacationsEntries());
        historySpinner.setAdapter(customAdapter);
        historySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                submitObject.setVacationId(shotVacationReturn.getVacationsEntries().get(i).getVacationId());
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        returnDateTV.setText(shotVacationReturn.getReturnDate());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        postHandler.removeCallbacksAndMessages(null);
        getHandler.removeCallbacksAndMessages(null);
    }
    public void submitClick(View view) {

        if (validate()){
            submitObject.setEmail(email);
            submitObject.setPassword(password);

            submitObject.setToDate(toDateTxt);
            submitObject.setReturnDate(returnDateTxt);
            submitObject.setLateReturnReason(findViewById(R.id.reasonET).getVisibility()==View.VISIBLE?((EditText) findViewById(R.id.reasonET)).getText().toString():"");
            submitTV.setEnabled(false);

            progressDialog= new ProgressDialog(this, R.style.customDialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.loading));


            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);



            SubmitThread submitThread=new SubmitThread();
            submitThread.start();
        }else{
            Toast.makeText(this, R.string.request_checking_fields, Toast.LENGTH_SHORT).show();
        }
    }


    private void startActivityEmployeeDashboard() {
        lunchActivity(EmployeeDashboard.class);
    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }

    }

    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                submitObject.setEmail(email);
                password = sharedPreferences.getString(PASSWORD, "");
                submitObject.setPassword(password);
                name = sharedPreferences.getString(NAME, "");
            }

            ArrayList<String> vacationTypeList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationReturn.this, VACATION_TYPES).split("\n")));
            ArrayList<String> vacationArList=new ArrayList<>(Arrays.asList(InternalStorage.readFullInternalStorage(EmployeeVacationReturn.this, VACATION_AR_TYPES).split("\n")));

            String[] txt= InternalStorage.readFullInternalStorage(EmployeeVacationReturn.this, VACATION_PREFERENCES).split("\n");
            txt=(txt.length<=1)?new String[]{"",""}:txt;

/////////////////////////
            String resulat="";
            ArrayList<VacationsEntry> vacationsEntries=new ArrayList<>();

            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeVacationReturn.this);

            try {
                if(isManager){
                    resulat=backgroundWorker.execute("managerHistoryVacation", email, password, managerId ).get();
                }else{
                    resulat=backgroundWorker.execute("historyVacation", email, password ).get();
                }

                JSONObject jsonObject=new JSONObject(resulat);
                if (!(jsonObject.get("year") instanceof JSONObject)){
                    Message msg=Message.obtain();
                    msg.arg1=-1;
                    getHandler.sendMessage(msg);
                    return;
                }
                JSONObject yearsObject=jsonObject.getJSONObject("year");
                Iterator<String> iter = yearsObject.keys();
                while (iter.hasNext()){
                    String year=iter.next();
                    Log.d(TAG, "setData: here: "+year);
                    JSONArray vacationEntriesArray=yearsObject.getJSONArray(year);
                    for (int j = 0; j < vacationEntriesArray.length(); j++) {

                        JSONObject vacationEntry=vacationEntriesArray.getJSONObject(j);
                        Log.d(TAG, "loadData: vacationEntry: "+vacationEntry.toString());
                        String dateFrom = vacationEntry.getString("DateFrom");
                        String dateTo = vacationEntry.getString("DateTo");
                        String period = vacationEntry.getString("Period");
                        String typeAr = vacationEntry.getString("Name-Ar");
                        String typeEn = vacationEntry.getString("Name-En");
                        String VacationId = vacationEntry.getString("VacationId");
                        Log.d(TAG, "loadData: dateFrom: "+dateFrom);
                        vacationsEntries.add(
                                new VacationsEntry(
                                        String.valueOf(j+1),
                                        typeEn,
                                        typeAr,
                                        dateFrom,
                                        dateTo,
                                        period,
                                        VacationId,
                                        EmployeeVacationReturn.this
                                )
                        );
                    }

                }
                ArrayList<VacationsEntry> vacationsEntriesSorted=new ArrayList<>();
                int l=vacationsEntries.size();
                int max=l-1;
                for (int i = 0; i < l; i++) {
                    vacationsEntriesSorted.add(vacationsEntries.get(max-i));
                    vacationsEntriesSorted.get(i).setNumber(String.valueOf(max-i+1));
                }
                shotVacationReturn=new ShotVacationReturnExtension(
                        vacationTypeList,
                        vacationArList,
                        vacationsEntriesSorted,
                        txt[1],//jsonObject.getString("returnDate")
                        locale
                );
                toDateTxt=txt[1];
                returnDateTxt=toDateTxt;

                Message msg=Message.obtain();
                msg.arg1=200;
                getHandler.sendMessage(msg);

            } catch (JSONException | InterruptedException | ExecutionException e) {
                Message msg=Message.obtain();
                msg.arg1=0;
                getHandler.sendMessage(msg);
                e.printStackTrace();
            }


        }
    }


    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);
        radio1Click(findViewById(R.id.radio1));
    }

    private void globalFindViewById() {
        historySpinner=findViewById(R.id.vacationHistory);
        returnDateTV=findViewById(R.id.returnDateTV);

        submitTV=findViewById(R.id.submit);

    }

    public void radio1Click(View view) {

        ((ImageView)findViewById(R.id.radio1)).setImageResource(R.drawable.radio_checked_icon);
        ((ImageView)findViewById(R.id.radio2)).setImageResource(R.drawable.radio_unchecked_icon);

        Typeface face;
        Typeface face2;
        if (!getResources().getConfiguration().locale.getLanguage().equals("ar"))
        {
            face = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        }else{
            face = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        }
        ((TextView) findViewById(R.id.did)).setTypeface(face);
        ((TextView) findViewById(R.id.did_not)).setTypeface(face2);




//        ((TextView) findViewById(R.id.reasonTV)).setVisibility(View.INVISIBLE);
//        ((EditText) findViewById(R.id.reasonET)).setVisibility(View.INVISIBLE);
        Utilitaire.fade(300, 1.0f,0.0f, (View) findViewById(R.id.reasonET), findViewById(R.id.reasonTV));

    }
    public void radio2Click(View view) {
        ((ImageView)findViewById(R.id.radio1)).setImageResource(R.drawable.radio_unchecked_icon);
        ((ImageView)findViewById(R.id.radio2)).setImageResource(R.drawable.radio_checked_icon);


        Typeface face;
        Typeface face2;

        if (!getResources().getConfiguration().locale.getLanguage().equals("ar"))
        {
            face = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        }else{
            face = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        }
        ((TextView) findViewById(R.id.did_not)).setTypeface(face);
        ((TextView) findViewById(R.id.did)).setTypeface(face2);


        Utilitaire.fade(300, 0.0f,1.0f, (View) findViewById(R.id.reasonET), findViewById(R.id.reasonTV));

    }
    public void showCalendarForReturnDate(View view) {
        dateType= DateType.RETURN_DATE;
        showCalendar();
    }




    public void deleteReturnDate(View view) {
        returnDateTV.setText("");
        dateType=DateType.RETURN_DATE;
    }

    public void showCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation=new TranslateAnimation(-constraintLayout.getWidth(),0,0,0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility()==View.INVISIBLE || constraintLayout.getVisibility()==View.GONE){
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                constraintLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public void hideCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation=new TranslateAnimation(0,-constraintLayout.getWidth(),0,0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility()==View.VISIBLE){
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                constraintLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    enum DateType{
        RETURN_DATE
    }

    @Override
    public void onDayClick(Date date) {
        String locale=getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat formatForDisplay=new SimpleDateFormat(
                locale.equals("ar")?CHOOSE_DATE_AR_DISPLAY:CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String displayed=formatForDisplay.format(date);
        displayed= (locale.equals("ar"))?Utilitaire.replaceArabicNumbers(displayed):displayed;

        SimpleDateFormat formatForOutput=new SimpleDateFormat(Constants.YEAR_MONTH_DAY, new Locale("en"));
        hideCalendar();
        if (dateType == DateType.RETURN_DATE) {
            returnDateTV.setText(displayed);
            returnDateTxt = formatForOutput.format(date);
        }
    }
    private void processTheCalendar() {
        RobotoCalendarView robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }
    @Override
    public void onDayLongClick(Date date) {

    }

    @Override
    public void onRightButtonClick() {

    }

    @Override
    public void onLeftButtonClick() {

    }


    @Override
    public void onBackPressed() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        if (constraintLayout.getVisibility()==View.VISIBLE){
            hideCalendar();
            Log.d(TAG, "backClick: backClick hide calendar");
        }else{
            super.onBackPressed();
            Log.d(TAG, "backClick: backClick super");

        }

    }


    boolean validate(){
        return true;//!toDateTxt.equals("") ;
    }

    private class SubmitThread extends Thread{

        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }
    }

    private void submit() {
        try {
            Log.d(TAG, "submit: "+submitObject.toString());
            String responseCode=new BackgroundWorker(this).execute(
                    "postVacationReturn",
                    submitObject.getEmail(),
                    submitObject.getPassword(),
                    submitObject.getVacationId(),
                    submitObject.getReturnDate(),
                    submitObject.getLateReturnReason()
            ).get();
            Message msg=Message.obtain();
            if (isInteger(responseCode)){
                msg.arg1=Integer.parseInt(responseCode);
            }else {
                msg.arg1=7;
            }
            postHandler.sendMessage(msg);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg=Message.obtain();
            msg.arg1=0;
            postHandler.sendMessage(msg);
        }

    }

    class SubmitObject {
        private String email;
        private String password;
        private String vacationType;
        private String vacationId;
        private String toDate;
        private String returnDate;
        private String lateReturnReason;

        public SubmitObject() {
        }

        public String getVacationId() {
            return vacationId;
        }

        public void setVacationId(String vacationId) {
            this.vacationId = vacationId;
        }

        public String getVacationType() {
            return vacationType;
        }

        public void setVacationType(String vacationType) {
            this.vacationType = vacationType;
        }


        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }



        public String getReturnDate() {
            return returnDate;
        }

        public void setReturnDate(String returnDate) {
            this.returnDate = returnDate;
        }

        public String getLateReturnReason() {
            return lateReturnReason;
        }

        public void setLateReturnReason(String lateReturnReason) {
            this.lateReturnReason = lateReturnReason;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "SubmitObject{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", vacationType='" + vacationType + '\'' +
                    ", toDate='" + toDate + '\'' +
                    ", returnDate='" + returnDate + '\'' +
                    ", lateReturnReason='" + lateReturnReason + '\'' +
                    '}';
        }
    }

}


