
package com.ma.kdipa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.PagerAdapterApplication;
import com.ma.kdipa.JavaResources.PagerAdapterAppointments;
import com.ma.kdipa.JavaResources.ShotInvestorDashboard;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.Utils;
import com.ma.kdipa.JavaResources.ViewPagerCustomDuration;
import com.ma.kdipa.JavaResources.ZoomOutPageTransformer;
import com.ma.kdipa.responses.AppointmentResponse;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ma.kdipa.JavaResources.Constants.TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class InvestorDashboard extends InvestorNavigationDrawer {
    private static final int TO_COMMON_LOGIN = 3215321;
    protected DrawerLayout mDrawer;

    private static final String TAG = "InvestorDashboard";
    ShotInvestorDashboard shotInvestorDashboard;

    private PieChart chart;

    SpringDotsIndicator springDotsIndicator;
    SpringDotsIndicator springDotsIndicator2;
    ViewPagerCustomDuration  viewPager;
    ViewPagerCustomDuration  viewPager2;
    private String token="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.i_dashboard,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        ImageView v =mDrawer.findViewById(R.id.home_iv);
        v.setImageResource(R.drawable.home_en);
        v.setTag(R.drawable.home_en);
        mDrawer.findViewById(R.id.home).setBackgroundColor(Color.parseColor("#F2F2F2"));




    loadData();


    }

    private void loadData() {

        if (Utilitaire.isNetworkAvailable(this)){
            showLoading();

            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
            if (sharedPreferences.contains(TOKEN)) {
                token = sharedPreferences.getString(TOKEN, "");
            }
            getAPIManager().getInvestorDashboard(token, Constants.INVESTOR_API_VALUE).enqueue(new Callback<ShotInvestorDashboard>() {
                @Override
                public void onResponse(Call<ShotInvestorDashboard> call, Response<ShotInvestorDashboard> response) {
                    hideLoading();
                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: null");
                        new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorDashboard.this)
                                .setMode(DialogBoxCustom2.ERROR)
                                .setTitle(getString(R.string.error))
                                .setBody(getString(R.string.an_error_occured))
                                .setPositive(getString(R.string.cancel))
                                .create().show();
//                        Toast.makeText(InvestorDashboard.this, getString(R.string.an_error_occured), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    shotInvestorDashboard=response.body();
                    if( shotInvestorDashboard.getAppointmentmeetingtime()!=null){
                        shotInvestorDashboard.setAppointmentmeetingtime(shotInvestorDashboard.getAppointmentmeetingtime().replace("_"," "));
                    }
                    loadDataAppointments();
                    setData();
                }

                @Override
                public void onFailure(Call<ShotInvestorDashboard> call, Throwable t) {
                    hideLoading();
                    lunchActivity(CommonLogin.class);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorDashboard.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(InvestorDashboard.this, getString(R.string.an_error_occured), Toast.LENGTH_SHORT).show();
                }
            });
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }

    }

    private void loadDataAppointments() {
        showLoading();
        getAPIManager().getAppointments(token, Constants.INVESTOR_API_VALUE, Constants.INVESTOR_BASE_URL+"bookingForm/review/"+ Utils.getValue(InvestorDashboard.this,Constants.COMPANY_ID, 0)).enqueue(new Callback<AppointmentResponse>() {
            @Override
            public void onResponse(Call<AppointmentResponse> call, Response<AppointmentResponse> response) {
                hideLoading();
                if (response.body() == null) {
                    return;
                }
                setDataAppointments(response.body());
            }

            @Override
            public void onFailure(Call<AppointmentResponse> call, Throwable t) {
                hideLoading();
                Log.d(TAG, "onResponse: failure: "+t);
                lunchActivity(CommonLogin.class);
                new DialogBoxCustom2.DialogBoxCustom2Builder(InvestorDashboard.this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.an_error_occured))
                        .setPositive(getString(R.string.cancel))
                        .create().show();
//                    Toast.makeText(InvestorDashboard.this, getString(R.string.an_error_occured), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setDataAppointments(AppointmentResponse body) {
        springDotsIndicator = findViewById(R.id.spring_dots_indicator);
        springDotsIndicator2 = findViewById(R.id.spring_dots_indicator2);
        viewPager = (ViewPagerCustomDuration) findViewById(R.id.appointmentsVP);
        viewPager2 = (ViewPagerCustomDuration) findViewById(R.id.applicationsVP);




        PagerAdapterAppointments adapter = new PagerAdapterAppointments(body.results);
        viewPager.setAdapter(adapter);
        //for animation purpose only:
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        springDotsIndicator.setViewPager(viewPager);
        viewPager.setScrollDurationFactor(3);




        ArrayList<ShotInvestorDashboard> list=new ArrayList<>();
        list.add(shotInvestorDashboard);
        PagerAdapterApplication adapter2 = new PagerAdapterApplication(list);
        viewPager2.setAdapter(adapter2);
        //for animation purpose only:
        viewPager2.setPageTransformer(true, new ZoomOutPageTransformer());

        springDotsIndicator2.setViewPager(viewPager2);
        viewPager2.setScrollDurationFactor(3);

    }



    private void setChartConfiguration() {
        chart = findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
//        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setCenterText(generateCenterSpannableText());

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.getLegend().setEnabled(false);


        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(false);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(false);



        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
//        chart.setOnChartValueSelectedListener(this);


        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);



        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTextSize(12f);

    }


    private void setData() {
        setChartConfiguration();
        int numberAppointments=shotInvestorDashboard.getNumberofappointments();
        int numberApplications=shotInvestorDashboard.getNumberofapplications();



        int total=numberApplications+numberAppointments;

        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        float f=numberAppointments/((float)total);
        Log.d(TAG, "setData: f: "+f);
        entries.add(new PieEntry(f));
        entries.add(new PieEntry(1.0f-f));
        int value= (int) (f*100);
        Log.d(TAG, "setData: value: "+value);

        ((TextView)findViewById(R.id.chart_main_value)).setText(value+"%");





        String appointmentsTxt=getResources().getConfiguration().locale.getLanguage().equals("ar")?(numberAppointments+" "+"تعيينات"):(numberAppointments+" "+"appointments");
        String applicationsTxt=getResources().getConfiguration().locale.getLanguage().equals("ar")?(numberApplications+" "+"تطبيقات"):(numberApplications+" "+"applications");


        ((TextView) findViewById(R.id.appointments_tv)).setText(appointmentsTxt);
        ((TextView) findViewById(R.id.applications_tv)).setText(applicationsTxt);
        ((TextView) findViewById(R.id.totalRequestsTV)).setText(String.valueOf(total));



        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setDrawValues(false);

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.COLORFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);
//
//        colors.add(ColorTemplate.getHoloBlue());
        colors.add(ContextCompat.getColor(this, R.color.royal_dark_blue));
        colors.add(ContextCompat.getColor(this, R.color.yellow));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();



    }






    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

}
