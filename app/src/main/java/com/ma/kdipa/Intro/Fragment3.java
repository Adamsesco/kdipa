package com.ma.kdipa.Intro;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {

    @BindView(R.id.bgIV)
    ImageView bgIV;

    Frag3Actions callback;

    public Fragment3(Frag3Actions callback) {
        this.callback = callback;
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment3, container, false);
        ButterKnife.bind(this, view);
        int[] a =Utilitaire.getScreenWidthHeight(container.getContext());
        bgIV.setImageDrawable(Utilitaire.resizeDrawable(container.getContext(), R.drawable.fragment3,a[0],true));
        bgIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onSkip();
            }
        });

        return view;
    }

    public interface Frag3Actions {
        void onSkip();
    }


}
