package com.ma.kdipa.Intro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.viewpager.widget.ViewPager;

import com.ma.kdipa.CommonLogin;
import com.ma.kdipa.EmployeeDashboard;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.R;
import com.ma.kdipa.SplashScreen;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN_2;
import static com.ma.kdipa.JavaResources.Constants.FIRE_BASE_TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class Intro extends AppCompatActivity {

    Fragment1 fragment1;
    Fragment2 fragment2;
    Fragment3 fragment3;
    Fragment4 fragment4;


    SharedPreferences sharedPreferences;
    String email = "";

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.dotsIndicator)
    DotsIndicator dotsIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);

        fragment1 = new Fragment1(new Fragment1.Frag1Actions() {
            @Override
            public void onNext() {
                nextAction(1);
            }

            @Override
            public void onSkip() {
                doneAction();
            }
        });
        fragment2 = new Fragment2(new Fragment2.Frag2Actions() {
            @Override
            public void onNext() {
                nextAction(2);
            }

            @Override
            public void onSkip() {
                doneAction();
            }
        });
        fragment3 = new Fragment3(new Fragment3.Frag3Actions() {
            @Override
            public void onSkip() {
                doneAction();
            }
        });

        fragment4 = new Fragment4(new Fragment4.Frag4Actions() {
                    @Override
                    public void onSkip() {
                        doneAction();
                    }
                });


        IntroPagerAdapter adapter = new IntroPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(fragment1);
        adapter.addFragment(fragment2);
        adapter.addFragment(fragment3);
        adapter.addFragment(fragment4);

        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(adapter);
        dotsIndicator.setViewPager(viewPager);
    }

    private void nextAction(int position) {
        viewPager.setCurrentItem(position);
    }

    private void doneAction() {
        SharedPreferences sharedPreferences;
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        sharedPreferences
                .edit()
                .putBoolean(Constants.DID_SEE_INTRO,  true)
                .apply();
        Intent intent;
        if (alreadyConnectedAsEmployee()) {
            intent = new Intent(Intro.this, EmployeeDashboard.class);
        } else {
            intent = new Intent(Intro.this, CommonLogin.class);
        }
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
        Pair<View, String> p2 = Pair.create((View) findViewById(R.id.theme_logo), "theme_logo");

        try {
            // Pass data object in the bundle and populate details activity.
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(Intro.this, p1, p2);
            startActivity(intent, options.toBundle());
        } catch (Exception e) {
            e.printStackTrace();
            startActivity(intent);
        }
        finish();
    }
    private boolean alreadyConnectedAsEmployee() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(FIRE_BASE_TOKEN) && sharedPreferences.contains(EMAIL)) {
            email = sharedPreferences.getString(EMAIL, "");
            String domainName = email.split("@")[1];
            return domainName.equals(EMPLOYEE_DOMAIN) || domainName.equals(EMPLOYEE_DOMAIN_2);
        }
        return false;
    }

}
