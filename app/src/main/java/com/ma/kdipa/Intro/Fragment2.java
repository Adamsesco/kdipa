package com.ma.kdipa.Intro;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {


    Frag2Actions callback;
    @BindView(R.id.bgIV)
    ImageView bgIV;

    public Fragment2(Frag2Actions callback) {
        this.callback = callback;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment2, container, false);
        ButterKnife.bind(this, view);
        int[] a = Utilitaire.getScreenWidthHeight(container.getContext());
        bgIV.setImageDrawable(Utilitaire.resizeDrawable(container.getContext(), R.drawable.fragment2,a[0],true));
        bgIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onSkip();
            }
        });

        return view;
    }

    public interface Frag2Actions {
        void onNext();

        void onSkip();
    }


}
