package com.ma.kdipa.Intro;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {

    Frag1Actions callback;


    public Fragment1(Frag1Actions callback) {
        this.callback = callback;
    }
    @BindView(R.id.bgIV)
    ImageView bgIV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment1, container, false);
        int[] a =Utilitaire.getScreenWidthHeight(container.getContext());
        ButterKnife.bind(this, view);
        bgIV.setImageDrawable(Utilitaire.resizeDrawable(container.getContext(), R.drawable.fragment1,a[0],true));
        bgIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onSkip();
            }
        });
        return view;
    }
    public interface Frag1Actions {
        void onNext();
        void onSkip();
    }
}
