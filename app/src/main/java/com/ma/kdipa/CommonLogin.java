package com.ma.kdipa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ma.kdipa.JavaResources.APIManager;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.BackgroundWorkerInvestor;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxChooseCompany;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.Utils;
import com.ma.kdipa.expire_logique.ABFSApplication;
import com.ma.kdipa.responses.GetCompaniesResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Response;

import static com.ma.kdipa.JavaResources.Constants.DEVICE_TYPE;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_DOMAIN_2;
import static com.ma.kdipa.JavaResources.Constants.FIRE_BASE_TOKEN;
import static com.ma.kdipa.JavaResources.Constants.FIRST_NAME;
import static com.ma.kdipa.JavaResources.Constants.ID;
import static com.ma.kdipa.JavaResources.Constants.IS_MANAGER;
import static com.ma.kdipa.JavaResources.Constants.LAST_NAME;
import static com.ma.kdipa.JavaResources.Constants.MANAGER_ID;
import static com.ma.kdipa.JavaResources.Constants.MISSING_FIREBASE_TOKEN;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Utilitaire.isValid;

public class CommonLogin extends AppCompatActivity {
    private static final String TAG = "CommonLogin";
    private static final int START_INVESTOR_SEARCH = 873513;
    ProgressDialog progressDialog;
    Handler handler;
    DialogBoxLoadingClass loadingDialogBox;
    SharedPreferences sharedPreferences;
    String name = "";
    String nameAr = "";
    String firstName;
    String username;
    String lastName;
    String email = "";
    String password = "";
    String token = "";
    String fireBaseToken = "";
    String id = "";
    String managerId = "";

    EditText emailET;
    EditText passwordET;
    private TextView loginTV;
    private boolean isManager = false;
    private String isAccountManager = "";
    private String finance = "";
    private String moci = "";
    private List<GetCompaniesResponse> getCompaniesResponse;

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        ((ABFSApplication) this.getApplication()).setInBackGround(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Utilitaire.refreshLocal(this);
        setContentView(R.layout.common_login);

        if (Constants.EMPLOYEE_BASE_URL.contains("216.250")) {
            findViewById(R.id.demo).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.demo).setVisibility(View.GONE);
        }
        findViewById(R.id.demo).setVisibility(Constants.DEMO?View.VISIBLE:View.GONE);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        fireBaseToken = task.getResult().getToken();
                        // Log and //Toast
                        String msg = "tokenCode: " + token;
                    }
                });
        emailET = findViewById(R.id.username_edit_text);
        loginTV = findViewById(R.id.login);
        passwordET = findViewById(R.id.password_edit_text);


        setEmailAndPasswordInfo();



        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 1) {
                    progressDialog.dismiss();
                    loginTV.setEnabled(true);
                    lunchActivity(EmployeeDashboard.class);
                } else if (msg.arg1 == 2) {
                    progressDialog.dismiss();
                    startActivityInvestorDashboard();
                } else if (msg.arg1 == 400) {
                    progressDialog.dismiss();
                    loginTV.setEnabled(true);
                    Toast.makeText(CommonLogin.this, R.string.wrong_email_or_password, Toast.LENGTH_SHORT).show();
                } else if (msg.arg1 == 0) {
                    progressDialog.dismiss();
                    loginTV.setEnabled(true);
                    Toast.makeText(CommonLogin.this, R.string.wrong_email_or_password, Toast.LENGTH_SHORT).show();
                } else if (msg.arg1 == MISSING_FIREBASE_TOKEN) {
                    progressDialog.dismiss();
                    loginTV.setEnabled(true);
                    Toast.makeText(CommonLogin.this, "Failed to retrieve Firebase Token, try later !", Toast.LENGTH_SHORT).show();
                } else if (msg.arg1 == START_INVESTOR_SEARCH) {
                    progressDialog.dismiss();
                    loginTV.setEnabled(true);
                    startInvestorSearch();
                }
            }
        };
    }

    private void setEmailAndPasswordInfo() {
        SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(Constants.LAST_LOGIN_USER_INFO, MODE_PRIVATE);
        emailET.setText( sharedPreferences.getString(EMAIL,"") );
        passwordET.setText( sharedPreferences.getString(PASSWORD,"") );
    }

    private void startInvestorSearch() {
        //maybe investor domain, then:
        //get companyId first (if several, pop up a dialog with choices
        showLoading();
        APIManager.retrofitInvestor.create(APIManager.class).getCompanies(token, Constants.INVESTOR_API_VALUE, Constants.INVESTOR_BASE_URL + "company/getAllCompaniesByUserAccount/" + email).enqueue(new retrofit2.Callback<List<GetCompaniesResponse>>() {
            @Override
            public void onResponse(Call<List<GetCompaniesResponse>> call, Response<List<GetCompaniesResponse>> response) {
                hideLoading();
                if (response.body() == null) {
                    return;
                }
                getCompaniesResponse = response.body();
//                getCompaniesResponse.add(new GetCompaniesResponse(3, 0, 3,1,"Some Name", null));
                if (getCompaniesResponse.size() == 0) {
                    new DialogBoxCustom2.DialogBoxCustom2Builder(CommonLogin.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.no_company_found))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                    return;
                }
                if (getCompaniesResponse.size() == 1) {
                    loginAsInvestor(getCompaniesResponse.get(0).companyId);
                    Utils.setValue(CommonLogin.this, Constants.COMPANY_ID, getCompaniesResponse.get(0).companyId);
                } else {
                    DialogBoxChooseCompany dialogBoxChooseCompany = new DialogBoxChooseCompany(CommonLogin.this, getCompaniesResponse);
                    dialogBoxChooseCompany.setOnPositiveListener(id1 -> {
                        Utils.setValue(CommonLogin.this, Constants.COMPANY_ID, id1);
                        loginAsInvestor(getCompaniesResponse.get(0).companyId);
                    });
                    dialogBoxChooseCompany.show();
                }
            }

            @Override
            public void onFailure(Call<List<GetCompaniesResponse>> call, Throwable t) {
                hideLoading();
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }


    private void startActivityInvestorDashboard() {
        lunchActivity(InvestorDashboard.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onTestButtonClick(View view) {
        Toast.makeText(this, "clicking was done successfully !", Toast.LENGTH_SHORT).show();
    }

    public void onClickOnForgotPassword(View view) {

//        lunchActivity(CommonForgotPassword.class);

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://emea01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fpasswordreset.microsoftonline.com%2F%3Fru%3Dhttps%253a%252f%252flogin.microsoftonline.com%252fcommon%252freprocess%253fctx%253drQIIAYWTPYzbdADF4-QuvZ4KtOWEilhOAgQCOfHn30kkhuT8EV9iOx92YnuxEsexHX_848SO4wwIiYWxEttNiPEmxELFxATSSUidu7N0QkyIpQSYK5an9_SkN_3e-Rmo4UQNr2GfVIga1vqAYgBNUfQcJQkboFRzjqEN0gEovmiSx4pggD3bPD5_-H72i3rzZYv_4eK3Dw8X6Oe3yKWXputtq17P87wGl0vfdmo2jOrhLF74sbsjniHIcwS5KZ86MaqNb8tbQDIMwGhANWmAkw2CwGsyYeSKquUm206NyMCUKwyTBLHoT_lAVoNUUieezAakNB0ezEgrTNWgZdYupIOdKoJGGD6GGRHv9VWXNldGKrF2bgqT8Li3P_r9i_JbSjtLPeIfgRv_4Lwsv7l1QsdOrZltwyxO_yjfX8JNZK3hNr2pfFFxSDJejvMob5rcdtUXl9fdPrnO-xhuSq6Ec8Nr1tsJpngt97q4gHE90JWzzc7bz9MRb9mHdVvMpk3AKZawUYbWxAQQT7XRdkjl1kSSOwTs9kcuBCwFDt32zt3OVlNd4JVwDgpDj6MGN1-o1KKbYNfQ2msroAOw9CNJF3QdWqrXSTfKYKqhg9VeN2HHHLJComhJjhUT2_TIoEOTU308w71sJQSNzKSpQU9u5wOMFmIV29Bis7nDUZzuoi5sEGogUxHDX-W83cM3BG8XbW6RZY2GMxdZn3Gw5gZNCj1K-DjiB1yHNXoi5xWxAZTeQR3kGB4zGZ6M9txaY3kbF_hEHOv01dJw3QT0J80sGTrK6gosJImDlDnaJVZjhxeBRyYOJWvsKBxvpYE5Y2G4EJjEYW4r776GrB3xfaV6NBGM7yoMXDuxv7hcb-DSD53X0bgj6sq_qQsjp9YOw-cnyMuTd86qDx88QS5LH19gldbZMVWelC5Lf54g354eYf9I5H79-Zu68OzVe19_V_xVujutT2Tp07owmIjBNJCDDrnSfFeXJ4Y43Lup2ceFeMzM_YE9HQ0_o1r40yrytFq9qz4SWUvm1LHaltn2iCUs7Pcq8tW90o_3__c-Lx68fX6e-VYI7VnobB__d6Of3ij9DQ2%26mkt%3Den-US%26hosted%3D0%26device_platform%3DWindows%2B8.1%26username%3Dfadi%2540burgan-systems.com&data=04%7C01%7C%7C195dfcc2faa64d1b19e108d9db622b9f%7C84df9e7fe9f640afb435aaaaaaaaaaaa%7C1%7C0%7C637782036953236682%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C3000&sdata=GvyZuTaWR8dvbI30XfW0FCodlFRaEmEP9el%2FKMBhuXQ%3D&reserved=0"));
        startActivity(browserIntent);
    }

    public void onClickOnapplyForUsername(View view) {
        lunchActivity(InvestorApplyForUsername.class);
    }


    public void loginClick(View view) {
        email = emailET.getText().toString();
        password = passwordET.getText().toString();
        if (isValid(email, password)) {
            if (Utilitaire.isNetworkAvailable(this)) {
                loginTV.setEnabled(false);
                progressDialog = new ProgressDialog(CommonLogin.this, R.style.customDialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.authenticating));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);

                LoginThread loginThread = new LoginThread();
                loginThread.start();
            } else {
               new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
            }
        } else {
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.wrong_email_address_or_password))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }
    }

    private void feedSharedPreferences() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        sharedPreferences
                .edit()
                .putString(NAME, name)
                .putString(FIRST_NAME, firstName)
                .putString(LAST_NAME, lastName)
                .putString(EMAIL, email)
                .putString(PASSWORD, password)
                .putString(TOKEN, token)
                .putString(ID, id)
                .putString(FIRE_BASE_TOKEN, fireBaseToken)
                .putBoolean(IS_MANAGER, isManager)
                .putString(MANAGER_ID, managerId)
                .apply();
    }

    private void feedInvestorSharedPreferences() {
        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        sharedPreferences
                .edit()
                .putString(Constants.USERNAME, username)
                .putString(TOKEN, token)
                .apply();
    }

    void lunchActivity(Class<?> someClass) {
        Intent intent = new Intent(getApplicationContext(), someClass);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }



    @SuppressLint("SetTextI18n")
    public void demoClick(View view) {
        if (emailET.getText().toString().equals("adam@burgan-systems.com")) {
//            emailET.setText("oss_test");
//            passwordET.setText("Lokalmanager7");
            emailET.setText("aleksandar.oroz@prozone-me.com");
            passwordET.setText("x");
        } else if (emailET.getText().toString().contains("aleksandar")) {
            emailET.setText("Kdipa_mob@kdipa.gov.kw");
            passwordET.setText("123456789");
        }
        else if (emailET.getText().toString().contains("Kdipa_mob")) {
            emailET.setText("pioneers2035@gmail.com");
            passwordET.setText("");
        }else if (emailET.getText().toString().contains("pioneers2035")) {
            emailET.setText("a_almansour@kdipa.gov.kw");
            passwordET.setText("123456789");
        }
        else if (emailET.getText().toString().contains("a_almansour")) {
            emailET.setText("a_esmail@kdipa.gov.kw");
            passwordET.setText("kdipa2021");
        }else if (emailET.getText().toString().contains("a_esmail")) {
            emailET.setText("test@mobile.com");
            passwordET.setText("x");
        } else {
            emailET.setText("adam@burgan-systems.com");
            passwordET.setText("Jak@3210");
        }
    }

    private void loginAsInvestor(int companyId) {
        BackgroundWorkerInvestor backgroundWorker = new BackgroundWorkerInvestor(getApplicationContext());
        String[] fullResponse;
        try {
            fullResponse = backgroundWorker.execute("signin", email, password, String.valueOf(companyId)).get();
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(fullResponse[1]);
            token = jsonObject.getString("token");
            if (!token.equals("")) {
                firstName = jsonObject.getString("firstName");
                lastName = jsonObject.getString("lastName");
                moci = jsonObject.getString("moci");
                isAccountManager = jsonObject.getString("isAccountManager");
                finance = jsonObject.getString("finance");
                id = jsonObject.getString("id");
                Utils.setValue(CommonLogin.this, Constants.INVESTOR_ACCOUNT_MANAGER_ID, id);
                name = firstName + " " + lastName;
                feedSharedPreferences();
                Message msg = Message.obtain();
                msg.arg1 = 2;
                handler.sendMessage(msg);
            } else if (token.equals("")) {
                Message msg = Message.obtain();
                msg.arg1 = 400;
                handler.sendMessage(msg);
            }

        } catch (ExecutionException | InterruptedException | JSONException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            handler.sendMessage(msg);
        }
    }

    public void showLoading() {
        loadingDialogBox = new DialogBoxLoadingClass(this);
        loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialogBox.show();
        loadingDialogBox.setCancelable(false);
        loadingDialogBox.setCanceledOnTouchOutside(false);
    }

    public void hideLoading() {
        if (loadingDialogBox != null) {
            loadingDialogBox.dismiss();
        }
    }

    private class LoginThread extends Thread {
        LoginThread() {
        }

        @Override
        public void run() {
            String domainName = email.split("@").length == 2 ? email.split("@")[1] : "";

            if (domainName.equals(EMPLOYEE_DOMAIN) || domainName.equals(EMPLOYEE_DOMAIN_2)) {
                if (true ){//!fireBaseToken.equals("")) {
                    BackgroundWorker backgroundWorker = new BackgroundWorker(getApplicationContext());
                    String result;
                    try {
                        result = backgroundWorker.execute("login", email, password, fireBaseToken, DEVICE_TYPE).get();
                        JSONObject jsonObject = null;
                        jsonObject = new JSONObject(result);
                        String resutlCode = "";
                        resutlCode = jsonObject.getString("Result");
                        if (resutlCode.equals("true")) {
                            firstName = jsonObject.getString("FirstName");
                            lastName = jsonObject.getString("LastName");
                            name = firstName + " " + lastName;

                            if (jsonObject.has("isManager")) {
                                InternalStorage.write(CommonLogin.this, EMPLOYEES_LIST, result, false, false);
                                if (jsonObject.getString("isManager").equals("true")) {
                                    isManager = true;
                                    EmployeesList.newInstance(CommonLogin.this, result);
                                    EmployeesList employeesList = EmployeesList.getInstance();
                                    for (int i = 0; i < employeesList.getEmployeesList().size(); i++) {
                                        if (employeesList.getEmployeesList().get(i).getEmail().equalsIgnoreCase(email)) {
                                            managerId = employeesList.getEmployeesList().get(i).getId();
                                        }
                                    }
                                }
                            }


                            feedSharedPreferences();

                            Message msg = Message.obtain();
                            msg.arg1 = 1;
                            handler.sendMessage(msg);


                        } else if (resutlCode.equals("false")) {
                            Message msg = Message.obtain();
                            msg.arg1 = 400;
                            handler.sendMessage(msg);
                        }

                    } catch (ExecutionException | InterruptedException | JSONException e) {
                        e.printStackTrace();
                        Message msg = Message.obtain();
                        msg.arg1 = 0;
                        handler.sendMessage(msg);
                    }
                } else {//fireBaseToken is ""
                    Message msg = Message.obtain();
                    msg.arg1 = MISSING_FIREBASE_TOKEN;
                    handler.sendMessage(msg);
                }
            } else {
                Message msg = Message.obtain();
                msg.arg1 = START_INVESTOR_SEARCH;
                handler.sendMessage(msg);
            }
        }
    }

}
