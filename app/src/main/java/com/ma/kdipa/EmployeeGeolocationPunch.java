package com.ma.kdipa;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.GEO_ATTEND;
import static com.ma.kdipa.JavaResources.Constants.GEO_ATTEND_CHECK;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.USE_TOUCH_ID;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.fivehundredpx.android.blur.BlurringView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Circle;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxClass;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.LocationManager;
import com.ma.kdipa.JavaResources.PermissionUtilsLocation;
import com.ma.kdipa.JavaResources.Utilitaire;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import co.infinum.goldfinger.Goldfinger;

public class EmployeeGeolocationPunch extends EmployeeNavigationDrawer implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        DialogBoxClass.OkClick {

    private static final float DEFAULT_ZOOM = 10.0f;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = "EmployeeGeolocationPunc";
    public static Integer ID_DELIVERY_MAN;
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    DialogBoxLoadingClass progressDialog;
    Handler postHandler;
    Handler getHandler;
    DialogBoxLoadingClass loadingDialogBox;
    Button punchInButton;
    Button punchOutButton;
    View mapView;
    Goldfinger goldfinger;
    BlurringView blurringView;
    ImageView imageView;
    ImageView darkIV;
    Location mLastKnownLocation;
    String useTouchId = "";
    private GoogleMap mMap;
    private boolean mPermissionDenied = false;
    private boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LatLng mDefaultLocation;
    private String successfulPunchTime = "";
    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.e_geolocation_punch,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);


        ImageView v = mDrawer.findViewById(R.id.geolocationPunch_iv);
        v.setImageResource(R.drawable.fp_en);
        v.setTag(R.drawable.fp_en);
        mDrawer.findViewById(R.id.geolocationPunch).setBackgroundColor(Color.parseColor("#F2F2F2"));


        mDefaultLocation = new LatLng(29.378757, 47.993330);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();

        mapFragment.getMapAsync(this);


        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
        }
        if (sharedPreferences.contains(USE_TOUCH_ID)) {
            useTouchId = sharedPreferences.getString(USE_TOUCH_ID, "true");
        } else {
            useTouchId = "true";
        }

        fingerPrintProcess();
        getHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 200) {
                    progressDialog.dismiss();
                    enableButton(punchInButton, true);
                    enableButton(punchOutButton, true);
                } else {
                    progressDialog.dismiss();
                    enableButton(punchInButton, false);
                    enableButton(punchOutButton, false);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.not_inside))
                            .setPositive(getString(R.string.cancel))
                            .create().show();

                }
            }
        };

        postHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 200) {
                    progressDialog.dismiss();
                    enableButton(punchInButton, true);
                    enableButton(punchOutButton, true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                            .setMode(DialogBoxCustom2.SUCCESS)
                            .setTitle(getString(R.string.success))
                            .setBody(successfulPunchTime)
                            .setPositive(getString(R.string.continuee))
                            .setOnPositive(() -> lunchActivity(EmployeeDashboard.class))
                            .create().show();
                } else if (msg.arg1 == 404) {
                    progressDialog.dismiss();
                    enableButton(punchInButton, true);
                    enableButton(punchOutButton, true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.incorrect_email))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                } else if (msg.arg1 == 0) {
                    progressDialog.dismiss();
                    enableButton(punchInButton, true);
                    enableButton(punchOutButton, true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.not_inside))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                } else {
                    showError();
                }
            }
        };


        enableButton(punchInButton, false);
        enableButton(punchOutButton, false);
        //handle cases where prerequisites are missing
        arePrerequisitesValid();

        mLocationManager = new LocationManager(this);
        mLocationManager.setListener(new LocationManager.Listener() {
            @Override
            public void onGetLocation(Location location) {
                // Set the map's camera position to the current location of the device.
                mLastKnownLocation = location;
                if (mLastKnownLocation != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(mLastKnownLocation.getLatitude(),
                                    mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                    if (arePrerequisitesValid()) {
                        loadData();
                    }
                }
            }

            @Override
            public void onLocationDenied() {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            }
        });



        startTimer(Constants.EXPIRY_PUNCH);
    }

    private void showError() {
        progressDialog.dismiss();
        enableButton(punchInButton, true);
        enableButton(punchOutButton, true);
        new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                .setMode(DialogBoxCustom2.ERROR)
                .setTitle(getString(R.string.error))
                .setBody(getString(R.string.an_error_occured))
                .setPositive(getString(R.string.cancel))
                .create().show();
    }

    private void enableButton(Button button, boolean b) {
        if(button==punchInButton) {
            if (b) {
                button.setEnabled(true);
                button.setBackgroundColor(Color.parseColor("#005384"));
            } else {
                button.setEnabled(false);
                button.setBackgroundColor(Color.parseColor("#D8D8D8"));
            }
        }else if(button==punchOutButton){
            if (b) {
                button.setEnabled(true);
                button.setBackgroundColor(Color.parseColor("#d7aa73"));
            } else {
                button.setEnabled(false);
                button.setBackgroundColor(Color.parseColor("#D8D8D8"));
            }
        }

    }

    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)) {
            progressDialog = new DialogBoxLoadingClass(this);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);


            if (mLastKnownLocation != null && mLastKnownLocation.getLatitude() != 0) {
                new LoadingThread().start();
            } else {
                progressDialog.dismiss();
                enableButton(punchInButton, true);
                enableButton(punchOutButton, true);
            }
        } else {
            new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.cancel))
                    .create().show();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        postHandler.removeCallbacksAndMessages(null);
        getHandler.removeCallbacksAndMessages(null);
    }

    private boolean setIsInside(Double xm, Double ym, Circle perimeter) {
        return Math.pow((xm - perimeter.getXo()), 2) + Math.pow((ym - perimeter.getYo()), 2) <= Math.pow(perimeter.getRadius(), 2);
    }

    private void fingerPrintProcess() {
        goldfinger = new Goldfinger.Builder(this).build();
        punchInButton = findViewById(R.id.punchInButton);
        punchOutButton = findViewById(R.id.punchOutButton);
        punchInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonClicked(1);

            }
        });
        punchOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonClicked(2);

            }
        });

    }

    private void onButtonClicked(int type) {
        if (arePrerequisitesValid()) {
            //check if the user is in
            loadData();
        }

        String currentDateS = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        if (true) {//Utilitaire.compareTimes(currentDateS,MAX_TIME)
            double latitude;//location.getLatitude();
            double longitude;//  location.getLongitude();

            if (mLastKnownLocation == null) {
                latitude = 0;
                longitude = 0;
            } else {
                latitude = mLastKnownLocation.getLatitude();
                longitude = mLastKnownLocation.getLongitude();
            }
            if (latitude == 0.0) {
                Toast.makeText(EmployeeGeolocationPunch.this, R.string.loading_coordinates, Toast.LENGTH_SHORT).show();
                mLocationManager.getLocation();
            } else {
                snapShot();
                Goldfinger.PromptParams params;
                params = new Goldfinger.PromptParams.Builder(EmployeeGeolocationPunch.this)
                        .title(getString(R.string.title))
                        .negativeButtonText(getString(R.string.cancel))
                        .description(getString(R.string.description))
                        .subtitle("")
                        .build();
                goldfinger.authenticate(params, new Goldfinger.Callback() {
                    @Override
                    public void onError(@NonNull Exception e) {
                        //Toast.makeText(EmployeeGeolocationPunch.this, e.toString()+"", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResult(@NonNull Goldfinger.Result result) {
                        if (result.reason() == Goldfinger.Reason.USER_CANCELED || result.reason() == Goldfinger.Reason.NEGATIVE_BUTTON) {
                            Utilitaire.fade(1000, 1.0f, 0f, blurringView, imageView, darkIV);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    findViewById(R.id.snapshot).setVisibility(View.GONE);
                                    findViewById(R.id.blurringView).setVisibility(View.GONE);
                                }
                            }, 1000);
                        } else if (result.reason() == Goldfinger.Reason.AUTHENTICATION_SUCCESS) {
                            submit(type);
                        } else {
//                                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeGeolocationPunch.this)
//                                            .setMode(DialogBoxCustom2.ERROR)
//                                            .setTitle(getString(R.string.error))
//                                            .setBody(result.reason() + "")
//                                            .setPositive(getString(R.string.continuee))
//                                            .create().show();
                        }
                    }
                });
            }
        } else {
            Toast.makeText(EmployeeGeolocationPunch.this, "You are too late, you cannot Punch In today !", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean arePrerequisitesValid() {
        if (!goldfinger.canAuthenticate()) {
            if (!goldfinger.hasEnrolledFingerprint()) {
                new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.no_enrolled_fingerprint))
                        .setPositive("Ok")
                        .create().show();
//                Toast.makeText(EmployeeGeolocationPunch.this, R.string.no_enrolled_fingerprint, Toast.LENGTH_SHORT).show();
            }
            if (!goldfinger.hasFingerprintHardware()) {
                new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.no_fingerprint_hardware))
                        .setPositive("Ok")
                        .setOnPositive(() -> {
                                    startActivity(new Intent(this, EmployeeGeolocationPunch.class));
                                    finish();
                                }
                        )
                        .create().show();
            }
            return false;
        }
        if (!Utilitaire.isNetworkAvailable(EmployeeGeolocationPunch.this)) {
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.refresh))
                    .setOnPositive(() -> {
                                startActivity(new Intent(this, EmployeeGeolocationPunch.class));
                                finish();
                            }
                    )
                    .create().show();

            return false;
        }
        if (useTouchId.equals("false")) {
            Toast.makeText(EmployeeGeolocationPunch.this, R.string.need_touch_id, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isGPSLocationEnabled()) {
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.request_gps_location))
                    .setPositive(getString(R.string.refresh))
                    .setOnPositive(() -> {
                                startActivity(new Intent(this, EmployeeGeolocationPunch.class));
                                finish();
                            }
                    )
                    .create().show();

            return false;
        }

        if ((ActivityCompat.checkSelfPermission(EmployeeGeolocationPunch.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            if ((ActivityCompat.checkSelfPermission(EmployeeGeolocationPunch.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(EmployeeGeolocationPunch.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.enable_requested_permissions_then_refresh))
                        .setPositive(getString(R.string.refresh))
                        .setOnPositive(() -> {
                                    startActivity(new Intent(this, EmployeeGeolocationPunch.class));
                                    finish();
                                }
                        )
                        .create().show();


                return false;
            }
        return true;

    }

    private void submit(int type) {
        if (Utilitaire.isNetworkAvailable(this)) {
            punchOutButton.setEnabled(false);
            punchInButton.setEnabled(false);
            progressDialog = new DialogBoxLoadingClass(this);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            new GeoThread(type).start();

        } else {
            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.retry))
                    .create().show();

        }
    }

    public void snapShot() {

        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {
                    blurringView = findViewById(R.id.blurringView);
                    imageView = findViewById(R.id.snapshot);
                    darkIV = findViewById(R.id.darkIV);
                    imageView.setImageBitmap(bitmap);
                    blurringView.setBlurredView(imageView);
                    blurringView.invalidate();
                    Utilitaire.fade(1000, 0.0f, 1.0f, blurringView, imageView, darkIV);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imageView.setVisibility(View.VISIBLE);
                            blurringView.setVisibility(View.VISIBLE);
                        }
                    }, 1000);
                    //Toast.makeText (EmployeeGeolocationPunch.this, "Capture", Toast.LENGTH_SHORT).show ();
                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText (EmployeeGeolocationPunch.this, "Not Capture", Toast.LENGTH_SHORT).show ();
                }
            }
        };
        mMap.snapshot(callback);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near kdipa.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add polygons to indicate areas on the map.
//        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
//                .clickable(true)
//                .add(
//                        new LatLng(boundings[0].getLatitudeInDegrees(), boundings[0].getLongitudeInDegrees()),
//                        new LatLng(boundings[1].getLatitudeInDegrees(), boundings[1].getLongitudeInDegrees())
//
//
//                ));


        mMap = googleMap;
        LatLng kdipa = new LatLng(29.378757, 47.993330);
        //mMap.addMarker(new MarkerOptions().position(casablanca).title("Marker in casablanca"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kdipa, 10));

        enableMyLocation();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
//                snapShot();
            }
        });
        if ((ActivityCompat.checkSelfPermission(EmployeeGeolocationPunch.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            if ((ActivityCompat.checkSelfPermission(EmployeeGeolocationPunch.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                return;
            } else mLocationManager.getLocation();

        else mLocationManager.getLocation();


        /***change the MyLocationButton position*/
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }


    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission to access the location is missing.
            PermissionUtilsLocation.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            mLocationPermissionGranted = true;
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        //Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtilsLocation.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    public void onOkClick() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create((View) findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(EmployeeGeolocationPunch.this, p1, p3);
        startActivity(intent, options.toBundle());
    }

//    private void getDeviceLocation() {
//        /*
//         * Get the best and most recent location of the device, which may be null in rare
//         * cases when a location is not available.
//         */
//        try {
//            if (mLocationPermissionGranted) {
//                Task locationResult = mFusedLocationProviderClient.getLastLocation();
//                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
//                    @Override
//                    public void onComplete(@NonNull Task task) {
//                        if (task.isSuccessful()) {
//                            // Set the map's camera position to the current location of the device.
//                            mLastKnownLocation = (Location) task.getResult();
//                            if (mLastKnownLocation != null) {
//                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                                        new LatLng(mLastKnownLocation.getLatitude(),
//                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
//                                if (arePrerequisitesValid()) {
//                                    loadData();
//                                }
//                            }
//                        } else {
//                            Log.d(TAG, "Current location is null. Using defaults.");
//                            Log.e(TAG, "Exception: %s", task.getException());
//                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
//                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
//                        }
//                    }
//                });
//            }
//        } catch (SecurityException e) {
//            Log.e("Exception: %s", e.getMessage());
//        }
//    }

    private boolean isGPSLocationEnabled() {
        android.location.LocationManager lm = (android.location.LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            if (lm != null) {
                gps_enabled = lm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            network_enabled = lm.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled || network_enabled;


//        if(!gps_enabled && !network_enabled) {
//            // notify user
//            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//
////            new AlertDialog.Builder(context)
////                    .setMessage(R.string.gps_network_not_enabled)
////                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
////                        @Override
////                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
////                            getApplicationContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
////                        }
////                    }
////                            .setNegativeButton(R.string.Cancel,null)
////                            .show();
//        }

    }

    private class GeoThread extends Thread {
        int type;
        GeoThread(int type) {
            this.type=type;
        }

        @Override
        public void run() {

            BackgroundWorker backgroundWorker = new BackgroundWorker(EmployeeGeolocationPunch.this);
            String result = "";

            try {
                result = backgroundWorker.execute(
                        GEO_ATTEND,
                        email,
                        password,
                        Constants.DEMO_KDIPA_LOCATION ? String.valueOf(mDefaultLocation.latitude) : String.valueOf(mLastKnownLocation.getLatitude()),
                        Constants.DEMO_KDIPA_LOCATION ? String.valueOf(mDefaultLocation.longitude) : String.valueOf(mLastKnownLocation.getLongitude()),
                        String.valueOf(type)
                ).get();
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("Location")) {
                    String location = jsonObject.getString("Location");
                    successfulPunchTime = jsonObject.getString("Time");
                    if (!successfulPunchTime.equals("")) {
                        successfulPunchTime = Utilitaire.formateDateFromstring(Constants.DATE_INPUT_FORMAT, locale.equals("ar") ? Constants.FULL_DATE_AR_OUTPUT_FORMAT2 : Constants.FULL_DATE_EN_OUTPUT_FORMAT2, successfulPunchTime, locale);
                    }
                    switch (location) {
                        case "1": {
                            Message msg = Message.obtain();
                            msg.arg1 = 200;
                            postHandler.sendMessage(msg);
                            break;
                        }
                        case "2": {
                            Message msg = Message.obtain();
                            msg.arg1 = 200;
                            postHandler.sendMessage(msg);
                            break;
                        }
                        case "": {
                            Message msg = Message.obtain();
                            msg.arg1 = 0;
                            postHandler.sendMessage(msg);
                            break;
                        }
                    }
                } else {
                    Message msg = Message.obtain();
                    msg.arg1 = 404;
                    postHandler.sendMessage(msg);
                }
                Log.d(TAG, "submit: result: " + result);
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class LoadingThread extends Thread {
        LoadingThread() {
        }
        @Override
        public void run() {
            BackgroundWorker backgroundWorker = new BackgroundWorker(EmployeeGeolocationPunch.this, EmployeeGeolocationPunch.this::showError);
            String result = "";
            try {
                result = backgroundWorker.execute(
                        GEO_ATTEND_CHECK,
                        email,
                        password,
                        Constants.DEMO_KDIPA_LOCATION ? String.valueOf(mDefaultLocation.latitude) : String.valueOf(mLastKnownLocation.getLatitude()),
                        Constants.DEMO_KDIPA_LOCATION ? String.valueOf(mDefaultLocation.longitude) : String.valueOf(mLastKnownLocation.getLongitude())
                ).get();
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("Result")) {
                    String resultCode = jsonObject.getString("Result");
                    if (resultCode.equals("true")) {
                        Message msg = Message.obtain();
                        msg.arg1 = 200;
                        getHandler.sendMessage(msg);
                    } else {
                        Message msg = Message.obtain();
                        msg.arg1 = 0;
                        getHandler.sendMessage(msg);
                    }
                } else {
                    Message msg = Message.obtain();
                    msg.arg1 = 404;
                    postHandler.sendMessage(msg);
                }
            } catch (ExecutionException | InterruptedException | JSONException e) {
                e.printStackTrace();
            }
        }
    }


    CountDownTimer cdt;

    private void stopTimer() {
        if (cdt != null)
            cdt.cancel();
        try {
            punchInButton.setVisibility(View.GONE);
            punchOutButton.setVisibility(View.GONE);
        } catch (Exception e) {
            Log.e("", "stopTimer: ");
        }
    }

    private void startTimer(long left) {
        stopTimer();
        punchInButton.setVisibility(View.VISIBLE);
        punchOutButton.setVisibility(View.VISIBLE);
        cdt = new CountDownTimer(left, 1000) {
            @SuppressLint("DefaultLocale")
            public void onTick(long millisUntilFinished) {
//                tvCounter.setText(String.format(new Locale("en"), "%d:%d",
//                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                stopTimer();
            }
        };
        cdt.start();
    }




}




