package com.ma.kdipa;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.drawerlayout.widget.DrawerLayout;

public class InvestorHowItWorks extends InvestorNavigationDrawer {


protected DrawerLayout mDrawer;
WebView webView;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView;
        contentView = inflater.inflate(
                        R.layout.i_how_it_works,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        ImageView v =mDrawer.findViewById(R.id.how_iv);
        v.setImageResource(R.drawable.how_en);
        v.setTag(R.drawable.how_en);
        mDrawer.findViewById(R.id.how).setBackgroundColor(Color.parseColor("#F2F2F2"));


        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(getResources().getConfiguration().locale.getLanguage().equals("ar")?"https://www.kdipa.gov.kw/faq/":"https://www.kdipa.gov.kw/en/faq-2/");
    }
}
