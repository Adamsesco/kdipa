package com.ma.kdipa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.JavaResources.APIManager;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.MySpinnerAdapter;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterOfficialDuty;
import com.ma.kdipa.JavaResources.RobotoCalendarView;
import com.ma.kdipa.JavaResources.ShotOfficialDuty;
import com.ma.kdipa.JavaResources.Spinner;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.responses.OfficialDutySubmitObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;
import static com.ma.kdipa.JavaResources.Constants.YEAR_MONTH_DAY_OFFICIAL_DUTY;
import static com.ma.kdipa.JavaResources.Utilitaire.isInteger;

public class ManagerOfficialDuty extends EmployeeNavigationDrawer implements RobotoCalendarView.RobotoCalendarListener {


    private static final String TAG = "EmployeeOfficialDuty";
    protected DrawerLayout mDrawer;
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    ScreenShot screenShot;
    ArrayList<ArrayList<String>> employeesList = new ArrayList<>();
    ArrayList<String> delegatedEmployeesList = new ArrayList<>();
    TextView startFromTV;
    TextView startToTV;
    String startFromTxt = "";
    String startToTxt = "";
    RecyclerViewAdapterOfficialDuty recyclerViewAdapterOfficialDuty;
    RecyclerView recyclerView;
    DateType dateType;
    TextView submitTV;
    OfficialDutySubmitObject submitObject = new OfficialDutySubmitObject();
    String radioChoice = "";
    ShotOfficialDuty shotOfficialDuty;

    ProgressDialog progressDialog;
    Handler postHandler;
    Handler getHandler;
    DialogBoxLoadingClass loadingDialogBox;

    private Spinner dutyTypesSpinner;
//    private Spinner dropdownDelegatedEmployee;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                R.layout.m_official_duty,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);
        mDrawer.findViewById(R.id.official_duty).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);

        View v = mDrawer.findViewById(R.id.logout_iv);
        v.setTag("Not null");


        globalFindViewById();
        initialization();
        setDefaultValues();

        loadData();

        processTheCalendar();

        postHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {

            }
        };

        getHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1 == 1) {
                    loadingDialogBox.dismiss();
                    setData();
                } else {
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(ManagerOfficialDuty.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    Toast.makeText(ManagerOfficialDuty.this, R.string.an_error_occured, Toast.LENGTH_SHORT).show();
                }
            }
        };

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getHandler.removeCallbacksAndMessages(null);
        postHandler.removeCallbacksAndMessages(null);
    }


    private void setData() {
        startFromTV.setText(Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY, screenShot.getFromDate(), locale));
        startFromTxt = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, YEAR_MONTH_DAY_OFFICIAL_DUTY, screenShot.getFromDate(), "en");
        startToTV.setText(Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY, screenShot.getToDate(), locale));
        startToTxt = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, YEAR_MONTH_DAY_OFFICIAL_DUTY, screenShot.getToDate(), "en");
        Log.d(TAG, "setData: startToTxt: " + startToTxt);
        ArrayList<String> dutyTypesItemList = getSpecificFieldFromArray(screenShot.getDutyTypesItemArray());
        ArrayList<String> dutyTypesArItemList = getSpecificFieldFromArray(screenShot.getDutyTypesArItemArray());

        //set the spinners adapter to the previously created one.
        MySpinnerAdapter adapter = new MySpinnerAdapter(this,
                locale.equals("ar") ? dutyTypesArItemList : dutyTypesItemList
        );
        dutyTypesSpinner.setAdapter(adapter);
        dutyTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayList<String> employeeNameList = getSpecificFieldFromArray(screenShot.getEmployeesItemArray());
        ArrayList<String> employeeArNameList = getSpecificFieldFromArray(screenShot.getEmployeesItemArray());

        MySpinnerAdapter adapter2 = new MySpinnerAdapter(this,
                locale.equals("ar") ? employeeArNameList : employeeNameList
        );


        ArrayList<String> employeeDNameList = getSpecificFieldFromArray(screenShot.getDelegatedEmployeesItemArray());
        ArrayList<String> employeeDArNameList = getSpecificFieldFromArray(screenShot.getDelegatedEmployeesItemArray());

        employeesList.add(locale.equals("ar") ? employeeArNameList : employeeNameList);
        delegatedEmployeesList = locale.equals("ar") ? employeeDArNameList : employeeDNameList;

        recyclerViewAdapterOfficialDuty = new RecyclerViewAdapterOfficialDuty(employeesList, delegatedEmployeesList, locale);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapterOfficialDuty);


        TextView punchIn = findViewById(R.id.punch_in);
        punchIn.setText(locale.equals("ar") ? screenShot.getAttendanceExemptionArItemArray().get(0) : screenShot.getAttendanceExemptionItemArray().get(0));
        //in the xml, it is radio 1 which is selected by default, so we prepare submitting radio:
//        radioChoice = locale.equals("ar") ? screenShot.getAttendanceExemptionArItemArray().get(0) : screenShot.getAttendanceExemptionItemArray().get(0);
        radioChoice = "1";

        TextView punchOut = findViewById(R.id.punch_out);
        punchOut.setText(locale.equals("ar") ? screenShot.getAttendanceExemptionArItemArray().get(1) : screenShot.getAttendanceExemptionItemArray().get(1));

        TextView punchInOut = findViewById(R.id.punchInAndOut);
        punchInOut.setText(locale.equals("ar") ? screenShot.getAttendanceExemptionArItemArray().get(2) : screenShot.getAttendanceExemptionItemArray().get(2));

    }

    private ArrayList<String> getSpecificFieldFromArray(ArrayList<?> array) {
        ArrayList<String> list = new ArrayList<>();

        if (EmployeesItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((EmployeesItem) array.get(i)).getEmployeeName());
            }
        }
        if (EmployeesArItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((EmployeesArItem) array.get(i)).getEmployeeName());
            }
        }
        if (DelegatedEmployeesItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((DelegatedEmployeesItem) array.get(i)).getEmployeeName());
            }
        }
        if (DelegatedEmployeesArItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((DelegatedEmployeesArItem) array.get(i)).getEmployeeName());
            }
        }
        if (DutyTypesItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((DutyTypesItem) array.get(i)).getDutyName());
            }
        }
        if (DutyTypesArItem.class.equals(array.get(0).getClass())) {
            for (int i = 0; i < array.size(); i++) {
                list.add(((DutyTypesArItem) array.get(i)).getDutyName());
            }
        }

        return list;
    }


    private void startActivityEmployeeDashboard() {
        Intent intent = new Intent(getApplicationContext(), EmployeeDashboard.class);
        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }


    private void setDefaultValues() {
        findViewById(R.id.hidable).setVisibility(View.INVISIBLE);
    }

    private void initialization() {

    }

    private void loadData() {

        if (Utilitaire.isNetworkAvailable(this)) {
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread = new LoadingThread();
            loadingThread.start();
        } else {

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                    .setMode(DialogBoxCustom2.ERROR)
                    .setTitle(getString(R.string.error))
                    .setBody(getString(R.string.connection_problem_reload))
                    .setPositive(getString(R.string.retry))
                    .create().show();
        }

    }


    private void globalFindViewById() {
        dutyTypesSpinner = findViewById(R.id.spinnerDutyType);
        startFromTV = findViewById(R.id.startFrom);
        startToTV = findViewById(R.id.startTo);
        recyclerView = findViewById(R.id.delegatedRV);
        submitTV = findViewById(R.id.submit);

    }

    public void radio1Click(View view) {

        ((ImageView) findViewById(R.id.radio1)).setImageResource(R.drawable.radio_checked_icon);
        ((ImageView) findViewById(R.id.radio2)).setImageResource(R.drawable.radio_unchecked_icon);
        ((ImageView) findViewById(R.id.radio3)).setImageResource(R.drawable.radio_unchecked_icon);
//        radioChoice = "Punch In";
        radioChoice = "1";

        Typeface face1;
        Typeface face2;
        Typeface face3;

        if (!getResources().getConfiguration().locale.getLanguage().equals("ar")) {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
        } else {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
        }

        ((TextView) findViewById(R.id.punch_in)).setTypeface(face1);
        ((TextView) findViewById(R.id.punch_out)).setTypeface(face2);
        ((TextView) findViewById(R.id.punchInAndOut)).setTypeface(face3);


    }

    public void radio2Click(View view) {

        ((ImageView) findViewById(R.id.radio1)).setImageResource(R.drawable.radio_unchecked_icon);
        ((ImageView) findViewById(R.id.radio2)).setImageResource(R.drawable.radio_checked_icon);
        ((ImageView) findViewById(R.id.radio3)).setImageResource(R.drawable.radio_unchecked_icon);
//        radioChoice = "Punch Out";
        radioChoice = "2";
        Typeface face1;
        Typeface face2;
        Typeface face3;
        if (!getResources().getConfiguration().locale.getLanguage().equals("ar")) {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
        } else {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
        }


        ((TextView) findViewById(R.id.punch_in)).setTypeface(face1);
        ((TextView) findViewById(R.id.punch_out)).setTypeface(face2);
        ((TextView) findViewById(R.id.punchInAndOut)).setTypeface(face3);


    }

    public void radio3Click(View view) {

        ((ImageView) findViewById(R.id.radio1)).setImageResource(R.drawable.radio_unchecked_icon);
        ((ImageView) findViewById(R.id.radio2)).setImageResource(R.drawable.radio_unchecked_icon);
        ((ImageView) findViewById(R.id.radio3)).setImageResource(R.drawable.radio_checked_icon);
//        radioChoice = "Punch In And Out";
        radioChoice = "3";

        Typeface face1;
        Typeface face2;
        Typeface face3;
        if (!getResources().getConfiguration().locale.getLanguage().equals("ar")) {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        } else {
            face1 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face2 = ResourcesCompat.getFont(this, R.font.frutiger_regular);
            face3 = ResourcesCompat.getFont(this, R.font.frutiger_bold);
        }


        ((TextView) findViewById(R.id.punch_in)).setTypeface(face1);
        ((TextView) findViewById(R.id.punch_out)).setTypeface(face2);
        ((TextView) findViewById(R.id.punchInAndOut)).setTypeface(face3);


    }


    public void deleteDepartureDate(View view) {
        startFromTV.setText("");
        startFromTxt = "";
        dateType = DateType.START_DATE;
        showCalendar();
    }

    public void deleteReturnDate(View view) {

        startToTV.setText("");
        startToTxt = "";
        dateType = DateType.TO_DATE;
        showCalendar();
    }


    public void deleteStartDate(View view) {
        startFromTV.setText("");
        startFromTxt = "";
        dateType = DateType.START_DATE;
        showCalendar();
    }

    public void deleteToDate(View view) {
        startToTV.setText("");
        startToTxt = "";
        dateType = DateType.TO_DATE;

        showCalendar();
    }

    public void showCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(-constraintLayout.getWidth(), 0, 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.INVISIBLE || constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                constraintLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void hideCalendar() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, -constraintLayout.getWidth(), 0, 0);
        translateAnimation.setDuration(300);

        if (constraintLayout.getVisibility() == View.VISIBLE) {
            constraintLayout.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                constraintLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onDayClick(Date date) {
        String locale = getResources().getConfiguration().locale.getLanguage();
        SimpleDateFormat formatForDisplay = new SimpleDateFormat(
                locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY,
                new Locale(locale));
        String displayed = formatForDisplay.format(date);
        displayed = (locale.equals("ar")) ? Utilitaire.replaceArabicNumbers(displayed) : displayed;

        SimpleDateFormat formatForOutput = new SimpleDateFormat(Constants.YEAR_MONTH_DAY_OFFICIAL_DUTY, new Locale("en"));
        hideCalendar();
        switch (dateType) {
            case START_DATE:
                startFromTV.setText(displayed);
                startFromTxt = formatForOutput.format(date);
                submitObject.setFromDate(startFromTxt);
                break;
            case TO_DATE:
                startToTV.setText(displayed);
                startToTxt = formatForOutput.format(date);
                submitObject.setToDate(startToTxt);
                break;
        }
    }

    private void processTheCalendar() {
        RobotoCalendarView robotoCalendarView = mDrawer.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }

    @Override
    public void onDayLongClick(Date date) {

    }

    @Override
    public void onRightButtonClick() {

    }

    @Override
    public void onLeftButtonClick() {

    }

    @Override
    public void onBackPressed() {
        ConstraintLayout constraintLayout = findViewById(R.id.hidable);
        if (constraintLayout.getVisibility() == View.VISIBLE) {
            hideCalendar();
            Log.d(TAG, "backClick: backClick hide calendar");
        } else {
            super.onBackPressed();
            Log.d(TAG, "backClick: backClick super");
        }


    }

    public void submitClick(View view) {

        if (validate()) {
            ArrayList<String> selectedEmployeesList = recyclerViewAdapterOfficialDuty.getSelectedEmployees();
            StringBuilder employeesTxt0 = new StringBuilder();
            for (int i = 0; i < selectedEmployeesList.size(); i++) {
                String selectedEmployeeName = selectedEmployeesList.get(i);
                int employeeIndex = employeesList.get(0).indexOf(selectedEmployeeName);
                employeesTxt0.append(screenShot.employeesItemArray.get(employeeIndex).employeeID).append(",");

            }
            String employeesTxt = employeesTxt0.toString();
            employeesTxt = employeesTxt.substring(0, employeesTxt.length() - 1);
            submitObject.setEmployees(employeesTxt);

            submitObject.setEmail(email);
            submitObject.setPassword(password);
            submitObject.setDutyType(screenShot.getDutyTypesItemArray().get(dutyTypesSpinner.getSelectedItemPosition()).getDutyName());
            submitObject.setFromDate(startFromTxt);
            submitObject.setToDate(startToTxt);
            submitObject.setAttendanceExemption(radioChoice);

            StringBuilder delegatedTxt0 = new StringBuilder();
            ArrayList<String> selectedDelegatedList = recyclerViewAdapterOfficialDuty.getSelectedDelegates();

            for (int i = 0; i < selectedDelegatedList.size(); i++) {

                String selectedDelegatedName = selectedDelegatedList.get(i);
                int DelegatedIndex = delegatedEmployeesList.indexOf(selectedDelegatedName);
                delegatedTxt0.append(screenShot.delegatedEmployeesItemArray.get(DelegatedIndex).employeeID).append(",");

            }
            String delegatedTxt = delegatedTxt0.toString();
            delegatedTxt = delegatedTxt.substring(0, delegatedTxt.length() - 1);


            submitObject.setDelegatedEmployees(delegatedTxt);


            submitTV.setEnabled(false);


//            SubmitThread submitThread = new SubmitThread();
//            submitThread.start();
            showLoading();
            APIManager.retrofitEmployee.create(APIManager.class).postNewDuty(submitObject).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    hideLoading();
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        submitTV.setEnabled(true);
                        new DialogBoxCustom2.DialogBoxCustom2Builder(ManagerOfficialDuty.this)
                                .setMode(DialogBoxCustom2.SUCCESS)
                                .setTitle(getString(R.string.success))
                                .setBody(getString(R.string.submitted_successfully))
                                .setPositive(getString(R.string.continuee))
                                .setOnPositive(() -> {
                                    startActivityEmployeeDashboard();
                                })
                                .create().show();

                    } else {
                        progressDialog.dismiss();
                        submitTV.setEnabled(true);
                        new DialogBoxCustom2.DialogBoxCustom2Builder(ManagerOfficialDuty.this)
                                .setMode(DialogBoxCustom2.ERROR)
                                .setTitle(getString(R.string.error))
                                .setBody(getString(R.string.an_error_occured))
                                .setPositive(getString(R.string.cancel))
                                .create().show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideLoading();
                    t.printStackTrace();
                    progressDialog.dismiss();
                    submitTV.setEnabled(true);
                    new DialogBoxCustom2.DialogBoxCustom2Builder(ManagerOfficialDuty.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
                }
            });


        } else {
            Toast.makeText(this, R.string.request_checking_fields, Toast.LENGTH_SHORT).show();
        }
    }



    boolean validate() {
        if (dutyTypesSpinner.getSelectedItemPosition() == -1) {
            Toast.makeText(this, getString(R.string.select_an_item), Toast.LENGTH_SHORT).show();
            return false;
        }
        return !startFromTxt.equals("") && !startToTxt.equals("");
    }

    private void submit() {
        try {
            Log.d(TAG, "submit: " + submitObject.toString());
            String responseCode = new BackgroundWorker(this).execute(
                    "postNewDuty",
                    submitObject.getEmail(),
                    submitObject.getPassword(),
                    submitObject.getDutyType(),
                    submitObject.getFromDate(),
                    submitObject.getToDate(),
                    submitObject.getAttendanceExemption(),
                    submitObject.getEmployees(),
                    submitObject.getDelegatedEmployees()
            ).get();
            Message msg = Message.obtain();
            if (isInteger(responseCode)) {
                msg.arg1 = Integer.parseInt(responseCode);
            } else {
                msg.arg1 = 7;
            }
            postHandler.sendMessage(msg);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Message msg = Message.obtain();
            msg.arg1 = 0;
            postHandler.sendMessage(msg);
        }

    }

    enum DateType {
        START_DATE,
        TO_DATE
    }

    private class SubmitThread extends Thread {

        SubmitThread() {
        }

        @Override
        public void run() {
            submit();
        }

    }

    private class EmployeesItem {
        String employeeID;
        String employeeName;

        public EmployeesItem(String employeeID, String employeeName) {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }
    }

    private class EmployeesArItem {
        String employeeID;
        String employeeName;

        public EmployeesArItem(String employeeID, String employeeName) {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }
    }

    private class DelegatedEmployeesItem {
        String employeeID;
        String employeeName;

        public DelegatedEmployeesItem(String employeeID, String employeeName) {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }
    }

    private class DelegatedEmployeesArItem {
        String employeeID;
        String employeeName;

        public DelegatedEmployeesArItem(String employeeID, String employeeName) {
            this.employeeID = employeeID;
            this.employeeName = employeeName;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }
    }

    private class ScreenShot {
        String dutyTypes;
        String dutyTypesAr;
        ArrayList<EmployeesItem> employeesItemArray;
        ArrayList<EmployeesArItem> employeesArItemArray;
        ArrayList<String> attendanceExemptionItemArray;
        ArrayList<String> attendanceExemptionArItemArray;
        ArrayList<DelegatedEmployeesItem> delegatedEmployeesItemArray;
        ArrayList<DelegatedEmployeesArItem> delegatedEmployeesArItemArray;
        ArrayList<DutyTypesItem> dutyTypesItemArray;
        ArrayList<DutyTypesArItem> dutyTypesArItemArray;
        String fromDate;
        String toDate;


        public ScreenShot(String dutyTypes, String dutyTypesAr, ArrayList<EmployeesItem> employeesItemArray, ArrayList<EmployeesArItem> employeesArItemArray, ArrayList<String> attendanceExemptionItemArray, ArrayList<String> attendanceExemptionArItemArray, ArrayList<DelegatedEmployeesItem> delegatedEmployeesItemArray, ArrayList<DelegatedEmployeesArItem> delegatedEmployeesArItemArray, ArrayList<DutyTypesItem> dutyTypesItemArray, ArrayList<DutyTypesArItem> dutyTypesArItemArray, String fromDate, String toDate) {
            this.dutyTypes = dutyTypes;
            this.dutyTypesAr = dutyTypesAr;
            this.employeesItemArray = employeesItemArray;
            this.employeesArItemArray = employeesArItemArray;
            this.attendanceExemptionItemArray = attendanceExemptionItemArray;
            this.attendanceExemptionArItemArray = attendanceExemptionArItemArray;
            this.delegatedEmployeesItemArray = delegatedEmployeesItemArray;
            this.delegatedEmployeesArItemArray = delegatedEmployeesArItemArray;
            this.dutyTypesItemArray = dutyTypesItemArray;
            this.dutyTypesArItemArray = dutyTypesArItemArray;

//            this.fromDate = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY, fromDate, locale);
//            this.toDate = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar") ? CHOOSE_DATE_AR_DISPLAY : CHOOSE_DATE_DISPLAY, toDate, locale);
            this.fromDate = fromDate;
            this.toDate = toDate;


        }

        public String getDutyTypes() {
            return dutyTypes;
        }

        public void setDutyTypes(String dutyTypes) {
            this.dutyTypes = dutyTypes;
        }

        public String getDutyTypesAr() {
            return dutyTypesAr;
        }

        public void setDutyTypesAr(String dutyTypesAr) {
            this.dutyTypesAr = dutyTypesAr;
        }

        public ArrayList<EmployeesItem> getEmployeesItemArray() {
            return employeesItemArray;
        }

        public void setEmployeesItemArray(ArrayList<EmployeesItem> employeesItemArray) {
            this.employeesItemArray = employeesItemArray;
        }

        public ArrayList<EmployeesArItem> getEmployeesArItemArray() {
            return employeesArItemArray;
        }

        public void setEmployeesArItemArray(ArrayList<EmployeesArItem> employeesArItemArray) {
            this.employeesArItemArray = employeesArItemArray;
        }

        public ArrayList<String> getAttendanceExemptionItemArray() {
            return attendanceExemptionItemArray;
        }

        public void setAttendanceExemptionItemArray(ArrayList<String> attendanceExemptionItemArray) {
            this.attendanceExemptionItemArray = attendanceExemptionItemArray;
        }

        public ArrayList<String> getAttendanceExemptionArItemArray() {
            return attendanceExemptionArItemArray;
        }

        public void setAttendanceExemptionArItemArray(ArrayList<String> attendanceExemptionArItemArray) {
            this.attendanceExemptionArItemArray = attendanceExemptionArItemArray;
        }

        public ArrayList<DelegatedEmployeesItem> getDelegatedEmployeesItemArray() {
            return delegatedEmployeesItemArray;
        }

        public void setDelegatedEmployeesItemArray(ArrayList<DelegatedEmployeesItem> delegatedEmployeesItemArray) {
            this.delegatedEmployeesItemArray = delegatedEmployeesItemArray;
        }

        public ArrayList<DelegatedEmployeesArItem> getDelegatedEmployeesArItemArray() {
            return delegatedEmployeesArItemArray;
        }

        public void setDelegatedEmployeesArItemArray(ArrayList<DelegatedEmployeesArItem> delegatedEmployeesArItemArray) {
            this.delegatedEmployeesArItemArray = delegatedEmployeesArItemArray;
        }

        public ArrayList<DutyTypesItem> getDutyTypesItemArray() {
            return dutyTypesItemArray;
        }

        public void setDutyTypesItemArray(ArrayList<DutyTypesItem> dutyTypesItemArray) {
            this.dutyTypesItemArray = dutyTypesItemArray;
        }

        public ArrayList<DutyTypesArItem> getDutyTypesArItemArray() {
            return dutyTypesArItemArray;
        }

        public void setDutyTypesArItemArray(ArrayList<DutyTypesArItem> dutyTypesArItemArray) {
            this.dutyTypesArItemArray = dutyTypesArItemArray;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        @Override
        public String toString() {
            return "ScreenShot{" +
                    "dutyTypes='" + dutyTypes + '\'' +
                    ", dutyTypesAr='" + dutyTypesAr + '\'' +
                    ", employeesItemArray=" + employeesItemArray +
                    ", employeesArItemArray=" + employeesArItemArray +
                    ", attendanceExemptionItemArray=" + attendanceExemptionItemArray +
                    ", attendanceExemptionArItemArray=" + attendanceExemptionArItemArray +
                    ", delegatedEmployeesItemArray=" + delegatedEmployeesItemArray +
                    ", delegatedEmployeesArItemArray=" + delegatedEmployeesArItemArray +
                    ", dutyTypesItemArray=" + dutyTypesItemArray +
                    ", dutyTypesArItemArray=" + dutyTypesArItemArray +
                    ", fromDate='" + fromDate + '\'' +
                    ", toDate='" + toDate + '\'' +
                    '}';
        }
    }


    private class DutyTypesItem {
        String dutyID;
        String dutyName;

        public DutyTypesItem(String dutyID, String dutyName) {
            this.dutyID = dutyID;
            this.dutyName = dutyName;
        }

        public String getDutyID() {
            return dutyID;
        }

        public void setDutyID(String dutyID) {
            this.dutyID = dutyID;
        }

        public String getDutyName() {
            return dutyName;
        }

        public void setDutyName(String dutyName) {
            this.dutyName = dutyName;
        }
    }

    private class DutyTypesArItem {
        String dutyID;
        String dutyName;

        public DutyTypesArItem(String dutyID, String dutyName) {
            this.dutyID = dutyID;
            this.dutyName = dutyName;
        }

        public String getDutyID() {
            return dutyID;
        }

        public void setDutyID(String dutyID) {
            this.dutyID = dutyID;
        }

        public String getDutyName() {
            return dutyName;
        }

        public void setDutyName(String dutyName) {
            this.dutyName = dutyName;
        }
    }


    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            BackgroundWorker backgroundWorker = new BackgroundWorker(ManagerOfficialDuty.this);
            try {
                String result = backgroundWorker.execute("newDuty", email, password).get();
                JSONObject jsonObject = new JSONObject(result);
                String dutyTypes = jsonObject.getString("DutyTypes");
                String dutyTypesAr = jsonObject.getString("DutyTypesAr");
                JSONArray employeesArray = jsonObject.getJSONArray("Employees");
                ArrayList<EmployeesItem> employeesItemArray = new ArrayList<>();
                for (int i = 0; i < employeesArray.length(); i++) {
                    JSONObject subObject = employeesArray.getJSONObject(i);
                    String employeeID = subObject.getString("EmployeeID");
                    String employeeName = subObject.getString("EmployeeName");
                    EmployeesItem employeesItem = new EmployeesItem(employeeID, employeeName);
                    employeesItemArray.add(employeesItem);
                }
                JSONArray employeesArArray = jsonObject.getJSONArray("EmployeesAr");
                ArrayList<EmployeesArItem> employeesArItemArray = new ArrayList<>();
                for (int i = 0; i < employeesArArray.length(); i++) {
                    JSONObject subObject = employeesArArray.getJSONObject(i);
                    String employeeID = subObject.getString("EmployeeID");
                    String employeeName = subObject.getString("EmployeeName");
                    EmployeesArItem employeesArItem = new EmployeesArItem(employeeID, employeeName);
                    employeesArItemArray.add(employeesArItem);
                }
                JSONArray attendanceExemptionArray = jsonObject.getJSONArray("attendanceExemption");
                ArrayList<String> attendanceExemptionItemArray = new ArrayList<>();
                for (int i = 0; i < attendanceExemptionArray.length(); i++) {
                    attendanceExemptionItemArray.add(attendanceExemptionArray.getString(i));
                }
                JSONArray attendanceExemptionArArray = jsonObject.getJSONArray("attendanceExemptionAr");
                ArrayList<String> attendanceExemptionArItemArray = new ArrayList<>();
                for (int i = 0; i < attendanceExemptionArArray.length(); i++) {
                    attendanceExemptionArItemArray.add(attendanceExemptionArArray.getString(i));
                }
                JSONArray delegatedEmployeesArray = jsonObject.getJSONArray("delegatedEmployees");
                ArrayList<DelegatedEmployeesItem> delegatedEmployeesItemArray = new ArrayList<>();
                for (int i = 0; i < delegatedEmployeesArray.length(); i++) {
                    JSONObject subObject = delegatedEmployeesArray.getJSONObject(i);
                    String employeeID = subObject.getString("EmployeeID");
                    String employeeName = subObject.getString("EmployeeName");
                    DelegatedEmployeesItem delegatedEmployeesItem = new DelegatedEmployeesItem(employeeID, employeeName);
                    delegatedEmployeesItemArray.add(delegatedEmployeesItem);
                }
                JSONArray delegatedEmployeesArArray = jsonObject.getJSONArray("delegatedEmployeesAr");
                ArrayList<DelegatedEmployeesArItem> delegatedEmployeesArItemArray = new ArrayList<>();
                for (int i = 0; i < delegatedEmployeesArArray.length(); i++) {
                    JSONObject subObject = delegatedEmployeesArArray.getJSONObject(i);
                    String employeeID = subObject.getString("EmployeeID");
                    String employeeName = subObject.getString("EmployeeName");
                    DelegatedEmployeesArItem delegatedEmployeesArItem = new DelegatedEmployeesArItem(employeeID, employeeName);
                    delegatedEmployeesArItemArray.add(delegatedEmployeesArItem);
                }
                JSONArray dutyTypesArray = jsonObject.getJSONArray("dutyTypes");
                ArrayList<DutyTypesItem> dutyTypesItemArray = new ArrayList<>();
                for (int i = 0; i < dutyTypesArray.length(); i++) {
                    JSONObject subObject = dutyTypesArray.getJSONObject(i);
                    String dutyID = subObject.getString("DutyID");
                    String dutyName = subObject.getString("DutyName");
                    DutyTypesItem dutyTypesItem = new DutyTypesItem(dutyID, dutyName);
                    dutyTypesItemArray.add(dutyTypesItem);
                }
                JSONArray dutyTypesArArray = jsonObject.getJSONArray("dutyTypesAr");
                ArrayList<DutyTypesArItem> dutyTypesArItemArray = new ArrayList<>();
                for (int i = 0; i < dutyTypesArArray.length(); i++) {
                    JSONObject subObject = dutyTypesArArray.getJSONObject(i);
                    String dutyID = subObject.getString("DutyID");
                    String dutyName = subObject.getString("DutyName");
                    DutyTypesArItem dutyTypesArItem = new DutyTypesArItem(dutyID, dutyName);
                    dutyTypesArItemArray.add(dutyTypesArItem);
                }
                String fromDate = jsonObject.getString("fromDate");
                String toDate = jsonObject.getString("toDate");
                screenShot = new ScreenShot(dutyTypes, dutyTypesAr, employeesItemArray, employeesArItemArray, attendanceExemptionItemArray, attendanceExemptionArItemArray, delegatedEmployeesItemArray, delegatedEmployeesArItemArray, dutyTypesItemArray, dutyTypesArItemArray, fromDate, toDate);
                Log.d(TAG, "run: screenShot: " + screenShot.toString());
                Message msg = Message.obtain();
                msg.arg1 = 1;
                getHandler.sendMessage(msg);

            } catch (JSONException | InterruptedException | ExecutionException e) {
                e.printStackTrace();
                Message msg = Message.obtain();
                msg.arg1 = 0;
                getHandler.sendMessage(msg);
            }
        }
    }

}
