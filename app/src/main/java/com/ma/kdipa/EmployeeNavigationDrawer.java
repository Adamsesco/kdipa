package com.ma.kdipa;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.FIRE_BASE_TOKEN;
import static com.ma.kdipa.JavaResources.Constants.FIRST_NAME;
import static com.ma.kdipa.JavaResources.Constants.ID;
import static com.ma.kdipa.JavaResources.Constants.IS_IN_FORGROUND;
import static com.ma.kdipa.JavaResources.Constants.IS_MANAGER;
import static com.ma.kdipa.JavaResources.Constants.LAST_NAME;
import static com.ma.kdipa.JavaResources.Constants.LOCALE;
import static com.ma.kdipa.JavaResources.Constants.MANAGER_ID;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.NOTIFICATION_INTENT;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.TOKEN;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.kennyc.bottomsheet.BottomSheetMenuDialogFragment;
import com.ma.kdipa.FilePicker.Image.ImagePicker;
import com.ma.kdipa.JavaResources.APIManager;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.Utils;
import com.ma.kdipa.expire_logique.ABFSApplication;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("Registered")
public class EmployeeNavigationDrawer extends AppCompatActivity {
    private static final String TAG = "ENavigationDrawer";
    private static int DELAY = 300;
    DialogBoxLoadingClass loadingDialogBox;
    SharedPreferences sharedPreferences;
    boolean isManager = false;
    SharedPreferences activityStateSharedPreferences;
    String name = "";
    String email = "";
    String password = "";
    String locale = "";
    String managerId = "";
    /*for drawerLayout*/
    int relevantSide;
    DrawerLayout.DrawerListener drawerListener;
    private DrawerLayout dl;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView nv;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            ((ImageView) findViewById(R.id.notification_header_icon)).setImageResource(R.drawable.notifications_en);
            Toast.makeText(getApplicationContext(), R.string.new_notification, Toast.LENGTH_SHORT).show();
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    };

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void showLoading() {
        loadingDialogBox = new DialogBoxLoadingClass(this);
        loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialogBox.show();
        loadingDialogBox.setCancelable(false);
        loadingDialogBox.setCanceledOnTouchOutside(false);
    }

    public void hideLoading() {
        if (loadingDialogBox != null) {
            loadingDialogBox.dismiss();
        }
    }

    public APIManager getAPIManager() {
        return APIManager.retrofitEmployee.create(APIManager.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD) && sharedPreferences.contains(IS_MANAGER) && sharedPreferences.contains(MANAGER_ID)) {
            email = sharedPreferences.getString(EMAIL, "");
            password = sharedPreferences.getString(PASSWORD, "");
            name = sharedPreferences.getString(NAME, "");
            isManager = sharedPreferences.getBoolean(IS_MANAGER, false);
            managerId = sharedPreferences.getString(MANAGER_ID, "");

        }
        if (sharedPreferences.contains(LOCALE)) {
            locale = sharedPreferences.getString(LOCALE, "");
        } else {
            locale = "en";
        }
        Utilitaire.refreshLocal(this);
        relevantSide = Gravity.START;


        if (isManager) {
            setContentView(R.layout.m_navigation_drawer_layout);
        } else {
            setContentView(
                    R.layout.e_navigation_drawer_layout);
            //this should be visible only as a manager
            findViewById(R.id.official_duty).setVisibility(View.GONE);
        }


        //navigation drawer processing
//        dl=(DrawerLayout) LayoutInflater.from(getApplicationContext()).inflate(R.layout.common_login, null, false).findViewById(R.id.activity_main);
        dl = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        handleCopyright();

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);


        dl.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        drawerListener = new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (findViewById(R.id.menu_content).getVisibility() == View.VISIBLE) {
                    findViewById(R.id.menu_content).setVisibility(View.GONE);
                    findViewById(R.id.request_content).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.menu_content).setVisibility(View.VISIBLE);
                    findViewById(R.id.request_content).setVisibility(View.GONE);
                }
                dl.openDrawer(relevantSide);
                dl.removeDrawerListener(drawerListener);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        };

        findViewById(R.id.complaintsAndSuggestions).setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://support.kdipa.gov.kw/?lang=en_US"));
            startActivity(browserIntent);
        });

    }

    private void handleCopyright() {
        @SuppressLint("SimpleDateFormat") String year = Utilitaire.replaceArabicNumbers(new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime()));
        String copyrightTxt = locale.equals("ar") ? "KDIPA © 2016–" + year + " حقوق الملكية" : "Copyright © 2016–" + year + " KDIPA";
        ((TextView) findViewById(R.id.copyright2)).setText(copyrightTxt);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    public void triggerNavDrawer(View view) {
        //hide keyboard:
        if (this.getCurrentFocus() != null && this.getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
        dl.openDrawer(relevantSide);
    }

    public void onTestButtonClick(View view) {
        Toast.makeText(this, "clicking was done successfully !", Toast.LENGTH_SHORT).show();
    }

    public void homeClick(View view) {
        ImageView iv = dl.findViewById(R.id.home_iv);
        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeDashboard.class);
                }
            }, DELAY);
        }
    }

    public void official_dutyClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(ManagerOfficialDuty.class);
            }
        }, DELAY);
    }

    public void absenceClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeAbsences.class);
            }
        }, DELAY);
    }

    public void sick_leaveClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(EmployeeNavigationDrawer.this, EmployeeSickLeave.class);
                intent.putExtra(Constants.IS_SICK_LEAVE_INTENT, true);
                startActivity(intent);
            }
        }, DELAY);
    }

    public void logoutClick(View view) {
//        ImageView iv= dl.findViewById(R.id.logout_iv);

        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void run() {
                sharedPreferences
                        .edit()
                        .remove(NAME)
                        .remove(FIRST_NAME)
                        .remove(LAST_NAME)
                        .remove(EMAIL)
                        .remove(PASSWORD)
                        .remove(TOKEN)
                        .remove(ID)
                        .remove(FIRE_BASE_TOKEN)
                        .remove(IS_MANAGER)
                        .remove(MANAGER_ID)
                        .commit();

                SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(Constants.LAST_LOGIN_USER_INFO, MODE_PRIVATE);
                sharedPreferences.edit()
                        .putString(EMAIL, email)
                        .putString(PASSWORD, password)
                        .apply();
                lunchActivity(CommonLogin.class);
//                Intent intent = new Intent(EmployeeNavigationDrawer.this, CommonLogin.class);
////                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                EmployeeNavigationDrawer.this.finish(); // if the activity running has it's own context

            }
        }, DELAY);
    }

    public void settingsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeSettings.class);
            }
        }, DELAY);
    }

    public void newRequestClick(View view) {
        dl.addDrawerListener(drawerListener);
        dl.closeDrawer(relevantSide);
    }

    public void backClick(View view) {
        dl.addDrawerListener(drawerListener);
        dl.closeDrawer(relevantSide);
    }

    public void historyInOutClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeHistoryInOut.class);
            }
        }, DELAY);
    }

    public void geolocationPunchClick(View view) {
        ImageView iv = dl.findViewById(R.id.geolocationPunch_iv);

        if (iv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeGeolocationPunch.class);
                }
            }, DELAY);
        }
    }

    public void notificationsClick(View view) {
//        dl.closeDrawer(relevantSide);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                lunchActivity(EmployeeNotifications.class);
//            }
//        }, DELAY);
        Toast.makeText(this, "Notifications click !", Toast.LENGTH_SHORT).show();
    }

    public void newsClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeNews.class);
            }
        }, DELAY);
    }

    public void permissionClick(View view) {
        View v = dl.findViewById(R.id.permission);
        if (v.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeePermission.class);
                }
            }, DELAY);
        }
    }

    public void vacationClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationsHistory.class);
            }
        }, DELAY);
    }

    public void vacation_returnClick(View view) {
        View tv = dl.findViewById(R.id.vacation_return);
        if (tv.getTag() == null) {
            dl.closeDrawer(relevantSide);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    lunchActivity(EmployeeVacationReturn.class);
                }
            }, DELAY);
        }
    }



    public void vacation_extensionClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationExtension.class);
            }
        }, DELAY);

    }

    public void vacation_amendmentClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeVacationAmendment.class);
            }
        }, DELAY);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        moveTaskToBack(true);
        if (dl.isDrawerOpen(relevantSide)) {
            dl.closeDrawer(relevantSide);
        } else {
            lunchActivity(EmployeeDashboard.class);
        }


    }

    void lunchActivity(Class<?> someClass) {
        Intent intent = new Intent(getApplicationContext(), someClass);

        Pair<View, String> p1 = Pair.create((View) findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create((View) findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(EmployeeNavigationDrawer.this, p1, p3);
        startActivity(intent, options.toBundle());
    }

    public void requestClick(View view) {
        dl.closeDrawer(relevantSide);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lunchActivity(EmployeeRequestStatus.class);
            }
        }, DELAY);

    }

    public void forRippleClick(View view) {
        //only for ripple purpuses
    }

    @Override
    public void onResume() {
        super.onResume();
        this.registerReceiver(mMessageReceiver, new IntentFilter(NOTIFICATION_INTENT));

        activityStateSharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        activityStateSharedPreferences
                .edit()
                .putBoolean(IS_IN_FORGROUND, true)
                .apply();
    }

    //Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mMessageReceiver);
        activityStateSharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        activityStateSharedPreferences
                .edit()
                .putBoolean(IS_IN_FORGROUND, false)
                .apply();
    }

    protected void showFilePickerSheet() {
        assert getFragmentManager() != null;
        new BottomSheetMenuDialogFragment.Builder(this, R.style.bottomSheetCustom)
                .setSheet(R.menu.file_picker_sheet)
                .setTitle(R.string.choose_action)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onSheetShown(@NonNull BottomSheetMenuDialogFragment bottomSheet, @Nullable Object object) {
                    }

                    @Override
                    public void onSheetItemSelected(@NonNull BottomSheetMenuDialogFragment bottomSheet, MenuItem item, @Nullable Object object) {
                        switch (item.getItemId()) {
                            case R.id.gallery:
                                if (!areAllPermissionsGranted()) return;
                                new ImagePicker.Builder(EmployeeNavigationDrawer.this)
                                        .mode(ImagePicker.Mode.GALLERY)
                                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                        .extension(ImagePicker.Extension.PNG)
                                        .scale(600, 600)
                                        .allowMultipleImages(false)
                                        .directory(String.valueOf(Environment.getExternalStorageDirectory()))
                                        .enableDebuggingMode(true)
                                        .build();
                                break;
                            case R.id.camera:
                                if (!areAllPermissionsGranted()) return;

                                new ImagePicker.Builder(EmployeeNavigationDrawer.this)
                                        .mode(ImagePicker.Mode.CAMERA)
                                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                        .extension(ImagePicker.Extension.PNG)
                                        .scale(600, 600)
                                        .allowMultipleImages(false)
                                        .enableDebuggingMode(true)
                                        .build();
                                break;
//                            case R.id.pdf:
//                                if (! areAllPermissionsGranted() )return;
//
//                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                                intent.setType("*/*");
//                                startActivityForResult(intent,PICKFILE_RESULT_CODE);
//                                break;
                        }
                    }

                    @Override
                    public void onSheetDismissed(@NonNull BottomSheetMenuDialogFragment bottomSheet, @Nullable Object object, int dismissEvent) {

                    }
                }).show(getSupportFragmentManager());

    }

    protected boolean areAllPermissionsGranted() {
        // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

            return false;
        }

        return true;

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (getApp().wasInBackground()) {
            getApp().setInBackGround(false);
            String lastTimeConnected = Utils.getValue(this, Constants.LAST_TIME_CONNECTED, null);
            if (lastTimeConnected == null) return;
            Date date = Utilitaire.stringToDate(lastTimeConnected, Constants.DATE_INPUT_FORMAT);
            Date currentTime = Calendar.getInstance().getTime();
            long diff = currentTime.getTime() - date.getTime();
            if (diff >= Constants.EXPIRY_TIME) {
                logoutClick(null);
            }
        }
    }

    public ABFSApplication getApp() {
        return (ABFSApplication) this.getApplication();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        getApp().touch();
    }


}
