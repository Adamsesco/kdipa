package com.ma.kdipa.JavaResources;

import android.graphics.Color;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

public class AnimationTextViewColor extends Animation {

    private TextView textView;
    private int     fromRed;
    private int     toRed;
    private int     fromGreen;
    private int     toGreen;
    private int     fromBlue;
    private int     toBlue;

    public AnimationTextViewColor(
            TextView textView,
            int from,
            int to
    ) {
        super();
        this.textView = textView;

        this.fromRed = Color.red(from);
        this.fromGreen = Color.green(from);
        this.fromBlue =Color.blue(from);


        this.toRed = Color.red(to);;

        this.toGreen = Color.green(to);

        this.toBlue = Color.blue(to);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        int valueRed = (int) (fromRed + (toRed - fromRed) * interpolatedTime);
        int valueGreen = (int) (fromGreen + (toGreen - fromGreen) * interpolatedTime);
        int valueBlue = (int) (fromBlue + (toBlue - fromBlue) * interpolatedTime);

        textView.setTextColor(Utilitaire.getIntFromColor(valueRed,valueGreen,valueBlue));

    }

}

