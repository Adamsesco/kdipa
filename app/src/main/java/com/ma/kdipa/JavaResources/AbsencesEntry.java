package com.ma.kdipa.JavaResources;

import android.content.Context;

import static com.ma.kdipa.JavaResources.Constants.DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;

public class AbsencesEntry {
    String number;
    String date;

    public AbsencesEntry(String number, String date, Context c) {
        this.number = number;
        String locale=c.getResources().getConfiguration().locale.getLanguage();
        this.date=Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?DATE_AR_OUTPUT_FORMAT:DATE_EN_OUTPUT_FORMAT,date, locale);
        this.date=Utilitaire.replaceArabicNumbers(this.date);
    }

    public String getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
