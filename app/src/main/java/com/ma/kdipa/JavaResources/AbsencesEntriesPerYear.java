package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class AbsencesEntriesPerYear {
    Integer year;
    ArrayList<AbsencesEntry> entriesList;

    public AbsencesEntriesPerYear(Integer year, ArrayList<AbsencesEntry> entriesList) {
        this.year = year;
        this.entriesList = entriesList;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setEntriesList(ArrayList<AbsencesEntry> datesList) {
        this.entriesList = entriesList;
    }

    public ArrayList<AbsencesEntry> getEntriesList() {
        return entriesList;
    }


}
