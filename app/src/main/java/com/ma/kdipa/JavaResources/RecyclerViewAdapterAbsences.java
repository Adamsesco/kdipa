package com.ma.kdipa.JavaResources;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class RecyclerViewAdapterAbsences extends RecyclerView.Adapter<RecyclerViewAdapterAbsences.MyViewHolder> {


    private ArrayList<AbsencesEntry> mDataset;
    private RecyclerView recycler;
    private static final String TAG = "RecyclerViewAdapterAbse";

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView numberTV;
        TextView dateTV;

        View v;
        MyViewHolder(View view) {
            super(view);
            this.numberTV = view.findViewById(R.id.number);
            this.dateTV = view.findViewById(R.id.date);
            this.v = view;
        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterAbsences(ArrayList<AbsencesEntry> myDataset) {
        mDataset = myDataset;

    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        Log.d(TAG, "onCreateViewHolder: language: "+parent.getContext().getResources().getConfiguration().locale.getLanguage());
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_absences_element,
                parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.setIsRecyclable(false);
//        recycler.getRecycledViewPool().setMaxRecycledViews(holder.getItemViewType(), 0);

        holder.numberTV.setText(mDataset.get(position).getNumber());
        holder.dateTV.setText(mDataset.get(position).getDate());

    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        String stringId=mDataset.get(position).getNumber();
        return Integer.parseInt(stringId);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
}


