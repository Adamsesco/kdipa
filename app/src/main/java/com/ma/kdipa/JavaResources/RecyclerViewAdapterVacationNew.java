package com.ma.kdipa.JavaResources;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.io.File;
import java.util.ArrayList;

public class RecyclerViewAdapterVacationNew extends RecyclerView.Adapter<RecyclerViewAdapterVacationNew.MyViewHolder> {


    private static final String TAG = "AdapterEmergencyLeave";
    private final int width;
    private final int height;
    private ArrayList<String> mDataset;
    private RecyclerView recycler;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        ImageView delete;


        View v;
        MyViewHolder(View view) {
            super(view);
            this.iv = view.findViewById(R.id.iv);
            this.delete=view.findViewById(R.id.delete);
            this.v = view;
        }
        View getView() {
            return v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterVacationNew(ArrayList<String> myDataset, int width, int height) {
        mDataset = myDataset;
        this.width=width;
        this.height=height;
    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.e_emergency_leave_element, parent, false);

        v.getLayoutParams().width=(int) (width/5.0f);
        v.getLayoutParams().height=(int) (width/5.0f*297.0f/210.0f);

        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.iv.setImageURI(Uri.fromFile(new File(mDataset.get(position))));

        if(mDataset.get(position).indexOf("pdf")>0){
            Log.d(TAG, "onActivityResult: f is pdf and not in sdcard");
            holder.iv.setImageBitmap(Utilitaire.generateImageFromPdf(holder.iv.getContext(),Uri.fromFile(new File(mDataset.get(position))) ));
        }else {
            Log.d(TAG, "onActivityResult: f is an image and not in sd card");
            holder.iv.setImageURI(Uri.fromFile(new File(mDataset.get(position))));
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeAt(position);
            }
        });


    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        String stringId=mDataset.get(position);
        return Integer.parseInt(stringId);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    private void removeAt(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataset.size());
    }
}


