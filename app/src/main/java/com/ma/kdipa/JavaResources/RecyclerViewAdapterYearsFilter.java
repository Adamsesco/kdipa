package com.ma.kdipa.JavaResources;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class RecyclerViewAdapterYearsFilter extends RecyclerView.Adapter<RecyclerViewAdapterYearsFilter.MyViewHolder> {


    private static final int DURATION = 100;
    private ArrayList<Integer> mDataset;
    private RecyclerView recycler;
    private static int previousClicked=-1;

    public int getSelectedItemPosition(){
        return previousClicked;
    }
    public void selectLastElement(){
        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(DURATION);
        scaleAnimation.setFillAfter(true);
        ((MyViewHolder)recycler.findViewHolderForAdapterPosition(mDataset.size()-1)).yearTV.startAnimation(scaleAnimation);
//        TextView yearTV=((MyViewHolder)recycler.findViewHolderForAdapterPosition(previousClicked)).yearTV;
//
//        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1f, 1.5f, 1f, 1.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
//                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
//        scaleAnimation2.setDuration(DURATION);
//
//        AnimationTextViewColor animationTextViewColor2=new AnimationTextViewColor(
//                yearTV,
//                Color.parseColor("#C5CAD6"),
//                Color.parseColor("#ffffff")
//        );
//        animationTextViewColor2.setDuration(DURATION);
//
//        AnimationSet as2=new AnimationSet(true);
//        as2.addAnimation(animationTextViewColor2);
//        as2.addAnimation(scaleAnimation2);
//        as2.setFillAfter(true);
//
//        yearTV.startAnimation(as2);


        previousClicked=mDataset.size()-1;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView yearTV;

        View v;
        MyViewHolder(View view) {
            super(view);
            this.yearTV = view.findViewById(R.id.yearTV);
            this.v = view;
            this.yearTV.setTextSize(25);


            this.yearTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //process only if clicked view is not already highlighted:
                    if (previousClicked!=getAdapterPosition()){

                        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                        scaleAnimation.setDuration(DURATION);

                        AnimationTextViewColor animationTextViewColor=new AnimationTextViewColor(
                                yearTV,
                                Color.parseColor("#C5CAD6"),
                                Color.parseColor("#ffffff")
                        );
                        animationTextViewColor.setDuration(DURATION);

                        AnimationSet as=new AnimationSet(true);
                        as.addAnimation(animationTextViewColor);
                        as.addAnimation(scaleAnimation);
                        as.setFillAfter(true);

                        yearTV.startAnimation(as);

                        if (previousClicked!=-1){
                            if (recycler.findViewHolderForAdapterPosition(previousClicked)!=null){
                                TextView yearTV=((MyViewHolder)recycler.findViewHolderForAdapterPosition(previousClicked)).yearTV;

                                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.5f, 1f, 1.5f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                                        ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                                scaleAnimation2.setDuration(DURATION);

                                AnimationTextViewColor animationTextViewColor2=new AnimationTextViewColor(
                                        yearTV,
                                        Color.parseColor("#ffffff"),
                                        Color.parseColor("#C5CAD6")
                                );
                                animationTextViewColor2.setDuration(DURATION);

                                AnimationSet as2=new AnimationSet(true);
                                as2.addAnimation(animationTextViewColor2);
                                as2.addAnimation(scaleAnimation2);
                                as2.setFillAfter(true);
                                yearTV.startAnimation(as2);



//                            AnimationTextViewSize anim2 = new AnimationTextViewSize(
//                                    ((MyViewHolder)recycler.findViewHolderForAdapterPosition(previousClicked)).yearTV,
//                                    30f,
//                                    25f
//                            );
//                            anim2.setDuration(100);
//                            ((MyViewHolder)recycler.findViewHolderForAdapterPosition(previousClicked)).yearTV.startAnimation(anim2);
                            }
                        }
                        previousClicked=getAdapterPosition();
                    }

                }
            });
        }



        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterYearsFilter(ArrayList<Integer> myDataset) {
        mDataset = myDataset;

    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.e_absences_years_element, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.setIsRecyclable(false);
//        recycler.getRecycledViewPool().setMaxRecycledViews(holder.getItemViewType(), 0);

        holder.yearTV.setText(String.valueOf(mDataset.get(position)));

    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return mDataset.get(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
}


