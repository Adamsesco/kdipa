package com.ma.kdipa.JavaResources;

import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.util.ArrayList;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class RecyclerViewAdapterNotifs extends RecyclerView.Adapter<RecyclerViewAdapterNotifs.MyViewHolder> {

    private ArrayList<SingleRowNotifs> mDataset;
    private RecyclerView recycler;
    private long previousExpanded=-1;

    private static final String TAG = "RecyclerViewAdapterNoti";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView date;
        public TextView content;
        public ImageView notif_icon;

        View v;
        public MyViewHolder(View view) {
            super(view);
            this.title = view.findViewById(R.id.title);
            this.date = view.findViewById(R.id.date);
            this.content = view.findViewById(R.id.content);
            this.notif_icon = view.findViewById(R.id.notif_icon);
            this.v = view;
        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterNotifs(ArrayList<SingleRowNotifs> myDataset) {
        mDataset = myDataset;
        this.setHasStableIds(true);


    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_notifications_element,
                parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        View.OnClickListener onClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean expanded = mDataset.get(position).isExpanded();
                // Change the state
                mDataset.get(position).setExpanded(!expanded);
                // Notify the adapter that item has changed
                notifyItemChanged(position);
                /*
                if (isExpanded(holder)){
                    collapse(holder);
                    previousExpanded=-1;
                    Log.d(TAG, "onClick: collapsed");

                }else{
                    expand(holder,position);
                    collapse(holder, previousExpanded);
                    previousExpanded=holder.getItemId();
                    Log.d(TAG, "onClick: expand");
                }
//                AutoTransition autoTransition = new AutoTransition();
//                autoTransition.setDuration(200);
//                TransitionManager.beginDelayedTransition(recycler,autoTransition);

            */

            }
        };
        /*
        Log.d(TAG, "onBindViewHolder: holder.content.getHeight(): "+holder.content.getHeight());
        if (holder.getItemId()!=previousExpanded && holder.content.getHeight()!=0) {
            Log.d(TAG, "onBindViewHolder: holder collapsed");
            collapse(holder);
        }else if (holder.getItemId()==previousExpanded){
            expand(holder, position);
            Log.d(TAG, "onBindViewHolder: holder expanded");
        }

         */
        //
        boolean expanded=mDataset.get(position).isExpanded();
        holder.content.setVisibility(expanded ? View.VISIBLE : View.GONE);
        //
        holder.title.setText(mDataset.get(position).getTitle());
        holder.date.setText(mDataset.get(position).getDate());
        holder.content.setText(mDataset.get(position).getContent());
        holder.notif_icon.setImageResource(mDataset.get(position).getIconCode());
        if (mDataset.get(position).getIconCode()==R.drawable.notif_dis){
            holder.getView().setBackgroundColor(ContextCompat.getColor(holder.getView().getContext(),R.color.white));
            Typeface face=ResourcesCompat.getFont(holder.content.getContext(),R.font.frutiger_regular);
            holder.title.setTypeface(face);
        } else{
            holder.getView().setBackgroundColor(Utilitaire.getIntFromColor(247,247,247));
            Typeface face=ResourcesCompat.getFont(holder.content.getContext(),R.font.frutiger_bold);
            holder.title.setTypeface(face);

        }
        holder.getView().setOnClickListener(onClickListener);


    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private boolean isExpanded(MyViewHolder holder) {
        int dip= Utilitaire.px2dip(holder.content.getContext(),holder.content.getHeight());
        Log.d(TAG, "isExpanded: holder.content.getHeight() in dip : "+dip);
        Log.d(TAG, "isExpanded: holder.content.getHeight(): "+holder.content.getHeight());
        return dip!=1;
    }
    private void expand(MyViewHolder holder,int position){
        Animations.expand(holder.content);
//        holder.content.getLayoutParams().height=WRAP_CONTENT;
//        holder.content.requestLayout();

//        holder.content.setVisibility(View.VISIBLE);

        holder.notif_icon.setImageResource(R.drawable.notif_dis);
        holder.getView().setBackgroundColor(ContextCompat.getColor(holder.getView().getContext(),R.color.white));
        Typeface face=ResourcesCompat.getFont(holder.content.getContext(),R.font.frutiger_regular);
        holder.title.setTypeface(face);
        mDataset.get(position).setIconCode(R.drawable.notif_dis);
        InternalStorage.write(holder.title.getContext(), "notifs", String.valueOf(holder.getItemId()), true,true);

    }
    private void collapse(MyViewHolder holder){
//        holder.content.setVisibility(View.GONE);
        Animations.collapse(holder.content);
    }
    private void collapse(MyViewHolder currentHolder, long previousExpanded){
        if (previousExpanded != -1 && previousExpanded != currentHolder.getItemId()) {
            MyViewHolder vh = (MyViewHolder) recycler.findViewHolderForItemId(previousExpanded);
            if (vh != null) {
                collapse(vh);
            } else {
                Log.d(TAG, "onClick: vh null");
            }
        }
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
    @Override
    public long getItemId(int position) {
//        Log.d(TAG, "getItemId: called with position: "+ position);
        return position*10;
    }
}


