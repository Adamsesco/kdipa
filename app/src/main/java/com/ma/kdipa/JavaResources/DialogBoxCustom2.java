package com.ma.kdipa.JavaResources;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.ma.kdipa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogBoxCustom2 extends Dialog {
    public static final int POSITIVE = 1;
    public static final int NEGATIVE = -1;
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int WARNING = 2;
    public static final int INFO = 3;
    private static final String TAG = "DialogBoxClass";
    @BindView(R.id.container)
    public View container;
    @BindView(R.id.icon)
    public ImageView icon;

    @BindView(R.id.titleTV)
    public TextView titleTV;
    @BindView(R.id.contentTV)
    public TextView contentTV;
    @BindView(R.id.positiveTV)
    public TextView positiveTV;
    @Nullable
    @BindView(R.id.negativeTV)
    public TextView negativeTV;
    OnPositiveListener onPositiveListener;
    OnNegativeListener onNegativeListener;
    private String title = null;
    private String body = null;
    private String positive = null;
    private String negative = null;
    private int mode;
    private int drawableIcon=0;

    private DialogBoxCustom2(Context a, int mode, String title, String body, String positive, String negative, int drawableIcon, OnPositiveListener onPositiveListener, OnNegativeListener onNegativeListener) {
        super(a);
        this.mode = mode;
        this.title = title;
        this.body = body;
        this.positive = positive;
        this.negative = negative;
        this.drawableIcon = drawableIcon;
        this.onPositiveListener = onPositiveListener;
        this.onNegativeListener = onNegativeListener;
    }

    public void setOnPositiveListener(OnPositiveListener onPositiveListener) {
        this.onPositiveListener = onPositiveListener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_custom_2);
        ButterKnife.bind(this);
        if (title != null) {
            titleTV.setText(title);
        }
        if (body != null) {
            contentTV.setText(body);
        }
        if (positive != null) {
            positiveTV.setText(positive);
        }
        if (negative != null) {
            negativeTV.setText(negative);
        } else {
            negativeTV.setVisibility(View.GONE);
        }

        switch (mode) {
            case SUCCESS:
                container.setBackgroundColor(Color.WHITE);
                titleTV.setTextColor(Color.BLACK);
                contentTV.setTextColor(Color.parseColor("#5A5A5A"));
                icon.setImageDrawable(getContext().getDrawable(R.drawable.ic_check));
                positiveTV.setBackground(getContext().getDrawable(R.drawable.dialog_box_success));
                break;
            case ERROR:
                container.setBackgroundColor(Color.parseColor("#EBE45648"));
                titleTV.setTextColor(Color.WHITE);
                contentTV.setTextColor(Color.WHITE);
                icon.setImageDrawable(getContext().getDrawable(R.drawable.ic_error_icon));
                positiveTV.setBackground(getContext().getDrawable(R.drawable.dialog_box_error));
                break;
            default:
                container.setBackgroundColor(Color.WHITE);
                titleTV.setTextColor(Color.BLACK);
                contentTV.setTextColor(Color.parseColor("#5A5A5A"));
                icon.setImageDrawable(getContext().getDrawable(drawableIcon==0? R.drawable.ic_check:drawableIcon));
                positiveTV.setBackground(getContext().getDrawable(R.drawable.dialog_box_success));



        }

//        Display display = activity.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//        findViewById(R.id.container).getLayoutParams().width=(int) (width/1.5);
//        findViewById(R.id.container).getLayoutParams().height=height/3;


        positiveTV.setOnClickListener(view -> {
            dismiss();
            if (onPositiveListener != null) {
                onPositiveListener.onPositiveClick();
            }
        });
        if (negativeTV != null) {
            negativeTV.setOnClickListener(view -> {
                dismiss();
                if (onNegativeListener != null) {
                    onNegativeListener.onNegativeClick();
                }
            });
        }
    }

    @Override
    public void show() {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        super.show();
    }

    public void setTitle(String title) {
        positiveTV.setText(title);
    }

    public void setBody(String body) {
        contentTV.setText(body);
    }

    public void setPositive(String txt) {
        positiveTV.setText(txt);
    }

    public void setNegative(String txt) {
        negativeTV.setText(txt);
    }

    public void setData(String title, String body, String positive, String negative) {
        this.title = title;
        this.body = body;
        this.positive = positive;
        this.negative = negative;
    }

    public void setData(String title, String body, String positive) {
        this.title = title;
        this.body = body;
        this.positive = positive;
    }


    public interface OnPositiveListener  {
        void onPositiveClick();
    }
    public interface OnNegativeListener {
        void onNegativeClick();
    }



    public static class DialogBoxCustom2Builder {
        private Context a;
        private int mode;
        private String title;
        private String body;
        private String positive;
        private String negative;
        private int drawableIcon;
        private OnPositiveListener onPositiveListener;
        private OnNegativeListener onNegativeListener;

        public DialogBoxCustom2Builder(Context a) {
            this.a = a;
        }

        public DialogBoxCustom2Builder setA(Context a) {
            this.a = a;
            return this;
        }

        public DialogBoxCustom2Builder setMode(int mode) {
            this.mode = mode;
            return this;
        }

        public DialogBoxCustom2Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public DialogBoxCustom2Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public DialogBoxCustom2Builder setPositive(String positive) {
            this.positive = positive;
            return this;
        }

        public DialogBoxCustom2Builder setNegative(String negative) {
            this.negative = negative;
            return this;
        }

        public DialogBoxCustom2Builder setIcon(int drawableIcon) {
            this.drawableIcon = drawableIcon;
            return this;
        }

        public DialogBoxCustom2Builder setOnPositive(OnPositiveListener onPositiveListener) {
            this.onPositiveListener = onPositiveListener;
            return this;
        }
        public DialogBoxCustom2Builder setOnNegative(OnNegativeListener onNegativeListener) {
            this.onNegativeListener = onNegativeListener;
            return this;
        }

        public DialogBoxCustom2 create() {
            return new DialogBoxCustom2(a, mode, title, body, positive, negative, drawableIcon, onPositiveListener, onNegativeListener);
        }
    }
}
