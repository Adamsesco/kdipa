package com.ma.kdipa.JavaResources;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.storage.StorageManager;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.ma.kdipa.FilePicker.FileProcessing.getDataColumn;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT_WITHOUT_TIME_ZONE;
import static com.ma.kdipa.JavaResources.Constants.DATE_SIMPLE_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class Utilitaire {
    public static Date stringToDate(String aDate,String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }
    public static void refreshLocal(Context context) {
        try {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
            if (sharedPreferences.getString(Constants.LOCALE, "en").equals("en")) {
                sharedPreferences.edit().putString(Constants.LOCALE, "en").apply();
                updateLanguage(context, sharedPreferences);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }
                setSelectedLanguageId("en",context);

            } else {
                sharedPreferences.edit().putString(Constants.LOCALE, "ar").apply();
                updateLanguage(context, sharedPreferences);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                setSelectedLanguageId("ar", context);

            }
        } catch (Exception e) {
            Log.e("fff", "refreshLocal: ");
        }

    }
    public static void setSelectedLanguageId(String id, Context context){
        final SharedPreferences prefs = getDefaultSharedPreference(context.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("app_language_id", id);
        editor.apply();
    }
    private static SharedPreferences getDefaultSharedPreference(Context context) {
        if (PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()) != null)
            return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        else
            return null;
    }
    public static void updateLanguage(Context ctx, String lang) {

        Resources resources = ctx.getResources();
        Configuration cfg = resources.getConfiguration();
        if (!TextUtils.isEmpty(lang)) {
            cfg.locale = new Locale(lang);
        } else {
            cfg.locale = Locale.getDefault();
        }


        Locale.setDefault(cfg.locale);
        resources.updateConfiguration(cfg, resources.getDisplayMetrics());

    }

    public static void updateLanguage(Context cxt, SharedPreferences sharedPreferences) {
        Locale local = new Locale(sharedPreferences.getString(Constants.LOCALE, "en"));
        Locale.setDefault(local);
        Configuration configuration = cxt.getResources().getConfiguration();
        configuration.setLocale(local);
        cxt.getResources().updateConfiguration(configuration, cxt.getResources().getDisplayMetrics());

    }

    public static Drawable resizeDrawable(Context c, int drawableInt, int w, int h) {
        Drawable dr = ContextCompat.getDrawable(c, drawableInt);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }
    public static Drawable resizeDrawable(Context c, int drawableInt, int dim, boolean isFixedWidth) {
        Drawable dr = ContextCompat.getDrawable(c, drawableInt);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();

        float ratio=((float) bitmap.getWidth()/bitmap.getHeight());
        int width= isFixedWidth?dim: (int) (dim * ratio);
        int height= isFixedWidth?(int) (dim / ratio): dim;
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, width, height, true));
    }
    public static String encodeString(String str){
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String decodeString(String str){
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * this function check if the string in its parameter is a correct fomrmed email
     * @param email
     * @return
     */
    public static boolean isValid(String email) {
        return (email.indexOf("@")>0);
    }

    /**
     * overloading of the isValid(String email) function
     * @param email
     * @param password
     * @return
     */
    public static boolean isValid(String email, String password) {
//        return (email.indexOf("@")>0) && (password.length()>0);
        return  (email.length()>0) && (password.length()>0);
    }

    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isSecondDateHigher(String date1, String date2, String format, boolean strictly) {
        Log.d(TAG, "isSecondDateHigher() called with: date1 = [" + date1 + "], date2 = [" + date2 + "], format = [" + format + "]");
        SimpleDateFormat sdf = new SimpleDateFormat(format, new Locale("en"));
        Date strDate1;
        Date strDate2;
        try {
            strDate1 = sdf.parse(date1);
            strDate2 = sdf.parse(date2);
            if (strDate2 != null) {
                return strDate2.after(strDate1) || (!strictly && strDate1.equals(strDate2));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

    public static boolean exist(Context context, String txt, String fileName) {
        String notifsTxt= InternalStorage.readFullInternalStorage(context, fileName);
        Log.d(TAG, "exist: notifsTxt: "+notifsTxt);
        String[] strings=notifsTxt.split("\n");
        Log.d(TAG, "exist: datesArray: "+ Arrays.toString(strings));
        for (String s : strings) {
            if (txt.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static String replaceArabicNumbers(String original) {
        return original.replaceAll("١","1")
                .replaceAll("٢","2")
                .replaceAll("٣","3")
                .replaceAll("٤","4")
                .replaceAll("٥","5")
                .replaceAll("٦","6")
                .replaceAll("٧","7")
                .replaceAll("٨","8")
                .replaceAll("٩","9")
                .replaceAll("٠","0");
    }

    public static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }
    public static boolean compareTimes(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }

    public static void copy(File src, OutputStream out, long length, UpdateListener updateListener) {
        try (InputStream in = new FileInputStream(src)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                int counter=0;
                Log.d(TAG, "copy: length: "+length);
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                    counter = counter + 1024;
                    Log.d(TAG, "copy: counter: "+counter);
                    updateListener.update((int)(counter*100/length));
                }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void toString(String tag, ArrayList<String> list) {
        for (int i = 0; i <list.size(); i++) {
            Log.d(tag, "toString: "+list.get(i));
        }
    }

    interface UpdateListener {
        public void update(int i);
    }
    private static double SPACE_KB = 1024;
    private static double SPACE_MB = 1024 * SPACE_KB;
    private static double SPACE_GB = 1024 * SPACE_MB;
    private static double SPACE_TB = 1024 * SPACE_GB;

    public static String bytes2String(long sizeInBytes) {

        NumberFormat nf = new DecimalFormat();
        nf.setMaximumFractionDigits(2);

        try {
            if ( sizeInBytes < SPACE_KB ) {
                return nf.format(sizeInBytes) + " Byte(s)";
            } else if ( sizeInBytes < SPACE_MB ) {
                return nf.format(sizeInBytes/SPACE_KB) + " KB";
            } else if ( sizeInBytes < SPACE_GB ) {
                return nf.format(sizeInBytes/SPACE_MB) + " MB";
            } else if ( sizeInBytes < SPACE_TB ) {
                return nf.format(sizeInBytes/SPACE_GB) + " GB";
            } else {
                return nf.format(sizeInBytes/SPACE_TB) + " TB";
            }
        } catch (Exception e) {
            return sizeInBytes + " Byte(s)";
        }

    }



    public static int copy(InputStream input, OutputStream output) throws Exception, IOException {
        final int BUFFER_SIZE = 1024 * 2;
        byte[] buffer = new byte[BUFFER_SIZE];


        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static void fade(int duration, float from, float to, View... views) {
        AlphaAnimation anim = new AlphaAnimation(from, to); //0 means invisible
        anim.setDuration(duration);
        anim.setRepeatCount(0);
//                    anim.setRepeatMode(Animation.REVERSE);
        for (View view : views) {
            view.startAnimation(anim);
        }


        anim.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {

                Log.d(TAG, "onAnimationStart: ");
                if (from==0.0f && to==1.0f){//if we are handling a fadeIn
//                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    for (View view : views) {
                        view.setVisibility(View.VISIBLE);
                    }
                }
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                Log.d(TAG, "onAnimationEnd: ");
                if (from==1.0f && to==0.0f){//if we are handling a fadeOut
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//hide status bar because it causes problemwhen screen shoting
                    for (View view : views) {
                        view.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    /**the given view must have view.getWidth==match parent
     * translates view
     * @param view
     */
    public static void translateFromLeft(View view) {
        TranslateAnimation translateAnimation=new TranslateAnimation(-view.getWidth(),0,0,0);
        translateAnimation.setDuration(300);

        if (view.getVisibility()==View.INVISIBLE){
            view.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation) {
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public static void translateToLeft(View view) {
        TranslateAnimation translateAnimation=new TranslateAnimation(0,-view.getWidth(),0,0);
        translateAnimation.setDuration(300);
        if (view.getVisibility()==View.VISIBLE){
            view.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }
    public static void translateOutToLeft(View view) {
        TranslateAnimation translateAnimation=new TranslateAnimation(0,-view.getWidth(),0,0);
        translateAnimation.setDuration(300);

        if (view.getVisibility()==View.INVISIBLE){
            view.startAnimation(translateAnimation);
        }
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation) {
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    public static String haveNetworkConnection(Context context) {
        String haveConnectedWifi = "";
        String haveConnectedMobile = "";

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = "wifi";
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = "mobile";
        }
        return haveConnectedWifi.equals("")?haveConnectedMobile:haveConnectedWifi;
    }






    public static SpannableStringBuilder emphasizeContent(String str, Typeface face, String ...s) {


        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(str);
        int[] start=new int[s.length];
        int[] end=new int[s.length];
        for (int i = 0; i < s.length; i++) {
            start[i]=str.indexOf(s[i]);
            end[i]=str.indexOf(s[i])+s[i].length();

            stringBuilder.setSpan(
                    new CustomTypefaceSpan("", face),
                    start[i],
                    end[i],
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            );
        }
        return stringBuilder;
    }

    public static float dip2px(float dip, Context c) {
        Resources r = c.getResources();
        float px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip,
                r.getDisplayMetrics()
        );
        return px;
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
    public static Date DateFromString(String inputFormat, String inputDate){

        Date parsed = null;

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df_input = new SimpleDateFormat(inputFormat);

        try {
            parsed = df_input.parse(inputDate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return parsed;

    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate, String local){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale(local));
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale(local));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            df_input = new SimpleDateFormat(DATE_INPUT_FORMAT_WITHOUT_TIME_ZONE, new Locale(local));
            df_output = new SimpleDateFormat(outputFormat, new Locale(local));
            try {
                String temp=inputDate.split("Z")[0];
                parsed = df_input.parse(temp);
                outputDate = df_output.format(parsed);

            } catch (ParseException e2) {
                e2.printStackTrace();
            }
            e.printStackTrace();
        }
        if (local.equals("ar")){
            outputDate=replaceArabicNumbers(outputDate);
        }
        return outputDate;

    }
    public static String formatDate(Date date, String locale){
        try {
            String dateString;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_SIMPLE_FORMAT);
            dateString = simpleDateFormat.format(date);
            String currentDate = simpleDateFormat.format(Calendar.getInstance().getTime());

            Calendar yesterdayCalendar = Calendar.getInstance();
            yesterdayCalendar.add(Calendar.DATE, -1);
            String yesterdaysDate = simpleDateFormat.format(yesterdayCalendar.getTime());

            SimpleDateFormat outputDateFormat;
            String outputDate;
            if (dateString.equals(currentDate)) {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? DATE_TODAY_AR_OUTPUT_FORMAT : DATE_TODAY_EN_OUTPUT_FORMAT, new Locale(locale));
            } else if (dateString.equals(yesterdaysDate)) {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? DATE_YESTERDAY_AR_OUTPUT_FORMAT : DATE_YESTERDAY_EN_OUTPUT_FORMAT, new Locale(locale));
            } else {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? FULL_DATE_AR_OUTPUT_FORMAT : FULL_DATE_EN_OUTPUT_FORMAT, new Locale(locale));
            }
            return replaceArabicNumbers(outputDateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }

    }

    public static int[] getScreenWidthHeight(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return new int[]{size.x, size.y};
    }
    public static int[] getScreenWidthHeight(Context context){
        Activity activity=getActivity(context);
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return new int[]{size.x, size.y};
    }

    public static File generateThumbnail(Context context, File file) {

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inJustDecodeBounds = true; // obtain the size of the image, without loading it in memory
        BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);

        // find the best scaling factor for the desired dimensions
        int desiredWidth = 400;
        int desiredHeight = 300;
        float widthScale = (float)bitmapOptions.outWidth/desiredWidth;
        float heightScale = (float)bitmapOptions.outHeight/desiredHeight;
        float scale = Math.min(widthScale, heightScale);

        int sampleSize = 1;
        while (sampleSize < scale) {
            sampleSize *= 2;
        }
        bitmapOptions.inSampleSize = sampleSize; // this value must be a power of 2,
        // this is why you can not have an image scaled as you would like
        bitmapOptions.inJustDecodeBounds = false; // now we want to load the image

        // Let's load just the part of the image necessary for creating the thumbnail, not the whole image
        Bitmap thumbnail = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
        File thumbnailFile=null;
        try {
            // Save the thumbnail
//            PackageManager m = context.getPackageManager();
//            String s = context.getPackageName();
//            PackageInfo p = m.getPackageInfo(s, 0);
//            s = p.applicationInfo.dataDir;
//            thumbnailFile = new File(context.getFilesDir().getAbsolutePath()+ File.separator +  "new_file.jpeg");
            String root = context.getExternalCacheDir().toString();
            thumbnailFile = new File(root+ File.separator +  "new_file.jpeg");

//            if (thumbnailFile.exists()){
//
//                thumbnailFile.delete();
//            }
//            thumbnailFile = new File(root+ File.separator +  "new_file.jpeg");
            Log.d(TAG, "generateThumbnail: "+thumbnailFile.getPath());
            thumbnailFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(thumbnailFile);

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, fos);

            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Use the thumbail on an ImageView or recycle it!
        thumbnail.recycle();
        return thumbnailFile;
    }

    public static Bitmap getBitmapFromView2(View view)
    {
//        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        view.draw(canvas);

        View v1 =view;// getActivity(view).getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        return bitmap;
    }
    public static Bitmap getBitmapFromView(View view)
    {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(getIntFromColor(255,255,255));
        view.draw(canvas);
        return bitmap;
    }

    public static Activity getActivity(View view) {
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
    public static Activity getActivity(Context context) {

        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
    public static Bitmap darkenBitMap(Bitmap bm) {

        Canvas canvas = new Canvas(bm);
        Paint p = new Paint(Color.RED);
        //ColorFilter filter = new LightingColorFilter(0xFFFFFFFF , 0x00222222); // lighten
        ColorFilter filter = new LightingColorFilter(0xFF7F7F7F, 0x00000000);    // darken
        p.setColorFilter(filter);
        canvas.drawBitmap(bm, new Matrix(), p);
        return bm;
    }
    public static Bitmap updateHSV(Bitmap src, float settingHue, float settingSat,
                             float settingVal) {

        int w = src.getWidth();
        int h = src.getHeight();
        int[] mapSrcColor = new int[w * h];
        int[] mapDestColor = new int[w * h];

        float[] pixelHSV = new float[3];

        src.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

        int index = 0;
        for (int y = 0; y < h; ++y) {
            for (int x = 0; x < w; ++x) {

                // Convert from Color to HSV
                Color.colorToHSV(mapSrcColor[index], pixelHSV);

                // Adjust HSV
//                pixelHSV[0] = pixelHSV[0] + settingHue;
//                if (pixelHSV[0] < 0.0f) {
//                    pixelHSV[0] = 0.0f;
//                } else if (pixelHSV[0] > 360.0f) {
//                    pixelHSV[0] = 360.0f;
//                }
//
//                pixelHSV[1] = pixelHSV[1] + settingSat;
//                if (pixelHSV[1] < 0.0f) {
//                    pixelHSV[1] = 0.0f;
//                } else if (pixelHSV[1] > 1.0f) {
//                    pixelHSV[1] = 1.0f;
//                }

                pixelHSV[2] = pixelHSV[2] + settingVal;
                if (pixelHSV[2] < 0.0f) {
                    pixelHSV[2] = 0.0f;
                } else if (pixelHSV[2] > 1.0f) {
                    pixelHSV[2] = 1.0f;
                }

                // Convert back from HSV to Color
                mapDestColor[index] = Color.HSVToColor(pixelHSV);

                index++;
            }
        }

        return Bitmap.createBitmap(mapDestColor, w, h, Bitmap.Config.ARGB_8888);

    }
    public static int getIntFromColor(int Red, int Green, int Blue){
        Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
        Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
        Blue = Blue & 0x000000FF; //Mask out anything not blue.

        return 0xFF000000 | Red | Green | Blue; //0xFF000000 for 100% Alpha. Bitwise OR everything together.
    }

    public static boolean exists(Context context, Uri self) {
        final ContentResolver resolver = context.getContentResolver();

        Cursor c = null;
        try {
            c = resolver.query(self, new String[] {
                    DocumentsContract.Document.COLUMN_DOCUMENT_ID }, null, null, null);
            return c.getCount() > 0;
        } catch (Exception e) {
            Log.w(TAG, "Failed query: " + e);
            return false;
        }
    }


    //TODO: need to add     implementation 'com.github.barteksc:pdfium-android:1.4.0'
    public static Bitmap generateImageFromPdf(Context c, Uri pdfUri) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(c);
        try {
            //http://www.programcreek.com/java-api-examples/index.php?api=android.os.ParcelFileDescriptor
            ParcelFileDescriptor fd = c.getContentResolver().openFileDescriptor(pdfUri, "r");
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);
            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
//            saveImage(bmp);

//            iv.setImageBitmap(bmp);
            pdfiumCore.closeDocument(pdfDocument); // important!
            return bmp;

        } catch(Exception e) {
            Toast.makeText(c, "A problem occured "+e, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "generateImageFromPdf: "+e);
        }
        return null;
    }


    public static String getRealPathFromURI(Context c, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(c, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    public static String getFilePath2(Context context, Uri uri)throws URISyntaxException {
        File file = new File(uri.getPath());//create path from uri
        final String[] split = file.getPath().split(":");//split the path.
        Log.d(TAG, "getFilePath2: skdfsd5 file.getPath(): "+file.getPath());
        Log.d(TAG, "getFilePath2: skdfsd5 Uri: "+uri);
        Log.d(TAG, "getFilePath2: skdfsd5 split[1]: "+split[1]);
        return split[1];//assign it to a string(your choice).
    }

    @SuppressLint("NewApi")
    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        final boolean aboveOreo = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;

        if (aboveOreo) {
//            File file = new File(uri.getPath());//create path from uri
//            Log.d(TAG, "getFilePath: file.getPath() before split: "+file.getPath());
//            final String[] split = file.getPath().split(":");//split the path.
//            Log.d(TAG, "getFilePath: filePathAboveOreo: "+split[1]);
//            return split[1];





        }
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {

            Log.d(TAG, "getFilePath: isExternalStorageDocument, docId: "+DocumentsContract.getDocumentId(uri));

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                Log.d(TAG, "getFilePath: isExternalStorageDocument");
                return Environment.getExternalStorageDirectory() + "/" + split[1];

            } else if (isDownloadsDocument(uri)) {
                Log.d(TAG, "getFilePath: isDownloadsDocument");

                final String id = DocumentsContract.getDocumentId(uri);
                if (id != null && id.startsWith("msf:")) {
                    final File file = new File(context.getCacheDir(), Constants.TEMP_FILE+"." + Objects.requireNonNull(context.getContentResolver().getType(uri)).split("/")[1]);
                    Log.d(TAG, "getFilePath: context.getContentResolver().getType(uri): "+context.getContentResolver().getType(uri));
                    try ( final InputStream inputStream = context.getContentResolver().openInputStream(uri);
                         OutputStream output = new FileOutputStream(file)) {
                        final byte[] buffer = new byte[4 * 1024]; // or other buffer size
                        int read;

                        while ((read = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, read);
                        }

                        output.flush();
                        Log.d(TAG, "getFilePath: file.getPath(): "+file.getPath());
                        return file.getPath();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                Log.d(TAG, "getFilePath: isMediaDocument");

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            Log.d(TAG, "getFilePath: content");


            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }

            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }
    private void deleteTempFile(Context context) {
        final File[] files = context.getCacheDir().listFiles();
        if (files != null) {
            for (final File file : files) {
                if (file.getName().contains(Constants.TEMP_FILE)) {
                    file.delete();
                }
            }
        }
    }
    public static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static HashSet<String> getExternalMounts() {
        final HashSet<String> out = new HashSet<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";
        try {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1) {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines) {
            if (!line.toLowerCase(Locale.US).contains("asec")) {
                if (line.matches(reg)) {
                    String[] parts = line.split(" ");
                    for (String part : parts) {
                        if (part.startsWith("/"))
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                                out.add(part);
                    }
                }
            }
        }
        return out;
    }

    /**
     * Get external sd card path using reflection
     * @param mContext
     * @param is_removable is external storage removable
     * @return
     */
    public static String getExternalStoragePath(Context mContext, boolean is_removable) {

        StorageManager mStorageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isRemovable = storageVolumeClazz.getMethod("isRemovable");
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removable == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
