package com.ma.kdipa.JavaResources;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final String TAG = "Utils";

    public static boolean isArabic(Context context) {
        return Utils.getValue(context, Constants.USER_LANGUAGE, Constants.ENGLISH_LANGUAGE).equalsIgnoreCase(Constants.ARABIC_LANGUAGE);
    }

    public static void share(String text, Context mContext) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);

        mContext.startActivity(intent);

    }

    public static String getCurrentLanguageFontPath(Context context) {
        String fontPath;
        if (Utils.getValue(context, Constants.USER_LANGUAGE, Constants.ENGLISH_LANGUAGE).equalsIgnoreCase(Constants.ENGLISH_LANGUAGE)) {
            fontPath = "fonts/english_regular.ttf";
        } else {
            fontPath = "fonts/arabic_regular.ttf";
        }
        return fontPath;
    }

    public static float dip2px(float dip, Context c) {
        Resources r = c.getResources();
        float px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip,
                r.getDisplayMetrics()
        );
        return px;
    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }



    public static long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    private static long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }


    public static void setLanguage(String language, Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(Constants.USER_LANGUAGE, language);
        editor.apply();

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }


    public static void updateLanguage(Context cxt, SharedPreferences sharedPreferences) {
        Locale local = new Locale(sharedPreferences.getString(Constants.USER_LANGUAGE, "en"));
        Locale.setDefault(local);
        Configuration configuration = cxt.getResources().getConfiguration();
        configuration.setLocale(local);
        cxt.getResources().updateConfiguration(configuration, cxt.getResources().getDisplayMetrics());

    }

    public static void updateLanguage(Context cxt, String locale) {
        Locale local = new Locale(locale);
        Locale.setDefault(local);
        Configuration configuration = cxt.getResources().getConfiguration();
        configuration.setLocale(local);
        cxt.getResources().updateConfiguration(configuration, cxt.getResources().getDisplayMetrics());
    }

    public static void openMap(Context mContext, double latitude, double longitude) {

        Uri.Builder directionsBuilder = new Uri.Builder()
                .scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", latitude + "," + longitude);

        mContext.startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));


    }

    public static void openWhatsAppChat(Context context, String phone) {
        try {
            phone = phone.replace(" ", "").replace("+", "");

            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(phone) + "@s.whatsapp.net");
            context.startActivity(sendIntent);

        } catch (Exception e) {
            Log.e("TAG", "ERROR_OPEN_WHATSAPP" + e.toString());
        }
    }

    public static void openInstagram(Context context, String instagram) {
        Uri uri = Uri.parse("http://instagram.com/_u/"+instagram);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            context.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/"+instagram)));
        }


//  cleaner solution with same result:
//        Uri uri = Uri.parse("http://instagram.com/_u/xxx");
//        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
//        insta.setPackage("com.instagram.android");
//
//        if (isIntentAvailable(mContext, insta)){
//            startActivity(insta);
//        } else{
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/xxx")));
//        }
//
//        private boolean isIntentAvailable(Context ctx, Intent intent) {
//            final PackageManager packageManager = ctx.getPackageManager();
//            List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
//            return list.size() > 0;
//        }
    }





    public static boolean isMobile(String s) {
        return Patterns.PHONE.matcher(s).matches();

    }

    public static synchronized void setValue(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(key, value).commit();
    }

    public static synchronized String getValue(Context context, String key, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, defaultValue);
    }

    public static synchronized void setValue(Context context, String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean(key, value).commit();
    }

    public static synchronized boolean getValue(Context context, String key, boolean defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, defaultValue);
    }

    public static synchronized void setValue(Context context, String key, float value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putFloat(key, value).commit();
    }

    public static synchronized float getValue(Context context, String key, float defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getFloat(key, defaultValue);
    }

    public static synchronized void setValue(Context context, String key, double value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putFloat(key, (float) value).commit();
    }

    public static synchronized double getValue(Context context, String key, double defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getFloat(key, (float) defaultValue);
    }

    public static synchronized void setValue(Context context, String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt(key, value).commit();
    }

    public static synchronized int getValue(Context context, String key, int defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(key, defaultValue);
    }


    public static void goToActivity(Context context, Class<?> to, boolean finishAfter) {
        Intent i = new Intent(context, to);
        context.startActivity(i);
        if (finishAfter) {
            ((Activity) context).finish();
        }
    }

    public static String convertDate(String isoDate) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
        String outputPattern = "dd/MM/yyyy hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(isoDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
