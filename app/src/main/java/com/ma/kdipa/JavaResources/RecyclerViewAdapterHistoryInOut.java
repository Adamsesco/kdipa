package com.ma.kdipa.JavaResources;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_SIMPLE_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_TODAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_YESTERDAY_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.FULL_DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Utilitaire.DateFromString;
import static com.ma.kdipa.JavaResources.Utilitaire.formatDate;
import static com.ma.kdipa.JavaResources.Utilitaire.replaceArabicNumbers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.ma.kdipa.R;
import com.ma.kdipa.responses.HistoryInOutElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapterHistoryInOut extends RecyclerView.Adapter<RecyclerViewAdapterHistoryInOut.MyViewHolder> {

    private List<HistoryInOutElement> mDataset;
    private RecyclerView recycler;
    private static final String TAG = "RecyclerViewAdapterNews";


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dateInTV;
        TextView dateOutTV;

        View v;
        public MyViewHolder(View view) {
            super(view);
            this.dateInTV = view.findViewById(R.id.dateInTV);
            this.dateOutTV = view.findViewById(R.id.dateOutTV);
            this.v = view;
        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterHistoryInOut(List<HistoryInOutElement> myDataset) {
        mDataset = myDataset;
        this.setHasStableIds(true);


    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_element_history_in_out,
                parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HistoryInOutElement center= mDataset.get(position);
        String temp = formatDate2(holder.itemView.getContext(), center.ActualIn);
        holder.dateInTV.setText(temp);

        temp = formatDate2(holder.itemView.getContext(), center.ActualOut);
        holder.dateOutTV.setText(temp);

    }
    public static String formatDate2(Context context , String text){
        if (text == null || text.equals("")) return "---";


        Date date= DateFromString(DATE_INPUT_FORMAT, text);
        String locale=context.getResources().getConfiguration().locale.getLanguage();
        try {
            String dateString;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_SIMPLE_FORMAT);
            dateString = simpleDateFormat.format(date);
            String currentDate = simpleDateFormat.format(Calendar.getInstance().getTime());

            Calendar yesterdayCalendar = Calendar.getInstance();
            yesterdayCalendar.add(Calendar.DATE, -1);
            String yesterdaysDate = simpleDateFormat.format(yesterdayCalendar.getTime());

            SimpleDateFormat outputDateFormat;
            String outputDate;
            if (dateString.equals(currentDate)) {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? DATE_TODAY_AR_OUTPUT_FORMAT : DATE_TODAY_EN_OUTPUT_FORMAT, new Locale(locale));
            } else if (dateString.equals(yesterdaysDate)) {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? DATE_YESTERDAY_AR_OUTPUT_FORMAT : DATE_YESTERDAY_EN_OUTPUT_FORMAT, new Locale(locale));
            } else {
                outputDateFormat = new SimpleDateFormat(locale.equals("ar") ? FULL_DATE_AR_OUTPUT_FORMAT : FULL_DATE_EN_OUTPUT_FORMAT, new Locale(locale));
            }
            return replaceArabicNumbers(outputDateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }

    }

    private String formatDate(Context context, String string) {
        if (string == null || string.equals("")) return "---";
        String locale=context.getResources().getConfiguration().locale.getLanguage();

        return Utilitaire.formateDateFromstring(
                DATE_INPUT_FORMAT,
                locale.equals("ar")?Constants.YEAR_MONTH_DAY_HOUR_MIN_SEC_AR:Constants.YEAR_MONTH_DAY_HOUR_MIN_SEC_EN,
                string,
                locale
        );
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
    @Override
    public long getItemId(int position) {
//        Log.d(TAG, "getItemId: called with position: "+ position);
        return position*10;
    }
}


