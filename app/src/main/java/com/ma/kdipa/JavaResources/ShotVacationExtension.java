package com.ma.kdipa.JavaResources;

import android.content.Context;

import java.util.ArrayList;

public class ShotVacationExtension {
    private ArrayList<String> typesList;
    private ArrayList<String> typesArList;

    private String startDate;
    private String toDate;
    private String extendedToDate;



    public ShotVacationExtension(Context context, ArrayList<String> typesList, ArrayList<String> typesArList, String startDate, String toDate,String extendedToDate ) {
        this.typesList =        typesList;
        this.typesArList =      typesArList;
//        this.startDate=         Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY,context.getString(R.string.output_format),startDate,context.getString(R.string.local));
//        this.toDate=            Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY,context.getString(R.string.output_format),toDate,context.getString(R.string.local));
//        this.extendedToDate=    Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY,context.getString(R.string.output_format),extendedToDate,context.getString(R.string.local));

        String locale=context.getResources().getConfiguration().locale.getLanguage();
        this.startDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",startDate, locale);
        this.startDate=Utilitaire.replaceArabicNumbers(this.startDate);
        this.toDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",toDate, locale);
        this.toDate=Utilitaire.replaceArabicNumbers(this.toDate);
        this.extendedToDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",extendedToDate, locale);
        this.extendedToDate=Utilitaire.replaceArabicNumbers(this.extendedToDate);

    }

    public ArrayList<String> getTypesList() {
        return typesList;
    }

    public void setTypesList(ArrayList<String> typesList) {
        this.typesList = typesList;
    }

    public ArrayList<String> getTypesArList() {
        return typesArList;
    }

    public void setTypesArList(ArrayList<String> typesArList) {
        this.typesArList = typesArList;
    }

    public String getstartDate() {
        return startDate;
    }

    public void setstartDate(String startDate) {
        this.startDate = startDate;
    }

    public String gettoDate() {
        return toDate;
    }

    public void settoDate(String toDate) {
        this.toDate = toDate;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getExtendedToDate() {
        return extendedToDate;
    }

    public void setExtendedToDate(String extendedToDate) {
        this.extendedToDate = extendedToDate;
    }
}
