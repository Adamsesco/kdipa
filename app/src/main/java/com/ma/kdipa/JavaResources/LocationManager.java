package com.ma.kdipa.JavaResources;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;


public class LocationManager implements LocationListener {

    private static final int REQUEST_LOCATION_PERMISSION = 3;
    public static final String ACTION_GET_LOCATION = "com.lamaa.getLocation";

    private Activity activity;
    private Fragment fragment;
    private Location location;
    private android.location.LocationManager mManager;
    private Listener mListener;
    public boolean showLocationIsNotRequested = true;

    public LocationManager(Activity activity) {
        this.activity = activity;
    }

    public LocationManager(Fragment fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
    }


    public void getLocation() {
        if (!checkPermissions())
            return;

        if (!isLocationEnabled() && showLocationIsNotRequested) {
            return;
        }

        requestLocation();

    }

    private boolean checkPermissions() {
        if ((ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)) {
            if (fragment != null) {
                fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            }

            return false;

        }
        return true;
    }



    private boolean requestLocation() {

        mManager = (android.location.LocationManager)
                activity.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            boolean requested = false;
            if (mManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
                mManager.requestLocationUpdates(android.location.LocationManager.GPS_PROVIDER, 1000, 1, this);
                requested = true;
            }

            if (mManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)) {
                mManager.requestLocationUpdates(android.location.LocationManager.NETWORK_PROVIDER, 1000, 1, this);
                requested = true;
            }

            return requested;
        }

        return false;

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d("Location", location.toString());
        if (this.location == null) {
            this.location = location;
            if (mListener != null)
                mListener.onGetLocation(location);
        }
    }



    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean isLocationEnabled() {
        boolean enabled = true;

        mManager = (android.location.LocationManager)
                activity.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (!mManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
                enabled = false;
            }

            if (!mManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)) {
                enabled = false;
            }

        }
        return enabled;
    }

    public void stop() {
        try {
            mManager.removeUpdates(this);
        } catch (Exception e) {

        }
    }

    public Listener getListener() {
        return mListener;
    }

    public void setListener(Listener mListener) {
        this.mListener = mListener;
    }


    public interface Listener {
        void onGetLocation(Location location);

        void onLocationDenied();
    }


}
