package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class RecyclerViewAdapterTimes extends RecyclerView.Adapter<RecyclerViewAdapterTimes.MyViewHolder> {
    private final int width;
    private final int height;

    private ArrayList<String> mDataset;
    private RecyclerView recycler;
    RecyclerView.SmoothScroller smoothScroller;
    private OnTimeSelectedListener mOnTimeSelectedListener;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView timeTV;
        OnTimeSelectedListener onTimeSelectedListener;
        View v;
        public MyViewHolder(View view, OnTimeSelectedListener onTimeSelectedListener) {
            super(view);
            this.timeTV = view.findViewById(R.id.time_tv);
            this.onTimeSelectedListener=onTimeSelectedListener;
            this.v = view;
            view.setOnClickListener(this);
        }

        public View getView() {
            return v;
        }


        @Override
        public void onClick(View view) {
            onTimeSelectedListener.doOnTimeSelected(getAdapterPosition(), smoothScroller);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterTimes(ArrayList<String> myDataset, int width, Context context, int height) {
        mDataset = myDataset;
        this.width=width;
        this.height =height;
        this.mOnTimeSelectedListener=(OnTimeSelectedListener) context;
        smoothScroller = new CenterLayoutManager.CenterSmoothScroller(context);
    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.e_time_element, parent, false);
//        v.findViewById(R.id.time_tv).setLayoutParams(new ConstraintLayout.LayoutParams((int) ( width/5.5), (int) (0.07* height)));
        v.findViewById(R.id.time_tv).getLayoutParams().width=(int) ( width/5.5);
//        v.findViewById(R.id.time_tv_container).setLayoutParams(new ConstraintLayout.LayoutParams((int) ( width/5.5), height));
        return new MyViewHolder(v, mOnTimeSelectedListener);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.setIsRecyclable(false);
//        recycler.getRecycledViewPool().setMaxRecycledViews(holder.getItemViewType(), 0);

        holder.timeTV.setText(mDataset.get(position));


//        holder.timeTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                recycler.scrollToPosition(1);
////                recycler.smoothScrollToPosition(1);
////                recycler.getchild
//                if (!((TextView)view).getText().toString().equals("") ){
//                    smoothScroller.setTargetPosition(position);
//                    recycler.getLayoutManager().startSmoothScroll(smoothScroller);
//                    selectedItemPosition=position;
//                }
//            }
//        });





    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        String stringId=mDataset.get(position);
        return Integer.parseInt(stringId);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

//    public int getIemSelectedPosition(){
//        return selectedItemPosition;
//    }

    public interface OnTimeSelectedListener {
        void doOnTimeSelected(int adapterPosition, RecyclerView.SmoothScroller smoothScroller);
    }

}


