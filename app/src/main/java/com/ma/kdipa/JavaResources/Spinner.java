package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;

public class Spinner extends androidx.appcompat.widget.AppCompatSpinner {
    public Spinner(Context context) {
        super(context);
    }

    public Spinner(Context context, int mode) {
        super(context, mode);
    }

    public Spinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Spinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Spinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    private static final String TAG = "MySpinner";
    @Override
    public int getSelectedItemPosition() {
        Log.d(TAG, "getSelectedItemPosition: super.getSelectedItemPosition():"+super.getSelectedItemPosition());
        return super.getSelectedItemPosition()-1;
    }

//    public void setOnItemSelectedListener(@Nullable OnItemSelectedListener listener) {
//        super.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (listener != null) {
//                    if (i == 0) {
//                        return;
//                    } else {
//                        i = i - 1;
//                    }
//                    listener.onItemSelected(adapterView, view, i, l);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                if (listener != null) {
//                    listener.onNothingSelected(adapterView);
//                }
//            }
//        });
//    }


}
