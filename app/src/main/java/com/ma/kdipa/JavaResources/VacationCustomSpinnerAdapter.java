package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class VacationCustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    Context context;
    private ArrayList<VacationsEntry> vacationsEntries;
    public VacationCustomSpinnerAdapter(Context context, ArrayList<VacationsEntry> vacationsEntries) {
        this.vacationsEntries=vacationsEntries;
        this.context=context;
    }

    @Override
    public int getCount() {
        return vacationsEntries.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }





    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  View.inflate(context,R.layout.e_vacations_element_return_extension,
                null);
        TextView number = view.findViewById(R.id.number);
        TextView type = view.findViewById(R.id.type);
        TextView date = view.findViewById(R.id.date);

        number.setText(vacationsEntries.get(position).getNumber());
        type.setText(vacationsEntries.get(position).getType());
        date.setText(vacationsEntries.get(position).getDateFromWithYear());
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view =  View.inflate(context,R.layout.e_vacations_element_return_extension,
                null);
        TextView number = view.findViewById(R.id.number);
        TextView type = view.findViewById(R.id.type);
        TextView date = view.findViewById(R.id.date);
        number.setText(vacationsEntries.get(position).getNumber());
        type.setText(vacationsEntries.get(position).getType());
        date.setText(vacationsEntries.get(position).getDateFromWithYear());
        return view;
    }

}
