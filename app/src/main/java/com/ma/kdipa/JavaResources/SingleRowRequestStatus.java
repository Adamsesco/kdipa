package com.ma.kdipa.JavaResources;

import com.ma.kdipa.R;

import static com.ma.kdipa.JavaResources.Constants.DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;

public class SingleRowRequestStatus {
    private String date;
    private String id;
    private String name;
    private String name_ar;
    private String statusText;
    private int statusColor;
    private int downloadIconCode;
    private int removeIconCode;

    String locale;

    public SingleRowRequestStatus(String locale, String date, String id, String name, String name_ar, String statusText) {
        this.locale=locale;
        this.date=Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?DATE_AR_OUTPUT_FORMAT:DATE_EN_OUTPUT_FORMAT,date, locale);
        this.date=Utilitaire.replaceArabicNumbers(this.date);
        this.id = id;
        this.name = name;
        this.name_ar=name_ar;
        this.statusText = statusText;
        this.statusColor=   (statusText.equals("APPROVED")?
                            (R.color.approvedRequest):  (
                                                            statusText.equals("REJECTED")?
                                                            (R.color.rejected):(R.color.pending)
                                                        )
                            );
        this.downloadIconCode=(statusText.equals("APPROVED")?R.drawable.download_en:R.drawable.download_dis);
        this.removeIconCode=(statusText.equals("PENDING")?R.drawable.remove_en:R.drawable.remove_dis);
        this.removeIconCode=(statusText.equals("")?R.drawable.remove_en:R.drawable.remove_dis);

    }

    //todo add link object (maybe String) to download

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public void setStatusColor(int statusColor) {
        this.statusColor = statusColor;
    }

    public void setDownloadIconCode(int downloadIconCode) {
        this.downloadIconCode = downloadIconCode;
    }

    public void setRemoveIconCode(int removeIconCode) {
        this.removeIconCode = removeIconCode;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getName()
    {
        return locale.equals("ar")?name_ar:name;
    }

    public String getStatusText() {
        return statusText;
    }

    public int getStatusColor() {
        return statusColor;
    }

    public int getDownloadIconCode() {
        return downloadIconCode;
    }

    public int getRemoveIconCode() {
        return removeIconCode;
    }
}
