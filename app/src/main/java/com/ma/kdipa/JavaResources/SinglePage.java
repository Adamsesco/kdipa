package com.ma.kdipa.JavaResources;

import android.content.Context;

import static com.ma.kdipa.JavaResources.Constants.DATE_AR_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_EN_OUTPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;

public class SinglePage {
    private String trxDate;
    private String type;
    private String from;
    private String to;
    private String periode;

    public SinglePage(String trxDate, String type, String from, String to, String periode, Context c) {
        String locale=c.getResources().getConfiguration().locale.getLanguage();
        this.trxDate=Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?DATE_AR_OUTPUT_FORMAT:DATE_EN_OUTPUT_FORMAT, trxDate,locale);
        this.trxDate=Utilitaire.replaceArabicNumbers(this.trxDate);

        this.type = type;
        this.from = from;
        this.to = to;
        this.periode = periode;

    }

    public String getType() {
        return type;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getPeriode() {
        return periode;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }
}
