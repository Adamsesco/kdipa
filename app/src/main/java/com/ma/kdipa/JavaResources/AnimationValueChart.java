package com.ma.kdipa.JavaResources;

import android.view.animation.Animation;
import android.view.animation.Transformation;

public class AnimationValueChart extends Animation {

    private CustomSolidArc customSolidArc;
    private float from;
    private float  to;

    public AnimationValueChart(CustomSolidArc customSolidArc, float from, float to) {
        super();
        this.customSolidArc = customSolidArc;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        customSolidArc.setPiePercent(value);
    }

}

