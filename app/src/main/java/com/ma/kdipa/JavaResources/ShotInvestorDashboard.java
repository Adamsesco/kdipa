package com.ma.kdipa.JavaResources;

import com.google.gson.annotations.SerializedName;

public class ShotInvestorDashboard {

    public ShotInvestorDashboard(String applicationstatus, String applicationtype, String applicationcompany, String appointmentdetails, String appointmentmeetingtime, String appointmentmeetingdate, String appointmentaccountmanager, String appointmentservice, int numberofapplications, int numberofappointments) {
        this.applicationstatus = applicationstatus;
        this.applicationtype = applicationtype;
        this.applicationcompany = applicationcompany;
        this.appointmentdetails = appointmentdetails;
        this.appointmentmeetingtime = appointmentmeetingtime;
        this.appointmentmeetingdate = appointmentmeetingdate;
        this.appointmentaccountmanager = appointmentaccountmanager;
        this.appointmentservice = appointmentservice;
        this.numberofapplications = numberofapplications;
        this.numberofappointments = numberofappointments;
    }

    public ShotInvestorDashboard() {
    }

    @SerializedName("applicationStatus")
    private String applicationstatus;
    @SerializedName("applicationType")
    private String applicationtype;
    @SerializedName("applicationCompany")
    private String applicationcompany;
    @SerializedName("appointmentDetails")
    private String appointmentdetails;
    @SerializedName("appointmentMeetingTime")
    private String appointmentmeetingtime;
    @SerializedName("appointmentMeetingDate")
    private String appointmentmeetingdate;
    @SerializedName("appointmentAccountManager")
    private String appointmentaccountmanager;
    @SerializedName("appointmentService")
    private String appointmentservice;
    @SerializedName("numberOfApplications")
    private int numberofapplications;
    @SerializedName("numberOfAppointments")
    private int numberofappointments;

    public String getApplicationstatus() {
        return applicationstatus;
    }

    public void setApplicationstatus(String applicationstatus) {
        this.applicationstatus = applicationstatus;
    }

    public String getApplicationtype() {
        return applicationtype;
    }

    public void setApplicationtype(String applicationtype) {
        this.applicationtype = applicationtype;
    }

    public String getApplicationcompany() {
        return applicationcompany;
    }

    public void setApplicationcompany(String applicationcompany) {
        this.applicationcompany = applicationcompany;
    }

    public String getAppointmentdetails() {
        return appointmentdetails;
    }

    public void setAppointmentdetails(String appointmentdetails) {
        this.appointmentdetails = appointmentdetails;
    }

    public String getAppointmentmeetingtime() {
        return appointmentmeetingtime;
    }

    public void setAppointmentmeetingtime(String appointmentmeetingtime) {
        this.appointmentmeetingtime = appointmentmeetingtime;
    }

    public String getAppointmentmeetingdate() {
        return appointmentmeetingdate;
    }

    public void setAppointmentmeetingdate(String appointmentmeetingdate) {
        this.appointmentmeetingdate = appointmentmeetingdate;
    }

    public String getAppointmentaccountmanager() {
        return appointmentaccountmanager;
    }

    public void setAppointmentaccountmanager(String appointmentaccountmanager) {
        this.appointmentaccountmanager = appointmentaccountmanager;
    }

    public String getAppointmentservice() {
        return appointmentservice;
    }

    public void setAppointmentservice(String appointmentservice) {
        this.appointmentservice = appointmentservice;
    }

    public int getNumberofapplications() {
        return numberofapplications;
    }

    public void setNumberofapplications(int numberofapplications) {
        this.numberofapplications = numberofapplications;
    }

    public int getNumberofappointments() {
        return numberofappointments;
    }

    public void setNumberofappointments(int numberofappointments) {
        this.numberofappointments = numberofappointments;
    }


    @Override
    public String toString() {
        return "ShotInvestorDashboard{" +
                "applicationstatus='" + applicationstatus + '\'' +
                ", applicationtype='" + applicationtype + '\'' +
                ", applicationcompany='" + applicationcompany + '\'' +
                ", appointmentdetails='" + appointmentdetails + '\'' +
                ", appointmentmeetingtime='" + appointmentmeetingtime + '\'' +
                ", appointmentmeetingdate='" + appointmentmeetingdate + '\'' +
                ", appointmentaccountmanager='" + appointmentaccountmanager + '\'' +
                ", appointmentservice='" + appointmentservice + '\'' +
                ", numberofapplications=" + numberofapplications +
                ", numberofappointments=" + numberofappointments +
                '}';
    }
}
