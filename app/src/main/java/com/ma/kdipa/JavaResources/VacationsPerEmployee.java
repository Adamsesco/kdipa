package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class VacationsPerEmployee {
    private String name="";
    private String nameAr ="";
    private ArrayList<VacationsEntriesPerYear> vacationsEntriesPerYearsList =new ArrayList<>();

    public VacationsPerEmployee(String name,String nameAr, ArrayList<VacationsEntriesPerYear> vacationsEntriesPerYearsList) {
        this.name = name;
        this.nameAr = nameAr;
        this.vacationsEntriesPerYearsList = vacationsEntriesPerYearsList;
    }

    public VacationsPerEmployee() {

    }


    public String getName() {
        return name;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public void setVacationsEntriesPerYearsList(ArrayList<VacationsEntriesPerYear> vacationsEntriesPerYearsList) {
        this.vacationsEntriesPerYearsList = vacationsEntriesPerYearsList;
    }

    public ArrayList<VacationsEntriesPerYear> getVacationsEntriesPerYearsList() {
        return vacationsEntriesPerYearsList;
    }


    @Override
    public String toString() {
        return "VacationsPerEmployee{" +
                "name='" + name + '\'' +
                ", nameAr='" + nameAr + '\'' +
                ", vacationsEntriesPerYearsList=" + vacationsEntriesPerYearsList +
                '}';
    }
}
