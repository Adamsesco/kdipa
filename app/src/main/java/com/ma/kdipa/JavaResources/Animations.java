package com.ma.kdipa.JavaResources;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class Animations {
    static Animation animation;

    private static final String TAG = "Animations";
    public static boolean toggleArrow(View view, boolean isExpanded) {

        if (isExpanded) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    public static void expand(View view) {
//        view.setVisibility(View.VISIBLE);
        Log.d(TAG, "expand: view: "+view.getVisibility());
        Animation animation = expandAction(view);
        view.startAnimation(animation);
    }

    private static Animation expandAction(final View view) {
//        view.setVisibility(View.VISIBLE);

//        view.setVisibility(View.VISIBLE);
//        view.getViewTreeObserver().addOnGlobalLayoutListener(
//                new ViewTreeObserver.OnGlobalLayoutListener(){
//
//                    @Override
//                    public void onGlobalLayout() {
//                        // gets called after layout has been done but before display
//                        // so we can get the height then hide the view
//
//
//                        actualheight = view.getHeight();  // Ahaha!  Gotcha
//
//                        view.getViewTreeObserver().removeOnGlobalLayoutListener( this );
//                        view.setVisibility( View.GONE );
//                    }
//
//                });
//        view.measure(MATCH_PARENT,WRAP_CONTENT);
        //        actualheight = view.getMeasuredHeight();

        int actualheight;

        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        view.getLayoutParams().height = WRAP_CONTENT;
        actualheight = view.getMeasuredHeight();
        Log.d(TAG, "expandAction: actualHeight: "+actualheight);


        view.getLayoutParams().height = (int) Utilitaire.dip2px(1,view.getContext());

        animation = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                Log.d(TAG, "applyTransformation: actualHeight: "+actualheight);
                Log.d(TAG, "applyTransformation() called with: interpolatedTime = [" + interpolatedTime + "], t = [" + t + "]");
                super.applyTransformation(interpolatedTime, t);
                view.getLayoutParams().height = (
                        interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) ((actualheight +1)* interpolatedTime)
                );
                view.requestLayout();
            }
        };
//        animation.setDuration((long) (actualheight / view.getContext().getResources().getDisplayMetrics().density));
        animation.setDuration(1000);
        view.startAnimation(animation);

        return animation;
    }

    public static void collapse(final View view) {

        final int actualHeight = view.getMeasuredHeight();
        Log.d(TAG, "collapse: actualHeight: "+actualHeight);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {

                if (interpolatedTime == 1) {
                    view.getLayoutParams().height=0;
//                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = actualHeight - (int) (actualHeight * interpolatedTime);
                    view.requestLayout();

                }
            }
        };

//        animation.setDuration((long) (actualHeight/ view.getContext().getResources().getDisplayMetrics().density));
        animation.setDuration(1000);

        view.startAnimation(animation);
    }


}
