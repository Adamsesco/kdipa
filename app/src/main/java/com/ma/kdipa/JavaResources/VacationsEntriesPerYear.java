package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class VacationsEntriesPerYear {
    String year;
    ArrayList<VacationsEntry> entriesList;

    public VacationsEntriesPerYear(String year, ArrayList<VacationsEntry> entriesList) {
        this.year = year;
        this.entriesList = entriesList;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setEntriesList(ArrayList<VacationsEntry> datesList) {
        this.entriesList = entriesList;
    }

    public ArrayList<VacationsEntry> getEntriesList() {
        return entriesList;
    }


}
