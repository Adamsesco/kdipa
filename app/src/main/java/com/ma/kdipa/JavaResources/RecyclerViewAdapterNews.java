package com.ma.kdipa.JavaResources;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class RecyclerViewAdapterNews extends RecyclerView.Adapter<RecyclerViewAdapterNews.MyViewHolder> {

    private ArrayList<SingleRowNews> mDataset;
    private RecyclerView recycler;
    private long previousExpanded=-1;
    private static final String TAG = "RecyclerViewAdapterNews";


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleTV;
        TextView dateTV;
        TextView contentTV;
        TextView readmore;

        View v;
        public MyViewHolder(View view) {
            super(view);
            this.titleTV = view.findViewById(R.id.title);
            this.dateTV = view.findViewById(R.id.date);
            this.contentTV = view.findViewById(R.id.contentTV);
            this.readmore = view.findViewById(R.id.readmore);
            this.v = view;
        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterNews(ArrayList<SingleRowNews> myDataset) {
        mDataset = myDataset;
        this.setHasStableIds(true);


    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_news_element,
                parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        View.OnClickListener onClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: called at position: "+position);
                if (isExpanded(holder)){
                    collapse(holder);
                    previousExpanded=-1;
                }else{
                    expand(holder);
                    if (previousExpanded != -1 && previousExpanded != holder.getItemId()) {
                        //recycler.findViewHolderForLayoutPosition(prev_expanded).itemView.setActivated(false);
                        MyViewHolder vh = (MyViewHolder) recycler.findViewHolderForItemId(previousExpanded);
                        if (vh != null) {
                            collapse(vh);
                        } else {
                            Log.d(TAG, "onClick: vh null");
                        }

                    }

                    previousExpanded=holder.getItemId();
                }
                AutoTransition autoTransition = new AutoTransition();
                autoTransition.setDuration(200);
                TransitionManager.beginDelayedTransition(recycler,autoTransition);
                Log.d(TAG, "onClick: previousExpanded"+previousExpanded);
            }
        };
        if (holder.getItemId()!=previousExpanded) {
            Log.d(TAG, "onBindViewHolder: holder.getItemId(): "+holder.getItemId());
            collapse(holder);
        }else{
            expand(holder);
        }
        holder.titleTV.setText(mDataset.get(position).getTitle());
        holder.dateTV.setText(mDataset.get(position).getDate());
        holder.contentTV.setText(mDataset.get(position).getContent());
        holder.readmore.setOnClickListener(onClickListener);
        holder.contentTV.setOnClickListener(onClickListener);


    }

    private boolean isExpanded(MyViewHolder holder) {
        return holder.readmore.getVisibility()==View.GONE;
    }
    private void expand(MyViewHolder holder){
        holder.contentTV.setMaxLines(200);
        holder.readmore.setVisibility(View.GONE);
    }
    private void collapse(MyViewHolder holder){
        holder.contentTV.setMaxLines(3);
        holder.readmore.setVisibility(View.VISIBLE);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
    @Override
    public long getItemId(int position) {
//        Log.d(TAG, "getItemId: called with position: "+ position);
        return position*10;
    }
}


