package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;
import com.ma.kdipa.responses.GetCompaniesResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompaniesAdapter extends RecyclerView.Adapter<CompaniesAdapter.ViewHolder> {

    Context context;
    OnClickListener callback;
    RecyclerView recyclerView;
    private List<GetCompaniesResponse> dataSet;
    private GetCompaniesResponse checkedCompany;


    public CompaniesAdapter(Context context, List<GetCompaniesResponse> items, OnClickListener callback) {
        super();
        this.context = context;
        this.dataSet = items;
        this.callback = callback;
    }

    @Override
    public CompaniesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.element_choose_company, viewGroup, false);
        return new CompaniesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CompaniesAdapter.ViewHolder viewHolder, final int position) {
        GetCompaniesResponse center = dataSet.get(position);

        check(viewHolder.radioIV, checkedCompany == center);

        viewHolder.entry.setText(center.companyName);
        viewHolder.itemView.setOnClickListener(view -> {
            if (checkedCompany == null) {
                // expand clicked view
                check(viewHolder.radioIV,  true);
                checkedCompany = center;
            } else if (checkedCompany == center) {
                // collapse clicked view
                check(viewHolder.radioIV,  false);
                checkedCompany = null;
            } else {
                // collapse previously expanded view
                final int  checkedCompanyPosition = dataSet.indexOf(checkedCompany);
                final ViewHolder oldViewHolder =
                        (ViewHolder) recyclerView.findViewHolderForAdapterPosition(checkedCompanyPosition);
                if (oldViewHolder != null) check(oldViewHolder.radioIV,  false);

                // expand clicked view
                check(viewHolder.radioIV,  true);
                checkedCompany = center;
            }
        });
    }

    private void check(ImageView radioIV, boolean b) {
        radioIV.setImageDrawable(radioIV.getContext().getDrawable(b?R.drawable.radio_checked_icon:R.drawable.radio_unchecked_icon));
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public int getCheckedCompanyId() {
        return checkedCompany.companyId;
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public void removeAt(int position) {
        dataSet.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataSet.size());
    }

    public interface OnClickListener {
        void onClick(int position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.entry)
        public TextView entry;

        @BindView(R.id.radioIV)
        public ImageView radioIV;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}