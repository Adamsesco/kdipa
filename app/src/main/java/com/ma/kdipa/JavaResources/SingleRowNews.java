package com.ma.kdipa.JavaResources;

public class SingleRowNews {
    private String  date;
    private String  title;
    private String  titleAr;
    private String  content;
    private String  contentAr;
    private String locale;


    public SingleRowNews(String date, String title, String titleAr, String content, String contentAr, String locale) {
       this.locale=locale;
        this.date = Utilitaire.formateDateFromstring(
                Constants.DATE_INPUT_FORMAT,
                Constants.DATE_EN_OUTPUT_FORMAT,
                date,
                locale
        );
        this.title = title;
        this.titleAr = titleAr;
        this.content = content;
        this.contentAr = contentAr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return locale.equals("ar") ?titleAr:title;
    }

    public void setTitle(String title) {
        if ((locale.equals("ar"))) {
            this.titleAr = title;
        } else {
            this.title = title;
        }
    }


    public String getContent() {
        return locale.equals("ar") ?contentAr:content;
    }

    public void setContent(String content) {
        if ((locale.equals("ar"))) {
            this.contentAr = content;
        } else {
            this.content = content;
        }
    }



    @Override
    public String toString() {
        return "SingleRowNews{" +
                "date='" + date + '\'' +
                ", title='" + title + '\'' +
                ", titleAr='" + titleAr + '\'' +
                ", content='" + content + '\'' +
                ", contentAr='" + contentAr + '\'' +
                '}';
    }
}
