package com.ma.kdipa.JavaResources;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_BASE_URL;
import static com.ma.kdipa.JavaResources.Constants.GEO_ATTEND;
import static com.ma.kdipa.JavaResources.Constants.GEO_ATTEND_CHECK;


public class BackgroundWorker extends AsyncTask<String, Void, String> {
//    public static String resultat;
    private static final String TAG = "BackgroundWorker";
    OnErrorListener onErrorListener;
    public interface OnErrorListener{
        void onError();
    }
    Context context;
    AlertDialog alertDialog;
    public BackgroundWorker(Context ctx){
        context=ctx;
    }
     public BackgroundWorker(Context ctx, OnErrorListener onErrorListener){
        context=ctx;
        this.onErrorListener=onErrorListener;
    }
    @Override
    protected String doInBackground(String... params) {
        String uri=params[0];

        try {
            URL url = new URL(EMPLOYEE_BASE_URL+uri);
            Log.d(TAG, "doInBackground: url: "+url);

            if (uri.equals(GEO_ATTEND) ) {
                String email = params[1];
                String password = params[2];
                String latitude = params[3];
                String logitude = params[4];
                String type = params[5];

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");
//
//
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("latitude", latitude);
                jsonParam.put("logitude", logitude);
                jsonParam.put("attendType", type);

                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }
            else if (uri.equals(GEO_ATTEND_CHECK)) {
                String email = params[1];
                String password = params[2];
                String latitude = params[3];
                String logitude = params[4];

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");
//
//
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("latitude", latitude);
                jsonParam.put("logitude", logitude);

                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }
                else if ("login".equals(uri)){
                String email = params[1];
                String password = params[2];
                String token = params[3];
                String device_type = params[4];

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("token", token);
                jsonParam.put("device_type", device_type);

                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }else if ("postNewVacation".equals(uri)){
                String email = params[1];
                String password = params[2];
                String vacationType = params[3];
                String fromDate = params[4];
                String toDate = params[5];
                String advancePayment = params[6];
                String delegatedEmployee = params[7];



                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("vacationType", vacationType);
                jsonParam.put("fromDate", fromDate);
                jsonParam.put("toDate", toDate);
                jsonParam.put("advancePayment", advancePayment);
                jsonParam.put("delegatedEmployee", delegatedEmployee);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();
                InputStream inputStream = httpURLConnection.getErrorStream();
                StringBuilder result;
                result = new StringBuilder();

                if (inputStream == null) {
                    inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result.append(line);
                    }
                    Log.d(TAG, "doInBackground: result: " + result);
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                }else{
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result.append(line);
                    }
                    Log.d(TAG, "doInBackground: result: " + result);
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                }
                return String.valueOf(httpURLConnection.getResponseCode());//result.toString();
            }else if ("postVacationReturn".equals(uri)){
                String email = params[1];
                String password = params[2];
                String vacationId = params[3];
                String returnDate = params[4];
                String lateReturnReason = params[5];




                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("vacationId", Utilitaire.encodeString(vacationId));
                jsonParam.put("returnDate", returnDate);
                jsonParam.put("lateReturnReason", lateReturnReason);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return String.valueOf(httpURLConnection.getResponseCode());//result.toString();
            }
            else if ("postVacationExtension".equals(uri)){
                String email = params[1];
                String password = params[2];
                String vacationId = params[3];
                String oldFromDate = params[4];
                String oldToDate = params[5];
                String newToDate = params[6];
                String advancedPayment = params[7];
                String delegatedEmployee = params[8];



                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("vacationId", Utilitaire.encodeString(vacationId));
                jsonParam.put("oldFromDate", oldFromDate);
                jsonParam.put("oldToDate", oldToDate);
                jsonParam.put("newToDate", newToDate);
                jsonParam.put("advancedPayment", advancedPayment);
                jsonParam.put("delegatedEmployee", delegatedEmployee);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return String.valueOf(httpURLConnection.getResponseCode());//result.toString();
            }
            else if ("postVacationAmendment".equals(uri)){
                String email = params[1];
                String password = params[2];
                String vacationType = params[3];
                String oldFromDate = params[4];
                String oldToDate = params[5];
                String newFromDate = params[6];
                String newToDate = params[7];
                String advancedPayment = params[8];
                String delegatedEmployee = params[9];



                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("vacationType", vacationType);
                jsonParam.put("oldFromDate", oldFromDate);
                jsonParam.put("oldToDate", oldToDate);
                jsonParam.put("newFromDate", newFromDate);
                jsonParam.put("newToDate", newToDate);
                jsonParam.put("advancedPayment", advancedPayment);
                jsonParam.put("delegatedEmployee", delegatedEmployee);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return String.valueOf(httpURLConnection.getResponseCode());//result.toString();
            }

            else if ("postNewDuty".equals(uri)){
                String email = params[1];
                String password = params[2];
                String dutyType = params[3];
                String fromDate = params[4];
                String toDate = params[5];
                String attendanceExemption = params[6];
                String employees = params[7];
                String delegatedEmployees = params[8];



                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
//                jsonParam.put("dutyType", Utilitaire.encodeString(dutyType));
                jsonParam.put("dutyType", dutyType);
                jsonParam.put("fromDate", fromDate);
                jsonParam.put("toDate", toDate);
                jsonParam.put("attendanceExemption", attendanceExemption);
                jsonParam.put("employees", employees);
                jsonParam.put("delegatedEmployees", delegatedEmployees);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getErrorStream();
                String result = "";
                if (inputStream == null) {
                    inputStream = httpURLConnection.getInputStream();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    Log.d(TAG, "doInBackground: result: " + result);
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                }else{
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    Log.d(TAG, "doInBackground: result: " + result);
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                }
                return String.valueOf(httpURLConnection.getResponseCode());//result.toString();
            }
            else if (uri.contains("manager")){
                String email = params[1];
                String password = params[2];
                String employee = params[3];

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("employee", employee);

                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }else if ("postVacationCancel".equals(uri)){
                String email = params[1];
                String password = params[2];
                String vacationId = params[3];
                String cancelDate = params[4];




                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);
                jsonParam.put("vacationId", Utilitaire.encodeString(vacationId));
                jsonParam.put("cancelDate", cancelDate);


                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }

            else{
                String email = params[1];
                String password = params[2];

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("email", email);
                jsonParam.put("password", password);

                Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                Log.d(TAG, "doInBackground: result: " + result);
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (onErrorListener != null) {
                onErrorListener.onError();
            }
        }
        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
