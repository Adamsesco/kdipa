package com.ma.kdipa.JavaResources;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class PagerAdapterDotIndicator extends PagerAdapter {
  private ArrayList<SinglePage> mList=new ArrayList<>();

  public PagerAdapterDotIndicator(ArrayList<SinglePage> mList) {
    this.mList = mList;
  }

  @NonNull
  @Override
  public Object instantiateItem(@NonNull ViewGroup container, int position) {
    View item = (View) LayoutInflater.from(container.getContext()).inflate(
                    R.layout.e_permission_element,
            container, false);


    ((TextView) item.findViewById(R.id.trxDate)).setText(mList.get(position).getTrxDate());
    ((TextView) item.findViewById(R.id.type)).setText(mList.get(position).getType());
    ((TextView) item.findViewById(R.id.from)).setText(mList.get(position).getFrom());
    ((TextView) item.findViewById(R.id.to)).setText(mList.get(position).getTo());
    ((TextView) item.findViewById(R.id.periode)).setText(mList.get(position).getPeriode());

    container.addView(item);
    return item;
  }

  @Override
  public int getCount() {
    return mList.size();
  }

  @Override
  public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
    return view == object;
  }

  @Override
  public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
    container.removeView((View) object);
  }

  private static class Item {
    private final int color;

    private Item(int color) {
      this.color = color;
    }
  }
}
