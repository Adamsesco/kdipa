package com.ma.kdipa.JavaResources;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static com.ma.kdipa.JavaResources.Constants.INVESTOR_API_VALUE;
import static com.ma.kdipa.JavaResources.Constants.INVESTOR_BASE_URL;


public class BackgroundWorkerInvestor extends AsyncTask<String, Void, String[]> {
    private static final String TAG = "BackgroundWorkerInvesto";

    Context context;
    AlertDialog alertDialog;
    public BackgroundWorkerInvestor(Context ctx){
        context=ctx;
    }
    @Override
    protected String[] doInBackground(String... params) {
        String uri=params[0];
        try {
            switch (uri) {
                case "token/refresh": {
                    URL url = new URL(INVESTOR_BASE_URL + "auth/"+uri);
                    Log.d(TAG, "doInBackground: url: " + url);


                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    httpURLConnection.setRequestProperty("Accept", "application/json");
                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);


                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());

                    os.flush();
                    os.close();


                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    Log.d(TAG, "doInBackground: result: " + result);
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return new String[] {result,""};
                }
                case "signin": {
                    URL url = new URL(INVESTOR_BASE_URL+"auth/signin");
                    Log.d(TAG, "doInBackground: url: " + url);
                    String email = params[1];
                    String password = params[2];
                    int companyId = Integer.parseInt(params[3]);

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    httpURLConnection.setRequestProperty("Accept", "application/json");
                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);


                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("username", email);
                    jsonParam.put("password", password);
                    jsonParam.put("companyId", companyId);

                    Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();


                    Log.d(TAG, "doInBackground: responseCode: "+httpURLConnection.getResponseCode());
                    InputStream inputStream = httpURLConnection.getErrorStream();
                    String result = "";
                    if (inputStream == null) {
                        inputStream = httpURLConnection.getInputStream();

                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }else{
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }

                    return new String[] {String.valueOf(httpURLConnection.getResponseCode()),result};
                }
                case "applyForUsername": {
                    URL url = new URL(INVESTOR_BASE_URL+uri);
                    Log.d(TAG, "doInBackground: url: " + url);
                    String account = params[1];
                    String firstName = params[2];
                    String lastName = params[3];
                    String companyName = params[4];
                    String phone = params[5];

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    httpURLConnection.setRequestProperty("Accept", "application/json");

                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);


                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("account", account);
                    jsonParam.put("firstName", firstName);
                    jsonParam.put("lastName", lastName);
                    jsonParam.put("companyName", companyName);
                    jsonParam.put("phone", phone);

                    Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();


                    Log.d(TAG, "doInBackground: responseCode: "+httpURLConnection.getResponseCode());
                    InputStream inputStream = httpURLConnection.getErrorStream();
                    String result = "";
                    if (inputStream == null) {
                        inputStream = httpURLConnection.getInputStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }
                    return new String[] {String.valueOf(httpURLConnection.getResponseCode()),result};

                }

                case "bookingForm": {
                    URL url = new URL(INVESTOR_BASE_URL +uri);
                    Log.d(TAG, "doInBackground: url: " + url);



                    String token = params[1];
                    Integer id = params[2]==null?null:Integer.valueOf(params[2]);
                    String version = params[3];
                    int bookingServiceId = Integer.parseInt(params[4]);
                    int accountManagerId = Integer.parseInt(params[5]);
                    String meetingDate = params[6];
                    String meetingTime = params[7];
                    String details = params[8];

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");

                    httpURLConnection.setRequestProperty("Accept", "application/json");
                    httpURLConnection.setRequestProperty("X-Auth-Token", token);
                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);


                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("id", id);
                    jsonParam.put("version", version);
                    jsonParam.put("bookingServiceId", bookingServiceId);
                    jsonParam.put("accountManagerId", accountManagerId);
                    jsonParam.put("meetingDate", meetingDate);
                    jsonParam.put("meetingTime", meetingTime);
                    jsonParam.put("details", details);


                    Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();


                    Log.d(TAG, "doInBackground: responseCode: "+httpURLConnection.getResponseCode());
                    for (Map.Entry<String, List<String>> header : httpURLConnection.getHeaderFields().entrySet()) {
                        Log.d(TAG, "doInBackground: "+header.getKey() + "=" + header.getValue());
                    }
                    InputStream inputStream = httpURLConnection.getErrorStream();
                    String result = "";
                    if (inputStream == null) {
                        inputStream = httpURLConnection.getInputStream();

                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }else{
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }
                    return new String[] {String.valueOf(httpURLConnection.getResponseCode()),result};
                }
                case "investor-dashboard": {
                    URL url = new URL(INVESTOR_BASE_URL +uri);
                    Log.d(TAG, "doInBackground: url: " + url);



                    String token = params[1];
                    Log.d(TAG, "doInBackground: token: "+token);


                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");

                    httpURLConnection.setRequestProperty("Content-Length", "0");
                    httpURLConnection.setRequestProperty("Host", "62.150.248.68:8080");
//                    httpURLConnection.setRequestProperty("Accept", "*/*");
                    httpURLConnection.setRequestProperty("X-Auth-Token", token);
                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();



                    Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
//                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
//                    os.writeBytes(jsonParam.toString());
//
//                    os.flush();
//                    os.close();


                    Log.d(TAG, "doInBackground: responseCode: "+httpURLConnection.getResponseCode());
                    for (Map.Entry<String, List<String>> header : httpURLConnection.getHeaderFields().entrySet()) {
                        Log.d(TAG, "doInBackground: "+header.getKey() + "=" + header.getValue());
                    }
                    InputStream inputStream = httpURLConnection.getErrorStream();
                    String result = "";
                    if (inputStream == null) {
                        inputStream = httpURLConnection.getInputStream();

                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }else{
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1

                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }
                        Log.d(TAG, "doInBackground: result2: " + result);
                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();
                    }
                    return new String[] {String.valueOf(httpURLConnection.getResponseCode()),result};
                }
                case "forgotPassword": {
                    URL url = new URL(INVESTOR_BASE_URL + "auth/"+uri);
                    Log.d(TAG, "doInBackground: url: " + url);
                    String email = params[1];

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    httpURLConnection.setRequestProperty("Accept", "application/json");
                    httpURLConnection.setRequestProperty("API-KEY", INVESTOR_API_VALUE);

                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);


                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("account", email);

                    Log.d(TAG, "doInBackground: Json: " + jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    return new String[] {String.valueOf(httpURLConnection.getResponseCode()),""};

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "doInBackground: error: " + e);
        }
        return new String[]{"",""};
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String[] result) {
        super.onPostExecute(result);
//        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
