package com.ma.kdipa.JavaResources;

public final class Constants {

    public static final String EMPLOYEE_BASE_URL = "http://62.150.248.71:3002/api/";//official
//public static final String EMPLOYEE_BASE_URL="http://216.250.119.123:3001/api/";//demo
    public static final String LAST_LOGIN_USER_INFO = "LAST_LOGIN_USER_INFO";
//    public static final String INVESTOR_BASE_URL="http://62.150.248.68:8080/api/";

    public static boolean DEMO = false;
    public static boolean DEMO_KDIPA_LOCATION = false;

    //    public static final String INVESTOR_BASE_URL="https://portal.kdipa.gov.kw/api/";
    public static final String INVESTOR_BASE_URL = "https://portaltest.kdipa.gov.kw/api/";

    public static final String INVESTOR_API_VALUE = "API-KEY-MOBILE-123";

    //the DATE_INPUT_FORMAT depends on the date format given by the server, it must not be changed unless
    // the server begins giving another format, otherwise an exception will be thrown
    public static final String DATE_INPUT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";//"2020-03-04T14:00:13.927.000Z" //
    public static final String DATE_INPUT_FORMAT_WITHOUT_TIME_ZONE = "yyyy-MM-dd'T'HH:mm:ss.SSS";//"2020-03-04T14:00:13.927.000Z"
    //"2018-11-25 T 00:00:00.000Z"
    public static final String DATE_SIMPLE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_SIMPLE2_FORMAT = "dd/MM/yyyy";

    public static final String DATE_EN_OUTPUT_FORMAT = "EEEE, d MMMM yyyy";
    public static final String DATE_AR_OUTPUT_FORMAT = "EEEE، d MMMM yyyy";

    public static final String FULL_DATE_EN_OUTPUT_FORMAT = "EEE d MMMM yyyy h':'m a";
    public static final String FULL_DATE_AR_OUTPUT_FORMAT = "EEE d MMMM yyyy HH':'mm";

    public static final String FULL_DATE_EN_OUTPUT_FORMAT2 = "EEEE d MMMM yyyy '\nat' h':'m a";
    public static final String FULL_DATE_AR_OUTPUT_FORMAT2 = "EEEE d MMMM yyyy '\n' HH':'mm";

    public static final String DATE_TODAY_EN_OUTPUT_FORMAT = "'Today' h':'m a";
    public static final String DATE_TODAY_AR_OUTPUT_FORMAT = "'اليوم' HH':'mm";
    public static final String DATE_YESTERDAY_EN_OUTPUT_FORMAT = "'Yesterday' h':'m a";
    public static final String DATE_YESTERDAY_AR_OUTPUT_FORMAT = "'الأمس' HH':'mm";
    public static final String YEAR_MONTH_DAY_HOUR_MIN_SEC_EN = "dd'-'MM'-'yyyy' at 'HH':'mm':'ss";
    public static final String YEAR_MONTH_DAY_HOUR_MIN_SEC_AR = "dd'-'MM'-'yyyy' مع 'HH':'mm':'ss";


    public static final String YEAR_MONTH_DAY = "yyyy'/'MM'/'dd";
    public static final String YEAR_MONTH_DAY2 = "yyyy'-'MM'-'dd";
    public static final String YEAR_MONTH_DAY_OFFICIAL_DUTY = "yyyy'/'MM'/'dd";
    public static final String YEAR_MONTH_DAY_with_dash = "yyyy-MM-dd";
    public static final String DAY_MONTH_YEAR = "dd'/'MM'/'yyyy";
    public static final String CHOOSE_DATE_DISPLAY = "EEE, MMM d";
    public static final String CHOOSE_DATE_AR_DISPLAY = "EEE، d MMM";


    //For SharedPreferences
    public static final String PASSWORD = "PASSWORD";
    public static final String EMAIL = "EMAIL";
    public static final String NAME = "NAME";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String USER_INFO = "USER_INFO";
    public static final String TOKEN = "TOKEN";
    public static final String FIRE_BASE_TOKEN = "FIRE_BASE_TOKEN";
    public static final String IS_MANAGER = "IS_MANAGER";
    public static final String MANAGER_ID = "MANAGER_ID";

    public static final String MOCI = "MOCI";
    public static final String IS_ACCOUNT_MANAGER = "IS_ACCOUNT_MANAGER";
    public static final String ID = "ID";
    public static final String LOCALE = "LOCALE";
    public static final String EMPLOYEE_DOMAIN = "burgan-systems.com";
    public static final String EMPLOYEE_DOMAIN_2 = "kdipa.gov.kw";
    public static final String USE_TOUCH_ID = "USE_TOUCH_ID";


    public static final String IS_IN_FORGROUND = "IS_IN_FORGROUND";


    //For uri:
    public static final String GEO_ATTEND = "geoAttend";
    public static final String GEO_ATTEND_CHECK = "geoAttendCheck";
    public static final String FORGOT_PASSWORD = "forgotPassword";


    //file names
    public static final String VACATION_TYPES = "VACATION_TYPES";
    public static final String VACATION_IDS = "VACATION_IDS";
    public static final String VACATION_AR_TYPES = "VACATION_AR_TYPES";
    public static final String VACATION_PREFERENCES = "VACATION_PREFERENCES";
    public static final String EMPLOYEES_LIST = "employees_list";
    public static final String DELEGATED_LIST = "delegated_list";


    //For max time to punch in
    public static final String MAX_TIME = "08:30";


    public static final String DEVICE_TYPE = "Android";
    public static final int MISSING_FIREBASE_TOKEN = 9543218;
    public static final int ALREADY_BOOKED_AT_THAT_TIME = 6513218;
    public static final int BOOKING_ERROR = 16548;
    public static final String NOTIFICATION_INTENT = "NOTIFICATION_INTENT";
    public static final String DID_SEE_INTRO = "DID_SEE_INTRO";
    public static final String USERNAME = "USERNAME";
    public static final String TEMP_FILE = "temp_file";
    public static final String USER_LANGUAGE = "USER_LANGUAGE";
    public static final String FIRST_TIME_LAUNCH = "FIRST_TIME_LAUNCH";
    public static final String ENGLISH_LANGUAGE = "en";
    public static final String ARABIC_LANGUAGE = "ar";
    public static final String COMPANY_ID = "COMPANY_ID";
    public static final String INVESTOR_ACCOUNT_MANAGER_ID = "INVESTOR_ACCOUNT_MANAGER_ID";
    public static final String IS_SICK_LEAVE_INTENT = "IS_SICK_LEAVE_INTENT";
    public static final String SICK = "مــرضـــيــة";
    public static final String LAST_TIME_CONNECTED = "LAST_TIME_CONNECTED";
    public static final long EXPIRY_TIME = 15 * 60 * 1000;
    public static final long EXPIRY_PUNCH = 5 * 60 * 1000;
    private static final String TAG = "Constants";


}