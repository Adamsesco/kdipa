package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.Nullable;

import com.ma.kdipa.R;

public class CustomSolidArc extends View {

    private static float STROKE_WIDTH;
    Paint mThickStrokePaint;
    Paint mSolidPaint;
//    Paint mBGPaint;
    RectF mRectF;
    float mStartingAngle;
    float  mSweepAngle;
    int mCustomSolidArcColor;
    float mCustomSolidArcSetValue;
    float mChartPercent;
    float mCustomSolidArcSetThickness;


    int mWidth;
    int mHeight;
    float mRadius;

    private static final String TAG = "CustomSolidArc";

    public CustomSolidArc(Context context) {
        super(context);
        init(null);
    }

    public CustomSolidArc(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomSolidArc(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);

    }


    private void init(@Nullable AttributeSet attrs){


        STROKE_WIDTH=dip2px(20.0f);
        mCustomSolidArcSetValue = 20.0f;


        mThickStrokePaint =new Paint(Paint.ANTI_ALIAS_FLAG);
        mSolidPaint =new Paint(Paint.ANTI_ALIAS_FLAG);
//        mBGPaint =new Paint(Paint.ANTI_ALIAS_FLAG);
        mStartingAngle=0.0f;
        mSweepAngle=180f;

        mRectF =new RectF();


        this.setWillNotDraw(false);

        if (attrs==null){
            return;
        }
        TypedArray ta=getContext().obtainStyledAttributes(attrs,R.styleable.CustomSolidArc);
        mCustomSolidArcColor =ta.getColor(R.styleable.CustomSolidArc_color, Color.GREEN);
        mChartPercent =ta.getFloat(R.styleable.CustomSolidArc_percent,70.0f);
        mCustomSolidArcSetValue =ta.getFloat(R.styleable.CustomSolidArc_value, mChartPercent *360.0f/100.0f);
        mCustomSolidArcSetThickness =ta.getDimension(R.styleable.CustomSolidArc_thickness,dip2px(10));
        STROKE_WIDTH=mCustomSolidArcSetThickness;

        mSolidPaint.setColor(mCustomSolidArcColor);
        mSolidPaint.setStyle(Paint.Style.FILL);

//        mBGPaint.setColor(Color.RED);
//        mBGPaint.setStyle(Paint.Style.FILL);

        mThickStrokePaint.setColor(mCustomSolidArcColor);
        mThickStrokePaint.setStyle(Paint.Style.STROKE);
        mThickStrokePaint.setStrokeWidth(STROKE_WIDTH);

        ta.recycle();

        setSolidArc(mCustomSolidArcSetValue);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth=getWidth();
        mHeight=getHeight();
        mRadius=mWidth/2.0f-STROKE_WIDTH/2.0f;

        mRectF.top=0+STROKE_WIDTH/2.0f;
        mRectF.bottom=mHeight-STROKE_WIDTH/2.0f;

        mRectF.left =0+STROKE_WIDTH/2.0f;
        mRectF.right=mWidth-STROKE_WIDTH/2.0f;
    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    private float dip2px(float dip) {
        Resources r = getResources();
        float px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip,
                r.getDisplayMetrics()
        );
        return px;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.drawRect(mRectF, mBGPaint);
        canvas.drawArc(mRectF, mStartingAngle+90, mSweepAngle, false, mThickStrokePaint);
        canvas.drawCircle(
                (float)((mRadius)*Math.cos(Math.toRadians(mSweepAngle+90))+mWidth/2.0f),
                (float) ((mRadius)*Math.sin(Math.toRadians(mSweepAngle+90))+mHeight/2.0f),
                STROKE_WIDTH/2.0f,
                mSolidPaint
        );
        canvas.drawCircle(
                (float)((mRadius)*Math.cos(Math.toRadians(90))+mWidth/2.0f),
                (float) ((mRadius)*Math.sin(Math.toRadians(90))+mHeight/2.0f),
                STROKE_WIDTH/2.0f,
                mSolidPaint
        );
    }

    public void setSolidArc(float f) {
        mStartingAngle=0;
        mSweepAngle=f;
        invalidate();
    }
    public void setPiePercent(float f) {
        mStartingAngle=0;
        mChartPercent =f;
        mSweepAngle=360*f/100.0f;
        invalidate();
    }
    public float getPiePercent(){
        return mChartPercent;
    }
    public void setPieColor(int color){
        mCustomSolidArcColor=color;
        mSolidPaint.setColor(mCustomSolidArcColor);
        mThickStrokePaint.setColor(mCustomSolidArcColor);


        invalidate();
    }

    public int getPieColor() {
        return mCustomSolidArcColor;
    }


}

