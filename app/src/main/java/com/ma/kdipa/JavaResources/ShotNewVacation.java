package com.ma.kdipa.JavaResources;

import android.content.Context;

import java.util.ArrayList;

public class ShotNewVacation {
    private ArrayList<String> typesList;
    private ArrayList<String> typesArList;

    private String departureDate;
    private String returnDate;



    private ArrayList<String> delegatedEmployeesList;
    private ArrayList<String> delegatedEmployeesArList;

    public ShotNewVacation(Context context, ArrayList<String> typesList, ArrayList<String> typesArList, String departureDate, String returnDate, ArrayList<String> delegatedEmployeesList, ArrayList<String> delegatedEmployeesArList) {
        this.typesList = typesList;
        this.typesArList = typesArList;

        String locale=context.getResources().getConfiguration().locale.getLanguage();
        this.departureDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",departureDate, locale);
        this.departureDate=Utilitaire.replaceArabicNumbers(this.departureDate);
        this.returnDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",returnDate, locale);
        this.returnDate=Utilitaire.replaceArabicNumbers(this.returnDate);


        this.delegatedEmployeesList=delegatedEmployeesList;
        this.delegatedEmployeesArList=delegatedEmployeesArList;
    }

    public ArrayList<String> getTypesList() {
        return typesList;
    }

    public void setTypesList(ArrayList<String> typesList) {
        this.typesList = typesList;
    }

    public ArrayList<String> getTypesArList() {
        return typesArList;
    }

    public void setTypesArList(ArrayList<String> typesArList) {
        this.typesArList = typesArList;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public ArrayList<String> getDelegatedEmployeesList() {
        return delegatedEmployeesList;
    }

    public void setDelegatedEmployeesList(ArrayList<String> delegatedEmployeesList) {
        this.delegatedEmployeesList = delegatedEmployeesList;
    }

    public ArrayList<String> getDelegatedEmployeesArList() {
        return delegatedEmployeesArList;
    }

    public void setDelegatedEmployeesArList(ArrayList<String> delegatedEmployeesArList) {
        this.delegatedEmployeesArList = delegatedEmployeesArList;
    }


}
