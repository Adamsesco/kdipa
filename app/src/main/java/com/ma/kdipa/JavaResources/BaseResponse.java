package com.ma.kdipa.JavaResources;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("Data")
    public T Data;
}
