package com.ma.kdipa.JavaResources;

import android.content.Context;

import java.util.ArrayList;

public class ShotVacationReturnExtension {
    private ArrayList<String> typesList;
    private ArrayList<String> typesArList;

    private ArrayList<VacationsEntry> vacationsEntries;

    private String returnDate;

    public ArrayList<VacationsEntry> getVacationsEntries() {
        return vacationsEntries;
    }

    public void setVacationsEntries(ArrayList<VacationsEntry> vacationsEntries) {
        this.vacationsEntries = vacationsEntries;
    }

    public ShotVacationReturnExtension(ArrayList<String> typesList, ArrayList<String> typesArList, ArrayList<VacationsEntry> vacationsEntries, String returnDate, String locale) {
        this.typesList = typesList;
        this.typesArList = typesArList;
        this.vacationsEntries = vacationsEntries;
        this.returnDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",returnDate, locale);
        this.returnDate=Utilitaire.replaceArabicNumbers(this.returnDate);    }


    public ArrayList<String> getTypesList() {
        return typesList;
    }

    public void setTypesList(ArrayList<String> typesList) {
        this.typesList = typesList;
    }

    public ArrayList<String> getTypesArList() {
        return typesArList;
    }

    public void setTypesArList(ArrayList<String> typesArList) {
        this.typesArList = typesArList;
    }



    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }


}
