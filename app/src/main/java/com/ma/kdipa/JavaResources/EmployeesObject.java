package com.ma.kdipa.JavaResources;

public class EmployeesObject{
    String id;
    String email;
    String name;

    public EmployeesObject(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public EmployeesObject(String id, String email, String name) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
