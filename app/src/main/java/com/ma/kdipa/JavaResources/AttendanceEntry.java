package com.ma.kdipa.JavaResources;

import android.util.Log;

public class AttendanceEntry {
    private Integer attendance;
    private Integer vacations;
    private Integer absences;

    private int totalDays;
    private int attendanceRatio;
    private int permissionsRatio;
    private int vacationsRatio;
    private int absencesRatio;

    private static final String TAG = "AttendanceEntry";
    public AttendanceEntry(Integer attendance, Integer vacations, Integer absences) {
        Log.d(TAG, "AttendanceEntry() called with: attendance = [" + attendance + "], vacations = [" + vacations + "], absences = [" + absences + "]");
        this.attendance = attendance;
        this.vacations = vacations;
        this.absences = absences;

        this.totalDays =attendance+absences;

        if(totalDays!=0){
            this.attendanceRatio=(attendance)*1000/totalDays;
            this.vacationsRatio=(vacations)*1000/totalDays;
            this.absencesRatio=(absences)*1000/totalDays;
        }else{
            this.attendanceRatio=0;
            this.vacationsRatio=0;
            this.absencesRatio=0;
        }

    }

    public Integer getAttendance() {
        return attendance;
    }

    public void setAttendance(Integer attendance) {
        this.attendance = attendance;
    }


    public Integer getVacations() {
        return vacations;
    }

    public void setVacations(Integer vacations) {
        this.vacations = vacations;
    }

    public Integer getAbsences() {
        return absences;
    }

    public void setAbsences(Integer absences) {
        this.absences = absences;
    }

    public int getTotalDays() {
        return totalDays;
    }

    public float getAttendanceRatio() {
        return attendanceRatio;
    }

    public float getPermissionsRatio() {
        return permissionsRatio;
    }

    public float getVacationsRatio() {
        return vacationsRatio;
    }

    public float getAbsencesRatio() {
        return absencesRatio;
    }
}
