package com.ma.kdipa.JavaResources;

import com.ma.kdipa.R;

public class SingleRowApplications {
    public String date;
    public String id;
    private String profile;
    public String type;
    private String status;
    private int statusCode;
    private int statusColor;

    public SingleRowApplications(String date, String id, String profile, String type, String status) {
        this.date = date;
        this.id = id;
        this.profile = profile;
        this.type = type;
        this.status = status;
        this.statusCode = status.equals("APPROVED")?(R.drawable.approved_icon):R.drawable.pending_icon;
        this.statusColor=status.equals("APPROVED")?(R.color.approved):R.color.pending;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }


    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(int statusColor) {
        this.statusColor = statusColor;
    }

    public String getProfile() {
        return profile;
    }

    public String getType() {
        return type;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void setType(String type) {
        this.type = type;
    }
}
