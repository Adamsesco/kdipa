package com.ma.kdipa.JavaResources;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.ma.kdipa.R;

public class DialogBoxLoadingClass extends Dialog {
    private static final String TAG = "DialogBoxClass";


    public DialogBoxLoadingClass(Activity a) {
        super(a);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_simple_loading);
    }
}
