package com.ma.kdipa.JavaResources;

public class SingleRowNotifs {
private String title;
private String date;
private String content;
private int iconCode;
private boolean isExpanded;

    public SingleRowNotifs(String title, String date, String content, int iconCode) {
        this.title = title;
        this.date = date;
        this.content = content;
        this.iconCode = iconCode;
    }
    public SingleRowNotifs(String title, String date, String content) {
        this.title = title;
        this.date = date;
        this.content = content;
        this.isExpanded=false;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public int getIconCode() {
        return iconCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setIconCode(int iconCode) {
        this.iconCode = iconCode;
    }

}
