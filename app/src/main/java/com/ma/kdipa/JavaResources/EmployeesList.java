package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EmployeesList {

    private static final String TAG = "EmployeesList";
    private static EmployeesList mInstance= null;

    private ArrayList<EmployeesObject> employeesList;
    private ArrayList<EmployeesArObject> employeesArList;

    private EmployeesList(ArrayList<EmployeesObject> employeesList, ArrayList<EmployeesArObject> employeesArList) {
        this.employeesList = employeesList;
        this.employeesArList = employeesArList;
    }


    public ArrayList<EmployeesObject> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(ArrayList<EmployeesObject> employeesList) {
        this.employeesList = employeesList;
    }

    public ArrayList<EmployeesArObject> getEmployeesArList() {
        return employeesArList;
    }

    public void setEmployeesArList(ArrayList<EmployeesArObject> employeesArList) {
        this.employeesArList = employeesArList;
    }
    public static synchronized void newInstance(Context context, String result) {
        if(null == mInstance){
            mInstance =  EmployeesList.loadEmployeesList(context, result);
            Log.d(TAG, "newInstance: called");
        }
    }
    public static synchronized EmployeesList getInstance() {
        return mInstance;
    }


    public static EmployeesList loadEmployeesList(Context context, String result){
    try {

        JSONObject jsonObject=new JSONObject(result);
        JSONArray employeesArray=jsonObject.getJSONArray("employees");
        ArrayList<EmployeesObject>  employeesItemArray=new ArrayList<>();
        for (int i=0; i<employeesArray.length(); i++){
            JSONObject subObject=employeesArray.getJSONObject(i);
            String employeeID=subObject.getString("EmployeeID");
            String email=subObject.getString("Email");
            String employeeName=subObject.getString("EmployeeName");
            EmployeesObject employeesItem = new EmployeesObject(employeeID,email,employeeName);
            employeesItemArray.add(employeesItem);
        }
        JSONArray employeesArArray=jsonObject.getJSONArray("employees");
        ArrayList<EmployeesArObject>  employeesArItemArray=new ArrayList<>();
        for (int i=0; i<employeesArArray.length(); i++){
            JSONObject subObject=employeesArArray.getJSONObject(i);
            String employeeID=subObject.getString("EmployeeID");
            String employeeName=subObject.getString("EmployeeName");
            EmployeesArObject employeesArItem = new EmployeesArObject(employeeID,employeeName);
            employeesArItemArray.add(employeesArItem);
        }
        return new EmployeesList(employeesItemArray,employeesArItemArray);

    } catch (JSONException e) {
        e.printStackTrace();
    }
    return null;
}
    public ArrayList<String> getEmployeesNamesList(){
        ArrayList<String> employeesNamesList=new ArrayList<>();
        for (int i = 0; i < this.getEmployeesList().size(); i++) {
            employeesNamesList.add(this.getEmployeesList().get(i).getName());
        }
        return employeesNamesList;
    }
    public  ArrayList<String> getEmployeesArNamesList(){
        ArrayList<String> employeesArNamesList=new ArrayList<>();
        for (int i = 0; i < this.getEmployeesArList().size(); i++) {
            employeesArNamesList.add(this.getEmployeesArList().get(i).getName());
        }
        return employeesArNamesList;
    }
}

