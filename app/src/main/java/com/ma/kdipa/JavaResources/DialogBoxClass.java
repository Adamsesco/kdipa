package com.ma.kdipa.JavaResources;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ma.kdipa.EmployeeGeolocationPunch;
import com.ma.kdipa.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DialogBoxClass extends Dialog implements android.view.View.OnClickListener {
    private static final String TAG = "DialogBoxClass";
    TextView dateTv;
    String dateTxt;
    TextView ok;

    OkClick okClick;

    Activity activity;
    public DialogBoxClass(Activity a) {
        super(a);
        activity=a;
        okClick= (OkClick) a;
    }

    public DialogBoxClass(Activity a, String successfulPunchTime) {
        super(a);
        activity=a;
        okClick= (OkClick) a;
        dateTxt=successfulPunchTime;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_succussful_punch);
        dateTv = findViewById(R.id.date);
        if (dateTxt != null) {
            dateTv.setText(dateTxt);
        }
//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat format1 = new SimpleDateFormat("EEEE dd MMMM yyyy'\n'hh':'mm a", new Locale("en"));
//
//
//        String formatted = format1.format(cal.getTime());

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        findViewById(R.id.topLevel).getLayoutParams().width=(int) (width/1.5);
        findViewById(R.id.topLevel).getLayoutParams().height=height/3;
//        Window window = getWindow();

//        window.setLayout(width/2, height/3);
//        window.setGravity(Gravity.CENTER);

        ok = (TextView) findViewById(R.id.button);
        ok.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.button){
//            Utilitaire.getActivity(v).finish();
            dismiss();
            Log.d(TAG, "onClick: dismiss");
            okClick.onOkClick();
        }
    }



    public interface OkClick{
        public void onOkClick();
    }
}
