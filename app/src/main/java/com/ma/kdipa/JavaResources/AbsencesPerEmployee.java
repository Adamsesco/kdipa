package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class AbsencesPerEmployee {






    private String name="";
    private String name_Ar="";
    private ArrayList<AbsencesEntriesPerYear> yearsList=new ArrayList<>();

    public AbsencesPerEmployee(String name,String name_Ar, ArrayList<AbsencesEntriesPerYear> yearsList) {
        this.name = name;
        this.name_Ar = name_Ar;
        this.yearsList = yearsList;
    }

    public AbsencesPerEmployee() {

    }


    public String getName() {
        return name;
    }

    public String getName_Ar() {
        return name_Ar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setName_Ar(String name_Ar) {
        this.name_Ar = name_Ar;
    }

    public void setYearsList(ArrayList<AbsencesEntriesPerYear> yearsList) {
        this.yearsList = yearsList;
    }

    public ArrayList<AbsencesEntriesPerYear> getYearsList() {
        return yearsList;
    }



}
