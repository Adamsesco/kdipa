package com.ma.kdipa.JavaResources;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ma.kdipa.R;

public class DialogBoxCustomClass extends Dialog implements View.OnClickListener {
    private static final String TAG = "DialogBoxClass";
    TextView content;
    TextView title;
    TextView button;

    OnButtonClickListener onButtonClickListener;
    String titleTxt;
    String contentTxt;
    String buttonTxt;


    Activity activity;


    public DialogBoxCustomClass(Activity a, String titleTxt, String contentTxt, String buttonTxt) {
        super(a);
        activity=a;
        onButtonClickListener = (OnButtonClickListener) a;
        this.contentTxt=contentTxt;
        this.buttonTxt=buttonTxt;
        this.titleTxt=titleTxt;
    }
    public DialogBoxCustomClass(Activity a, String titleTxt, String contentTxt, String buttonTxt, OnButtonClickListener onButtonClickListener) {
        super(a);
        activity=a;
        this.onButtonClickListener =onButtonClickListener;
        this.contentTxt=contentTxt;
        this.buttonTxt=buttonTxt;
        this.titleTxt=titleTxt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_custom);




        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        findViewById(R.id.topLevel).getLayoutParams().width=(int) (width/1.5);
        findViewById(R.id.topLevel).getLayoutParams().height=height/3;
        

        title=findViewById(R.id.title);
        content=findViewById(R.id.content);
        title.setText(titleTxt);
        content.setText(contentTxt);
//        Window window = getWindow();

//        window.setLayout(width/2, height/3);
//        window.setGravity(Gravity.CENTER);

        button = (TextView) findViewById(R.id.button);
        button.setText(buttonTxt);
        button.setOnClickListener((View.OnClickListener) this);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.button){
//            Utilitaire.getActivity(v).finish();
            dismiss();
            Log.d(TAG, "onClick: dismiss");
            onButtonClickListener.onButtonClick(buttonTxt);
        }
    }
    public interface OnButtonClickListener {
        public void onButtonClick(String buttonTxt);
    }
}
