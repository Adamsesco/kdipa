package com.ma.kdipa.JavaResources;

import com.google.gson.annotations.SerializedName;

public class BasicResponse {
    @SerializedName("ErrorCode")
    public int ErrorCode;
    @SerializedName("ErrorMessage")
    public String ErrorMessage;
}
