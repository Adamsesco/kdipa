package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class AttendancePerEmployee {
    private String name="";
    private String nameAr ="";
    private ArrayList<AttendanceEntriesPerYear> attendanceEntriesPerYears =new ArrayList<>();

    public AttendancePerEmployee(String name, String nameAr, ArrayList<AttendanceEntriesPerYear> attendanceEntriesPerYears) {
        this.name = name;
        this.nameAr = nameAr;
        this.attendanceEntriesPerYears = attendanceEntriesPerYears;
    }

    public AttendancePerEmployee() {

    }


    public String getName() {
        return name;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public void setAttendanceEntriesPerYears(ArrayList<AttendanceEntriesPerYear> attendanceEntriesPerYears) {
        this.attendanceEntriesPerYears = attendanceEntriesPerYears;
    }

    public ArrayList<AttendanceEntriesPerYear> getAttendanceEntriesPerYears() {
        return attendanceEntriesPerYears;
    }



}
