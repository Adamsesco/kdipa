package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DelegatedList {

    private static final String TAG = "EmployeesList";
    private static DelegatedList mInstance= null;

    private ArrayList<DelegatedObject> employeesList;
    private ArrayList<DelegatedArObject> employeesArList;

    private DelegatedList(ArrayList<DelegatedObject> employeesList, ArrayList<DelegatedArObject> employeesArList) {
        this.employeesList = employeesList;
        this.employeesArList = employeesArList;
    }


    public ArrayList<DelegatedObject> getEmployeesList() {
        return employeesList;
    }

    public void setEmployeesList(ArrayList<DelegatedObject> employeesList) {
        this.employeesList = employeesList;
    }

    public ArrayList<DelegatedArObject> getEmployeesArList() {
        return employeesArList;
    }

    public void setEmployeesArList(ArrayList<DelegatedArObject> employeesArList) {
        this.employeesArList = employeesArList;
    }
    public static synchronized void newInstance(Context context, String result) {
        mInstance =  DelegatedList.loadEmployeesList(context, result);
        Log.d(TAG, "newInstance: called");
    }
    public static synchronized DelegatedList getInstance() {
        return mInstance;
    }


    public static DelegatedList loadEmployeesList(Context context, String result){
    try {

        JSONObject jsonObject=new JSONObject(result);
        JSONArray employeesArray=jsonObject.getJSONArray("delegatedEmployees");
        ArrayList<DelegatedObject>  employeesItemArray=new ArrayList<>();
        for (int i=0; i<employeesArray.length(); i++){
            JSONObject subObject=employeesArray.getJSONObject(i);
            String employeeID=subObject.getString("EmployeeID");
            String employeeName=subObject.getString("EmployeeName");
            DelegatedObject employeesItem = new DelegatedObject(employeeID,employeeName);
            employeesItemArray.add(employeesItem);
        }
        JSONArray employeesArArray=jsonObject.getJSONArray("delegatedEmployeesAr");
        ArrayList<DelegatedArObject>  employeesArItemArray=new ArrayList<>();
        for (int i=0; i<employeesArArray.length(); i++){
            JSONObject subObject=employeesArArray.getJSONObject(i);
            String employeeID=subObject.getString("EmployeeID");
            String employeeName=subObject.getString("EmployeeName");
            DelegatedArObject employeesArItem = new DelegatedArObject(employeeID,employeeName);
            employeesArItemArray.add(employeesArItem);
        }
        return new DelegatedList(employeesItemArray,employeesArItemArray);

    } catch (JSONException e) {
        e.printStackTrace();
    }
    return null;
}
    public ArrayList<String> getEmployeesNamesList(){
        ArrayList<String> employeesNamesList=new ArrayList<>();
        for (int i = 0; i < this.getEmployeesList().size(); i++) {
            employeesNamesList.add(this.getEmployeesList().get(i).getName());
        }
        return employeesNamesList;
    }
    public  ArrayList<String> getEmployeesArNamesList(){
        ArrayList<String> employeesArNamesList=new ArrayList<>();
        for (int i = 0; i < this.getEmployeesArList().size(); i++) {
            employeesArNamesList.add(this.getEmployeesArList().get(i).getName());
        }
        return employeesArNamesList;
    }
}

class DelegatedObject{
    String id;
    String name;

    public DelegatedObject(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class DelegatedArObject{
    String id;
    String name;

    public DelegatedArObject(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

