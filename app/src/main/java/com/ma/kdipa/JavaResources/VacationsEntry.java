package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.Log;

import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_AR_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.CHOOSE_DATE_DISPLAY;
import static com.ma.kdipa.JavaResources.Constants.DATE_INPUT_FORMAT;
import static com.ma.kdipa.JavaResources.Constants.DAY_MONTH_YEAR;

public class VacationsEntry {
    private String number;
    private String typeEn;
    private String typeAr;
    private String period;
    private String dateFrom;
    private String dateTo;
    private String dateFromWithYear;
    private String vacationId;

    public String getTypeEn() {
        return typeEn;
    }

    public String getTypeAr() {
        return typeAr;
    }

    public String getVacationId() {
        return vacationId;
    }

    public static String getTAG() {
        return TAG;
    }

    public String getLocale() {
        return locale;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setVacationId(String vacationId) {
        this.vacationId = vacationId;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    private static final String TAG = "VacationsEntry";
    String locale;

    public String getDateFromWithYear() {
        return dateFromWithYear;
    }

    public VacationsEntry(String number, String typeEn, String typeAr, String dateFrom, String dateTo, String period, String vacationId, Context c) {
        this.number = number;
        this.typeEn = typeEn;
        this.typeAr = typeAr;
        this.period = period;
        this.vacationId=vacationId;
        locale=c.getResources().getConfiguration().locale.getLanguage();
        this.dateFrom = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?CHOOSE_DATE_AR_DISPLAY:CHOOSE_DATE_DISPLAY,dateFrom, locale);
        this.dateTo = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?CHOOSE_DATE_AR_DISPLAY:CHOOSE_DATE_DISPLAY,dateTo, locale);
        this.dateFromWithYear = Utilitaire.formateDateFromstring(DATE_INPUT_FORMAT, locale.equals("ar")?DAY_MONTH_YEAR:DAY_MONTH_YEAR,dateFrom, locale);

        Log.d(TAG, "VacationsEntry: dateFrom input: "+dateFrom);
        Log.d(TAG, "VacationsEntry: dateFrom output: "+this.dateFrom);
    }


    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }



    public void setTypeEn(String typeEn) {
        this.typeEn = typeEn;
    }

    public String getType() {
        return (locale.equals("ar"))?typeAr:typeEn;
    }

    public void setTypeAr(String typeAr) {
        this.typeAr = typeAr;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "VacationsEntry{" +
                "number='" + number + '\'' +
                ", typeEn='" + typeEn + '\'' +
                ", typeAr='" + typeAr + '\'' +
                ", period='" + period + '\'' +
                ", dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", vacationId='" + vacationId + '\'' +
                ", locale='" + locale + '\'' +
                '}';
    }
}
