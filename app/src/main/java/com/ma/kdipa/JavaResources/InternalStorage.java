package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.content.Context.MODE_PRIVATE;

public class InternalStorage   {

    private static final int READ_BLOCK_SIZE = 100;
    private static final String TAG = "InternalStorage";

    // write text to file
    public static void write(Context context, String filename, String text, boolean appendMode, boolean unique) {
        if (appendMode){
            if (unique){
                String[] strings= InternalStorage.readFullInternalStorage(context,filename).split("\n");
                for (String s : strings) {
                    if (s.equals(text)) {
                        Log.d(TAG, "write: must not be written (1)");
                        return;
                    }
                }
            }
        }

        Log.d(TAG, "write: this statment must not be reached if (1) is showed");

        // add-write text into file
        try {
            FileOutputStream fileout=context.openFileOutput(filename, (appendMode?Context.MODE_APPEND:0) | MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write(text);
            outputWriter.write("\n");
            outputWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void clearFile(Context context, String filename) {
        try {
            FileOutputStream fileout=context.openFileOutput(filename, MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write("");
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read text from file
    public static String readFullInternalStorage(Context context, String filename) {
        //reading text from file
        try {
            FileInputStream fileIn=context.openFileInput(filename);
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[READ_BLOCK_SIZE];
            String s="";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
            return s;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
