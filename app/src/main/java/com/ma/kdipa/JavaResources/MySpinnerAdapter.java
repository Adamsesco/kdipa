package com.ma.kdipa.JavaResources;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ma.kdipa.R;

import org.w3c.dom.Text;

import java.util.List;

public class MySpinnerAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> dataSet;
    public MySpinnerAdapter(Context context, List<String> items) {
        super(context, R.layout.element_spinner, items);
        dataSet=items;


    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        if (position == 0) {
            return initialSelection(true);
        }
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (position == 0) {
            return initialSelection(false);
        }
        return getCustomView(position, convertView, parent);
    }


    @Override
    public int getCount() {
        return super.getCount() + 1; // Adjust for initial selection item
    }

    private View initialSelection(boolean dropdown) {
        // Just an example using a simple TextView. Create whatever default view 
        // to suit your needs, inflating a separate layout if it's cleaner.
        TextView view = new TextView(getContext());
        view.setText(R.string.select_an_item);
        int spacing = getContext().getResources().getDimensionPixelSize(R.dimen.normal);
//        view.setPadding(0, spacing, 0, spacing);

        if (dropdown) { // Hidden when the dropdown is opened
            view.setHeight(0);
        }

        return view;
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        // Distinguish "real" spinner items (that can be reused) from initial selection item
        View row = convertView != null && !(convertView instanceof TextView)
                ? convertView :
                LayoutInflater.from(getContext()).inflate(R.layout.element_spinner, parent, false);

        position = position - 1; // Adjust for initial selection item
        String item = dataSet.get(position);

        try {
            ((TextView)row.findViewById(R.id.text1)).setText(item);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return row;
    }

}