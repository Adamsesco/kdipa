package com.ma.kdipa.JavaResources;

public class Circle {
    private Double radius;
    private Double xo;
    private Double yo;
    private Double xe;
    private Double ye;

    public Circle() {
        this.radius = 1.0;
        this.xo = 0.0;
        this.yo = 0.0;
    }

    public Circle(Double xo, Double yo, Double radius) {
        this.radius = radius;
        this.xo = xo;
        this.yo = yo;
    }
    public Circle(Double xo, Double yo, Double xe, Double ye) {

        this.xo = xo;
        this.yo = yo;
        this.xe = xe;
        this.ye = ye;
        this.radius=measure(xo,yo,xe,ye);
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setXo(Double xo) {
        this.xo = xo;
    }

    public void setYo(Double yo) {
        this.yo = yo;
    }

    public Double getRadius() {
        return radius;
    }

    public Double getXo() {
        return xo;
    }

    public Double getYo() {
        return yo;
    }

    public Double getXe() {
        return xe;
    }

    public Double getYe() {
        return ye;
    }
    private Double measureInSphericCoordinates(Double lat1, Double lon1, Double lat2, Double lon2){  // generally used geo measurement function
        Double R = 6378.137; // Radius of earth in KM
        Double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        Double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double d = R * c;
        return d * 1000; // meters
    }
    private Double measure(Double x1, Double y1, Double x2, Double y2){
        return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
    }
}
