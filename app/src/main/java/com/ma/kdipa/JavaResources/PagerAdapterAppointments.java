package com.ma.kdipa.JavaResources;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.ma.kdipa.R;
import com.ma.kdipa.responses.AppointmentResponse;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapterAppointments extends PagerAdapter {
    private List<AppointmentResponse.Result> mList=new ArrayList<>();

    public PagerAdapterAppointments(List<AppointmentResponse.Result> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View item = (View) LayoutInflater.from(container.getContext()).inflate(
                        R.layout.i_appointment_element,
                container, false);
        ((TextView) item.findViewById(R.id.appointmentMeetingDateTV)).setText(String.valueOf(mList.get(position).meetingDate));
        ((TextView) item.findViewById(R.id.appointmentMeetingTimeTV)).setText(mList.get(position).meetingTimeFormated);
        ((TextView) item.findViewById(R.id.appointmentServiceTV)).setText(mList.get(position).service);
        ((TextView) item.findViewById(R.id.appointmentDetailsTV)).setText(mList.get(position).details);
        ((TextView) item.findViewById(R.id.appointmentAccountManagerTV)).setText(mList.get(position).accManager);

        container.addView(item);
        return item;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private static class Item {
        private final int color;

        private Item(int color) {
            this.color = color;
        }
    }
}
