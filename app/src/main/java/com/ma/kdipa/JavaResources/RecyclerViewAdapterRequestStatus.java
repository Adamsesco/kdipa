package com.ma.kdipa.JavaResources;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class RecyclerViewAdapterRequestStatus extends RecyclerView.Adapter<RecyclerViewAdapterRequestStatus.MyViewHolder> {

    private ArrayList<SingleRowRequestStatus> mDataset;
    private RecyclerView recycler;
    private Context context;
    private ProgressDialog pDialog;

    OnClickListener onClickListener;
    public interface OnClickListener{
        void onClick(int position);
    }
    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener=onClickListener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView dateTV;
        public TextView id;
        public TextView type;
        public TextView statusTV;
        public ImageView downloadIV;
        public ImageView removeIV;
        ConstraintLayout cardTheme;

        View v;
        public MyViewHolder(View view) {
            super(view);
            this.id = view.findViewById(R.id.appointment_value);
            this.dateTV = view.findViewById(R.id.date);
            this.type = view.findViewById(R.id.Request_type_value);
            this.statusTV = view.findViewById(R.id.statusTV);
            this.downloadIV = view.findViewById(R.id.download);
            this.removeIV = view.findViewById(R.id.remove);
            this.cardTheme = view.findViewById(R.id.cardTheme);

            this.v = view;

            this.removeIV.setOnClickListener(view1 -> {
                if (mDataset.get(getAdapterPosition()).getRemoveIconCode()==R.drawable.remove_en){{
                    onClickListener.onClick(getAdapterPosition());
                }}
            });
            this.downloadIV.setOnClickListener(view1 -> {
                if (mDataset.get(getAdapterPosition()).getDownloadIconCode()==R.drawable.download_en){{
                    new DownloadFileFromURL().execute("https://learnenglishkids.britishcouncil.org/sites/kids/files/attachment/short-stories-little-red-riding-hood-transcript.pdf");
                }}
            });

        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterRequestStatus(ArrayList<SingleRowRequestStatus> myDataset, Context context) {
        mDataset = myDataset;
        this.context=context;

    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_request_status_element,
                parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        holder.id.setText(mDataset.get(position).getId());
        holder.dateTV.setText(mDataset.get(position).getDate());
        holder.type.setText(mDataset.get(position).getName());

        holder.statusTV.setText(mDataset.get(position).getStatusText());
//        holder.statusTV.setTextColor(ContextCompat.getColor(holder.statusTV.getContext(),mDataset.get(position).getStatusColor()));
//        holder.cardTheme.setBackgroundColor(ContextCompat.getColor(holder.cardTheme.getContext(),mDataset.get(position).getStatusColor()));

        holder.downloadIV.setImageResource(mDataset.get(position).getDownloadIconCode());
        holder.removeIV.setImageResource(mDataset.get(position).getRemoveIconCode());







    }
    public void removeAt(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataset.size());
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
    class  DownloadFileFromURL extends AsyncTask<String, String, String> {
        String root;
        /**
         * Before starting background thread
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.println("Starting download");

            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                root = context.getExternalCacheDir().toString();

                System.out.println("Downloading");
                URL url = new URL(f_url[0]);

                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file

                OutputStream output = new FileOutputStream(root+"/downloadedfile.pdf");
                byte data[] = new byte[1024];

                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;

                    // writing data to file
                    output.write(data, 0, count);

                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }



        /**
         * After completing background task
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            System.out.println("Downloaded");

//            File file = new File(Environment.getExternalStorageDirectory(),
//                    file_url);
            File file=new File(root+"/downloadedfile.pdf");
            Uri path = Uri.fromFile(file);
            Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
            pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfOpenintent.setDataAndType(path, "application/pdf");
            try {
                context.startActivity(pdfOpenintent);
            }
            catch (ActivityNotFoundException e) {

            }
            pDialog.dismiss();
        }
    }
}


