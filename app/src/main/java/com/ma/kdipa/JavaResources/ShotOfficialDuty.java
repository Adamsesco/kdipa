package com.ma.kdipa.JavaResources;

import android.content.Context;

public class ShotOfficialDuty {


    private String departureDate;
    private String returnDate;


    public ShotOfficialDuty(Context context, String departureDate, String returnDate) {
        String locale=context.getResources().getConfiguration().locale.getLanguage();
        this.departureDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",departureDate, locale);
        this.departureDate=Utilitaire.replaceArabicNumbers(this.departureDate);
        this.returnDate=Utilitaire.formateDateFromstring(Constants.YEAR_MONTH_DAY, locale.equals("ar")?"EEE dd MMM":"EEE, MMM dd",returnDate, locale);
        this.returnDate=Utilitaire.replaceArabicNumbers(this.returnDate);
    }



    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }



}
