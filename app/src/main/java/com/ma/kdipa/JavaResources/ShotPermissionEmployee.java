package com.ma.kdipa.JavaResources;

import java.util.ArrayList;

public class ShotPermissionEmployee {
    private String name;
    private String nameAr;
    private String permissionsRemained;
    private String permissionsTaken;
    private ArrayList<SinglePage> permissionsHistoryList;

    public ShotPermissionEmployee(String name, String nameAr, String permissionsRemained, String permissionsTaken, ArrayList<SinglePage> permissionsHistoryList) {
        this.name = name;
        this.nameAr = nameAr;
        this.permissionsRemained = permissionsRemained;
        this.permissionsTaken = permissionsTaken;
        this.permissionsHistoryList = permissionsHistoryList;
    }
    public ShotPermissionEmployee() {
        this.name = "";
        this.nameAr = "";
        this.permissionsRemained = "0";
        this.permissionsTaken = "1";
        this.permissionsHistoryList = new ArrayList<>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getPermissionsRemained() {
        return permissionsRemained;
    }

    public void setPermissionsRemained(String permissionsRemained) {
        this.permissionsRemained = permissionsRemained;
    }

    public String getPermissionsTaken() {
        return permissionsTaken;
    }

    public void setPermissionsTaken(String permissionsTaken) {
        this.permissionsTaken = permissionsTaken;
    }

    public ArrayList<SinglePage> getPermissionsHistoryList() {
        return permissionsHistoryList;
    }

    public void setPermissionsHistoryList(ArrayList<SinglePage> permissionsHistoryList) {
        this.permissionsHistoryList = permissionsHistoryList;
    }
}