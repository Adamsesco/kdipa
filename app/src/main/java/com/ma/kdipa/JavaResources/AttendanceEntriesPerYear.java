package com.ma.kdipa.JavaResources;

public class AttendanceEntriesPerYear {
    private Integer year;
    private AttendanceEntry attendanceEntry;


    public AttendanceEntriesPerYear(Integer year, AttendanceEntry attendanceEntry) {
        this.year = year;
        this.attendanceEntry = attendanceEntry;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public AttendanceEntry getAttendanceEntry() {
        return attendanceEntry;
    }

    public void setAttendanceEntry(AttendanceEntry attendanceEntry) {
        this.attendanceEntry = attendanceEntry;
    }
}
