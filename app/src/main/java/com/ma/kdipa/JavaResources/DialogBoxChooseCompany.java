package com.ma.kdipa.JavaResources;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;
import com.ma.kdipa.responses.GetCompaniesResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DialogBoxChooseCompany extends Dialog {
    public static final int POSITIVE = 1;
    public static final int NEGATIVE = -1;
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int WARNING = 2;
    public static final int INFO = 3;
    private static final String TAG = "DialogBoxCompany";


    @BindView(R.id.companiesRV)
    public RecyclerView companiesRV;


    @BindView(R.id.container)
    public ConstraintLayout container;

    @BindView(R.id.positiveTV)
    public TextView positiveTV;


    OnPositiveListener onPositiveListener;
    List<GetCompaniesResponse> dataSet = new ArrayList<>();
    private String positive = null;


    public DialogBoxChooseCompany(@NonNull Context context, List<GetCompaniesResponse> dataSet) {
        super(context);
        this.dataSet = dataSet;
    }

    public void setOnPositiveListener(OnPositiveListener onPositiveListener) {
        this.onPositiveListener = onPositiveListener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_choose_company);
        ButterKnife.bind(this);

        if (positive != null) {
            positiveTV.setText(positive);
        }


        CompaniesAdapter companiesAdapter = new CompaniesAdapter(getContext(), dataSet, position -> {
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        companiesRV.setLayoutManager(linearLayoutManager);
        companiesRV.setAdapter(companiesAdapter);

        positiveTV.setOnClickListener(view -> {
            dismiss();
            if (onPositiveListener != null) {
                onPositiveListener.onPositiveClick(companiesAdapter.getCheckedCompanyId());
            }
        });
    }

    @Override
    public void show() {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        super.show();
    }


    public interface OnPositiveListener {
        void onPositiveClick(int id);
    }

}
