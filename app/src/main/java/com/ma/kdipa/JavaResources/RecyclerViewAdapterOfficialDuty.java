package com.ma.kdipa.JavaResources;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class RecyclerViewAdapterOfficialDuty extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = "RecyclerViewAdapterOf";
    private final ArrayList<String> delegatedEmployeesList;
    String locale;
    boolean neutralized = false;
    ActionButtonState actionButtonState = ActionButtonState.ONLY_ADD_VISIBLE;
    private ArrayList<ArrayList<String>> employeesList;
    //selected list contains the list of items that are selected in all spinners, indexed as following:
    //first element in selectedList is the selected element in the first spinner, and so on...
    private ArrayList<String> selectedListE;
    private ArrayList<String> selectedListD;
    private ArrayList<String> feederE;
    private RecyclerView recycler;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterOfficialDuty(ArrayList<ArrayList<String>> myDataSet, ArrayList<String> delegatedEmployeesList, String locale) {
        employeesList = myDataSet;
        this.delegatedEmployeesList = delegatedEmployeesList;
        this.locale = locale;
        feederE = myDataSet.get(0);

        selectedListE = new ArrayList<>();
        selectedListD = new ArrayList<>();
        selectedListE.add("");
        selectedListD.add("");


    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            // create a new view
            Log.d(TAG, "onCreateViewHolder: language: " + parent.getContext().getResources().getConfiguration().locale.getLanguage());
            View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.m_official_duty_element,
                    parent, false);
//            v.findViewById(R.id.delegatedEmployeeSpinner).getLayoutParams().height=(int) ( Utilitaire.getScreenWidthHeight(Utilitaire.getActivity(v.findViewById(R.id.delegatedEmployeeSpinner)))[1]*0.052);

            return new MyViewHolder(v);
        } else {
            View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.m_official_duty_add_element,
                    parent, false);

            v.findViewById(R.id.addMoreDelegate).getLayoutParams().height = (int) (Utilitaire.getScreenWidthHeight(Utilitaire.getActivity(v.findViewById(R.id.addMoreDelegate)))[1] * 0.052);
            v.findViewById(R.id.removeDelegate).getLayoutParams().height = (int) (Utilitaire.getScreenWidthHeight(Utilitaire.getActivity(v.findViewById(R.id.removeDelegate)))[1] * 0.052);

            return new MyViewHolder2(v);
        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder() called with: holder = [" + holder + "], position = [" + position + "]");
        Log.d(TAG, "onBindViewHolder: called");
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.setIsRecyclable(false);
//        recycler.getRecycledViewPool().setMaxRecycledViews(holder.getItemViewType(), 0);

        if (holder.getItemViewType() == 0) {

            ((MyViewHolder) holder).customListE = new ArrayList<>(feederE);
            updateE(((MyViewHolder) holder).customListE, position);


            ((MyViewHolder) holder).adapterE = new ArrayAdapter<>(((MyViewHolder) holder).spinnerE.getContext(), R.layout.drop_down_list_element, ((MyViewHolder) holder).customListE);
            //set the spinners adapter to the previously created one.
            ((MyViewHolder) holder).spinnerE.setAdapter(((MyViewHolder) holder).adapterE);
            ((MyViewHolder) holder).spinnerE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int selectedItemPosition, long l) {

                    if (!neutralized) {
                        neutralized = true;
                        selectedListE.set(position, ((MyViewHolder) holder).customListE.get(selectedItemPosition));
                        for (int j = 0; j < employeesList.size(); j++) {
                            if (j == position) continue;
                            updateSpinnerE(j);
                        }
                        neutralized = false;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });


            ((MyViewHolder) holder).adapterD = new ArrayAdapter<>(((MyViewHolder) holder).spinnerD.getContext(), R.layout.drop_down_list_element, delegatedEmployeesList);
            //set the spinners adapter to the previously created one.
            ((MyViewHolder) holder).spinnerD.setAdapter(((MyViewHolder) holder).adapterD);
            ((MyViewHolder) holder).spinnerD.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int selectedItemPosition, long l) {
                        selectedListD.set(position, delegatedEmployeesList.get(selectedItemPosition));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

        } else {
            configureActionButtons((MyViewHolder2) holder);
            holder.itemView.findViewById(R.id.addMoreDelegate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick() called with: view = [" + view + "]");
                    selectedListE.add("");
                    selectedListD.add("");
                    employeesList.add(employeesList.get(0));
                    notifyItemInserted(employeesList.size() - 1);
                    notifyItemRangeChanged(employeesList.size() - 1, 1);
                    if (((MyViewHolder2) holder).miceTV.getVisibility() == View.INVISIBLE) {
//                        ((MyViewHolder2) holder).miceTV.setVisibility(View.VISIBLE);
//                        ((MyViewHolder2) holder).miceIV.setVisibility(View.VISIBLE);
                        Log.d(TAG, "onClick: fade in");
//                        Utilitaire.fade(300, 0.0f,1.0f, holder.itemView.findViewById(R.id.removeDelegate),holder.itemView.findViewById(R.id.mice));
                        actionButtonState = ActionButtonState.ALL_VISIBLE;
                        configureActionButtons((MyViewHolder2) holder);

                    }
                    if (holder.getAdapterPosition() == feederE.size()) {
                        actionButtonState = ActionButtonState.ONLY_REMOVE_VISIBLE;
                        configureActionButtons((MyViewHolder2) holder);
                    }
                }
            });
            ((MyViewHolder2) holder).miceTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick() called with: view = [" + view + "]");
                    selectedListE.remove(selectedListE.size() - 1);
                    selectedListD.remove(selectedListD.size() - 1);

                    employeesList.remove(employeesList.size() - 1);
                    Log.d(TAG, "onClick: mDataset size: " + employeesList.size());
                    for (int j = 0; j < employeesList.size(); j++) {
                        updateSpinnerE(j);
                    }
                    notifyItemRemoved(employeesList.size());//previously removed position
                    notifyItemRangeChanged(employeesList.size(), 1);
                    if (employeesList.size() == 1) {
                        Log.d(TAG, "onClick: fade out");
                        actionButtonState = ActionButtonState.ONLY_ADD_VISIBLE;
                        configureActionButtons((MyViewHolder2) holder);
                    } else {
                        actionButtonState = ActionButtonState.ALL_VISIBLE;
                        configureActionButtons((MyViewHolder2) holder);
                    }
                }
            });
        }
    }

    private void configureActionButtons(MyViewHolder2 holder) {
        switch (actionButtonState) {
            case ONLY_ADD_VISIBLE:
                holder.itemView.findViewById(R.id.addMoreDelegate).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.add).setVisibility(View.VISIBLE);

                holder.miceTV.setVisibility(View.INVISIBLE);
                holder.miceIV.setVisibility(View.INVISIBLE);
                break;
            case ONLY_REMOVE_VISIBLE:
                holder.miceTV.setVisibility(View.VISIBLE);
                holder.miceIV.setVisibility(View.VISIBLE);

                holder.itemView.findViewById(R.id.addMoreDelegate).setVisibility(View.INVISIBLE);
                holder.itemView.findViewById(R.id.add).setVisibility(View.INVISIBLE);
                break;
            case ALL_VISIBLE:
                holder.miceTV.setVisibility(View.VISIBLE);
                holder.miceIV.setVisibility(View.VISIBLE);

                holder.itemView.findViewById(R.id.addMoreDelegate).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.add).setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateSpinnerE(int j) {
        if (recycler.findViewHolderForAdapterPosition(j) != null) {
            MyViewHolder holder = (MyViewHolder) recycler.findViewHolderForAdapterPosition(j);
            holder.customListE.clear();
            holder.customListE.addAll(feederE);
            updateE(holder.customListE, j);
            neutralized = true;
            holder.adapterE.notifyDataSetChanged();
            neutralized = false;
            int selected = 0;
            for (int k = 0; k < holder.customListE.size(); k++) {
                if (holder.customListE.get(k).equals(selectedListE.get(j))) {
                    selected = k;
                    break;
                }
            }
            neutralized = true;
            holder.spinnerE.setSelection(selected);
            neutralized = false;
        } else {
            Log.d(TAG, "onItemSelected: null at position: " + j);
        }
    }


    private void cloneFeederInto(ArrayList<String> customList) {
        Log.d(TAG, "cloneFeederInto() called with: customList = [" + customList + "]");
        for (int i = 0; i < feederE.size(); i++) {
            if (i > customList.size() - 1) {
                customList.add(feederE.get(i));
            } else {
                customList.set(i, feederE.get(i));
            }
        }
    }

    private void updateE(ArrayList<String> customList, int position) {
        //remove items
        removeFromE(customList);
        //add item
        if (!selectedListE.get(position).equals("")) {
            customList.add(selectedListE.get(position));
        }
    }


    private void removeFromE(ArrayList<String> customList) {
        Log.d(TAG, "removeFrom() called with: customList = [" + customList + "]");
        for (int i = 0; i < customList.size(); i++) {
            for (int j = 0; j < selectedListE.size(); j++) {
                if (customList.get(i).equals(selectedListE.get(j))) {
                    customList.remove(i);
                    i = i - 1;
                    break;
                }
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return (position == employeesList.size()) ? 2 : 0;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return employeesList.size() + 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    public ArrayList<String> getSelectedEmployees() {
        return selectedListE;
    }

    public ArrayList<String> getSelectedDelegates() {
        return selectedListD;
    }

    enum ActionButtonState {
        ALL_VISIBLE,
        ONLY_ADD_VISIBLE,
        ONLY_REMOVE_VISIBLE
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Spinner spinnerD;
        ArrayAdapter<String> adapterE;
        ArrayAdapter<String> adapterD;
        ArrayList<String> customListE;
        View v;
        private Spinner spinnerE;

        MyViewHolder(View view) {
            super(view);

            this.spinnerD = view.findViewById(R.id.delegatedEmployeeSpinner);
            this.spinnerE = view.findViewById(R.id.employeeSpinner);
            this.v = view;
        }


        View getView() {
            return v;
        }

    }

    public static class MyViewHolder2 extends RecyclerView.ViewHolder {


        TextView miceTV;
        ImageView miceIV;
        View v;

        MyViewHolder2(View view) {
            super(view);
            this.v = view;
            this.miceTV = view.findViewById(R.id.removeDelegate);
            this.miceIV = view.findViewById(R.id.mice);
        }

        View getView() {
            return v;
        }

    }
}


