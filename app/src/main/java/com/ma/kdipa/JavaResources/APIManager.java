package com.ma.kdipa.JavaResources;

import androidx.annotation.Nullable;

import com.ma.kdipa.requests.HistoryInOutRequest;
import com.ma.kdipa.requests.UserCredentials;
import com.ma.kdipa.responses.AccountsManagerResponse;
import com.ma.kdipa.responses.AppointmentResponse;
import com.ma.kdipa.responses.GetCompaniesResponse;
import com.ma.kdipa.responses.HistoryInOutElement;
import com.ma.kdipa.responses.OfficialDutySubmitObject;
import com.ma.kdipa.responses.ServicesResponse;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface APIManager {

    String BASE_URL_INVESTOR = Constants.INVESTOR_BASE_URL;
    String BASE_URL_EMPLOYEE = Constants.EMPLOYEE_BASE_URL;

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    Retrofit retrofitInvestor = new Retrofit.Builder()
            .baseUrl(BASE_URL_INVESTOR)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    Retrofit retrofitEmployee = new Retrofit.Builder()
            .baseUrl(BASE_URL_EMPLOYEE)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();


    @GET("investor-dashboard")
    Call<ShotInvestorDashboard> getInvestorDashboard(@Header("X-Auth-Token") String token, @Header("API-KEY") String apiKey);

    @GET
    Call<List<GetCompaniesResponse>> getCompanies(@Header("X-Auth-Token") String token, @Header("API-KEY") String apiKey, @Url String url);


    @GET
    Call<AppointmentResponse> getAppointments(@Header("X-Auth-Token") String token, @Header("API-KEY") String apiKey, @Url String url);


    @GET("bookingForm/getAllBookingServices")
    Call<List<ServicesResponse>> getServices(@Header("X-Auth-Token") String token, @Header("API-KEY") String apiKey);

    @GET
    Call<List<AccountsManagerResponse>> getAccountManager(@Header("X-Auth-Token") String token, @Header("API-KEY") String apiKey, @Url String url);


    @POST("postNewDuty")
    Call<ResponseBody> postNewDuty(@Body OfficialDutySubmitObject officialDutySubmitObject);

    @POST("historyInOut")
    Call<List<HistoryInOutElement>> getHistoryInOut(@Body HistoryInOutRequest historyInOutRequest);


    @Multipart
    @POST("sickLeave")
    Call<ResponseBody> postSickLeave(
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("trxDate") RequestBody trxDate,
            @Nullable  @Part MultipartBody.Part filename
    );

}
