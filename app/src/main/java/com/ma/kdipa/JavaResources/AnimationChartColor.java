package com.ma.kdipa.JavaResources;

import android.graphics.Color;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class AnimationChartColor extends Animation {

    private CustomSolidArc customSolidArc;
    private int     fromRed;
    private int     toRed;
    private int     fromGreen;
    private int     toGreen;
    private int     fromBlue;
    private int     toBlue;

    public AnimationChartColor(
            CustomSolidArc customSolidArc,
            int from,
            int to
    ) {
        super();
        this.customSolidArc = customSolidArc;

        this.fromRed = Color.red(from);
        this.fromGreen = Color.green(from);
        this.fromBlue =Color.blue(from);


        this.toRed = Color.red(to);;

        this.toGreen = Color.green(to);

        this.toBlue = Color.blue(to);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        int valueRed = (int) (fromRed + (toRed - fromRed) * interpolatedTime);
        int valueGreen = (int) (fromGreen + (toGreen - fromGreen) * interpolatedTime);
        int valueBlue = (int) (fromBlue + (toBlue - fromBlue) * interpolatedTime);

        customSolidArc.setPieColor(Utilitaire.getIntFromColor(valueRed,valueGreen,valueBlue));

    }

}

