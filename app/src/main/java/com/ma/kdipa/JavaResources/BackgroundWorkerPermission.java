package com.ma.kdipa.JavaResources;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import static com.ma.kdipa.JavaResources.Constants.EMPLOYEE_BASE_URL;
import static com.ma.kdipa.JavaResources.Utilitaire.haveNetworkConnection;


public class BackgroundWorkerPermission extends AsyncTask<String, Integer, String> implements Utilitaire.UpdateListener {
    //    public static String resultat;
    private static final String TAG = "BackgroundWorker";
//    private static final String INVESTOR_LOGIN = "INVESTOR_LOGIN";
//    private static final String EMPLOYEE_LOGIN = "EMPLOYEE_LOGIN";
//    private static final String HISTORY_ABSENT = "HISTORY_ABSENT";
//    private static final String ABSENCES = "ABSENCES";
//    private static final String HISTORY_PERMISSION = "HISTORY_PERMISSION";


    Context context;
    ProgressDialog progressDialog;

    public BackgroundWorkerPermission(Context ctx, ProgressDialog progressDialog) {
        context = ctx;
        this.progressDialog = progressDialog;

    }

    @Override
    protected String doInBackground(String... params) {

        try {

            String urlString = EMPLOYEE_BASE_URL + "permission";
            URL url = new URL(urlString);
            String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()


            String email = params[0];
            String trxDate = params[1];
            String permissionType = params[2];
            String fromTime = params[3];
            String toTime = params[4];
            File binaryFile = new File(params[5]);
            String password = params[6];
            String isMedical = params[7];

            for (int i = 0; i < 7; i++) {
                Log.d(TAG, "doInBackground: Param N° " + i + " = " + params[i]);
            }

            String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
            String CRLF = "\r\n"; // Line separator required by multipart/form-data.
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);//++++
            connection.setRequestProperty("Connection", "Keep-Alive");//++++

            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            try (
                    OutputStream output = connection.getOutputStream();
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
            ) {
                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"email\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(email).append(CRLF).flush();

                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"trxDate\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(trxDate).append(CRLF).flush();

                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"permissionType\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(permissionType).append(CRLF).flush();

                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"fromTime\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(fromTime).append(CRLF).flush();

                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"toTime\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(toTime).append(CRLF).flush();
                // password
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"password\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(password).append(CRLF).flush();
                // Send isMedical param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"isMedical\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(isMedical).append(CRLF).flush();
/*
                // Send text file.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"textFile\"; filename=\"" + textFile.getName() + "\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF); // Text file itself must be saved in this charset!
                writer.append(CRLF).flush();
                Files.copy(textFile.toPath(), output);
                output.flush(); // Important before continuing with writer!
                writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
*/
                // Send binary file.
                if (!params[5].equals("")) {
                    Log.d(TAG, "doInBackground: binaryFile.getName(): " + binaryFile.getName());
                    writer.append("--").append(boundary).append(CRLF);
                    writer.append("Content-Disposition: form-data; name=\"filename\"; filename=\"").append(binaryFile.getName()).append("\"").append(CRLF);
                    writer.append("Content-Type: ").append(HttpURLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
                    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
                    writer.append(CRLF).flush();


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        Files.copy(binaryFile.toPath(), output);
                    } else {
//                        InputStream input= new FileInputStream(binaryFile);
                        Utilitaire.copy(binaryFile, output, binaryFile.length(), this);
                    }
                    long startTime = System.nanoTime();
                    output.flush(); // Important before continuing with writer!
                    long stopTime = System.nanoTime();
                    Log.d(TAG, "doInBackground: timing: output flush: " + (stopTime - startTime) + " ns");
                    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
                    long stopTime2 = System.nanoTime();
                    Log.d(TAG, "doInBackground: timing: writer flush " + (stopTime2 - stopTime) + " ns");
                }
                long t1;
                // End of multipart/form-data.
                writer.append("--" + boundary + "--").append(CRLF).flush();

//                InputStream error = ((HttpURLConnection) connection).getErrorStream(); this takes time to be performed
//                Log.d(TAG, "doInBackground: error:"+error);

                t1 = System.currentTimeMillis();
//                int status = connection.getResponseCode();
//                Log.d(TAG, "doInBackground: status: "+status);
                Log.d(TAG, "doInBackground: timing getResponseCode: " + (System.currentTimeMillis() - t1) + " ms");

                t1 = System.currentTimeMillis();
                for (Map.Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
                    System.out.println();
                    Log.d(TAG, "doInBackground: headers: " + header.getKey() + "=" + header.getValue());
                }
                Log.d(TAG, "doInBackground: timing headers: " + (System.currentTimeMillis() - t1) + " ms");

                t1 = System.currentTimeMillis();
                InputStream inputStream = connection.getInputStream();
                Log.d(TAG, "doInBackground: timing inputStream: " + (System.currentTimeMillis() - t1) + " ms");


                t1 = System.currentTimeMillis();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));//StandardCharsets.ISO_8859_1
                Log.d(TAG, "doInBackground: timing bufferedReader: " + (System.currentTimeMillis() - t1) + " ms");

                String result = "";
                String line = "";

                t1 = System.currentTimeMillis();
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                Log.d(TAG, "doInBackground: timing while: " + (System.currentTimeMillis() - t1) + " ms");


                Log.d(TAG, "doInBackground: result: " + result);
                t1 = System.currentTimeMillis();
                bufferedReader.close();
                inputStream.close();
                connection.disconnect();
                Log.d(TAG, "doInBackground: timing closing all: " + (System.currentTimeMillis() - t1) + " ms");


                return result;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
//        super.onProgressUpdate(values);
//        progressDialog.setProgress(values[0]);
//        progressDialog.setTitle(values[0]);
//        Log.d(TAG, "onProgressUpdate: "+values[0]);

    }


    @Override
    public void update(int i) {
        publishProgress(i);
    }
}
