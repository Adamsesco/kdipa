package com.ma.kdipa.JavaResources;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.ma.kdipa.R;

import java.util.ArrayList;

public class PagerAdapterApplication extends PagerAdapter {
    private ArrayList<ShotInvestorDashboard> mList=new ArrayList<>();

    public PagerAdapterApplication(ArrayList<ShotInvestorDashboard> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View item = (View) LayoutInflater.from(container.getContext()).inflate(
                        R.layout.i_application_element,
                container, false);
        ((TextView) item.findViewById(R.id.applicationCompanyTV)).setText(mList.get(position).getApplicationcompany());
        ((TextView) item.findViewById(R.id.applicationTypeTV)).setText(mList.get(position).getApplicationtype());
        ((TextView) item.findViewById(R.id.applicationStatusTV)).setText(mList.get(position).getApplicationstatus());
        container.addView(item);
        return item;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private static class Item {
        private final int color;

        private Item(int color) {
            this.color = color;
        }
    }
}
