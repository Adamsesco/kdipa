package com.ma.kdipa.JavaResources;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

public class AnimationTextViewSize extends Animation {

    private TextView textView;
    private float from;
    private float  to;

    public AnimationTextViewSize(TextView textView, float from, float to) {
        super();
        this.textView = textView;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        textView.setTextSize((int) value);
    }

}

