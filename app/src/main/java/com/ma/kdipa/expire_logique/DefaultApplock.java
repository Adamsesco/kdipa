package com.ma.kdipa.expire_logique;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ma.kdipa.EmployeeNavigationDrawer;
import com.ma.kdipa.InvestorNavigationDrawer;
import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.ManagerNavigationDrawer;
import com.ma.kdipa.SplashScreen;

import java.util.Date;

public class DefaultApplock implements Application.ActivityLifecycleCallbacks {


    private Application mCurrentApp;

    private long WAIT_TIME = Constants.EXPIRY_TIME;
    private Waiter waiter;
    private Date mLostFocusDate;

    public DefaultApplock(Application app) {
        super();
        mCurrentApp = app;

        //Registering Activity lifecycle callbacks
        mCurrentApp.unregisterActivityLifecycleCallbacks(this);
        mCurrentApp.registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        // for UserInactivity
        if(waiter!=null) {
            waiter.stopThread();
        }
        waiter=new Waiter(activity,WAIT_TIME,  ()->{
           activity.runOnUiThread(()->{
               if(activity instanceof EmployeeNavigationDrawer){
                   ((EmployeeNavigationDrawer)activity).logoutClick(null);
               }else if (activity instanceof ManagerNavigationDrawer){
                   ((ManagerNavigationDrawer)activity).logoutClick(null);
               }else if (activity instanceof InvestorNavigationDrawer){
                   ((InvestorNavigationDrawer)activity).logoutClick(null);
               }
           });
        });
        waiter.start();

        // for Screen lock
        if (shouldShowUnlockScreen()) {

            Intent intent = new Intent(activity.getApplicationContext(), SplashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            mLostFocusDate = null;
            activity.getApplicationContext().startActivity(intent);

        }
    }

    private boolean shouldShowUnlockScreen() {
        Boolean isvalid = false;
        if (mLostFocusDate == null) {
            isvalid = false;
        } else {
            if (timeSinceLocked() >= (WAIT_TIME/1000)) {
                isvalid = true;
            } else {
                mLostFocusDate = null;
            }
        }
        return isvalid;
    }

    private int timeSinceLocked() {
        return Math.abs((int) ((new Date().getTime() - mLostFocusDate.getTime()) / 1000));
    }


    @Override
    public void onActivityPaused(Activity activity) {
        if(waiter!=null) {
            waiter.stopThread();
        }
        mLostFocusDate = new Date();
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public void updateTouch() {
        if(waiter!=null) {
            waiter.touch();
        }
        mLostFocusDate = new Date();
    }
}