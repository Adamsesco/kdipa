package com.ma.kdipa.expire_logique;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.util.Log;

import androidx.annotation.NonNull;

import com.ma.kdipa.JavaResources.Constants;
import com.ma.kdipa.JavaResources.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ABFSApplication extends Application {
    private static final String TAG = "ABFSApplication";
    boolean isInBackGround = true;
    ComponentCallbacks2 componentCallbacks2 = new ComponentCallbacks2() {
        @Override
        public void onTrimMemory(int level) {
            if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
                Date date = Calendar.getInstance().getTime();
                DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_INPUT_FORMAT);
                String strDate = dateFormat.format(date);
                Utils.setValue(getApplicationContext(), Constants.LAST_TIME_CONNECTED, strDate);
                setInBackGround(true);
            }
        }

        @Override
        public void onConfigurationChanged(@NonNull Configuration configuration) {

        }

        @Override
        public void onLowMemory() {

        }
    };

    public void touch() {
        ApplockManager.getInstance(this).updateTouch();
    }

    public boolean wasInBackground() {
        return isInBackGround;
    }

    public void setInBackGround(boolean b) {
        isInBackGround = b;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerComponentCallbacks(componentCallbacks2);
    }

}
