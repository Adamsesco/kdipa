package com.ma.kdipa.expire_logique;

import android.app.Application;

public class ApplockManager {
    private static ApplockManager instance;
    private DefaultApplock currentAppLocker;

    public static ApplockManager getInstance(Application application) {
        if (instance == null) {
            instance = new ApplockManager(application);
        }
        return instance;
    }
    private ApplockManager(Application application){
        enableDefaultAppLockIfAvailable(application);
    }
    public void enableDefaultAppLockIfAvailable(Application currentApp) {

        currentAppLocker = new DefaultApplock(currentApp);

    }

    public void updateTouch(){
        currentAppLocker.updateTouch();
    }
}