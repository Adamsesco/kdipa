package com.ma.kdipa;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.drawerlayout.widget.DrawerLayout;

import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.Locale;

import static com.ma.kdipa.JavaResources.Constants.LOCALE;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class InvestorSettings extends InvestorNavigationDrawer {
    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name="";
    String email="";
    String password="";
    String locale="";



    private static final String TAG = "EmployeeSettings";


    SwitchButton switcher;
    private Spinner dropdown;
    protected DrawerLayout mDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;


        Locale current = getResources().getConfiguration().locale;

        sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);
        if (sharedPreferences.contains(LOCALE)) {
            Log.d(TAG, "onCreate: sharedpreferences has no problem");
            locale = sharedPreferences.getString(LOCALE, "");
            Log.d(TAG, "onCreate: in fact: local= "+locale);

        } else{
            locale="en";
        }

        contentView = inflater.inflate(R.layout.common_settings, null, false);

        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        ImageView v =mDrawer.findViewById(R.id.settings_iv);
        v.setImageResource(R.drawable.settings_en);
        v.setTag(R.drawable.settings_en);
        mDrawer.findViewById(R.id.settings).setBackgroundColor(Color.parseColor("#F2F2F2"));


        switcher=findViewById(R.id.touch_id);
        switcher.setVisibility(View.GONE);
        (findViewById(R.id.view_separator)).setVisibility(View.GONE);
        (findViewById(R.id.use_touch_id_entry)).setVisibility(View.GONE);
        dropdown=findViewById(R.id.drop_down_language);

        ArrayList<String> languagesList=new ArrayList<>();
        languagesList.add("English");
        languagesList.add("Arabic");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.drop_down_list_element, languagesList);
        //set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dropdown.setSelection(getResources().getConfiguration().locale.getLanguage().equals("ar") ?1:0);



    }


    public void saveChangesClick(View view) {
        locale= (dropdown.getSelectedItemPosition()==0?
                "en":
                "ar"
        );
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = new Locale(locale);
        res.updateConfiguration(conf, dm);


        // Refresh the activity
        sharedPreferences
                .edit()
                .putString(LOCALE, locale)
                .apply();
        finish();
        startActivity(getIntent());
    }
}
