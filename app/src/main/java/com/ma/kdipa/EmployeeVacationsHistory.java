
package com.ma.kdipa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fivehundredpx.android.blur.BlurringView;
import com.ma.kdipa.JavaResources.BackgroundWorker;
import com.ma.kdipa.JavaResources.DialogBoxCustom2;

import com.ma.kdipa.JavaResources.DialogBoxLoadingClass;
import com.ma.kdipa.JavaResources.EmployeesList;
import com.ma.kdipa.JavaResources.InternalStorage;
import com.ma.kdipa.JavaResources.RecyclerViewAdapterYearsFilter;
import com.ma.kdipa.JavaResources.Utilitaire;
import com.ma.kdipa.JavaResources.VacationsEntriesPerYear;
import com.ma.kdipa.JavaResources.VacationsEntry;
import com.ma.kdipa.JavaResources.VacationsPerEmployee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import static com.ma.kdipa.JavaResources.Constants.EMAIL;
import static com.ma.kdipa.JavaResources.Constants.EMPLOYEES_LIST;
import static com.ma.kdipa.JavaResources.Constants.NAME;
import static com.ma.kdipa.JavaResources.Constants.PASSWORD;
import static com.ma.kdipa.JavaResources.Constants.USER_INFO;

public class EmployeeVacationsHistory extends EmployeeNavigationDrawer {


    SharedPreferences sharedPreferences;
    SharedPreferences activityStateSharedPreferences;
    String name="";
    String email="";
    String password="";

    private static Handler handler;
    DialogBoxLoadingClass loadingDialogBox;




    BlurringView blurringView;
    ImageView snapShotIV;
    ImageView darkIV;
    ImageView closeIV;
    TextView yearTV;
    TextView yearsEntryTV;
    RecyclerView yearsRV;

    VacationsPerEmployee vacationsPerEmployee;


    RecyclerViewAdapterYearsFilter recyclerViewAdapterYearsFilter;


    protected DrawerLayout mDrawer;

    //    private ArrayList<String> fullNamesList;
//    private Spinner dropdown1;
    TextView employeeTV; //will be replaced with spinner in manager screen

    private RecyclerView recyclerView;


    private static final String TAG = "VacationsHistory";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;
        contentView = inflater.inflate(
                        R.layout.e_vacations,
                null, false);
        mDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_view);
        mDrawer.addView(contentView, 0);

        mDrawer.findViewById(R.id.vacation).setBackgroundColor(Color.parseColor("#F2F2F2"));
        mDrawer.findViewById(R.id.menu_content).setVisibility(View.GONE);
        mDrawer.findViewById(R.id.request_content).setVisibility(View.VISIBLE);


        handler=new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (msg.arg1==1){
                    loadingDialogBox.dismiss();
                    setData();
                }else if (msg.arg1==-1){//empty
                    findViewById(R.id.container).setVisibility(View.GONE);
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationsHistory.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.no_data_available))
                            .setBody(getString(R.string.no_data_available_so_far))
                            .setPositive(getString(R.string.to_home_page))
                            .setOnPositive(() -> {
                                        startActivity(new Intent(EmployeeVacationsHistory.this, EmployeeDashboard.class));
                                        finish();
                                    }
                            )
                            .create().show();
                }else if(msg.arg1==0){
                    loadingDialogBox.dismiss();
                    new DialogBoxCustom2.DialogBoxCustom2Builder(EmployeeVacationsHistory.this)
                            .setMode(DialogBoxCustom2.ERROR)
                            .setTitle(getString(R.string.error))
                            .setBody(getString(R.string.an_error_occured))
                            .setPositive(getString(R.string.cancel))
                            .create().show();
//                    DialogBoxCustomClass cdd=new DialogBoxCustomClass(EmployeeVacationsHistory.this,getString(R.string.error),getString(R.string.an_error_occured), getString(R.string.reload));
//                    cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    cdd.show();
                }
            }
        };
        globalFindViewById();

        if (isManager){
            processEmployeeSpinner();
        }else{
            loadData();
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
    Spinner employeeSpinner;
    private void processEmployeeSpinner() {
        employeeSpinner=findViewById(R.id.employeeSpinner);
        employeeSpinner.setVisibility(View.VISIBLE);
        employeeTV.setVisibility(View.INVISIBLE);
        EmployeesList employeesList=EmployeesList.getInstance();
        if (employeesList == null) {
            EmployeesList.newInstance(this, InternalStorage.readFullInternalStorage(this,EMPLOYEES_LIST) );
            employeesList=EmployeesList.getInstance();
        }
            //set the spinners adapter to the previously created one.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.drop_down_list_element,
                locale.equals("ar")?employeesList.getEmployeesArNamesList():employeesList.getEmployeesNamesList());

        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadData();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private class LoadingThread extends Thread {
        LoadingThread() {
        }

        @Override
        public void run() {
            vacationsPerEmployee=new VacationsPerEmployee();
            String resulat="";
            BackgroundWorker backgroundWorker=new BackgroundWorker(EmployeeVacationsHistory.this);

            sharedPreferences = getBaseContext().getSharedPreferences(USER_INFO, MODE_PRIVATE);

            if (sharedPreferences.contains(EMAIL) && sharedPreferences.contains(PASSWORD)) {
                email = sharedPreferences.getString(EMAIL, "");
                password = sharedPreferences.getString(PASSWORD, "");
                name = sharedPreferences.getString(NAME, "");
            }
            try {
                if(isManager){
                    resulat=backgroundWorker.execute("managerHistoryVacation", email, password,EmployeesList.getInstance().getEmployeesList().get(employeeSpinner.getSelectedItemPosition()).getId() ).get();
                }else{
                    resulat=backgroundWorker.execute("historyVacation", email, password ).get();
                }

                JSONObject jsonObject=new JSONObject(resulat);
                if (!(jsonObject.get("year") instanceof JSONObject)){
                    Message msg=Message.obtain();
                    msg.arg1=-1;
                    handler.sendMessage(msg);
                    return;
                }
                JSONObject yearsObject=jsonObject.getJSONObject("year");
                ArrayList<VacationsEntriesPerYear> vacationsEntriesPerYearsList =new ArrayList<>();
                Iterator<String> iter = yearsObject.keys();
                while (iter.hasNext()){
                    String year=iter.next();
                    JSONArray vacationEntriesArray=yearsObject.getJSONArray(year);
                    ArrayList<VacationsEntry> singleRowEntriesList=new ArrayList<>();
                    for (int j = 0; j < vacationEntriesArray.length(); j++) {

                        JSONObject vacationEntry=vacationEntriesArray.getJSONObject(j);
                        String dateFrom = vacationEntry.getString("DateFrom");
                        String dateTo = vacationEntry.getString("DateTo");
                        String period = vacationEntry.getString("Period");
                        String typeAr = vacationEntry.getString("Name-Ar");
                        String typeEn = vacationEntry.getString("Name-En");
                        String VacationId = vacationEntry.getString("VacationId");
                        singleRowEntriesList.add(
                                new VacationsEntry(
                                        String.valueOf(j+1),
                                        typeEn,
                                        typeAr,
                                        dateFrom,
                                        dateTo,
                                        period,
                                        VacationId,
                                        EmployeeVacationsHistory.this
                                )
                        );
                    }
                    vacationsEntriesPerYearsList.add(new VacationsEntriesPerYear(
                            year,
                            singleRowEntriesList
                    ));

                }
                vacationsPerEmployee=new VacationsPerEmployee(
                        name,
                        name,
                        vacationsEntriesPerYearsList
                );


                Message msg=Message.obtain();
                msg.arg1=1;
                handler.sendMessage(msg);

            } catch (JSONException | InterruptedException | ExecutionException e) {
                Message msg=Message.obtain();
                msg.arg1=0;
                handler.sendMessage(msg);
                e.printStackTrace();
            }


        }
    }
    private void loadData() {
        if (Utilitaire.isNetworkAvailable(this)){
            loadingDialogBox = new DialogBoxLoadingClass(this);
            loadingDialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loadingDialogBox.show();
            loadingDialogBox.setCancelable(false);
            loadingDialogBox.setCanceledOnTouchOutside(false);


            LoadingThread loadingThread=new LoadingThread();
            loadingThread.start();
        }else{

            new DialogBoxCustom2.DialogBoxCustom2Builder(this)
                        .setMode(DialogBoxCustom2.ERROR)
                        .setTitle(getString(R.string.error))
                        .setBody(getString(R.string.connection_problem_reload))
                        .setPositive(getString(R.string.retry))
                        .create().show();
        }
    }

    private void processYearsRV() {
        ArrayList<Integer> yearsList=new ArrayList<>();
        for (int i = 0; i < vacationsPerEmployee.getVacationsEntriesPerYearsList().size(); i++) {
            yearsList.add(Integer.valueOf(vacationsPerEmployee.getVacationsEntriesPerYearsList().get(i).getYear()));
        }
        recyclerViewAdapterYearsFilter = new RecyclerViewAdapterYearsFilter(yearsList);
        LinearLayoutManager linearLayoutManager2=new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(RecyclerView.VERTICAL);
        yearsRV.setLayoutManager(linearLayoutManager2);
        yearsRV.setAdapter(recyclerViewAdapterYearsFilter);

    }

    private void processVacationsRV(ArrayList<VacationsEntry> entriesList) {
        RecyclerViewAdapterVacationsHistory recyclerViewAdapterVacations = new RecyclerViewAdapterVacationsHistory(entriesList);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapterVacations);

    }

    private VacationsEntriesPerYear getSingleRowYear(String year) {
        for (int i = 0; i < vacationsPerEmployee.getVacationsEntriesPerYearsList().size(); i++) {
            if (vacationsPerEmployee.getVacationsEntriesPerYearsList().get(i).getYear().equals(year)){
                return vacationsPerEmployee.getVacationsEntriesPerYearsList().get(i);
            }
        }
        return null;
    }


    private void fade(int duration, float from, float to) {
        AlphaAnimation anim = new AlphaAnimation(from, to); //0 means invisible
        anim.setDuration(duration);
        anim.setRepeatCount(0);
//                    anim.setRepeatMode(Animation.REVERSE);
        blurringView.startAnimation(anim);
        yearsRV.startAnimation(anim);
        closeIV.startAnimation(anim);
        yearsEntryTV.startAnimation(anim);
        darkIV.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {

                if (from==0.0f && to==1.0f){//if we are handling a fadeIn
//                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

                    snapShotIV.setVisibility(View.VISIBLE);
                    darkIV.setVisibility(View.VISIBLE);
                    blurringView.setVisibility(View.VISIBLE);
                    yearsRV.setVisibility(View.VISIBLE);
                    closeIV.setVisibility(View.VISIBLE);
                    yearsEntryTV.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                if (from==1.0f && to==0.0f){//if we are handling a fadeOut
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//hide status bar because it causes problemwhen screen shoting

                    snapShotIV.setVisibility(View.INVISIBLE);
                    darkIV.setVisibility(View.INVISIBLE);
                    blurringView.setVisibility(View.INVISIBLE);
                    yearsRV.setVisibility(View.INVISIBLE);
                    closeIV.setVisibility(View.INVISIBLE);
                    yearsEntryTV.setVisibility(View.INVISIBLE);
                }
            }
        });



    }

    private void setData()  {

        employeeTV.setText(vacationsPerEmployee.getName());
        String maxYear=vacationsPerEmployee.getVacationsEntriesPerYearsList().get(vacationsPerEmployee.getVacationsEntriesPerYearsList().size()-1).getYear();
        yearTV.setText(maxYear);
        processVacationsRV(getSingleRowYear(maxYear).getEntriesList());
        processYearsRV();

        //initialize years button chooser, this is delayed because we want to make sure the adapter is initialized
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerViewAdapterYearsFilter.selectLastElement();
            }
        }, 1000);



        yearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstraintLayout constraintLayout=findViewById(R.id.container);
                snapShotIV.setImageBitmap( Utilitaire.getBitmapFromView((View) constraintLayout));

                blurringView.setBlurredView(snapShotIV);
                blurringView.invalidate();
                fade(300, 0.0f, 1.0f);//fadeIn
            }
        });
        blurringView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedYear=vacationsPerEmployee.getVacationsEntriesPerYearsList().get(recyclerViewAdapterYearsFilter.getSelectedItemPosition()).getYear();
                processVacationsRV(getSingleRowYear(selectedYear).getEntriesList());
                yearTV.setText(selectedYear);
                fade(300,1.0f, 0.0f);//fadeOut
            }
        });

    }

    private void globalFindViewById() {

        recyclerView=findViewById(R.id.vacations_rv);
        employeeTV=findViewById(R.id.employeeTV);

        yearsRV=findViewById(R.id.yearsRV);
        blurringView = findViewById(R.id.blurringView);
        snapShotIV = findViewById(R.id.snapshot);
        darkIV = findViewById(R.id.darkIV);
        yearTV=findViewById(R.id.year);
        closeIV=findViewById(R.id.closeIV);
        yearsEntryTV=findViewById(R.id.yearsEntryTV);

        //get the spinner from the xml.
//        dropdown1 = findViewById(R.id.drop_down_list1);

    }

    public void closeButtonClick(View view) {
        String selectedYear=vacationsPerEmployee.getVacationsEntriesPerYearsList().get(recyclerViewAdapterYearsFilter.getSelectedItemPosition()).getYear();
        processVacationsRV(getSingleRowYear(selectedYear).getEntriesList());
        yearTV.setText(selectedYear);
        fade(300,1.0f,0.0f);

    }

    public void newVacationClick(View view) {
        Intent intent = new Intent(getApplicationContext(), EmployeeVacationNew.class);
        Pair<View, String> p1 = Pair.create((View)findViewById(R.id.kdipa), "logo");
        Pair<View, String> p3 = Pair.create((View)findViewById(R.id.toggle), "toggle");

        // Pass data object in the bundle and populate details activity.
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(EmployeeVacationsHistory.this, p1,p3);
        startActivity(intent,options.toBundle());
    }
}

class RecyclerViewAdapterVacationsHistory extends RecyclerView.Adapter<RecyclerViewAdapterVacationsHistory.MyViewHolder> {


    private ArrayList<VacationsEntry> mDataset;
    private RecyclerView recycler;
    private static final String TAG = "RecyclerViewAdapterAbse";

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView numberTV;
        TextView typeTV;
        TextView dateTV;

        View v;
        MyViewHolder(View view) {
            super(view);
            this.numberTV = view.findViewById(R.id.number);
            this.dateTV = view.findViewById(R.id.date);
            this.typeTV = view.findViewById(R.id.type);
            this.v = view;
        }

        View getView() {
            return v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapterVacationsHistory(ArrayList<VacationsEntry> myDataset) {
        mDataset = myDataset;

    }
    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.e_vacations_element,
                parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recycler = recyclerView;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        holder.setIsRecyclable(false);
//        recycler.getRecycledViewPool().setMaxRecycledViews(holder.getItemViewType(), 0);

        holder.numberTV.setText(mDataset.get(position).getNumber());
        holder.dateTV.setText(mDataset.get(position).getDateFrom());
        holder.typeTV.setText(mDataset.get(position).getType());

    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        String stringId=mDataset.get(position).getNumber();
        return Integer.parseInt(stringId);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
}



